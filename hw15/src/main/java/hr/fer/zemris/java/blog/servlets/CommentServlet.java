package hr.fer.zemris.java.blog.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;

/**
 * Comments servlet that adds support for adding comments to blog posts.
 *
 * @author Matija Bartolac
 */
@WebServlet({"/servleti/comment"})
public class CommentServlet extends HttpServlet {
    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String message = req.getParameter("commentText");
        String userNick = req.getParameter("userId");
        long postId = Long.parseLong(req.getParameter("postId"));

        DAOProvider.getDAO().addComment(message, userNick, postId);
        resp.sendRedirect(req.getContextPath()
                          + String.format("/servleti/author/%s/%d", userNick, postId));
    }

}
