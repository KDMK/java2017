package hr.fer.zemris.java.blog.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;

/**
 * This servlet processes all post operation. Depending on full path of request
 * (and request type) desired operation is performed. Supported operations are
 * adding and editing new post and getting single post or post lists. All users
 * are allowed to send GET request, but only logged users can send POST requests
 * (add and edit).
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet({ "/servleti/author/*" })
public class PostServlet extends HttpServlet {

    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("current.user.id") == null) {
            req.setAttribute("MESSAGE", "UNAUTHORIZED");
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

        String pathInfo = req.getPathInfo();

        if (pathInfo.endsWith("/new")) {
            String title = req.getParameter("postTitleNew");
            String content = req.getParameter("postContentNew");
            String userNick = req.getParameter("nick");

            DAOProvider.getDAO().newBlogPosts(title, content, userNick);
            req.setAttribute("MESSAGE", "NEW_POST_SUCCESSFULL");
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

        if (pathInfo.endsWith("/edit")) {
            String title = req.getParameter("postTitleEdit");
            String content = req.getParameter("postContentEdit");
            String nickname = req.getParameter("nick");
            Long id = Long.parseLong(req.getParameter("postId"));

            DAOProvider.getDAO().editBlogPosts(id, title, content, nickname);

            resp.sendRedirect(req.getContextPath()
                    + String.format("/servleti/author/%s/%d", nickname, id));

            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        String[] nickname = pathInfo.substring(1, pathInfo.length())
                .split("[/]");

        if (nickname.length == 2) {
            Long id;
            try {
                id = Long.parseLong(nickname[1]);
            } catch (NumberFormatException e) {
                req.setAttribute("MESSAGE", "UNAUTHORIZED");
                req.getRequestDispatcher("/index.html").forward(req, resp);
                return;
            }

            BlogEntry post = DAOProvider.getDAO().getBlogEntry(id);

            if (post == null) {
                req.setAttribute("MESSAGE", "NO_POST");
                req.getRequestDispatcher("/index.html").forward(req, resp);
                return;
            }

            req.setAttribute("post", post);
            req.getRequestDispatcher("/WEB-INF/pages/post.jsp").forward(req,
                    resp);
            return;
        } else if (nickname.length == 1) {
            List<BlogEntry> requestedPosts = DAOProvider.getDAO()
                    .getUserPosts(nickname[0]);
            if (requestedPosts.size() == 0) {
                req.setAttribute("MESSAGE", "NO_POSTS");
            }
 
            req.setAttribute("postList", requestedPosts);

            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

    }

}
