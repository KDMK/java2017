package hr.fer.zemris.java.tecaj_13.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author matija
 *
 */
@Entity
@Table(name = "blog_entries")
@Cacheable(true)
public class BlogEntry {

    /**
     * 
     */
    private Long id;
    /**
     * 
     */
    private List<BlogComment> comments = new ArrayList<>();
    /**
     * 
     */
    private Date createdAt;
    /**
     * 
     */
    private Date lastModifiedAt;
    /**
     * 
     */
    private String title;
    /**
     * 
     */
    private String text;
    /**
     * 
     */
    private BlogUser blogUser;

    /**
     * @return user id
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * Sets user id
     * 
     * @param id
     *            value
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return list of all comments
     */
    @OneToMany(mappedBy = "blogEntry", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
    @OrderBy("postedOn")
    public List<BlogComment> getComments() {
        return comments;
    }

    /**
     * @param comments
     *            list
     */
    public void setComments(List<BlogComment> comments) {
        this.comments = comments;
    }

    /**
     * @return the CreatedAt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets createdAt
     * 
     * @param createdAt
     *            value
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the lastModifiedAt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    public Date getLastModifiedAt() {
        return lastModifiedAt;
    }

    /**
     * Sets lastModifiedAt
     * 
     * @param lastModifiedAt
     *            value
     */
    public void setLastModifiedAt(Date lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    /**
     * @return the title
     */
    @Column(length = 200, nullable = false)
    public String getTitle() {
        return title;
    }

    /**
     * Sets title
     * 
     * @param title
     *            value
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the text
     */
    @Column(length = 4096, nullable = false)
    public String getText() {
        return text;
    }

    /**
     * Sets text value.
     * 
     * @param text
     *            value
     */
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BlogEntry other = (BlogEntry) obj;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        return true;
    }

    /**
     * @return the author
     */
    @ManyToOne
    public BlogUser getBlogUser() {
        return blogUser;
    }

    /**
     * Sets the author
     * 
     * @param blogUser
     *            value
     */
    public void setBlogUser(BlogUser blogUser) {
        this.blogUser = blogUser;
    }
}