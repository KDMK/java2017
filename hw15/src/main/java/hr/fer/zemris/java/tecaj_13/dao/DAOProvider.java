package hr.fer.zemris.java.tecaj_13.dao;

import hr.fer.zemris.java.tecaj_13.dao.jpa.JPADAOImpl;

/**
 * Singleton that is used to get instance of DAO object.
 * 
 * @author Matija Bartolac
 *
 */
public class DAOProvider {

	/**
	 * Database access object instance
	 */
	private static DAO dao = new JPADAOImpl();
	
	/**
	 * @return DAO instance
	 */
	public static DAO getDAO() {
		return dao;
	}
	
}