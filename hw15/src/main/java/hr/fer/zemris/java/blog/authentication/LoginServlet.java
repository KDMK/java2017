package hr.fer.zemris.java.blog.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Servlet is used to process user login. If login process was succesfull user
 * is stored in current session. If login process failed user is notified by
 * showing appropriate message in toast.
 * 
 * @author Matij Bartolac
 *
 */
@WebServlet("/servleti/login")
public class LoginServlet extends HttpServlet {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("current.user.id") != null) {
            req.setAttribute("MESSAGE", "ALREADY_LOGGED");
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

        String nickname = req.getParameter("nicknameLogin");
        String password = req.getParameter("passwordLogin");

        BlogUser user = DAOProvider.getDAO().getBlogUser(nickname);

        if (user == null) {
            req.setAttribute("MESSAGE", "NICKNAME_NOT_FOUND");
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

        String passwordHash = Util.hashPassword(password);

        if (!passwordHash.equals(user.getPasswordHash())) {
            req.setAttribute("MESSAGE", "WRONG_PASSWORD");
            req.setAttribute("nickname", nickname);
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return;
        }

        // Postavi session parametre i preusmjeri na main!
        req.getSession().setAttribute("current.user.id", user.getId());
        req.getSession().setAttribute("current.user.fn", user.getFirstName());
        req.getSession().setAttribute("current.user.ln", user.getLastName());
        req.getSession().setAttribute("current.user.nick", user.getNick());

        resp.sendRedirect("/blog/index.html");
    }

}
