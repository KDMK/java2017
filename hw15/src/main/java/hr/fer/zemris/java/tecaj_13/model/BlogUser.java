package hr.fer.zemris.java.tecaj_13.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * BlogUser class models single database user entity.
 * 
 * @author Matija Bartolac
 *
 */
@Entity
@Table(name = "blog_users")
public class BlogUser {
    /**
     * User id 
     */
    private long id;
    /**
     * First name of user.
     */
    private String firstName;
    /**
     * Last name of user.
     */
    private String lastName;
    /**
     * Blog nickname of user.
     */
    private String nick;
    /**
     * User email.
     */
    private String email;
    /**
     * Hashed password
     */
    private String passwordHash;
    /**
     * 
     */
    private List<BlogEntry> posts;

    /**
     * Sets user id
     * 
     * @param id value
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /**
     * @return the id
     */
    @Id
    @GeneratedValue
    public long getId() {
        return id;
    }

    /**
     * @return the firstName
     */
    @Column(length = 20)
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    @Column(length = 30)
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the nick
     */
    @Column(length = 40, unique = true)
    public String getNick() {
        return nick;
    }

    /**
     * @param nick
     *            the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the email
     */
    @Column(length=80)
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the passwordHash
     */
    @Column(length=40)
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * @param passwordHash
     *            the passwordHash to set
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    
    /**
     * @return list of posts
     */
    @OneToMany(mappedBy = "blogUser", fetch = FetchType.LAZY, 
            cascade = CascadeType.PERSIST, orphanRemoval = true)
    public List<BlogEntry> getPosts() {
        return posts;
    }
    
    /**
     * Sets list of posts
     * 
     * @param posts list of posts
     */
    public void setPosts(List<BlogEntry> posts) {
        this.posts = posts;
    }
}
