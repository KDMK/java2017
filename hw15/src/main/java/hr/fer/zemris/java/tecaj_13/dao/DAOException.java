package hr.fer.zemris.java.tecaj_13.dao;

/**
 * Exception that can occur when working with DAO object.
 * 
 * @author Matija Bartolac
 *
 */
public class DAOException extends RuntimeException {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor with message and cause.
	 * 
	 * @param message of exception
	 * @param cause of exception 
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message of exception
	 */
	public DAOException(String message) {
		super(message);
	}
}