package hr.fer.zemris.java.tecaj_13.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BlogComment class models single database comment entity.
 * 
 * @author Matija Bartolac
 *
 */
@Entity
@Table(name = "blog_comments")
public class BlogComment {
    /**
     * Id of blog comment
     */
    private Long id;
    /**
     * Reference to parent blog entry
     */
    private BlogEntry blogEntry;
    /**
     * Author email
     */
    private String usersEMail;
    /**
     * Comment message
     */
    private String message;
    /**
     * Posting date.
     */
    private Date postedOn;

    /**
     * @return the Id
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     * 
     * @param id
     *            of post
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the BlogEntry
     */
    @ManyToOne
    @JoinColumn(nullable = false)
    public BlogEntry getBlogEntry() {
        return blogEntry;
    }

    /**
     * Sets blog entry
     * 
     * @param blogEntry
     *            value
     */
    public void setBlogEntry(BlogEntry blogEntry) {
        this.blogEntry = blogEntry;
    }

    /**
     * @return this usersEMail
     */
    @Column(length = 100, nullable = false)
    public String getUsersEMail() {
        return usersEMail;
    }

    /**
     * Sets usersEMail
     * 
     * @param usersEMail
     *            value
     */
    public void setUsersEMail(String usersEMail) {
        this.usersEMail = usersEMail;
    }

    /**
     * @return the Message
     */
    @Column(length = 4096, nullable = false)
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message
     * 
     * @param message
     *            value
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the PostedOn
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getPostedOn() {
        return postedOn;
    }

    /**
     * Sets creation date
     * 
     * @param postedOn
     *            value
     */
    public void setPostedOn(Date postedOn) {
        this.postedOn = postedOn;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BlogComment other = (BlogComment) obj;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        return true;
    }
}