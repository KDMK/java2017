package hr.fer.zemris.java.blog.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Main servlet serves as request dispatcher. Depending on parameters we passed
 * in request it will generate redirect to appropirate page.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet({"/index.jsp","/index.html", "/servleti/main"})
public class MainServlet extends HttpServlet {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String path = "/WEB-INF/pages/korisnik.jsp";
        
        List<BlogUser> users = DAOProvider.getDAO().getBlogUsers(); 
        List<BlogEntry> posts = DAOProvider.getDAO().getBlogPosts();
        
        req.setAttribute("userList", users);
        if (req.getAttribute("postList") == null) {
            req.setAttribute("postList", posts);
            path = "/WEB-INF/pages/index.jsp";
        }
        
        req.getRequestDispatcher(path).forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
