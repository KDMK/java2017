package hr.fer.zemris.java.blog.authentication;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.mchange.v2.log.log4j.Log4jMLog;

/**
 * Utility class that provides additional functionality that is used in login
 * and registration classes.
 * 
 * @author Matija Bartolac
 *
 */
public class Util {
    /**
     * Used for extracting lower half of byte.
     */
    private static final byte LOWER_MASK = 0x0f;

    /**
     * Method for creating hashed password using SHA-1 algorithm;
     * 
     * @param password
     *            plain password
     * @return hashed password in form of string.
     */
    public static String hashPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            Log4jMLog.info(e.getMessage());
            return "";
        }
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        md.reset();

        md.update(passwordBytes);

        return bytetohex(md.digest());
    }

    /**
     * Method converts byte array to hexadecimal representation string.
     * 
     * @param inputByteArray
     *            array of bytes that we are converting to hex string
     * @return String representation of given byte array
     * @throws IllegalArgumentException
     *             if user passes null
     */
    public static String bytetohex(byte[] inputByteArray) {
        if (inputByteArray == null) {
            throw new IllegalArgumentException("Input array can't be null");
        }
        StringBuilder output = new StringBuilder(inputByteArray.length * 2);

        for (byte b : inputByteArray) {
            byte upper = (byte) ((b >> 4) & LOWER_MASK);
            byte lower = (byte) (b & LOWER_MASK);

            output.append(Integer.toHexString(upper));
            output.append(Integer.toHexString(lower));
        }
        return output.toString();
    }

}
