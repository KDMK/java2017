package hr.fer.zemris.java.blog.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Logout servlet is responsible for invalidating current session in order to
 * log out user.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet("/servleti/logout")
public class LogoutServlet extends HttpServlet {
    /**
     * Default ID.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getSession().invalidate();

        resp.sendRedirect("/blog/servleti/main");
    }
}
