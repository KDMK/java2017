package hr.fer.zemris.java.blog.authentication;

import static hr.fer.zemris.java.blog.authentication.Util.hashPassword;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;

/**
 * Registration servlet supports user registration process. When provided with
 * needed parameters it tries to create new user. Appropriate message is
 * generated(in form of toast) in both cases: if registration was succesfull or
 * not.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet("/servleti/register")
public class RegistrationServlet extends HttpServlet {
    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("current.user.id") != null) {
            req.setAttribute("MESSAGE", "ALREADY_LOGGED");
            req.getRequestDispatcher("/servleti/main").forward(req, resp);
            return;
        }

        String nickname = req.getParameter("nickname");

        // Check for duplicate nicknames
        if (DAOProvider.getDAO().getBlogUser(nickname) != null) {
            req.setAttribute("MESSAGE", "NICKNAME_ERROR");
            req.getRequestDispatcher("/servleti/main").forward(req, resp);
            return;
        }

        String firstName = req.getParameter("first_name");
        String lastName = req.getParameter("last_name");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        String hashedPassword = hashPassword(password);

        if (hashedPassword.length() == 0) {
            req.setAttribute("MESSAGE", "PASSWORD_ERROR");
            req.getRequestDispatcher("/servleti/main").forward(req, resp);
            return;
        }

        try {
            DAOProvider.getDAO().addUser(firstName, lastName, nickname, email,
                    hashedPassword);
        } catch (DAOException e) {
            e.printStackTrace();
            req.setAttribute("MESSAGE", "DATABASE_ERROR");
            req.getRequestDispatcher("/servleti/main").forward(req, resp);
            return;
        }

        req.setAttribute("MESSAGE", "REGISTRATION_SUCCESS");
        req.getRequestDispatcher("/servleti/main").forward(req, resp);
        return;
    }
}
