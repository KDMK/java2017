package hr.fer.zemris.java.tecaj_13.dao.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Implementation of DAO interface that uses JPA technology.
 * 
 * @author Matija Bartolac
 *
 */
public class JPADAOImpl implements DAO {

    @Override
    public BlogEntry getBlogEntry(Long id) throws DAOException {
        BlogEntry blogEntry = JPAEMProvider.getEntityManager()
                .find(BlogEntry.class, id);
        return blogEntry;
    }

    @Override
    public void addUser(String firstName, String lastName, String nick,
            String email, String passwordHash) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        BlogUser blogUser = new BlogUser();
        blogUser.setFirstName(firstName);
        blogUser.setLastName(lastName);
        blogUser.setNick(nick);
        blogUser.setEmail(email);
        blogUser.setPasswordHash(passwordHash);

        em.persist(blogUser);

        em.getTransaction().commit();
    }

    @Override
    public BlogUser getBlogUser(String nickname) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        BlogUser blogUser = null;
        try {
            blogUser = (BlogUser) em
                    .createQuery(
                            "SELECT b FROM BlogUser as b WHERE b.nick=:nck")
                    .setParameter("nck", nickname).getSingleResult();
        } catch (NoResultException e) {
        }

        return blogUser;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BlogUser> getBlogUsers() {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        List<BlogUser> blogUser = null;
        try {
            blogUser = em.createQuery("SELECT b FROM BlogUser as b")
                    .getResultList();
        } catch (NoResultException e) {
        }

        return blogUser;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BlogEntry> getBlogPosts() {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        List<BlogEntry> blogEntry = null;
        try {
            blogEntry = em.createQuery("SELECT b FROM BlogEntry as b")
                    .getResultList();
        } catch (NoResultException e) {
        }

        Collections.reverse(blogEntry);

        return blogEntry;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BlogEntry> getUserPosts(String nickname) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        List<BlogEntry> blogEntry = null;
        try {
            blogEntry = em
                    .createQuery(
                            "SELECT b FROM BlogEntry as b WHERE b.blogUser.nick=:nck")
                    .setParameter("nck", nickname).getResultList();
        } catch (NoResultException e) {
        }

        Collections.reverse(blogEntry);

        return blogEntry;
    }

    @Override
    public void editBlogPosts(Long id, String title, String content,
            String nickname) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        BlogEntry newBlogEntry = getBlogEntry(id);
        if (newBlogEntry == null) return;

        System.out.println(newBlogEntry.getId());

        newBlogEntry.setLastModifiedAt(new Date());
        newBlogEntry.setText(content);
        newBlogEntry.setTitle(title);

        // em.merge(newBlogEntry);
    }

    @Override
    public void newBlogPosts(String title, String content, String nickname) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        BlogUser user = getBlogUser(nickname);

        BlogEntry newBlogEntry = new BlogEntry();
        newBlogEntry.setBlogUser(user);
        newBlogEntry.setComments(new ArrayList<BlogComment>());
        newBlogEntry.setCreatedAt(new Date());
        newBlogEntry.setLastModifiedAt(null);
        newBlogEntry.setText(content);
        newBlogEntry.setTitle(title);

        em.persist(newBlogEntry);
    }

    @Override
    public void addComment(String message, String userNick, long postId) {
        BlogEntry entry = getBlogEntry(postId);
        BlogUser user = getBlogUser(userNick);

        BlogComment comment = new BlogComment();
        comment.setBlogEntry(entry);
        comment.setMessage(message);
        comment.setPostedOn(new Date());
        comment.setUsersEMail(user.getEmail());

        EntityManager em = JPAEMProvider.getEntityManager();

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        em.persist(comment);
        // em.flush();
    }
}
