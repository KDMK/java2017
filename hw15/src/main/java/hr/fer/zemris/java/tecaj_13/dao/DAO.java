package hr.fer.zemris.java.tecaj_13.dao;

import java.util.List;

import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * DAO interface serves as middleware between our application and database.
 * It defines methods that eases use of database.
 * 
 * @author Matija Bartolac
 *
 */
public interface DAO {

    /**
     * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
     * vraća <code>null</code>.
     * 
     * @param id
     *            ključ zapisa
     * @return entry ili <code>null</code> ako entry ne postoji
     * @throws DAOException
     *             ako dođe do pogreške pri dohvatu podataka
     */
    public BlogEntry getBlogEntry(Long id) throws DAOException;

    /**
     * Adds user to database.
     * 
     * @param firstName
     *            value
     * @param lastName
     *            value
     * @param nick
     *            nickname value
     * @param email
     *            email value
     * @param passwordHash
     *            value
     */
    void addUser(String firstName, String lastName, String nick, String email,
            String passwordHash);

    /**
     * Gets blog user with given nickname
     * 
     * @param nickname
     *            of desired user.
     * @return BlogUser if present, null otherwise.
     */
    BlogUser getBlogUser(String nickname);

    /**
     * Gets list of all blog users.
     * 
     * @return list of blog users
     */
    List<BlogUser> getBlogUsers();

    /**
     * Gets list of all blog posts
     * 
     * @return list of blog posts
     */
    List<BlogEntry> getBlogPosts();

    /**
     * Updates blog post.
     * 
     * @param id
     *            of blog post
     * @param title
     *            edited title
     * @param content
     *            edited content
     * @param nickname
     *            user nickname
     */
    void editBlogPosts(Long id, String title, String content, String nickname);

    /**
     * Add new blog post.
     * 
     * @param title
     *            new title
     * @param content
     *            new content
     * @param nickname
     *            new nickname
     */
    void newBlogPosts(String title, String content, String nickname);

    /**
     * Gets list of all users posts.
     * 
     * @param nickname
     *            of user
     * @return List of user posts, empty list if no posts.
     */
    List<BlogEntry> getUserPosts(String nickname);

    /**
     * Adds comment to blog post.
     * 
     * @param message
     *            comment message
     * @param userNick
     *            comment author
     * @param postId
     *            post id
     */
    public void addComment(String message, String userNick, long postId);

}