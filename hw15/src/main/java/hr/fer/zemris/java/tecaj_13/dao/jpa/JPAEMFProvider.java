package hr.fer.zemris.java.tecaj_13.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * Java persistance API entity manager factory provider class.
 * 
 * @author matija
 *
 */
public class JPAEMFProvider {

    /**
     * Entity manager factory used to produce EntityManagers
     */
    public static EntityManagerFactory emf;

    /**
     * @return the EntityManagerFactory
     */
    public static EntityManagerFactory getEmf() {
        return emf;
    }

    /**
     * Sets entity manager factory.
     * 
     * @param emf
     *            the entity manager factory
     */
    public static void setEmf(EntityManagerFactory emf) {
        JPAEMFProvider.emf = emf;
    }
}