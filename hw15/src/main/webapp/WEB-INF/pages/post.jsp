<%@page import="org.hibernate.query.criteria.internal.expression.function.LengthFunction"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Date, hr.fer.zemris.java.tecaj_13.model.*" %>

<html>
  <head>
    <meta charset="utf-8">
    <title>My blog</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	  rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/blog/css/materialize.min.css"
	  media="screen,projection" />

    <link type="text/css" rel="stylesheet" href="/blog/css/style.css"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>

  <body>
    <!-- Modal start -->
    <!-- Modal Structure -->
    <div id="modalLogin" class="modal modal2">
      <!-- <div class="modal-content"> -->
        <div class="container white z-depth-2">
             <ul class="tabs yellow darken-4">
                   <li class="tab col s3"><a class="white-text active" href="#login">login</a></li>
                   <li class="tab col s3"><a class="white-text" href="#register">register</a></li>
             </ul>
          <div id="login" class="col s12">
               <form class="col s12" action="/blog/servleti/login" method="POST">
                      <div class="form-container">
                              <div class="row">
                                  <div class="input-field col s12">
                                      <input id="nicknameLogin" name="nicknameLogin" type="text" class="validate" required>
                                      <label for="nicknameLogin">Nickname</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="input-field col s12">
                                      <input id="passwordLogin" name="passwordLogin" type="password" class="validate" required>
                                      <label for="passwordLogin">Password</label>
                                </div>
                              </div>
                       <br>
                       <center>
                           <button class="btn waves-effect waves-light yellow darken-4" type="submit" name="login">Log in</button>
                       </center>
                    </div>
              </form>
          </div>
          <div id="register" class="col s12">
             <form class="col s12" method="POST" action="/blog/servleti/register">
                 <div class="form-container">
                     <div class="row">
                         <div class="input-field col s6">
                             <input id="first_name" name="first_name" type="text" class="validate" required>
                             <label for="first_name">First Name</label>
                         </div>
                         <div class="input-field col s6">
                             <input id="last_name" name="last_name" type="text" class="validate" required>
                             <label for="last_name">Last Name</label>
                         </div>
                     </div>
                   <div class="row">
                       <div class="input-field col s12">
                           <input id="email" name="email" type="email" class="validate" required>
                           <label for="email">Email</label>
                       </div>
                   </div>
                   <div class="row">
                       <div class="input-field col s12">
                           <input id="nickname" name="nickname" type="text" class="validate" required>
                           <label for="nickname">Nickname</label>
                       </div>
                   </div>
                   <div class="row">
                       <div class="input-field col s12">
                           <input id="password" name="password" type="password" class="validate" required>
                           <label for="password">Password</label>
                       </div>
                   </div>
                   <center>
                       <button class="btn waves-effect waves-light yellow darken-4" type="submit" name="register">Register</button>
                   </center>
              </div>
           </form>
        </div>
     </div>
    </div>
    <!-- Modal end -->

    <!-- Modal new post -->
    <div id="modalNew" class="modal">
      <div class="modal-content">
        <h4>New post</h4>
        <form method="POST" action="/blog/servleti/author/${sessionScope['current.user.nick']}/new">
          <div class="row">
            <div class="input-field col s12">
              <input id="postTitleNew" name="postTitleNew" type="text">
              <label for="postTitleNew">Title</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="postContentNew" name="postContentNew" class="materialize-textarea" rows="10"></textarea>
              <label for="postContentNew">Content</label>
            </div>
          </div>
          <div class="row">
            <div class="col s3 offset-s9">
              <button class="waves-effect waves-light btn yellow darken-4" type="submit" name="submit" style="min-width:100%;">Submit</button>
            </div>
          </div>
          <input type="hidden" name="nick" value="${sessionScope['current.user.nick']}">
          <input type="hidden" name="userId" value="${sessionScope['current.user.id']}">
        </form>
      </div>
    </div>
    <!-- Modal new post end -->

    <!-- Modal edit start -->
      <div id="modalEdit" class="modal">
        <div class="modal-content">
          <h4>Edit post</h4>
          <form method="POST" action="/blog/servleti/author/${sessionScope['current.user.nick']}/edit">
            <div class="row">
              <div class="input-field col s12">
                <input id="postTitleEdit" name="postTitleEdit" type="text" value="${post.title}">
                <label for="postTitleEdit">Title</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea id="postContentEdit" name="postContentEdit" class="materialize-textarea" rows="10">${post.text}</textarea>
                <label for="postContentEdit">Content</label>
              </div>
            </div>
            <input type="hidden" name="postId" value="${post.id}"/>
            <input type="hidden" name="nick" value="${post.blogUser.nick}"/>
            <div class="row">
              <div class="col s3 offset-s9">
                <button class="waves-effect waves-light btn yellow darken-4" type="submit" name="submit" style="min-width:100%;">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    <!-- Modal edit end -->

    <nav class="yellow darken-4">
    <div class="nav-wrapper">
      <a href="/blog/index.html" class="brand-logo center">Blog</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <c:choose>
        <c:when test="${sessionScope['current.user.id'] != null}">
          <ul id="nav-mobile" class="left hide-on-med-and-down" style="margin-left:5em;">
            <li><i class="material-icons">perm_identity</i></li>
            <li> &nbsp;&nbsp; ${sessionScope['current.user.fn']}&nbsp;${sessionScope['current.user.ln']} &nbsp;&nbsp;</li>
            <li> ${sessionScope['current.user.nick']} </li>
          </ul>
        </c:when>
      </c:choose>
      <ul id="nav-mobile" class="right hide-on-med-and-down" style="margin-right:5em;">
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] != null}">
            <li><a href="/blog/servleti/author/${sessionScope['current.user.nick']}">My posts</a></li>
          </c:when>
        </c:choose>
        <!-- If logged -->
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] == null}">
            <li><a href="#modalLogin">Login/Register</a></li>
          </c:when>
          <c:otherwise>
            <li><a href="/blog/servleti/logout">Logout</a></li>          
          </c:otherwise>
        </c:choose>
        <!-- If logged -->
      </ul>
      <!-- If logged -->
      <c:choose>
        <c:when test="${sessionScope['current.user.id'] != null}">
          <a class="btn-floating btn-large halfway-fab waves-effect waves-light brown darken-1" href="#modalNew">
            <i class="material-icons">add</i>
          </a>
        </c:when>
      </c:choose>
      <!-- If logged -->
      <ul class="side-nav" id="mobile-demo">
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] != null}">
            <li><div class="userView center">
              <div class="background" style="background-color:gray;">
              </div>
              <img class="circle" src="/blog/images/placeholder.png" style="margin:auto;">
              <span class="white-text name">${sessionScope['current.user.fn']}&nbsp;${sessionScope['current.user.ln']}</span>
              <span class="white-text email">${sessionScope['current.user.nick']}</span>
            </div></li>
          </c:when>
        </c:choose>
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] != null}">
            <li><a href="/blog/servleti/author/${sessionScope['current.user.nick']}">My posts</a></li>
          </c:when>
        </c:choose>
        <!-- If logged -->
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] == null}">
            <li><a href="#modalLogin">Login/Register</a></li>
          </c:when>
          <c:otherwise>
            <li><a href="/blog/servleti/logout">Logout</a></li>          
          </c:otherwise>
        </c:choose>
        <!-- If logged -->
      </ul>
    </div>
  </nav>

  <main class="grey lighten-5">
  <div class="container">
    <div class="row">
      <div class="col s12">
        <div class="blog-post-expanded">
          <h6><%= ((BlogEntry) request.getAttribute("post")).getCreatedAt().toString() %></h6>
          <h3>${post.title}</h3>
          <h5>Author -- ${post.blogUser.nick}</h5>
          <p>${post.text}</p>
          <c:choose>
            <c:when test="${sessionScope['current.user.id'] == post.blogUser.id}">  
              <span id="edit" style="float: right;"><a href="#modalEdit"><i class="small material-icons">mode_edit</i></a></span>
            </c:when>
          </c:choose>  
        </div>
        <br>
        <br>
        <c:choose>
          <c:when test="${not empty post.comments}">
              <h4>Comments:</h4>
          </c:when>
        </c:choose>
              
        <c:forEach var="comment" items="${post.comments}" varStatus="loop">
        <div class="row">
          <div class="blog-comment col s11 offset-s1">
            <span><i class="tiny material-icons">schedule</i> ${comment.postedOn}</span> <br>
            <span><i class="tiny material-icons">perm_identity</i> ${comment.usersEMail}</span>
            <p>${comment.message}</p>
          </div>
        </div>
        <c:if test="${!loop.last}"><div class="divider"></div></c:if>
        </c:forEach>
        
        <c:choose>
          <c:when test="${sessionScope['current.user.id'] != null}">
        <div class="row">
          <form class="col s12" method="POST" action="/blog/servleti/comment">
            <div class="row">
              <textarea id="commentText" name="commentText" class="materialize-textarea" rows="8" cols="80"></textarea>
              <label for="commentText">Add new comment:</label>
            </div>
            <div class="row">
              <div class="col s3 offset-s9">
                <button class="waves-effect waves-light btn yellow darken-4" type="submit" name="submit">Submit comment</button>
              </div>
            </div>
            <input type="hidden" name="userId" value="${sessionScope['current.user.nick']}">
            <input type="hidden" name="postId" value="${post.id}">
          </form>
        </div>
        </c:when>
        </c:choose>
      </div>
    </div>
  </div>
  </main>

  <footer class="page-footer brown darken-1">
    <div class="container">
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
              <li><a class="grey-text text-lighten-3" href="/blog/index.html">Home</a></li>
              <c:choose>
                <c:when test="${sessionScope['current.user.id'] != null}">
                <li><a class="grey-text text-lighten-3" href="servleti/author/${sessionScope['current.user.nick']}">My posts</a></li>
                <li><a class="grey-text text-lighten-3" href="/blog/servleti/logout">Log out</a></li>
               </c:when>
               <c:otherwise>
                <li><a class="grey-text text-lighten-3" href="#modalLogin">Login/Register</a></li>
               </c:otherwise>
             </c:choose>
            
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2017 Bartolac
        <a class="grey-text text-lighten-4 right" href="http://materializecss.com/">Where magic comes from...</a>
      </div>
    </div>
    </footer>

    <!--Import jQuery before materialize.js-->
		<script type="text/javascript" src="/blog/js/jquery.min.js"></script>
		<script type="text/javascript" src="/blog/js/materialize.min.js"></script>
		<script type="text/javascript" src="/blog/js/init.js"></script>
    <script type="text/javascript" src="/blog/js/modals.js"></script>
  </body>

</html>
