package hr.fer.zemris.math;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class Vector3Test {

    @Test
    public void testNorm() {
        Vector3 v = new Vector3(0, 3, 4); // TODO
        
        assertEquals(v.norm(), 5, 1e-10);
    }

    @Test
    public void testNormalized() {
        Vector3 v = new Vector3(2, 4, 5); // TODO
        
        assertEquals(v.normalized().norm(), 1, 1e-10);
    }
    
    @Test
    public void testAdd() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(1, 1, 1);
        
        Vector3 v = v1.add(v2);
        Vector3 exp = new Vector3(2,2,2);
        
        assertEquals(v,exp);
    }
    
    @Test
    public void testSub() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(1, 1, 1);
        
        Vector3 v = v1.sub(v2);
        Vector3 exp = new Vector3(0,0,0);
        
        assertEquals(v,exp);
    }
    
    @Test
    public void testDot() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(1, 2, 3);
        
        double v = v1.dot(v2);
        double exp = 6;
        
        assertEquals(v,exp, 1e-10);
    }
    
    @Test
    public void testCross() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(1, 2, 3);
        
        Vector3 v = v1.cross(v2);
        Vector3 exp = new Vector3(1, -2, 1);
    
        assertEquals(v,exp);
    }
    
    @Test
    public void testScale() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(3, 3, 3);
        
        v1 = v1.scale(3);
        
        assertEquals(v1,v2);
    }
    
    @Test
    public void testCos() {
        Vector3 v1 = new Vector3(1, 1, 1);
        Vector3 v2 = new Vector3(1, 2, 3);
        
        double ang = v1.cosAngle(v2);
        
        assertEquals(ang, 0.9258200997725514, 1e-10);
    }
}
