package hr.fer.zemris.math;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ComplexTest {

    @Test
    public void testAdd() {
        Complex c1 = new Complex(2, 2);
        Complex c2 = new Complex(3, 4);

        Complex c3 = c1.add(c2);
        Complex c4 = new Complex(5, 6);

        assertEquals(c3, c4);
    }

    @Test
    public void testSub() {
        Complex c1 = new Complex(2, 2);
        Complex c2 = new Complex(2, 2);

        Complex c3 = c1.sub(c2);
        Complex c4 = Complex.ZERO;

        assertEquals(c3, c4);
    }

    @Test
    public void testMul() {
        Complex c1 = new Complex(2, 2);
        Complex c2 = new Complex(2, 2);

        Complex c3 = c1.multiply(c2);
        Complex c4 = new Complex(0, 8);

        assertEquals(c3, c4);
    }

    @Test
    public void testDiv() {
        Complex c1 = new Complex(2, 2);
        Complex c2 = new Complex(2, 2);

        Complex c3 = c1.divide(c2);
        Complex c4 = new Complex(1, 0);

        assertEquals(c3, c4);
    }

    @Test
    public void testPow() {
        Complex c1 = new Complex(2, 2);

        Complex c2 = c1.negate();
        Complex c3 = new Complex(-2, -2);

        assertEquals(c2, c3);
    }

    @Test
    public void testRoot() {
        Complex root1 = new Complex(1.6181754193833349, -0.0687856308561136);
        Complex root2 = new Complex(-1.6181754193833349, 0.0687856308561136);

        Complex c1 = new Complex(2, 3);
        Complex c2 = Complex.parse("2.5-3i");

        List<Complex> roots = c1
                .add(new Complex(0.0015926534214665267, 1.9999993658636692))
                .divide(c2).power(3).root(2);

        assertEquals(root1, roots.get(0));
        assertEquals(root2, roots.get(1));
    }
}
