package hr.fer.zemris.java.raytracer.model;

/**
 * Class models sphere geometric representation.
 * 
 * @author Matija Bartolac
 *
 */
public class Sphere extends GraphicalObject {
    /**
     * Sphere center.
     */
    private Point3D center;
    /**
     * Sphere radius.
     */
    private double radius;

    /**
     * Diffusion red intensity factor.
     */
    private double kdr;
    /**
     * Diffusion green intensity factor.
     */
    private double kdg;
    /**
     * Diffusion blue intensity factor.
     */
    private double kdb;

    /**
     * Reflection intensity of red color.
     */
    private double krr;

    /**
     * Reflection green intensity factor.
     */
    private double krg;
    /**
     * Reflection blue intensity factor.
     */
    private double krb;
    /**
     * Reflection coefficient.
     */
    private double krn;

    /**
     * Default constructor
     * 
     * @param center
     *            sphere center.
     * @param radius
     *            sphere radius.
     * @param kdr
     *            red color diffusion factor.
     * @param kdg
     *            green color diffusion factor.
     * @param kdb
     *            blue color diffusion factor.
     * @param krr
     *            red color reflection factor.
     * @param krg
     *            green color reflection factor
     * @param krb
     *            blue color reflection factor
     * @param krn
     *            refletion coefficient.
     */
    public Sphere(Point3D center, double radius, double kdr, double kdg,
            double kdb, double krr, double krg, double krb, double krn) {
        this.center = center;
        this.radius = radius;
        this.kdr = kdr;
        this.kdg = kdg;
        this.kdb = kdb;
        this.krr = krr;
        this.krg = krg;
        this.krb = krb;
        this.krn = krn;
    }

    @Override
    public RayIntersection findClosestRayIntersection(Ray ray) {
        double x0 = ray.start.x;
        double y0 = ray.start.y;
        double z0 = ray.start.z;

        double dx = ray.direction.x;
        double dy = ray.direction.y;
        double dz = ray.direction.z;

        double cx = center.x;
        double cy = center.y;
        double cz = center.z;

        Point3D L = ray.start.sub(center);

        // double a = ray.direction.scalarProduct(ray.direction);
        double b = 2 * (ray.direction.scalarProduct(L));
        double c = (L.scalarProduct(L)) - (radius * radius);

        double discriminant = b * b - 4 * c;

        if (discriminant <= 0) {
            return null;
        }

        double t = (-b - Math.sqrt(discriminant)) / 2;
        double iscX = x0 + t * dx;
        double iscY = y0 + t * dy;
        double iscZ = z0 + t * dz;

        Point3D intersection = new Point3D(iscX, iscY, iscZ);

        double dist = Math.sqrt((iscX - x0) * (iscX - x0)
                + (iscY - y0) * (iscY - y0) + (iscZ - z0) * (iscZ - z0));
        Point3D normal = new Point3D((iscX - cx) / 2, (iscY - cy) / 2,
                (iscZ - cz) / 2).normalize();

        return new SphereIntersection(intersection, dist, true, this, normal);
    }

    /**
     * @return the center
     */
    public Point3D getCenter() {
        return center;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @return the kdr
     */
    public double getKdr() {
        return kdr;
    }

    /**
     * @return the kdg
     */
    public double getKdg() {
        return kdg;
    }

    /**
     * @return the kdb
     */
    public double getKdb() {
        return kdb;
    }

    /**
     * @return the krr
     */
    public double getKrr() {
        return krr;
    }

    /**
     * @return the krg
     */
    public double getKrg() {
        return krg;
    }

    /**
     * @return the krb
     */
    public double getKrb() {
        return krb;
    }

    /**
     * @return the krn
     */
    public double getKrn() {
        return krn;
    }

}
