package hr.fer.zemris.java.raytracer.model;

/**
 * Class models sphere ray intersection.
 * 
 * @author Matija Bartolac
 *
 */
public class SphereIntersection extends RayIntersection {
    /**
     * Reference to this sphere.
     */
    private Sphere sphere;
    /**
     * Normal on this plane.
     */
    private Point3D normal;

    /**
     * Default constructor.
     * 
     * @param point
     *            intersection point
     * @param distance
     *            distance from eye to intersection
     * @param outer
     *            true if point is inner point.
     * @param sphere
     *            reference on this sphere
     * @param normal
     *            normal on current plane.
     */
    protected SphereIntersection(Point3D point, double distance, boolean outer,
            Sphere sphere, Point3D normal) {
        super(point, distance, outer);
        this.sphere = sphere;
        this.normal = normal;
    }

    @Override
    public Point3D getNormal() {
        return this.normal;
    }

    @Override
    public double getKdr() {
        return this.sphere.getKdr();
    }

    @Override
    public double getKdg() {
        return this.sphere.getKdg();
    }

    @Override
    public double getKdb() {
        return this.sphere.getKdb();
    }

    @Override
    public double getKrr() {
        return this.sphere.getKrr();
    }

    @Override
    public double getKrg() {
        return this.sphere.getKrg();
    }

    @Override
    public double getKrb() {
        return this.sphere.getKrb();
    }

    @Override
    public double getKrn() {
        return this.sphere.getKrn();
    }

}
