package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Class models ray caster. Ray caster is used to calculate visible object in
 * current viewport and creating data set for displaying image on screen.
 * 
 * @author Matija Bartolac
 *
 */
public class RayCaster {

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0),
                new Point3D(0, 0, 0), new Point3D(0, 0, 10), 20, 20);
    }

    /**
     * Method creates new producer used to calculate all visible image elements.
     * 
     * @return new IRayTracerProducer
     */
    private static IRayTracerProducer getIRayTracerProducer() {
        return new IRayTracerProducer() {
            @Override
            public void produce(Point3D eye, Point3D view, Point3D viewUp,
                    double horizontal, double vertical, int width, int height,
                    long requestNo, IRayTracerResultObserver observer) {
                System.out.println("Započinjem izračune...");
                short[] red = new short[width * height];
                short[] green = new short[width * height];
                short[] blue = new short[width * height];

                Point3D OG = (view.sub(eye)).normalize();
                Point3D VUV = viewUp.normalize();

                Point3D yAxis = VUV
                        .sub(OG.scalarMultiply(OG.scalarProduct(viewUp)))
                        .normalize();
                Point3D xAxis = (OG.vectorProduct(yAxis)).normalize();

                Point3D screenCorner = view
                        .sub(xAxis.scalarMultiply(horizontal / 2))
                        .add(yAxis.scalarMultiply(vertical / 2));

                System.out.println(String.format("[%.3f, %.3f, %.3f]",
                        screenCorner.x, screenCorner.y, screenCorner.z));

                Scene scene = RayTracerViewer.createPredefinedScene();
                short[] rgb = new short[3];
                int offset = 0;
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        double coefX = (x * horizontal) / (double) (width - 1);
                        double coefY = (y * vertical) / (double) (height - 1);

                        Point3D screenPoint = screenCorner
                                .add(xAxis.scalarMultiply(coefX))
                                .sub(yAxis.scalarMultiply(coefY));

                        Ray ray = Ray.fromPoints(eye, screenPoint);

                        tracer(scene, ray, rgb);

                        red[offset] = rgb[0] > 255 ? 255 : rgb[0];
                        green[offset] = rgb[1] > 255 ? 255 : rgb[1];
                        blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

                        offset++;
                    }
                }

                System.out.println("Izračuni gotovi...");
                observer.acceptResult(red, green, blue, requestNo);
                System.out.println("Dojava gotova...");
            }

            /**
             * Finds intersection of passed ray and closest object in given
             * scene.
             * 
             * @param scene
             *            working scene
             * @param ray
             *            current ray
             * @return RayIntersection object if ray intersects with object and
             *         null if doesn't
             */
            private RayIntersection findClosestIntersection(Scene scene,
                    Ray ray) {
                RayIntersection closestIntersection = null;
                for (GraphicalObject obj : scene.getObjects()) {
                    RayIntersection curIntersection = obj
                            .findClosestRayIntersection(ray);
                    if (curIntersection == null) {
                        continue;
                    }

                    if (closestIntersection == null || curIntersection
                            .getDistance() < closestIntersection
                                    .getDistance()) {
                        closestIntersection = curIntersection;
                    }
                }

                return closestIntersection;
            }

            /**
             * Inspects if current ray intersects with some object and colors
             * image accordingly.
             * 
             * @param scene
             *            working scene
             * @param ray
             *            current ray
             * @param rgb
             *            current pixel data
             */
            private void tracer(Scene scene, Ray ray, short[] rgb) {
                RayIntersection closestIntersection = findClosestIntersection(
                        scene, ray);

                if (closestIntersection == null) {
                    rgb[0] = 0;
                    rgb[1] = 0;
                    rgb[2] = 0;
                    return;
                }

                colorPoint(scene, closestIntersection, rgb, ray.start);
            }

            /**
             * Colors point based on input data.
             * 
             * @param scene
             *            working scene
             * @param closestIntersection
             *            closest intersection ray
             * @param rgb
             *            current pixel data
             * @param eye
             *            vector
             */
            private void colorPoint(Scene scene,
                    RayIntersection closestIntersection, short[] rgb,
                    Point3D eye) {
                rgb[0] = 15;
                rgb[1] = 15;
                rgb[2] = 15;

                for (LightSource l : scene.getLights()) {
                    Ray lightRay = Ray.fromPoints(l.getPoint(),
                            closestIntersection.getPoint());

                    RayIntersection lightRayIntersection = this
                            .findClosestIntersection(scene, lightRay);

                    double closestDistance;
                    double passedIntersectionDistance;
                    if (lightRayIntersection != null) {
                        closestDistance = (l.getPoint()
                                .sub(closestIntersection.getPoint())).norm();
                        passedIntersectionDistance = (l.getPoint()
                                .sub(lightRayIntersection.getPoint())).norm();
                        if ((closestDistance > passedIntersectionDistance
                                + 0.01)) {
                            continue;
                        }
                    }

                    colorUsingPhong(l.getPoint(), closestIntersection, l, eye,
                            rgb);
                }
            }

            /**
             * Phongs algorithm for coloring point.
             * 
             * @param lightSource
             *            light source point
             * @param intersectionPoint
             *            intersection point
             * @param light
             *            light source
             * @param eye
             *            vector
             * @param rgb
             *            current pixel data
             */
            private void colorUsingPhong(Point3D lightSource,
                    RayIntersection intersectionPoint, LightSource light,
                    Point3D eye, short[] rgb) {
                Point3D normalVector = intersectionPoint.getNormal();
                Point3D lUnitVector = (lightSource
                        .sub(intersectionPoint.getPoint())).normalize();

                double fctr = normalVector.scalarProduct(lUnitVector)
                        / (normalVector.norm() * lUnitVector.norm());

                rgb[0] += (short) ((intersectionPoint.getKrr() * light.getR())
                        + (intersectionPoint.getKdr() * fctr * light.getR()));
                rgb[1] += (short) ((intersectionPoint.getKrg() * light.getG())
                        + (intersectionPoint.getKdg() * fctr * light.getG()));
                rgb[2] += (short) ((intersectionPoint.getKrb() * light.getB())
                        + (intersectionPoint.getKdb() * fctr * light.getB()));

                Point3D r = normalVector
                        .scalarMultiply(normalVector.scalarProduct(lUnitVector))
                        .scalarMultiply(2).sub(lUnitVector).normalize();
                Point3D v = (eye.sub(intersectionPoint.getPoint())).normalize();

                double reflectionFctr = Math.max(r.scalarProduct(v), 0);

                rgb[0] += (short) (Math.pow(reflectionFctr,
                        intersectionPoint.getKrn())
                        * (intersectionPoint.getKrr() * light.getR()));
                rgb[1] += (short) (Math.pow(reflectionFctr,
                        intersectionPoint.getKrn())
                        * (intersectionPoint.getKrg() * light.getG()));
                rgb[2] += (short) (Math.pow(reflectionFctr,
                        intersectionPoint.getKrn())
                        * (intersectionPoint.getKrb() * light.getB()));
            }
        };
    }
}
