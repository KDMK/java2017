package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Class models workers which are used to split work in multiple threads in
 * order to speed up processing.
 * 
 * @author Matija Bartolac
 *
 */
public class NewtonFractalProducer implements IFractalProducer {
    /**
     * Polynomial function.
     */
    private ComplexRootedPolynomial polynomial;

    /**
     * Default constructor.
     * 
     * @param polynomial
     *            function
     */
    public NewtonFractalProducer(ComplexRootedPolynomial polynomial) {
        this.polynomial = polynomial;
    }

    @Override
    public void produce(double reMin, double reMax, double imMin, double imMax,
            int width, int height, long requestNo,
            IFractalResultObserver observer) {
        System.out.println("Starting calculation...");

        short[] data = new short[width * height];
        int m = polynomial.toComplexPolynom().order() + 1;
        final int numOfFields = 8;
        int fieldSize = height / numOfFields;

        ExecutorService pool = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors(),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        Thread nwThr = new Thread(r);
                        nwThr.setDaemon(true);
                        return nwThr;
                    }
                });
        List<Future<Void>> results = new ArrayList<>();

        for (int i = 0; i <= numOfFields; i++) {
            int yMax = i * fieldSize;
            int yMin = (i + 1) * fieldSize - 1;

            if (i == numOfFields - 1) {
                yMax = height - 1;
            }
            CalculationTask job = new CalculationTask(reMin, reMax, reMin,
                    imMax, width, height, yMin, yMax, data, polynomial);
            results.add(pool.submit(job));
        }

        for (Future<Void> job : results) {
            try {
                job.get();
            } catch (InterruptedException | ExecutionException e) {
            }
        }

        System.out.println("Calculation done. Notifiying observer...");
        observer.acceptResult(data, (short) (m), requestNo);
    }

    /**
     * Class models single worker that calculates Newton-Raphson iteration data
     * set.
     * 
     * @author Matija Bartolac
     *
     */
    public static class CalculationTask implements Callable<Void> {
        /**
         * Lower boundary of real set.
         */
        double reMin;
        /**
         * Upper boundary of real set.
         */
        double reMax;
        /**
         * Lower boundary of imaginary set.
         */
        double imMin;
        /**
         * Upper boundary of imaginary set.
         */
        double imMax;
        /**
         * Width of viewport.
         */
        int width;
        /**
         * Height of viewport.
         */
        int height;
        /**
         * Minimal y value.
         */
        int yMin;
        /**
         * Maximal y value.
         */
        int yMax;
        /**
         * Data set.
         */
        short[] data;
        /**
         * Polynomial function
         */
        ComplexRootedPolynomial polynomial;
        /**
         * Derivation of polynomial function.
         */
        ComplexPolynomial derived;

        /**
         * Default constructor.
         * 
         * @param reMin
         *            lower boundary of real set
         * @param reMax
         *            upper boundary of real set
         * @param imMin
         *            lower boundary of imaginary set
         * @param imMax
         *            upper boundary of imaginary set
         * @param width
         *            of viewport
         * @param height
         *            of viewport
         * @param yMin
         *            minimal y value
         * @param yMax
         *            maximal y value
         * @param data
         *            data set
         * @param polynomial
         *            f(x), polynomial
         */
        public CalculationTask(double reMin, double reMax, double imMin,
                double imMax, int width, int height, int yMin, int yMax,
                short[] data, ComplexRootedPolynomial polynomial) {
            super();
            this.reMin = reMin;
            this.reMax = reMax;
            this.imMin = imMin;
            this.imMax = imMax;
            this.width = width;
            this.height = height;
            this.yMin = yMin;
            this.yMax = yMax;
            this.data = data;
            this.polynomial = polynomial;
            this.derived = polynomial.toComplexPolynom().derive();
        }

        @Override
        public Void call() {
            Newton.calculate(reMin, reMax, imMin, imMax, width, height,
                    derived.order() + 1, yMin, yMax, data, polynomial, derived);
            return null;
        }
    }

}
