package hr.fer.zemris.java.fractals;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Class models methods for calculating Newton-Raphson iteration.
 * 
 * @author Matija Bartolac
 *
 */
public class Newton {
    /**
     * Maximum number of iterations.
     */
    private static short MAX_ITER = 16 * 16;
    /**
     * Threshold for comparing roots to calculated values.
     */
    private static double ROOT_THRESHOLD = 2E-3;
    /**
     * Convergence threshold.
     */
    private static double CONVERGENCE_THRESHOLD = 1E-3;

    /**
     * Method models Newton-Raphson iteration:
     * <p>
     * <code> Xn+1 = Xn - f(Xn)/f'(Xn)</code>
     * <p>
     * Method creates data set for given function on desired interval.
     * 
     * @param reMin
     *            lower boundary of real set
     * @param reMax
     *            upper boundary of real set
     * @param imMin
     *            lower boundary of imaginary set
     * @param imMax
     *            upper boundary of imaginary set
     * @param width
     *            of viewport
     * @param height
     *            of viewport
     * @param yMin
     *            minimal y value
     * @param yMax
     *            maximal y value
     * @param m number of polynomial members
     * @param data
     *            data set
     * @param p
     *            f(x), polynomial
     * @param d
     *            f'(x), derivation of polynomial
     */
    public static void calculate(double reMin, double reMax, double imMin,
            double imMax, int width, int height, int yMin, int yMax, int m,
            short[] data, ComplexRootedPolynomial p, ComplexPolynomial d) {
        int offset = yMin * width;
        for (int i = yMin; i < yMax; i++) {
            for (int j = 0; j < width; j++) {
                Complex c = mapToComplex(j, i, width, yMin, yMax, reMin, reMax,
                        imMin, imMax);
                Complex zn = c;

                int iter = 0;
                double module;

                do {
                    Complex numerator = p.apply(zn);
                    Complex denominator = d.apply(zn);
                    Complex fraction = numerator.divide(denominator);
                    Complex zn1 = zn.sub(fraction);
                    module = zn1.sub(zn).module();
                    zn = zn1;
                    iter++;
                } while (module > CONVERGENCE_THRESHOLD && iter < MAX_ITER);

                short index = (short) p.indexOfClosestRootFor(zn,
                        ROOT_THRESHOLD);
                if (index == -1) {
                    data[offset++] = 0;
                } else {
                    data[offset++] = index;
                }
            }
        }
    }

    /**
     * Method maps current pixel to input domain.
     * 
     * @param x
     *            current x
     * @param y
     *            current y
     * @param width
     *            of viewport
     * @param reMin
     *            lower boundary of real set
     * @param reMax
     *            upper boundary of real set
     * @param imMin
     *            lower boundary of imaginary set
     * @param imMax
     *            upper boundary of imaginary set
     * @param yMin
     *            minimal y value
     * @param yMax
     *            maximal y value
     * @return new Complex number from current viewport
     */
    private static Complex mapToComplex(double x, double y, double width,
            double yMin, double yMax, double reMin, double reMax, double imMin,
            double imMax) {
        double height = (yMax - yMin);

        double real = (x * (reMax - reMin)) / (width - 1.0) + reMin;
        double imaginary = ((height - 1.0 - y) * (imMax - imMin))
                / (height - 1.0) + imMin;

        return new Complex(real, imaginary);
    }
}
