package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Demonstrational program for Newton fractal calculator.
 * 
 * @author Matija Bartolac
 *
 */
public class FractalDemo {

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Complex> roots = new ArrayList<>();

        System.out.println(
                "Welcome to Newton-Raphson iteration-based fractal viewer.");
        System.out.println(
                "Please enter at least two roots, one root per line. Enter 'done' when done.");

        int i = 0;
        String input;
        while (true) {
            System.out.print(String.format("Root%d> ", i + 1));
            input = sc.nextLine().trim();

            if (input.equalsIgnoreCase("done")) {
                break;
            }

            Complex c = null;
            try {
                c = Complex.parse(input);
                i++;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
            roots.add(c);
        }

        sc.close();

        ComplexRootedPolynomial poly = new ComplexRootedPolynomial(
                roots.toArray(new Complex[roots.size()]));
        ComplexPolynomial derivation = poly.toComplexPolynom().derive();

        System.out.println(poly.toComplexPolynom().toString());
        System.out.println(derivation.toString());

        FractalViewer.show(new NewtonFractalProducer(poly));
    }
}
