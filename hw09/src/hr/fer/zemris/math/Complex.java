package hr.fer.zemris.math;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Model of complex number.
 * 
 * @author Matija Bartolac
 *
 */
public class Complex {
    /**
     * Real part of complex number.
     */
    private double real;
    /**
     * Imaginary part of complex number.
     */
    private double imaginary;
    /**
     * Module of complex number.
     */
    private double module;
    /**
     * Angle of complex number.
     */
    private double angle;

    /**
     * 0 + 0i
     */
    public static final Complex ZERO = new Complex(0, 0);
    /**
     * 1 + 0i
     */
    public static final Complex ONE = new Complex(1, 0);
    /**
     * -1 + 0i
     */
    public static final Complex ONE_NEG = new Complex(-1, 0);
    /**
     * 0 + i
     */
    public static final Complex IM = new Complex(0, 1);
    /**
     * 0 - i
     */
    public static final Complex IM_NEG = new Complex(0, -1);

    /**
     * Default constructor.
     */
    public Complex() {
        this(0, 0);
    }

    /**
     * Crates new complex number with desired real and imaginary components.
     * 
     * @param re
     *            real part of complex number.
     * @param im
     *            imaginary part of complex number.
     */
    public Complex(double re, double im) {
        this.real = re;
        this.imaginary = im;
        this.module = Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
        this.angle = Math.atan2(imaginary, real);
    }

    /**
     * Creates new complex number from module and angle.
     * 
     * @param module
     *            of complex number.
     * @param angle
     *            angle of complex number.
     * @return new complex number.
     */
    private static Complex fromAngleAndModule(double module, double angle) {
        double newReal = module * Math.cos(angle);
        double newImaginary = module * Math.sin(angle);

        return new Complex(newReal, newImaginary);
    }

    /**
     * Parse string and creates complex number from it. Expects from user to
     * input single number. Method does not resolve multi-number inputs. Throws
     * exception if called upon empty string or string that contains characters
     * that are not elements of complex numbers.
     *
     * @param s
     *            String containing complex number.
     * @return Reference to new complex number.
     */
    public static Complex parse(String s) {
        if (s.trim().isEmpty()) {
            throw new IllegalArgumentException("Cannot parse empty string!");
        }

        if (s.length() == 1
                && (!Character.isDigit(s.charAt(0)) && !s.equals("i"))) {
            throw new IllegalArgumentException(
                    "Cannot parse: Illegal characters.");
        }

        Pattern realPattern = Pattern
                .compile("([+-]{0,1}[\\d]*[.,]{0,1}[\\d]*)(?!i|\\d|\\.|,)\\b");
        Pattern imaginaryPattern = Pattern
                .compile("[-+]{0,1}[\\d]*[.,]{0,1}[\\d]*i");
        Pattern forbiddenPattern = Pattern.compile("[^i\\d\\.+\\-,]");

        Matcher realMatcher = realPattern.matcher(s);
        Matcher imaginaryMatcher = imaginaryPattern.matcher(s);
        Matcher forbiddenMatcher = forbiddenPattern.matcher(s);

        if (forbiddenMatcher.find()) {
            throw new IllegalArgumentException(
                    "Cannot parse: Illegal characters.");
        } else {
            double real = 0;
            double imaginary = 0;

            if (realMatcher.find()
                    && !(realMatcher.group(0).trim().length() == 0)) {
                real = Double.parseDouble(realMatcher.group(0));
            }
            if (imaginaryMatcher.find()
                    && !(imaginaryMatcher.group(0).trim().length() == 0)) {
                String imag = imaginaryMatcher.group(0).replace("i", "").trim();
                if (imag.length() == 0) {
                    imaginary = 1;
                } else if (imag.equals("-")) {
                    imaginary = -1;
                } else {
                    imaginary = Double.parseDouble(imag);
                }
            }

            return new Complex(real, imaginary);
        }
    }

    /**
     * @return module of complex number
     */
    public double module() {
        return this.module;
    }

    /**
     * @return angle of complex number
     */
    public double angle() {
        return this.angle;
    }

    // returns this*c
    /**
     * Multiplies this complex number with passed number.
     * 
     * @param c
     *            other number
     * @return new Complex number, result of multiplication.
     */
    public Complex multiply(Complex c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newModule = module * c.module();
        double newAngle = angle + c.angle();

        return fromAngleAndModule(newModule, newAngle);
    }

    /**
     * @return real part of complex number.
     */
    public double getReal() {
        return this.real;
    }

    /**
     * @return imaginary part of complex number.
     */
    public double getImaginary() {
        return this.imaginary;
    }

    /**
     * Divides this complex number with passed number.
     * 
     * @param c
     *            other number
     * @return new Complex number, result of division
     */
    public Complex divide(Complex c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        if (c.module() == 0) {
            return new Complex(Double.MAX_VALUE, Double.MAX_VALUE);
        }

        double newModule = module / c.module();
        double newAngle = angle - c.angle();

        return fromAngleAndModule(newModule, newAngle);
    }

    /**
     * Adds two complex numbers
     * 
     * @param c
     *            other number
     * @return new Complex number, result of multiplication.
     */
    public Complex add(Complex c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newRealValue = this.real + c.getReal();
        double newImaginaryValue = this.imaginary + c.getImaginary();

        return new Complex(newRealValue, newImaginaryValue);
    }

    /**
     * Subtracts two complex numbers
     * 
     * @param c
     *            other complex number.
     * @return new Complex number, result of subtraction.
     */
    public Complex sub(Complex c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newRealValue = this.real - c.getReal();
        double newImaginaryValue = this.imaginary - c.getImaginary();

        return new Complex(newRealValue, newImaginaryValue);
    }

    /**
     * @return -this
     */
    public Complex negate() {
        return new Complex(-this.real, -this.imaginary);
    }

    // returns this^n, n is non-negative integer
    /**
     * Raises this complex number to given power.
     * 
     * @param n
     *            power
     * @return new Complex number raised to power n
     */
    public Complex power(int n) {
        double newModule = Math.pow(module, n);
        double newAngle = angle * n;

        return fromAngleAndModule(newModule, newAngle);
    }

    // returns n-th root of this, n is positive integer
    /**
     * Calculates root of this complex number
     * 
     * @param n
     *            root
     * @return list of all roots of current complex number.
     */
    public List<Complex> root(int n) {
        if (n == 0) {
            throw new IllegalArgumentException("Cannot calculate 0 root!");
        }

        List<Complex> roots = new ArrayList<>();
        double newMagnitude = Math.pow(module, 1.0 / n);
        double nAngle = Math.atan2(imaginary, real);
        double newAngle;

        for (int k = 0; k < n; k++) {
            newAngle = (nAngle + (2 * k * Math.PI)) / n;
            roots.add(fromAngleAndModule(newMagnitude, newAngle));
        }

        return roots;
    }

    @Override
    public String toString() {
        DecimalFormat numFormat = new DecimalFormat("+0.00000;-0.00000");
        String output = numFormat.format(real) + numFormat.format(imaginary)
                + "i";

        return output;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(imaginary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(real);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Complex)) {
            return false;
        }
        Complex other = (Complex) obj;
        if (this.imaginary - other.imaginary > 1e-10) {
            return false;
        }
        if (this.real - other.real > 1e-10) {
            return false;
        }
        return true;
    }
}
