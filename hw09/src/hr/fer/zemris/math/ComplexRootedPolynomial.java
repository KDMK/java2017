package hr.fer.zemris.math;

import java.util.Arrays;

/**
 * Class models polynomial in form:
 * <p>
 * f(z) = (z - x1) * (z - x2) * ...
 * 
 * @author Matija Bartolac
 *
 */
public class ComplexRootedPolynomial {
    /**
     * Array of polynomial function roots.
     */
    private Complex[] roots;

    /**
     * Default constructor.
     * 
     * @param roots
     *            array of roots.
     */
    public ComplexRootedPolynomial(Complex... roots) {
        if (roots == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        if (roots.length == 0) {
            throw new IllegalArgumentException("There is no arguments!");
        }

        this.roots = Arrays.copyOf(roots, roots.length);
    }

    /**
     * Calculates f(z).
     * 
     * @param z
     *            function argument
     * @return function value.
     */
    public Complex apply(Complex z) {
        if (z == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        Complex result = z.sub(roots[0]);
        for (int i = 1, len = roots.length; i < len; i++) {
            result = result.multiply(z.sub(roots[i]));
        }

        return result;
    }

    /**
     * Converts polynomial to sum of products.
     * 
     * @return ComplexPolynomial
     */
    public ComplexPolynomial toComplexPolynom() {
        ComplexPolynomial newPolynomial = new ComplexPolynomial(
                roots[0].negate(), Complex.ONE);

        for (int i = 1, len = roots.length; i < len; i++) {
            ComplexPolynomial temp = new ComplexPolynomial(roots[i].negate(),
                    Complex.ONE);
            newPolynomial = newPolynomial.multiply(temp);
        }

        return newPolynomial;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("f(z) = ");

        for (Complex c : roots) {
            sb.append(String.format("(z %s)*", c.negate().toString()));
        }

        sb.replace(sb.length() - 1, sb.length(), "");

        return sb.toString();
    }

    /**
     * Finds index of closest root for given complex number z that is within
     * threshold. If there is no such root, returns -1
     * 
     * @param z
     *            input complex number.
     * @param treshold
     *            desired threshold.
     * @return -1 if there is no such root, index of root else.
     */
    public int indexOfClosestRootFor(Complex z, double treshold) {
        for (int i = 0, len = roots.length; i < len; i++) {
            double realDiff = Math.abs(z.getReal() - roots[i].getReal());
            double imaginaryDiff = Math
                    .abs(z.getImaginary() - roots[i].getImaginary());

            if (realDiff < treshold && imaginaryDiff < treshold) {
                return i;
            }
        }
        return -1;
    }
}
