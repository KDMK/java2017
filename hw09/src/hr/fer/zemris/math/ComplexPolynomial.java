package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Class models polynomial function and methods that allows us to create and
 * work with them.
 * 
 * @author Matija Bartolac
 *
 */
public class ComplexPolynomial {
    /**
     * Coefficients of polynomial
     */
    Complex[] factors;

    // constructor
    /**
     * Default constructor.
     * 
     * @param factors
     *            array of function coefficients, sorted ascending.
     */
    public ComplexPolynomial(Complex... factors) {
        if (factors == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        if (factors.length == 0) {
            throw new IllegalArgumentException("There is no arguments!");
        }

        this.factors = Arrays.copyOf(factors, factors.length);
    }

    /**
     * @return order of this polynom
     */
    public short order() {
        return (short) (factors.length - 1);
    }

    /**
     * Multiplies this polynomial with other.
     * 
     * @param p
     *            other polynomial
     * @return new ComplexPolynomial, result of multiplication.
     */
    public ComplexPolynomial multiply(ComplexPolynomial p) {
        int totalLength = this.order() + p.order() + 1;
        Complex[] newFactors = new Complex[totalLength];
        List<Complex> pFactors = p.getFactors();

        for (int i = 0; i < newFactors.length; i++) {
            newFactors[i] = Complex.ZERO;
        }

        for (int i = 0, len1 = this.order(); i <= len1; i++) {
            for (int j = 0, len2 = p.order(); j <= len2; j++) {
                newFactors[i + j] = newFactors[i + j]
                        .add(factors[i].multiply(pFactors.get(j)));
            }
        }

        return new ComplexPolynomial(newFactors);
    }

    // computes first derivative of this polynomial; for example, for
    // (7+2i)z^3+2z^2+5z+1 returns (21+6i)z^2+4z+5
    /**
     * Calculates first derivation of this polynomial.
     * 
     * @return derived Polynomial
     */
    public ComplexPolynomial derive() {
        Complex[] newFactors = new Complex[this.order()];

        newFactors[0] = factors[1];
        for (int i = 1, ord = this.order(); i < ord; i++) {
            newFactors[i] = factors[i + 1].multiply(new Complex(i + 1, 0));
        }

        return new ComplexPolynomial(newFactors);
    }

    /**
     * Computes polynomial value at given point z.
     * 
     * @param z
     *            point in complex plane.
     * @return result of calculation.
     */
    public Complex apply(Complex z) {
        if (z == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        Complex result = z.multiply(factors[0]);
        for (int i = 1, len = factors.length; i < len; i++) {
            result = result.add(z.power(i).multiply(factors[i]));
        }

        return result;
    }

    /**
     * Returns list of function coefficients.
     * 
     * @return list of all function coefficients.
     */
    public List<Complex> getFactors() {
        return Collections.unmodifiableList(Arrays.asList(factors));
    }

    @Override
    public String toString() {
        List<String> components = new ArrayList<>();

        if ((factors[0].module() - 0.0) > 1E-15) {
            components.add(0, String.format("(%s)*z", factors[1]));
        }
        if ((factors[1].module() - 0.0) > 1E-15) {
            components.add(0, String.format("(%s)*z", factors[1]));
        }

        for (int i = 2, len = factors.length; i < len; i++) {
            if (Math.abs(factors[i].module() - 0.0) > 1E-15) {
                String newFactor = String.format("(%s)*z^%d", factors[i], i);
                components.add(0, newFactor);
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("f(z) = ");

        Iterator<String> i = components.iterator();
        while (i.hasNext()) {
            sb.append(i.next());
            if (i.hasNext()) {
                sb.append(" + ");
            }
        }

        return sb.toString();
    }
}
