package hr.fer.zemris.math;

/**
 * Class models three dimensional vector. It provides basic operations with
 * vectors. Result of every operation is immutable vector.
 * 
 * @author Matija Bartolac
 *
 */
/**
 * @author Matija
 *
 */
public class Vector3 {
    /**
     * X value of this vector.
     */
    private double x;
    /**
     * Y value of this vector.
     */
    private double y;
    /**
     * Z value of this vector.
     */
    private double z;
    /**
     * Absolute value of this vector.
     */
    private double norm;

    /**
     * Default constructor.
     * 
     * @param x value
     * @param y value
     * @param z value
     */
    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.norm = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    } 

    /**
     * @return absolute value(length) of vector
     */
    public double norm() {
        return this.norm;
    } // norma vektora (“duljina”)

    /**
     * @return normalized vector(unit vector)
     */
    public Vector3 normalized() {
        double newX = x / norm;
        double newY = y / norm;
        double newZ = z / norm;

        return new Vector3(newX, newY, newZ);
    } 

    /**
     * Adds this vector to other.
     * 
     * @param other vector
     * @return result of addition
     */
    public Vector3 add(Vector3 other) {
        double addedX = this.x + other.getX();
        double addedY = this.y + other.getY();
        double addedZ = this.z + other.getZ();

        return new Vector3(addedX, addedY, addedZ);
    } 

    /**
     * Subtracts other vector from this.
     * 
     * @param other vector
     * @return result of subtraction
     */
    public Vector3 sub(Vector3 other) {
        double subX = this.x - other.getX();
        double subY = this.y - other.getY();
        double subZ = this.z - other.getZ();

        return new Vector3(subX, subY, subZ);
    } 

    /**
     * Calculates scalar product of two vectors.
     * 
     * @param other vector
     * @return scalar product
     */
    public double dot(Vector3 other) {
        double newX = this.x * other.getX();
        double newY = this.y * other.getY();
        double newZ = this.z * other.getZ();

        return newX + newY + newZ;
    } 

    /**
     * Calculates cross product of two vectors.
     * 
     * @param other vector
     * @return cross product
     */
    public Vector3 cross(Vector3 other) {
        double newX = this.y * other.getZ() - this.z * other.getY();
        double newY = this.z * other.getX() - this.x * other.getZ();
        double newZ = this.x * other.getY() - this.y * other.getX();

        return new Vector3(newX, newY, newZ);
    } 

    /**
     * Scale this vector for given factor.
     * 
     * @param s scale factor
     * @return new scaled vector
     */
    public Vector3 scale(double s) {
        double newX = s * this.x;
        double newY = s * this.y;
        double newZ = s * this.z;

        return new Vector3(newX, newY, newZ);
    } 

    /**
     * Calculates angle between two vectors
     * 
     * @param other vector
     * @return cosine of angle
     */
    public double cosAngle(Vector3 other) {
        return this.dot(other) / (this.norm * other.norm());
    } 

    /**
     * @return x value
     */
    public double getX() {
        return this.x;
    } 

    /**
     * @return y value
     */
    public double getY() {
        return this.y;
    } 

    /**
     * @return z value
     */
    public double getZ() {
        return this.z;
    } 

    /**
     * @return vector as array of its components
     */
    public double[] toArray() {
        double[] retVal = { x, y, z };
        return retVal;
    } 

    @Override
    public String toString() {
        return String.format("(%.6f, %.6f, %.6f)", x, y, z);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Vector3)) {
            return false;
        }
        Vector3 other = (Vector3) obj;
        if (x - other.x > 1E-10) {
            return false;
        }
        if (y - other.y > 1E-10) {
            return false;
        }
        if (z - other.z > 1E-10) {
            return false;
        }
        return true;
    } 
}
