package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Class contains methods for calculating area and perimeter of rectangle. If
 * user don't specify command line arguments program expects user to input
 * values for width and height. If user enters 2 command line arguments program
 * use them to calculate area and perimeter. First argument is interpreted as
 * width and second as height. If user enters one argument or more than two
 * program will end with error message.
 *
 * @author Matija Bartolac
 * @version 1.0
 */
public class Rectangle {
    private static final String USER_INPUT_PROMPT = "Unesite %s > ";
    private static final String ERROR_NEGATIVE_NUMBER = "Unijeli ste negativnu vrijednost!";
    private static final String ERROR_NOT_A_NUMBER = "'%s' se ne može protumačiti kao broj.%n";
    private static final String ERROR_INVALID_CONSOLE_ARGUMENTS = "Predani su neispravni argumenti. Program će završiti sa radom!";
    private static final String ERROR_INVALID_NUM_OF_ARGS = "Predan je neispravan broj argumenata. Program će završiti sa radom!";

    /**
     * Returns area of rectangle defined with input parameters.
     *
     * @param width  Width of rectangle
     * @param height Height of rectangle
     * @return Area of rectangle
     */
    public static double area(double width, double height) {
        return width * height;
    }

    /**
     * Returns perimeter of rectangle defined with input parameters.
     *
     * @param width  Width of rectangle
     * @param height Height of rectangle
     * @return Perimeter of rectangle
     */
    public static double perimeter(double width, double height) {
        return 2 * (width + height);
    }

    /**
     * Gets input value from keyboard. Method loops until user enters valid number. Number must be
     * greater than zero.
     *
     * @param sc           Scanner that handles user input
     * @param variableName name of the variable that we expect user to input
     * @return value of user input
     */
    private static double getInput(Scanner sc, String variableName) {
        double retValue;

        while (true) {
            System.out.printf(USER_INPUT_PROMPT, variableName);

            if (sc.hasNextDouble()) {
                retValue = sc.nextDouble();
                if (retValue <= 0) {
                    System.out.println(ERROR_NEGATIVE_NUMBER);
                } else {
                    break;
                }
            } else {
                System.out.printf(ERROR_NOT_A_NUMBER, sc.next());
            }
        }

        return retValue;
    }

    /**
     * Starting point of our program.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        double width = 0;
        double height = 0;

        if (args.length == 2) {
            try {
                width = Double.parseDouble(args[0]);
                height = Double.parseDouble(args[1]);
            } catch (NumberFormatException e) {
                System.out.println(ERROR_INVALID_CONSOLE_ARGUMENTS);
                System.exit(1);
            }
        } else if (args.length == 0) {
            Scanner sc = new Scanner(System.in);
            width = getInput(sc, "visinu");
            height = getInput(sc, "širinu");
            sc.close();
        } else {
            System.out.println(ERROR_INVALID_NUM_OF_ARGS);
            System.exit(1);
        }

        System.out.printf("Pravokutnik širine %.1f i visine %.1f ima površinu %.1f i opseg %.1f.%n",
                width, height, area(width, height), perimeter(width, height));
    }
}