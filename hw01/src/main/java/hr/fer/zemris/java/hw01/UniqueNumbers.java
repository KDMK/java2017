package hr.fer.zemris.java.hw01;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Program contains methods and structures for building binary tree. If element
 * is already in tree new element must not be added. Program expects user to
 * input elements via keyboard, and user input will be terminated when he types
 * 'kraj'. Once user stopped inputing numbers tree elements will print sorted
 * from smallest to largest number and also from largest to smallest.
 *
 * @author Matija Bartolac
 * @version 1.0
 */
public class UniqueNumbers {

    /**
     * Tree node structure required to build binary tree. It contains value of
     * node and references to left and right child.
     *
     * @author Matija Bartolac
     */
    public static class TreeNode {
        /**
         * Reference to left child node
         */
        public TreeNode left;
        /**
         * Reference to right child node
         */
        public TreeNode right;
        /**
         * Value that is stored in node
         */
        public int value;

        public TreeNode(TreeNode left, TreeNode right, int value) {
            this.left = left;
            this.right = right;
            this.value = value;
        }

        public TreeNode(int value) {
            this.value = value;
        }
    }

    /**
     * Adds new TreeNode with newValue. If value is already in tree it doesn't
     * change anything.
     *
     * @param root     Root node of tree in which we want to add new node
     * @param newValue Value of our new node
     * @return Reference to current node; it's equal to root node or newNode
     * that we are adding to tree
     */
    public static TreeNode addNode(TreeNode root, int newValue) {
        TreeNode newNode = new TreeNode(newValue);

        // If root is empty add new node
        if (root == null) {
            System.out.println("Dodao sam novi node.");
            return newNode;
        }

        // If element is already in list do nothing
        if (root.value == newValue) {
            System.out.println("Broj već postoji. Preskačem...");
            return root;
        }

        // If newValue is greater than value of the current node add element to
        // right subtree
        if (root.value < newValue) {
            root.right = addNode(root.right, newValue);
            // If newValue is greater than value of the current node add element
            // to left subtree
        } else if (root.value > newValue) {
            root.left = addNode(root.left, newValue);
        }

        return root;
    }

    /**
     * Returns size of tree. Recursively traverse tree and count number of
     * visited nodes.
     *
     * @param root Root node of tree
     * @return Size of a tree or a sub tree
     */
    public static int treeSize(TreeNode root) {
        if (root == null) {
            return 0;
        }

        return 1 + treeSize(root.left) + treeSize(root.right);
    }

    /**
     * Checks if element is in tree. Traverses tree and returns true if element
     * is found.
     *
     * @param root  Root node of tree
     * @param value Value of element that we search
     * @return Returns true if element is in tree, false if element isn't
     * present in tree
     */
    public static boolean containsValue(TreeNode root, int value) {
        // If root is empty return false
        if (root == null) {
            return false;
        }

        // If newValue is greater than value of the current node search element
        // in right subtree
        if (root.value < value) {
            return containsValue(root.right, value);
            // If newValue is greater than value of the current node search
            // element in left subtree
        } else if (root.value > value) {
            return containsValue(root.left, value);
        }

        return true;
    }

    /**
     * Print out elements of given tree starting from smallest to largest
     * number.
     *
     * @param root Root node of tree that we want to print
     * @return Return string representation of binary tree
     */
    public static String inorderPrint(TreeNode root) {
        if (root == null) {
            return "";
        }

        return inorderPrint(root.left) + String.valueOf(root.value) +
                "  " + inorderPrint(root.right);
    }

    /**
     * Print out elements of given tree starting from largest to smallest
     * number.
     *
     * @param root Root node of tree that we want to print
     * @return Return string representation of binary tree
     */
    public static String reverseInorderPrint(TreeNode root) {
        if (root == null) {
            return "";
        }

        return reverseInorderPrint(root.right) + String.valueOf(root.value) +
                "  " + reverseInorderPrint(root.left);
    }

    /**
     * Reads value from input. Method checks if argument is valid number. When
     * we type 'kraj' method returns null to signal end of input.
     *
     * @param sc Scanner which we use to read numbers
     * @return Returns Integer with input value or null if user typed 'kraj'
     */
    private static Integer inputValue(Scanner sc) {
        int value;

        while (true) {
            System.out.printf("Unesite broj > ");
            try {
                value = sc.nextInt();
                break;
            } catch (InputMismatchException ex) {
                String nextValue = sc.next();
                if (nextValue.toLowerCase().equals("kraj")) {
                    return null;
                }
                System.out.printf("'%s' se ne može protumačiti kao broj.\n", nextValue);
            }
        }
        return value;
    }

    /**
     * Starting point of our program.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        TreeNode head = null;
        Scanner sc = new Scanner(System.in);

        // Fill tree
        while (true) {
            Integer newValue = inputValue(sc);
            if (newValue == null) {
                break;
            }
            head = addNode(head, newValue);
        }

        sc.close();

        System.out.printf("Ispis od najmanjeg: " + inorderPrint(head));
        System.out.println();

        System.out.printf("Ispis od najvećeg: " + reverseInorderPrint(head));
        System.out.println();
    }
}
