package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Simple factorial calculator. Program takes no command line arguments, and takes input values from standard input(keyboard).
 * Acceptable input values are integers from 1 to 20.
 *
 * @author Matija Bartolac
 * @version 1.0
 */
public class Factorial {

    /**
     * Method calculates factorial of given number recursively. Acceptable range of input values is in interval [1,20].
     *
     * @param inputValue Input value of factorial that are calculating
     * @return factorial  Factorial of given input value, -1 if input value is out of range
     */
    public static long factorial(int inputValue) {
        // Check if input value is in acceptable range
        if (inputValue < 1 || inputValue > 20) {
            return -1;
        }

        if (inputValue == 1) {
            return 1;
        }
        return inputValue * factorial(inputValue - 1);
    }

    /**
     * Starting point of our factorial program.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Unesite broj > ");

            int inputValue;
            if (sc.hasNextInt()) {
                inputValue = sc.nextInt();
            } else {
                String inputString = sc.next();
                if (inputString.equalsIgnoreCase("kraj")) {
                    break;
                }
                System.out.printf("'%s' nije cijeli broj!%n", inputString);
                continue;
            }

            long factorialResult = factorial(inputValue);

            if (factorialResult == -1) {
                System.out.printf("'%d' nije u dozvoljenom rasponu!%n", inputValue);
            } else {
                System.out.printf("%d! = %d%n", inputValue, factorialResult);
            }
        }

        sc.close();
        System.out.println("Doviđenja!");
    }
}
