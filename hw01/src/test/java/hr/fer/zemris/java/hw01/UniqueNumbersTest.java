package hr.fer.zemris.java.hw01;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Series of short test for testing UniqueNumbers class functionality
 * 
 * @author Matija Bartolac
 * @version 1.0
 *
 */
public class UniqueNumbersTest {

	/**
	 * Test if root node is correctly inserted
	 */
	@Test
	public void testRootNode() {
		UniqueNumbers.TreeNode head = null;
		int testValue = 42;

		head = UniqueNumbers.addNode(head, testValue);

		assertEquals(testValue, head.value);

	}

	/**
	 * Test if left child node is correctly inserted
	 */
	@Test
	public void testChildrenNodesLeft() {
		UniqueNumbers.TreeNode head = null;
		int rootValue = 42;
		int testValue = 21;

		head = UniqueNumbers.addNode(head, rootValue);
		head = UniqueNumbers.addNode(head, testValue);

		assertEquals(testValue, head.left.value);
	}

	/**
	 * Test treeSize method when we insert different elements
	 */
	@Test
	public void testTreeSizeWithoutSameElements() {
		UniqueNumbers.TreeNode head = null;

		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);

		int n = UniqueNumbers.treeSize(head);
		
		assertEquals(2, n);
	}

	/**
	 * Test treeSize method when we insert same elements
	 */
	@Test
	public void testTreeSizeWithSameElements() {
		UniqueNumbers.TreeNode head = null;

		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);
		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);

		int n = UniqueNumbers.treeSize(head);

		assertEquals(2, n);
	}
	
	/**
	 * Tests if containsValue method work when we look for number that is in tree
	 */
	@Test
	public void testContainsValueTrue(){
		UniqueNumbers.TreeNode head = null;

		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);
		head = UniqueNumbers.addNode(head, 26);
		head = UniqueNumbers.addNode(head, 7);
		
		boolean n = UniqueNumbers.containsValue(head, 23);
		
		assertEquals(n, true);
	}
	/**
	 * Tests if containsValue method work when we look for number that isn't in tree
	 */
	@Test
	public void testContainsValueFalse(){
		UniqueNumbers.TreeNode head = null;
		
		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);
		head = UniqueNumbers.addNode(head, 26);
		head = UniqueNumbers.addNode(head, 7);
		
		boolean n = UniqueNumbers.containsValue(head, 24);
		
		assertEquals(n, false);
	}

	/**
	 * Checks if elements are printed smallest to largest
	 */
	@Test
	public void testInorderPrint(){
		UniqueNumbers.TreeNode head = null;
		String expectedValue = "7  23  26  34  ";
		
		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);
		head = UniqueNumbers.addNode(head, 26);
		head = UniqueNumbers.addNode(head, 7);
		
		String testedValue = UniqueNumbers.inorderPrint(head);
		
		assertEquals(expectedValue, testedValue);
	}
	
	/**
	 * Checks if elements are printed largest to smallest
	 */
	@Test
	public void testReverseInorderPrint(){
		UniqueNumbers.TreeNode head = null;
		String expectedValue = "34  26  23  7  ";
		
		head = UniqueNumbers.addNode(head, 23);
		head = UniqueNumbers.addNode(head, 34);
		head = UniqueNumbers.addNode(head, 26);
		head = UniqueNumbers.addNode(head, 7);
		
		String testedValue = UniqueNumbers.reverseInorderPrint(head);
		
		assertEquals(expectedValue, testedValue);
	}
}
