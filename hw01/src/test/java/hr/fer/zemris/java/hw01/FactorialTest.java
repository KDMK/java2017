package hr.fer.zemris.java.hw01;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class containing methods for testing factorial function from Factorial class.
 * 
 * @author Matija Bartolac
 * @version 1.0
 *
 */
public class FactorialTest {

	/**
	 * Check if function works correctly if input number is less than 1.
	 * Expected return value of function is -1
	 */
	@Test
	public void lessThanOne() {
		long n = Factorial.factorial(-1);
		assertEquals(-1,n);
	}
	
	/**
	 * Check if function works correctly if input number is greater than twenty.
	 * Expected return value of function in -1
	 */
	@Test
	public void greaterThanTwenty(){
		long n = Factorial.factorial(25);
		assertEquals(-1,n);
	}
	
	/**
	 * Check if function works correctly if input number is in given range.
	 * Expected return value for 5 is 120.
	 */
	@Test
	public void numInRange5(){
		long n = Factorial.factorial(5);
		assertEquals(120,n);
	}
	
	/**
	 * Check if function works correctly if input number is in given range.
	 * Expected return value for 10 is 3 628 800
	 */
	@Test
	public void numInRange10(){
		long n = Factorial.factorial(10);
		assertEquals(3628800,n);
	}
}
