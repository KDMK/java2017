package hr.fer.zemris.java.gui.prim;

import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.gui.prim.PrimDemo.PrimListModel;

@SuppressWarnings("javadoc")
public class PrimeTest {

    @Test
    public void testConstructor() {
        PrimListModel primeList = new PrimListModel();

        assertEquals(1, primeList.getSize());
    }

    @Test
    public void testNext() {
        PrimListModel primeList = new PrimListModel();
        int expected1 = 2;

        primeList.next();
        int result1 = primeList.getElementAt(primeList.getSize() - 1);

        assertEquals(expected1, result1);

        primeList.next();
        int expected2 = 3;
        int result2 = primeList.getElementAt(primeList.getSize() - 1);

        assertEquals(expected2, result2);
    }

}
