package hr.fer.zemris.java.gui.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * Simple calculator with graphical user interface. It supports basic arithmetic
 * operations
 * 
 * @author Matija Bartolac
 *
 */
public class CalcTest extends JFrame {
    /**
     * Serialization check value.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Calculator buffer.
     */
    private double buffer;
    /**
     * Current operation.
     */
    private String operator;
    /**
     * Signals that binary operator is selected and calculator expects next user
     * input.
     */
    private boolean next;
    /**
     * Last operator was binary operation.
     */
    private boolean wasBinOperator;
    /**
     * Last operator was equals.
     */
    private boolean wasEquals;
    /**
     * Last operator was unary operator.
     */
    private boolean wasUnaryOperator;
    /**
     * Signals that input already has dot and don't accept more dots when we
     * press that button.
     */
    private boolean hasDot;

    /**
     * Auxiliary stack.
     */
    private Stack<Double> numStack;

    /**
     * Addition operation.
     */
    private static BinaryOperator<Double> add;
    /**
     * Subtraction operation.
     */
    private static BinaryOperator<Double> sub;
    /**
     * Multiplication operation.
     */
    private static BinaryOperator<Double> mul;
    /**
     * Division operation.
     */
    private static BinaryOperator<Double> div;
    /**
     * Power operation.
     */
    private static BinaryOperator<Double> pow;
    /**
     * Root operation.
     */
    private static BinaryOperator<Double> root;

    /**
     * Sine operation.
     */
    private static UnaryOperator<Double> sin;
    /**
     * Cosine operation.
     */
    private static UnaryOperator<Double> cos;
    /**
     * Tan operation.
     */
    private static UnaryOperator<Double> tan;
    /**
     * Co-tan operation.
     */
    private static UnaryOperator<Double> ctg;

    /**
     * Inverse sine operation.
     */
    private static UnaryOperator<Double> asin;
    /**
     * Inverse cosine operation.
     */
    private static UnaryOperator<Double> acos;
    /**
     * Inverse tan operation
     */
    private static UnaryOperator<Double> atan;
    /**
     * Inverse co-tan operation
     */
    private static UnaryOperator<Double> actg;

    /**
     * Base 10 logarithm operator.
     */
    private static UnaryOperator<Double> log;
    /**
     * Base e logarithm operator.
     */
    private static UnaryOperator<Double> ln;
    /**
     * 10^x operation.
     */
    private static UnaryOperator<Double> pow10;
    /**
     * e^x operation.
     */
    private static UnaryOperator<Double> powE;
    /**
     * Calculates negative value of current number.
     */
    private static UnaryOperator<Double> negate;
    /**
     * Calculates reciprocal value of current number.
     */
    private static UnaryOperator<Double> reciprocal;

    static {
        add = (a1, a2) -> a1 + a2;
        sub = (a1, a2) -> a2 - a1;
        mul = (a1, a2) -> a1 * a2;
        div = (a1, a2) -> a1 / a2;
        pow = (a1, a2) -> Math.pow(a1, a2);
        root = (a1, a2) -> Math.pow(a1, 1 / a2);

        sin = a -> Math.sin(a);
        cos = a -> Math.cos(a);
        tan = a -> Math.tan(a);
        ctg = a -> 1.0 / Math.tan(a);

        asin = a -> Math.asin(a);
        acos = a -> Math.acos(a);
        atan = a -> Math.atan(a);
        actg = a -> Math.atan(1.0 / a);

        log = a -> Math.log10(a);
        ln = a -> Math.log(a);
        pow10 = a -> Math.pow(10, a);
        powE = a -> Math.pow(Math.E, a);
        negate = a -> -a;
        reciprocal = a -> 1 / a;
    }

    /**
     * Default constructor.
     */
    public CalcTest() {
        this.numStack = new Stack<Double>();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("MyCalc");

        initGUI();
    }

    /**
     * Initializes graphical user interface.
     */
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new CalcLayout());
        cp.setLayout(new BorderLayout());

        JPanel p = new JPanel(new CalcLayout(3));

        // first row ----------------------------------------------------------
        JTextField result = new JTextField("");
        result.setEnabled(false);
        result.setHorizontalAlignment(JTextField.RIGHT);
        result.setDisabledTextColor(Color.BLACK);

        JButton equals = new JButton("=");
        JButton clear = new JButton("clr");

        // second row ---------------------------------------------------------
        JButton reciprocal = new JButton("1/x");
        JButton sin = new JButton("sin");
        JButton num7 = new JButton("7");
        JButton num8 = new JButton("8");
        JButton num9 = new JButton("9");
        JButton div = new JButton("/");
        JButton res = new JButton("res");

        // third row ----------------------------------------------------------
        JButton log = new JButton("log");
        JButton cos = new JButton("cos");
        JButton num4 = new JButton("4");
        JButton num5 = new JButton("5");
        JButton num6 = new JButton("6");
        JButton mul = new JButton("*");
        JButton push = new JButton("push");

        // fourth row ---------------------------------------------------------
        JButton ln = new JButton("ln");
        JButton tan = new JButton("tan");
        JButton num1 = new JButton("1");
        JButton num2 = new JButton("2");
        JButton num3 = new JButton("3");
        JButton minus = new JButton("-");
        JButton pop = new JButton("pop");

        // fourth row ---------------------------------------------------------
        JButton x_n = new JButton("x^n");
        JButton ctg = new JButton("ctg");
        JButton num0 = new JButton("0");
        JButton sign = new JButton("+/-");
        JButton dot = new JButton(".");
        JButton plus = new JButton("+");
        JCheckBox inv = new JCheckBox("Inv");

        // Add all buttons to layout ------------------------------------------
        p.add(result, new RCPosition(1, 1));
        p.add(equals, new RCPosition(1, 6));
        p.add(clear, new RCPosition(1, 7));

        p.add(reciprocal, new RCPosition(2, 1));
        p.add(sin, new RCPosition(2, 2));
        p.add(num7, new RCPosition(2, 3));
        p.add(num8, new RCPosition(2, 4));
        p.add(num9, new RCPosition(2, 5));
        p.add(div, new RCPosition(2, 6));
        p.add(res, new RCPosition(2, 7));

        p.add(log, new RCPosition(3, 1));
        p.add(cos, new RCPosition(3, 2));
        p.add(num4, new RCPosition(3, 3));
        p.add(num5, new RCPosition(3, 4));
        p.add(num6, new RCPosition(3, 5));
        p.add(mul, new RCPosition(3, 6));
        p.add(push, new RCPosition(3, 7));

        p.add(ln, new RCPosition(4, 1));
        p.add(tan, new RCPosition(4, 2));
        p.add(num1, new RCPosition(4, 3));
        p.add(num2, new RCPosition(4, 4));
        p.add(num3, new RCPosition(4, 5));
        p.add(minus, new RCPosition(4, 6));
        p.add(pop, new RCPosition(4, 7));

        p.add(x_n, new RCPosition(5, 1));
        p.add(ctg, new RCPosition(5, 2));
        p.add(num0, new RCPosition(5, 3));
        p.add(sign, new RCPosition(5, 4));
        p.add(dot, new RCPosition(5, 5));
        p.add(plus, new RCPosition(5, 6));
        p.add(inv, new RCPosition(5, 7));

        // Action listeners ---------------------------------------------------
        ActionListener numInput = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton b = (JButton) e.getSource();

                if (!result.getText().equals("") && next) {
                    result.setText("");
                    next = false;
                }
                if (wasEquals || wasUnaryOperator) {
                    buffer = 0;
                    operator = null;
                    wasEquals = false;
                    wasUnaryOperator = false;
                    next = false;
                    result.setText("");
                }

                wasBinOperator = false;

                result.setText(result.getText() + b.getActionCommand());
            }
        };

        ActionListener equalsOper = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (operator != null) {
                    try {
                        boolean inverted = inv.isSelected();
                        Double first = Double.parseDouble(result.getText());
                        Double second = buffer;
                        BinaryOperator<Double> operation = determineBinaryOperation(
                                operator, inverted);

                        buffer = operation.apply(first, second);
                        result.setText(String.valueOf(buffer));
                        wasEquals = true;
                    } catch (NumberFormatException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        };

        ActionListener performBinOper = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JButton src = (JButton) e.getSource();
                boolean inverted = inv.isSelected();

                if (wasBinOperator) {
                    operator = e.getActionCommand();
                    wasEquals = false;
                    return;
                }
                if (wasEquals) {
                    operator = e.getActionCommand();
                    wasEquals = false;
                    next = true;
                    return;
                }

                // Starting state and state after clr
                if (operator == null || wasUnaryOperator) {
                    buffer = Double.parseDouble(result.getText());
                    operator = src.getActionCommand();
                    next = true;
                    wasUnaryOperator = false;
                    return;
                }

                BinaryOperator<Double> oper = determineBinaryOperation(operator,
                        inverted);
                Double first = buffer;
                Double second = Double.parseDouble(result.getText());

                buffer = oper.apply(first, second);
                result.setText(String.valueOf(buffer));
                operator = src.getActionCommand();
                next = true;
                wasBinOperator = true;
            }
        };

        ActionListener performUnaryOperation = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton src = (JButton) e.getSource();
                boolean inverted = inv.isSelected();
                String unOper = src.getActionCommand();

                if (!result.getText().equals("")) {
                    UnaryOperator<Double> oper = determineUnaryOperation(unOper,
                            inverted);

                    buffer = oper.apply(Double.parseDouble(result.getText()));
                    result.setText(String.valueOf(buffer));
                    wasUnaryOperator = true;
                }
            }
        };

        ActionListener clearOperation = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hasDot = false;
                result.setText("");
            }
        };

        ActionListener resetOperation = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                buffer = 0;
                operator = null;
                wasBinOperator = false;
                wasEquals = false;
                hasDot = false;
                result.setText("");
            }
        };

        ActionListener dotOperation = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!hasDot) {
                    hasDot = true;
                    result.setText(result.getText() + ".");
                }
            }
        };

        ActionListener stackPush = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String resultBuffer = result.getText();

                if (resultBuffer.equals("")) {
                    JOptionPane.showMessageDialog(result.getParent(),
                            "Nothing to put on stack!", "",
                            JOptionPane.INFORMATION_MESSAGE);
                    return;
                }

                double curNum = Double.parseDouble(resultBuffer);
                if (operator == null || wasUnaryOperator) {
                    numStack.push(curNum);
                    return;
                }

                numStack.push(curNum);
                equalsOper.actionPerformed(e);
            }

        };

        ActionListener stackPop = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (numStack.isEmpty()) {
                    JOptionPane.showMessageDialog(result.getParent(),
                            "Cannot pop from empty stack!", "Error!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                result.setText(String.valueOf(numStack.pop()));
            }
        };
        // --------------------------------------------------------------------
        // Register listeners -------------------------------------------------
        num0.addActionListener(numInput);
        num1.addActionListener(numInput);
        num2.addActionListener(numInput);
        num3.addActionListener(numInput);
        num4.addActionListener(numInput);
        num5.addActionListener(numInput);
        num6.addActionListener(numInput);
        num7.addActionListener(numInput);
        num8.addActionListener(numInput);
        num9.addActionListener(numInput);

        plus.addActionListener(performBinOper);
        minus.addActionListener(performBinOper);
        mul.addActionListener(performBinOper);
        div.addActionListener(performBinOper);
        x_n.addActionListener(performBinOper);

        sign.addActionListener(performUnaryOperation);
        reciprocal.addActionListener(performUnaryOperation);
        sin.addActionListener(performUnaryOperation);
        log.addActionListener(performUnaryOperation);
        cos.addActionListener(performUnaryOperation);
        ln.addActionListener(performUnaryOperation);
        tan.addActionListener(performUnaryOperation);
        ctg.addActionListener(performUnaryOperation);

        clear.addActionListener(clearOperation);
        res.addActionListener(resetOperation);
        equals.addActionListener(equalsOper);
        dot.addActionListener(dotOperation);

        push.addActionListener(stackPush);
        pop.addActionListener(stackPop);

        // --------------------------------------------------------------------
        p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        cp.add(p);
        Dimension d = cp.getPreferredSize();
        setSize(d);
    }

    /**
     * Determines correct binary operator based on input string.
     * 
     * @param operation
     *            as string value
     * @param inverted
     *            is operation inverse operation.
     * @return appropriate operator.
     * @throws IllegalArgumentException
     *             if operation is unsupported.
     */
    private BinaryOperator<Double> determineBinaryOperation(String operation,
            boolean inverted) {
        switch (operation) {
        case "+":
            return add;
        case "-":
            return sub;
        case "/":
            return div;
        case "*":
            return mul;
        case "x^n":
            if (inverted) {
                return root;
            }
            return pow;
        }
        throw new IllegalArgumentException("Illegal operation: " + operation);
    }

    /**
     * Determines correct unary operator base don input string.
     * 
     * @param unOper
     *            unary operator as string
     * @param inverted
     *            is unary operator inverted.
     * 
     * @return appropriate unary operator
     */
    private UnaryOperator<Double> determineUnaryOperation(String unOper,
            boolean inverted) {

        switch (unOper) {
        case "sin":
            if (inverted) {
                return CalcTest.asin;
            }
            return CalcTest.sin;
        case "cos":
            if (inverted) {
                return CalcTest.acos;
            }
            return CalcTest.cos;
        case "tan":
            if (inverted) {
                return CalcTest.atan;
            }
            return CalcTest.tan;
        case "ctg":
            if (inverted) {
                return CalcTest.actg;
            }
            return CalcTest.ctg;
        case "log":
            if (inverted) {
                return CalcTest.pow10;
            }
            return CalcTest.log;
        case "ln":
            if (inverted) {
                return CalcTest.powE;
            }
            return CalcTest.ln;
        case "+/-":
            return CalcTest.negate;
        case "1/x":
            return CalcTest.reciprocal;
        }
        return null;
    }

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new CalcTest().setVisible(true);
        });
    }
}
