package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Container;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Test program for simple graphics program that draws graphs on screen using
 * swing library. Program reads graph input parameters from file.
 * 
 * @author Matija Bartolac
 *
 */
public class BarChartDemo extends JFrame {

    /**
     * Serial version UID; necessary for serializable objects.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @param ref
     *            reference to BarChart object that contains information how to
     *            draw chart.
     */
    public BarChartDemo(BarChart ref) {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(20, 20);
        setSize(800, 600);
        setTitle("BarChart");

        initGUI(ref);
    }

    /**
     * GUI initializer.
     * 
     * @param ref
     *            reference to BarChart object that contains information how to
     *            draw chart.
     */
    private void initGUI(BarChart ref) {
        Container cp = getContentPane();
        cp.setLayout(null);

        JComponent komponenta = new BarChartComponent(ref);
        komponenta.setLocation(0, 0);
        komponenta.setSize(this.getWidth(), this.getHeight());
        komponenta.setOpaque(true);
        komponenta.setForeground(Color.WHITE);
        komponenta.setBackground(Color.BLACK);
        komponenta.setBorder(BorderFactory.createEmptyBorder(3, 5, 64, 30));

        // cp.addComponentListener(new ComponentAdapter() {
        // @Override
        // public void componentResized(ComponentEvent e) {
        // komponenta.setSize(getWidth(), getHeight());
        // komponenta.paintComponents(getGraphics());;
        // }
        // });

        cp.add(komponenta);
    }

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Illegal number of arguments.");
            return;
        }

        String basePath = new File(args[0]).getAbsolutePath();
        String inputData = null;

        try {
            inputData = new String(Files.readAllBytes(Paths.get(basePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scanner sc = new Scanner(inputData);
        String xValue = "";
        String yValue = "";
        List<XYValue> values = new ArrayList<>();
        int minY;
        int maxY;
        int deltaY;

        BarChart model;
        try {
            xValue = sc.nextLine();
            yValue = sc.nextLine();
            for (String pair : sc.nextLine().split(" ")) {
                String[] value = pair.split(",");
                values.add(new XYValue(Integer.parseInt(value[0]),
                        Integer.parseInt(value[1])));
            }
            minY = Integer.parseInt(sc.nextLine());
            maxY = Integer.parseInt(sc.nextLine());
            deltaY = Integer.parseInt(sc.nextLine());
            
            model = new BarChart(values, xValue, yValue, minY, maxY, deltaY);
        } catch (Exception e) {
            System.out.println("Error reading input file!");
            return;
        } finally {
            sc.close();
        }

        SwingUtilities.invokeLater(() -> {
            new BarChartDemo(model).setVisible(true);
        });
    }
}
