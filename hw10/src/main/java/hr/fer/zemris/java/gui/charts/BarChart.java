package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Bar chart models. Contains relevant data for creating bar chart graphic
 * representations.
 * 
 * @author Matija Bartolac
 *
 */
public class BarChart {
    /**
     * Dataset.
     */
    private List<XYValue> points;
    /**
     * Y axis label.
     */
    private String yLabel;
    /**
     * X axis label.
     */
    private String xLabel;
    /**
     * Maximal y value.
     */
    private int maxY;
    /**
     * Minimum y value.
     */
    private int minY;
    /**
     * Division between two y elements
     */
    private int yDelta;

    /**
     * Default constructor.
     * 
     * @param points
     *            dataset.
     * @param yLabel
     *            y axis label.
     * @param xLabel
     *            x axis label.
     * @param maxY
     *            maximal y value.
     * @param minY
     *            minimal y value.
     * @param yDelta
     *            division between two y elements
     */
    public BarChart(List<XYValue> points, String yLabel, String xLabel,
            int minY, int maxY, int yDelta) {
        this.points = points;
        this.yLabel = yLabel;
        this.xLabel = xLabel;
        this.minY = determineMinY(minY, maxY, yDelta);
        this.maxY = maxY;
        this.yDelta = yDelta;
    }

    /**
     * Determines if minimal y is on valid interval when we take deltaY in
     * calculation.
     * 
     * @param minY
     *            minimal Y value.
     * @param maxY
     *            maximal Y value.
     * @param yDelta
     *            division between two y elements
     * @return minimal y value.
     */
    private int determineMinY(int minY, int maxY, int yDelta) {
        int diff = Math.abs(maxY - minY) % yDelta;

        return diff == 0 ? minY : minY + diff;
    }

    /**
     * @return the points
     */
    public List<XYValue> getPoints() {
        return points;
    }

    /**
     * @return the yLabel
     */
    public String getyLabel() {
        return yLabel;
    }

    /**
     * @return the xLabel
     */
    public String getxLabel() {
        return xLabel;
    }

    /**
     * @return the maxY
     */
    public int getMaxY() {
        return maxY;
    }

    /**
     * @return the minY
     */
    public int getMinY() {
        return minY;
    }

    /**
     * @return the deltaY
     */
    public int getDeltaY() {
        return yDelta;
    }

}
