package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;

/**
 * Custom layout used to design calculator from this homework. 
 * 
 * @author Matija Bartolac
 *
 */
public class CalcLayout implements LayoutManager2 {
    /**
     * Padding around elements.
     */
    private int padding;
    /**
     * Number of layout rows.
     */
    private static final int ROWS = 5;
    /**
     * Number of layout columns
     */
    private static final int COLS = 7;

    /**
     * All layout components.
     */
    private Component[][] layoutComponents;
    /**
     * Number of components in container.
     */
    private int numOfComponents;

    /**
     * Default constructors.
     */
    public CalcLayout() {
        this.layoutComponents = new Component[ROWS][COLS];
    }

    /**
     * Default constructor.
     * 
     * @param padding
     *            around elements.
     */
    public CalcLayout(int padding) {
        this();
        if (padding < 0) {
            throw new IllegalArgumentException("Padding cannot be negative.");
        }

        this.padding = padding;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        synchronized (comp.getTreeLock()) {
            // If there is no components return
            if (numOfComponents <= 0) return;

            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    if (layoutComponents[row][col] == null) continue;
                    if (comp.equals(layoutComponents[row][col])) {
                        layoutComponents[row][col] = null;
                        numOfComponents--;
                        return;
                    }
                }
            }
        }
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            Insets insets = parent.getInsets();

            int minWidth = 0;
            int minHeight = 0;

            // find "largest smallest" width and height
            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    Component comp = layoutComponents[row][col];

                    if (comp == null) {
                        continue;
                    }

                    Dimension d = comp.getPreferredSize();
                    if (minWidth < d.width) {
                        minWidth = d.width;
                    }
                    if (minHeight < d.height) {
                        minHeight = d.height;
                    }
                }
            }
            return new Dimension(
                    insets.left + insets.right + COLS * minWidth
                            + (COLS - 1) * padding,
                    insets.top + insets.bottom + ROWS * minHeight
                            + (ROWS - 1) * padding);
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            Insets insets = parent.getInsets();

            int minWidth = 0;
            int minHeight = 0;

            // find "largest smallest" width and height
            // It's different from preferred in line 76/49
            // We are skipping first component
            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    Component comp = layoutComponents[row][col];

                    if (comp == null) {
                        continue;
                    }
                    Dimension d = comp.getMinimumSize();
                    if (minWidth < d.width) {
                        minWidth = d.width;
                    }
                    if (minHeight < d.height) {
                        minHeight = d.height;
                    }
                }
            }
            return new Dimension(
                    insets.left + insets.right + COLS * minWidth
                            + (COLS - 1) * padding,
                    insets.top + insets.bottom + ROWS * minHeight
                            + (ROWS - 1) * padding);
        }
    }

    @Override
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()) {
            Insets insets = parent.getInsets();
            int upperLeftX = parent.getX() + insets.left;
            int upperLeftY = parent.getY() + insets.top;

            // Determine height and width of each component
            int totalGapsWidth = (COLS - 1) * padding;
            int availableWidth = parent.getWidth()
                    - (insets.left + insets.right);
            int componentWidth = (availableWidth - totalGapsWidth) / COLS;

            int totalGapsHeight = (ROWS - 1) * padding;
            int availableHeight = parent.getHeight()
                    - (insets.bottom + insets.top);
            int componentHeight = (availableHeight - totalGapsHeight) / ROWS;

            // First element width
            int width11 = componentWidth * 5 + 4 * padding;

            // Set first row manually
            if (!(layoutComponents[0][0] == null)) {
                layoutComponents[0][0].setBounds(upperLeftX, upperLeftY,
                        width11, componentHeight);
            }
            if (!(layoutComponents[0][5] == null)) {
                layoutComponents[0][5].setBounds(upperLeftX + width11 + padding,
                        upperLeftY, componentWidth, componentHeight);
            }
            if (!(layoutComponents[0][5] == null)) {
                layoutComponents[0][6].setBounds(
                        upperLeftX + width11 + componentWidth + 2 * padding,
                        upperLeftY, componentWidth, componentHeight);
            }

            for (int row = 1; row < ROWS; row++) {
                int curRowUpperY = upperLeftY
                        + row * (componentHeight + padding);
                for (int col = 0; col < COLS; col++) {
                    Component curComponent = layoutComponents[row][col];
                    if (curComponent == null) continue;
                    curComponent.setBounds(
                            upperLeftX + col * (padding + componentWidth),
                            curRowUpperY, componentWidth, componentHeight);
                }
            }
        }
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {
        synchronized (comp.getTreeLock()) {
            if (constraints == null) {
                throw new IllegalArgumentException(
                        "Constraints can't be null!");
            }

            int rPos = 0;
            int cPos = 0;

            if (constraints instanceof String) {
                RCPosition compPosition = RCPosition
                        .parse((String) constraints);
                rPos = compPosition.getRow();
                cPos = compPosition.getCol();

            } else if (constraints instanceof RCPosition) {
                RCPosition compPosition = (RCPosition) constraints;
                rPos = compPosition.getRow();
                cPos = compPosition.getCol();
            } else {
                throw new IllegalArgumentException(
                        "Illegal constraint format!");
            }

            validPosition(rPos, cPos);

            layoutComponents[rPos - 1][cPos - 1] = comp;
        }
    }

    /**
     * Determine if given input position is valid position for current layout.
     * 
     * @param rPos
     *            row position
     * @param hPos
     *            column position.
     */
    private void validPosition(int rPos, int hPos) {
        if (rPos > ROWS || rPos <= 0) {
            throw new IllegalArgumentException("Row index out of bounds!");
        }
        if (hPos > COLS || hPos <= 0) {
            throw new IllegalArgumentException("Column index out of bounds!");
        }

        if (rPos == 1 && (hPos == 2 || hPos == 3 || hPos == 4 || hPos == 3)) {
            throw new IllegalArgumentException(
                    String.format("Can't put element to %d, %d.", rPos, hPos));
        }
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0;
    }

    @Override
    public void invalidateLayout(Container target) {
    }

}
