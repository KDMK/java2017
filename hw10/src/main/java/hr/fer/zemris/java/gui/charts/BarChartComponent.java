package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

/**
 * Class models graphical components that displays graph data on screen. Graph
 * is type of bar graph. 
 * 
 * @author Matija Bartolac
 *
 */
/**
 * @author Matija
 *
 */
public class BarChartComponent extends JComponent {
    /**
     * Default serial version UID, used for serialization
     */
    private static final long serialVersionUID = 1L;
    /**
     * Reference to BarChart model.
     */
    private BarChart ref;
    /**
     * Horizontal grid.
     */
    private List<Line2D> horizontalGrid;
    /**
     * Vertical grid.
     */
    private List<Line2D> verticalGrid;
    /**
     * Background fill.
     */
    private Rectangle background;
    /**
     * Data columns.
     */
    private List<Rectangle> bars;
    /**
     * X axis label coordinates.
     */
    private Point xLabel;
    /**
     * Y axis label coordinates.
     */
    private Point yLabel;

    /**
     * Default constructor.
     * 
     * @param ref
     *            reference to Bar chart model
     */
    public BarChartComponent(BarChart ref) {
        this.ref = ref;
        this.horizontalGrid = new ArrayList<>();
        this.verticalGrid = new ArrayList<>();
        this.bars = new ArrayList<>();
    }

    /**
     * Function calculates all necessary parameters needed in order to draw
     * graph on screen.
     */
    private void createGraph() {
        Insets ins = getInsets();
        int width = getWidth() - (ins.left + ins.right);
        int height = getHeight() - (ins.top + ins.bottom);

        FontMetrics fm = getFontMetrics(getFont());
        width -= 3 * fm.getHeight();
        height -= 3 * fm.getHeight();

        int topLeftX = getX() + ins.left;

        int topLeftY = getY() + ins.top;
        int topRightY = topLeftY;

        int bottomLeftY = topLeftY + height;
        int bottomLeftX = topLeftX;

        int numOfY = (ref.getMaxY() - ref.getMinY()) / ref.getDeltaY();
        int numOfX = ref.getPoints().size();

        int singleYSize = (int) (height / (double) numOfY);
        int singleXSize = width / numOfX;

        int bottomRightX = bottomLeftX + numOfX * singleXSize;

        topLeftX += 3 * fm.getHeight();
        bottomLeftX = topLeftX;
        int topRightX = topLeftX + width;

        System.out.println(String.format("T0(%d, %d)", topLeftX, topLeftY));
        System.out.println(String.format("T1(%d, %d)", topRightX, topRightY));
        System.out
                .println(String.format("T2(%d, %d)", bottomLeftX, bottomLeftY));
        System.out.println("num of x: " + numOfX);
        System.out.println("num of y: " + numOfY);

        System.out.println("Single y size " + singleYSize);
        System.out.println("Single x size " + singleXSize);

        printYLabel(fm, topLeftX, topLeftY, bottomLeftY);
        printXLabel(fm, bottomLeftX, bottomRightX, bottomLeftY);

        createGrid(width, height, topLeftX, topLeftY, topRightX, topRightY,
                bottomLeftX, bottomLeftY, singleXSize, singleYSize, numOfY,
                numOfX);
        createBars(width, height, topLeftX, topLeftY, topRightX, topRightY,
                bottomLeftX, bottomLeftY, singleXSize, singleYSize, numOfY,
                numOfX);
    }

    /**
     * Calculates X axis label position on screen.
     * 
     * @param fm
     *            FontMetrics object for current class font
     * @param bottomLeftX
     *            x coordinate of bottom left corner
     * @param bottomRightX
     *            x coordinate of bottom right corner
     * @param bottomLeftY
     *            y coordinate of bottom corners
     */
    private void printXLabel(FontMetrics fm, int bottomLeftX, int bottomRightX,
            int bottomLeftY) {
        int y = bottomLeftY + 3 * fm.getHeight();
        int x = (bottomRightX + bottomLeftY) / 2
                - 5 * fm.stringWidth(ref.getxLabel());

        xLabel = new Point(x, y);
    }

    /**
     * Calculates Y axis label position on screen
     * 
     * @param fm
     *            FontMetrics object for current class font.
     * @param topLeftX
     *            x coordinate of top left corner.
     * @param topLeftY
     *            y coordinate of top left corner.
     * @param bottomLeftY
     *            y coordinate of bottom left corner.
     */
    private void printYLabel(FontMetrics fm, int topLeftX, int topLeftY,
            int bottomLeftY) {
        int x = (topLeftY + bottomLeftY) / 2
                - fm.stringWidth(ref.getyLabel()) / 2;
        int y = topLeftY + fm.getHeight();

        System.out.println(x + " " + y);

        yLabel = new Point(x, y);
    }

    /**
     * Calculate bar postions and sizes on screen.
     * 
     * @param width
     *            of chart
     * @param height
     *            of height
     * @param topLeftX
     *            top left corner x coordinate
     * @param topLeftY
     *            top left corner y coordinate
     * @param topRightX
     *            top right corner x coordinate
     * @param topRightY
     *            top right corner y coordinate
     * @param bottomLeftX
     *            bottom left corner x coordinate
     * @param bottomLeftY
     *            bottom left corner y coordinate
     * @param singleXSize
     *            x axis division
     * @param singleYSize
     *            y axis division
     * @param numOfY
     *            number of y subdivisions
     * @param numOfX
     *            number of x subdivisions
     */
    private void createBars(int width, int height, int topLeftX, int topLeftY,
            int topRightX, int topRightY, int bottomLeftX, int bottomLeftY,
            int singleXSize, int singleYSize, int numOfY, int numOfX) {
        List<XYValue> entries = ref.getPoints();

        for (int offset = 0, len = entries.size(); offset < len; offset++) {
            XYValue entry = entries.get(offset);

            int x = bottomLeftX + (offset * singleXSize);
            int y = (int) (bottomLeftY
                    - (entry.getY() / ref.getDeltaY()) * singleYSize);

            Rectangle newBar = new Rectangle(x, y, singleXSize,
                    (int) (0.5 * entry.getY() * singleYSize));
            bars.add(newBar);
        }
    }

    /**
     * Calculates and creates grid.
     * 
     * @param width
     *            of chart
     * @param height
     *            of height
     * @param topLeftX
     *            top left corner x coordinate
     * @param topLeftY
     *            top left corner y coordinate
     * @param topRightX
     *            top right corner x coordinate
     * @param topRightY
     *            top right corner y coordinate
     * @param bottomLeftX
     *            bottom left corner x coordinate
     * @param bottomLeftY
     *            bottom left corner y coordinate
     * @param singleXSize
     *            x axis division
     * @param singleYSize
     *            y axis division
     * @param numOfY
     *            number of y subdivisions
     * @param numOfX
     *            number of x subdivisions
     */
    private void createGrid(int width, int height, int topLeftX, int topLeftY,
            int topRightX, int topRightY, int bottomLeftX, int bottomLeftY,
            int singleXSize, int singleYSize, int numOfY, int numOfX) {
        List<Line2D> horizontalGrid = new ArrayList<>();
        List<Line2D> verticalGrid = new ArrayList<>();

        // Create horizontal lines
        for (int i = 0; i <= numOfY; i++) {
            Line2D newLine = new Line2D.Double(topLeftX,
                    topLeftY + i * singleYSize, topRightX,
                    topLeftY + i * singleYSize);

            horizontalGrid.add(newLine);
        }

        // Create vertical lines
        for (int i = 0; i <= numOfX; i++) {
            Line2D newLine = new Line2D.Double(topLeftX + (i * singleXSize - 1),
                    topLeftY, topLeftX + (i * singleXSize - 1), bottomLeftY);

            verticalGrid.add(newLine);
        }

        System.out.println(width);
        System.out.println(height);

        background = new Rectangle(topLeftX, topLeftY, width, height);
        this.horizontalGrid = horizontalGrid;
        this.verticalGrid = verticalGrid;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        System.out.println("Entered painting.");
        Graphics2D g2d = (Graphics2D) g;
        createGraph();

        g2d.setColor(Color.BLACK);
        AffineTransform at = new AffineTransform();
        at.rotate(-Math.PI / 2);
        g2d.setTransform(at);
        g2d.drawString(ref.getyLabel(),
                -(2 * getWidth() / 3 - (int) yLabel.getX()),
                (int) yLabel.getY());
        at.rotate(Math.PI / 2);
        g2d.setTransform(at);

        g2d.drawString(ref.getxLabel(), (int) xLabel.getX(),
                (int) xLabel.getY());

        System.out.println("Painted background.");
        g2d.setColor(Color.WHITE);
        g2d.fill(background);

        System.out.println("Painting lines.");
        int i = ref.getMaxY();
        FontMetrics fm = getFontMetrics(getFont());
        for (Line2D ln : horizontalGrid) {
            g2d.setColor(Color.GRAY);
            g2d.draw(ln);
            String label = String.valueOf(i);
            g2d.setPaint(Color.BLACK);
            g2d.drawString(label, (int) ln.getX1() - fm.stringWidth(label) - 3,
                    (int) ln.getY1() + fm.getHeight() / 2);
            i -= ref.getDeltaY();
        }

        i = 0;
        int barWidth = (int) bars.get(0).getWidth();
        for (Line2D ln : verticalGrid) {
            g2d.setColor(Color.GRAY);
            g2d.draw(ln);
            if (i < ref.getPoints().size()) {
                String label = String.valueOf(ref.getPoints().get(i++).getX());
                g2d.drawString(label,
                        (int) ln.getX1() + barWidth / 2
                                - fm.stringWidth(label) / 2,
                        (int) ln.getY2() + fm.getHeight() + 3);

            }
            g2d.setPaint(Color.BLACK);
        }

        System.out.println("Painting bars.");
        for (Rectangle bar : bars) {
            g.setColor(Color.RED);
            g2d.fill(bar);
            g2d.setColor(Color.WHITE);
            g2d.draw(bar);
        }
    }

}