package hr.fer.zemris.java.gui.layouts;

/**
 * Class models constraint for custom layout that we use for creating custom
 * calculator layout.
 * 
 * @author Matija Bartolac
 *
 */
public class RCPosition {
    /**
     * Row constraint.
     */
    private int row;
    /**
     * Column constraint.
     */
    private int col;

    /**
     * Default constructor.
     * 
     * @param row
     *            constraint.
     * @param col
     *            constraint.
     */
    public RCPosition(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * @return row constraint.
     */
    public int getRow() {
        return row;
    }

    /**
     * @return col constraint.
     */
    public int getCol() {
        return col;
    }

    /**
     * Function creates new constraint from input string.
     * 
     * @param constraints
     *            as string.
     * @return RCPosition constraint.
     * @throws IllegalArgumentException
     *             if input cannot be parsed.
     */
    public static RCPosition parse(String constraints) {
        String[] components = constraints.split(",");
        if (components.length != 2) {
            throw new IllegalArgumentException(
                    "Illegal number of constraints!");
        }

        int row = 0;
        int col = 0;

        try {
            row = Integer.parseInt(components[0]);
            col = Integer.parseInt(components[1]);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }

        return new RCPosition(row, col);
    }
}
