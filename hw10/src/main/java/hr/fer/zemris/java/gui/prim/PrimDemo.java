package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Program demonstrates usage of swing list elements and appropriate event
 * listeners.
 * 
 * @author Matija Bartolac
 *
 */
public class PrimDemo extends JFrame {
    /**
     * Serialization check value.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public PrimDemo() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(50, 50);
        setSize(500, 200);
        setTitle("Prime numbers.");

        initGUI();
    }

    /**
     * Initialize GUI elements.
     */
    @SuppressWarnings("unchecked")
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        JPanel lists = new JPanel();
        lists.setLayout(new GridLayout(1, 2));

        cp.add(lists, BorderLayout.CENTER);

        PrimListModel collection = new PrimListModel();

        JList<Integer> listViewer1 = new JList<Integer>(collection);
        JList<Integer> listViewer2 = new JList<Integer>(collection);

        lists.add(new JScrollPane(listViewer1));
        lists.add(new JScrollPane(listViewer2));

        JButton generatePrime = new JButton("Next");
        cp.add(generatePrime, BorderLayout.PAGE_END);

        generatePrime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                collection.next();
            }
        });
    }

    /**
     * Class models list for storing prime numbers and using with grapihical
     * user interface.
     * 
     * @author Matija Bartolac
     *
     */
    @SuppressWarnings("rawtypes")
    protected static class PrimListModel implements ListModel {
        /**
         * Collection of all already generated prime numbers.
         */
        private List<Integer> primeNumbers;
        /**
         * List of registered data listeners.
         */
        private List<ListDataListener> listeners;

        /**
         * Default constructor.
         */
        public PrimListModel() {
            this.primeNumbers = new ArrayList<>();
            this.listeners = new ArrayList<>();

            this.primeNumbers.add(1);
        }

        @Override
        public int getSize() {
            return primeNumbers.size();
        }

        @Override
        public Integer getElementAt(int index) {
            return primeNumbers.get(index);
        }

        @Override
        public void addListDataListener(ListDataListener l) {
            listeners.add(l);
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
            listeners.remove(l);
        }

        /**
         * Generates next prime number.
         */
        public void next() {
            int curPrime = primeNumbers.get(primeNumbers.size() - 1);

            while (true) {
                curPrime++;
                if (isPrime(curPrime)) {
                    break;
                }
            }

            primeNumbers.add(curPrime);
            ListDataEvent event = new ListDataEvent(this,
                    ListDataEvent.INTERVAL_ADDED, primeNumbers.size(),
                    primeNumbers.size());
            for (ListDataListener l : listeners) {
                l.intervalAdded(event);
            }
        }

        /**
         * Checks if passed number is prime number.
         * 
         * @param i
         *            number that is under test
         * @return <code>True</code> if number is prime, false otherwise.
         */
        private boolean isPrime(int i) {
            for (int div = 2; div < i; div++) {
                if ((i % div) == 0) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Starting point of program.
     * 
     * @param args command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new PrimDemo().setVisible(true);
        });
    }
}
