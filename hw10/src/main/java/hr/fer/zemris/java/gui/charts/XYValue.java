package hr.fer.zemris.java.gui.charts;

/**
 * Class models single chart entry. Chart entry is represented as a point
 * relative to coordinate system origin. Class can be used to model any two
 * dimensional graph.
 * 
 * @author Matija Bartolac
 *
 */
public class XYValue {
    /**
     * X coordinate of point.
     */
    private int x;
    /**
     * Y coordinate of point.
     */
    private int y;

    /**
     * Default constructor.
     * 
     * @param x
     *            coordinate
     * @param y
     *            coordinate
     */
    public XYValue(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return x coordinate of point
     */
    public int getX() {
        return x;
    }

    /**
     * @return y coordinate of point
     */
    public int getY() {
        return y;
    }
}
