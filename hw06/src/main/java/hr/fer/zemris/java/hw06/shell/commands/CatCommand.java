package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.parser.ShellParser;
import hr.fer.zemris.java.hw06.shell.parser.ShellParserException;

/**
 * Class represents command that prints file content on environments standard
 * output. Commands accepts two parameters. First one file name, and it is
 * obligatory. Second parameter is optional and if provided input file is
 * decoded using provided character set. User can get list of supported character
 * sets by calling command {@link CharsetsCommand}.
 * 
 * @author Matija Bartolac
 *
 */
public class CatCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    // Note:
    // Command semantics doesn't change when we create new instances of specific
    // ShellCommand. Therefore, we can define these arguments as final and static.
    // They will not change in any instance. Declaring list as final we assure
    // that this reference will not be abused somewhere else in code. That doesn't
    // ensure immutability of list when we pass it to user so we have to deal with
    // that separately. 
    // Using this static parameters doesn't change much in this implementation of
    // MyShell as we create single instance of all this commands. But if we were
    // to implement support for multiple shell terminals under same process, this
    // will lower memory demands of our application.
    static {
        commandName = "cat";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "This command opens given file and writes its content to console.");
        commandDescription.add("File is provided as first argument to command. "
                + "Second parameter is optional. It specifies charset that user wants to use.");
        commandDescription.add(
                "If command got only one parameter it assumes that user wants to use default charset.");
        commandDescription.add("cat fileName [charset]");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        String[] args;
        String charset = Charset.defaultCharset().name();

        try {
            args = (new ShellParser(arguments)).getArgumentsList();
        } catch(IllegalArgumentException | ShellParserException e) {
            env.writeln(e.getMessage());
            return ShellStatus.CONTINUE;
        }
            
        if (args[0].length() <= 0 || args.length > 2) {
            env.writeln("Illegal number of parameters");
            return ShellStatus.CONTINUE;
        }
        if (args.length == 2) {
            charset = args[1];
            if (!Charset.isSupported(charset)) {
                env.writeln("Charset is not supproted!");
                return ShellStatus.CONTINUE;
            }
        }

        FileInputStream is;
        try {
            is = new FileInputStream(args[0]);
        } catch (FileNotFoundException e) {
            env.writeln("Error opening: " + e.getMessage());
            return ShellStatus.CONTINUE;
        }

        try (BufferedReader buffReader = new BufferedReader(
                new InputStreamReader(is, Charset.forName(charset)))) {
            String buffer;
            while ((buffer = buffReader.readLine()) != null) {
                System.out.println(buffer);
            }
        } catch (IOException e) {
            env.writeln("Error opening: " + e.getMessage());
            return ShellStatus.CONTINUE;
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
