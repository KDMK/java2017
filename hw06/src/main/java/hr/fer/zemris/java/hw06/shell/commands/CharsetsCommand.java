package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * Class represents command that prints all supported charsets on screen. It
 * takes no arguments. Each charset is printed in new line.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class CharsetsCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    // Command must be immutable in every environment?
    static {
        commandName = "charsets";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "List names of supported charsets for your Java platform.");
        commandDescription.add("Each charset is printed in new line.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        SortedMap<String, Charset> chars = Charset.availableCharsets();
        
        if(arguments.trim().length() != 0) {
            env.writeln("Illegal number of arguments!");
            return ShellStatus.CONTINUE;
        }

        System.out.println("Available charsets: ");
        for (String ent : chars.keySet()) {
            env.writeln(ent);
        }
        
        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
