package hr.fer.zemris.java.hw06.shell;

/**
 * ShellIOException models specific exception that can occur while we perform
 * shell operations.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ShellIOException extends RuntimeException {

    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Does not take any arguments.
     */
    public ShellIOException() {
        super();
    }

    /**
     * Constructor that accepts message.
     *
     * @param message
     *            Error message that will be displayed.
     */
    public ShellIOException(String message) {
        super(message);
    }

    /**
     * Constructor that creates exception using cause object.
     *
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public ShellIOException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor that creates exception using cause and string.
     *
     * @param message
     *            Error message that will be displayed.
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public ShellIOException(String message, Throwable cause) {
        super(message, cause);
    }    
}
