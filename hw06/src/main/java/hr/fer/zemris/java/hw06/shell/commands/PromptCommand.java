package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * Class models command for changing prompt sign.
 * 
 * @author Matija Bartolac
 *
 */
public class PromptCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "prompt";
        commandDescription = new ArrayList<>();

        commandDescription.add("Changes current prompt character.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        if (arguments.length() == 0) {
            env.writeln(String.format("Current PROMPT symbol is '%c'",
                    env.getPromptSymbol()));
            return ShellStatus.CONTINUE;
        }
        if (arguments.length() > 1) {
            env.writeln("Cannot set symbol to: " + arguments);
            return ShellStatus.CONTINUE;
        }
        env.setPromptSymbol(arguments.charAt(0));
        env.writeln(String.format("Succesfully set PROMPT symbol to '%c'",
                env.getPromptSymbol()));

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
