package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.parser.ShellParser;
import hr.fer.zemris.java.hw06.shell.parser.ShellParserException;

/**
 * Class models command that is used for creating directories. If directory
 * already exists, command writes appropriate message. Directories are created
 * recursively. That means that this is legal input
 * <p>
 * <code> mkdir ".\\folder1\\folder2"<\code>
 * 
 * @author Matija Bartolac
 *
 */
public class MkdirCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "mkdir";
        commandDescription = new ArrayList<>();

        commandDescription
                .add("Creates new folder. Operation is recursive. If folder already "
                        + "exists it will not be overwritten.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        String dirPath;
        try {
            ShellParser parser = new ShellParser(arguments);
            if (parser.getArgumentsList().length != 1) {
                env.writeln("Illegal number of arguments.");
                return ShellStatus.CONTINUE;
            }
            dirPath = parser.getArgumentsList()[0];

            if (dirPath.matches(".*/.*\\..*$")) {
                env.writeln("Folder name cannot contain . !");
                return ShellStatus.CONTINUE;
            }
        } catch (ShellParserException | IllegalArgumentException e) {
            env.writeln(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        Path dir = Paths.get(dirPath).toAbsolutePath();
        if (!Files.exists(dir)) {
            try {
                Files.createDirectories(dir);
                env.writeln("Succesfully created: " + dir.toString());
            } catch (IOException e) {
                env.writeln("Cannot create " + dir.toString());
            }
        }
        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
