package hr.fer.zemris.java.hw06.shell.parser;

/**
 * Token class models basic lexical analysis object - token. Token groups one or
 * more consecutive characters from input text. Token is immutable object.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class ShellToken {
    /**
     * Type of stored token as described in @see TokenType.
     */
    private final ShellTokenType type;
    /**
     * Value of token.
     */
    private final String value;

    /**
     * Default Token constructor. It creates new token based on token type and
     * value of passed object.
     *
     * @param type
     *            <code>TokenType</code>
     * @param value
     *            Value of token
     * @throws IllegalArgumentException
     *             If any of arguments is <code>null</code>
     */
    public ShellToken(ShellTokenType type, String value) {
        if (type == null) {
            throw new IllegalArgumentException("Token type can't be null!");
        }
        this.type = type;
        this.value = value;
    }

    /**
     * Returns type of token. Valid types for tokens are defined in
     * {@link ShellTokenType}.
     *
     * @return the type of token.
     */
    public ShellTokenType getType() {
        return type;
    }

    /**
     * Returns the value of token.
     * 
     * @return the value of token.
     */
    public String getValue() {
        return value;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ShellToken)) {
            return false;
        }
        ShellToken other = (ShellToken) obj;
        if (type != other.type) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

}
