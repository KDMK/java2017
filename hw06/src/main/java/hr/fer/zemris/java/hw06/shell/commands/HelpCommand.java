package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.ShellUtil;

/**
 * Class models help command which shows all supported operation of given
 * environment.
 * 
 * @author Matija Bartolac
 *
 */
public class HelpCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;
    /**
     * Longest command name. Used to create correct padding for output
     */
    private final static byte LONGEST_COMMAND_NAME = 13;

    static {
        commandName = "help";
        commandDescription = new ArrayList<>();

        commandDescription
                .add("Displays information about supported functions.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        for (ShellCommand sc : env.commands().values()) {
            boolean firstLine = true;
            System.out.print(String.format("%10s -", sc.getCommandName()));
            String blanks = " ";

            for (String s : sc.getCommandDescription()) {
                System.out.print(String.format(blanks + "%s%n", s));
                if (firstLine) {
                    firstLine = false;
                    blanks = ShellUtil.createPadding(LONGEST_COMMAND_NAME, ' ');
                }
            }
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
