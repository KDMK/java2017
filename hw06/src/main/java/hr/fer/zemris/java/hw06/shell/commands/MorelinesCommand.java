package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * Class models command for changing more-lines sign.
 * 
 * @author Matija Bartolac
 *
 */
public class MorelinesCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "morelines";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "Changes current more-lines character.");
    }
    
    
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        if (arguments.length() == 0) {
            env.writeln(String.format("Current MORELINES symbol is '%c'",
                    env.getMorelinesSymbol()));
            return ShellStatus.CONTINUE;
        }
        if (arguments.length() > 1) {
            env.writeln("Cannot set symbol to: " + arguments);
            return ShellStatus.CONTINUE;
        }
        env.setMorelinesSymbol(arguments.charAt(0));
        env.writeln(String.format("Succesfully set MORELINES symbol to '%c'",
                env.getMorelinesSymbol()));

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
