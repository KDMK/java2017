package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.ShellUtil;
import hr.fer.zemris.java.hw06.shell.parser.ShellParserException;

/**
 * Class models symbol command which is used to change more-lines, multi-line or
 * prompt symbol in specified shell implementation
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class SymbolCommand implements ShellCommand {
    /**
     * Commands for changing or printing desired symbol.
     */
    private Map<String, ShellCommand> commands;
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "symbol";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "Changes value of desired symbol if provided. Supported symbols"
                + " are PROMPT, MORELINES, MULTILINE");
        commandDescription
                .add("If only symbol name provided prints current symbol.");
    }

    /**
     * Default constructor.
     */
    public SymbolCommand() {
        this.commands = new HashMap<>();
        commands.put("PROMPT", new PromptCommand());
        commands.put("MULTILINE", new MultilineCommand());
        commands.put("MORELINES", new MorelinesCommand());
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> args;
        try {
            args = new LinkedList<String>(
                    Arrays.asList(ShellUtil.resolveArguments(arguments)));
            if (args.size() < 1 || args.size() > 2) {
                env.writeln("Illegal number of arguments.");
                return ShellStatus.CONTINUE;
            }
        } catch (IllegalArgumentException | ShellParserException e) {
            env.write(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        ShellCommand command = commands.get(args.get(0));
        if (command != null) {
            args.remove(0);
            command.executeCommand(env, String.join("", args));
        } else {
            env.writeln(String.format("Variable %s not found!", args.get(0)));
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
