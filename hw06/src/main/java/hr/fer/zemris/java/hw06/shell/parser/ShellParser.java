package hr.fer.zemris.java.hw06.shell.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Parser for described simple shell implementation. It builds argument list
 * based on set of simple parsing rules.
 *
 * @author Matija Bartolac
 *
 */
public class ShellParser {
    /**
     * Top level node containing all other nodes.
     */
    private List<String> arguments;

    /**
     * Constructor for parser. Constructor takes document text and create syntax
     * tree if document is correctly parsed.
     *
     * @param argumentString
     *            user passed arguments
     */
    public ShellParser(String argumentString) {
        arguments = new ArrayList<String>();

        try {
            parseDocument(argumentString);
        } catch (ShellLexerException | IllegalArgumentException ex) {
            throw new ShellParserException(ex.getMessage());
        }
    }

    /**
     * Function travels through input argument string and builds arguments list.
     *
     * @param argumentString
     *            user input arguments
     * @throws ShellParserException
     *             user
     */
    private void parseDocument(String argumentString) {
        ShellLexer lexer = new ShellLexer(argumentString);
        ShellToken currentToken = lexer.nextToken();

        while (currentToken.getType() != ShellTokenType.EOF) {
            if (currentToken.getType() == ShellTokenType.SWITCH) {
                lexer.setState(LexerState.EXTENDED);
                currentToken = lexer.nextToken();
                
                if(currentToken.getType() == ShellTokenType.WORD) {
                    arguments.add(currentToken.getValue());
                    currentToken = lexer.nextToken();
                    
                    if(currentToken.getType() != ShellTokenType.SWITCH) {
                        throw new ShellParserException("Invalid quoted token!");
                    }
                    
                    lexer.setState(LexerState.BASIC);
                    currentToken = lexer.nextToken();
                    continue;
                }
            }
            if (currentToken.getType() == ShellTokenType.WORD) {
                arguments.add(currentToken.getValue());
                currentToken = lexer.nextToken();
            }
        }
    }

    /**
     * Returns string array of all passed arguments.
     *
     * @return string array
     */
    public String[] getArgumentsList() {
        return arguments.toArray(new String[arguments.size()]);
    }
}
