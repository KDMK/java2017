package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.ShellUtil;

/**
 * Class models tree command that recursively traverses in current directory and
 * print all files that are in that directory.
 * 
 * @author Matija Bartolac
 *
 */
public class TreeCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "tree";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "Prints current folder content including all subfolders.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        File[] folderContent = new File(
                Paths.get("").toAbsolutePath().toString()).listFiles();
        
        env.write(recursiveVisit(folderContent, 0));

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

    /**
     * Traverses folder structure recursively and creates string of all files
     * that were found during traversal.
     * 
     * @param files
     *            root folder
     * @param depth
     *            depth of traversal
     * @return String in form of tree that represents current directory
     *         structure
     */
    private String recursiveVisit(File[] files, int depth) {
        StringBuilder structure = new StringBuilder();
        String depthBlanks = ShellUtil.createPadding(depth, '-');
        String nl = System.lineSeparator();
        
        for (File f : files) {
            if (f.isDirectory()) {
                structure.append(depthBlanks + f.getName() + nl);
                structure.append(recursiveVisit(f.listFiles(), depth + 2));
            } else {
                structure.append(depthBlanks + f.getName() + nl);
            }
        }
        
        return structure.toString();
    }

}
