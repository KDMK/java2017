package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.parser.ShellParser;

/**
 * Class models copy command.
 * 
 * @author Matija Bartolac
 *
 */
public class CopyCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;
    /**
     * Buffer size used for copying.
     */
    private static final int BUFFER_SIZE = 4096;

    static {
        commandName = "copy";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "Copies file1 to desired location. Command takes two parameters.");
        commandDescription.add("If second parameter is directory, file will be "
                + "copied to that directori under original name");
        commandDescription.add("If destination file already exists user is "
                + "prompted to overwrite it.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        Path fileInput = null;
        Path fileOutput = null;

        // Try to parse arguments
        try {
            ShellParser parser = new ShellParser(arguments);
            String[] argumentList = parser.getArgumentsList();

            if (argumentList.length != 2) {
                env.writeln("Invalid number of arguments.");
                return ShellStatus.CONTINUE;
            }
            fileInput = Paths.get(argumentList[0]).toAbsolutePath();
            fileOutput = Paths.get(argumentList[1]).toAbsolutePath();
        } catch (Exception e) {
            env.write(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        try {
            if (!Files.exists(fileInput)) {
                env.writeln("Input file does not exists!");
                return ShellStatus.CONTINUE;
            }

            StandardOpenOption newFileOpt;
            if (Files.isDirectory(fileOutput)) {
                fileOutput = Paths
                        .get(fileOutput.toString() + '/' + fileInput.getFileName());
                newFileOpt = StandardOpenOption.CREATE_NEW;
            } else if (Files.exists(fileOutput)) {
                String userDecision;
                while (true) {
                    env.writeln("Overwrite(Y/N) - ");
                    userDecision = env.readLine();
                    if (userDecision.equals("Y")) {
                        newFileOpt = StandardOpenOption.WRITE;
                        break;
                    } else if (userDecision.equals("N")) {
                        return ShellStatus.CONTINUE;
                    }
                }
            } else {
                newFileOpt = StandardOpenOption.CREATE_NEW;
            }

            BufferedInputStream bufferedIs = new BufferedInputStream(
                    Files.newInputStream(fileInput, StandardOpenOption.READ),
                    BUFFER_SIZE);
            BufferedOutputStream bufferedOs = new BufferedOutputStream(
                    Files.newOutputStream(fileOutput, newFileOpt), BUFFER_SIZE);

            byte[] inputBuffer = new byte[BUFFER_SIZE];
            int length;
            while ((length = bufferedIs.read(inputBuffer)) > 0) {
                bufferedOs.write(inputBuffer, 0, length);
            }
            bufferedOs.flush();
            bufferedOs.close();
            bufferedIs.close();
        } catch (Exception e) {
            env.writeln(e.getMessage());
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
