package hr.fer.zemris.java.hw06.shell.parser;

/**
 * Enumeration of token types present in language for which we are building
 * lexer.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public enum ShellTokenType {
    /**
     * Symbols end of file. There are no more tokens further.
     */
    EOF,
    /**
     * Every sequence of characters, digits or symbols.
     */
    WORD,
    /**
     * Switch token indicates that consumed character was quote-mark and we have
     * to switch to another set of rules for parsing quoted strings.
     */
    SWITCH;
}
