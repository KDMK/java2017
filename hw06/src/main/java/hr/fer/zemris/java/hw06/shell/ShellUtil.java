package hr.fer.zemris.java.hw06.shell;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.hw06.shell.parser.ShellParser;
import hr.fer.zemris.java.hw06.shell.parser.ShellParserException;

/**
 * Helper functions used in commands
 * 
 * @author Matija Bartolac
 *
 */
public class ShellUtil {

    /**
     * Mask for performing bitwise operataions.
     */
    private static final int LOWER_MASK = 0x0F;

    /**
     * Resolves file path to absolute path. If file doesn's exists method
     * returns null.
     * 
     * @param curPath
     *            passed file path
     * @return absolute path to given file if it exists, or null if it doesn't
     */
    public static Path resolve(String curPath) {
        Path p = Paths.get(curPath);

        if (!p.isAbsolute()) {
            p = p.toAbsolutePath();
        }
        if (Files.exists(p)) {
            return p;
        }
        return null;
    }

    /**
     * Resolves command name given as string to {@link ShellCommand}
     * 
     * @param commandName
     *            name of command that we want to resolve
     * @param env
     *            execution environment from which this function was called
     * @return null if command does not exist, correct command otherwise
     */
    public static ShellCommand resolveCommand(String commandName,
            Environment env) {
        if (commandName == null) {
            throw new IllegalArgumentException("Command name can't be null!");
        }

        return env.commands().get(commandName);
    }

    /**
     * Checks if user provided arguments and returns appropriate string.
     * 
     * @param input
     *            user input string
     * @return arguments string array, or empty array if no arguments were
     *         provided
     * @throws ShellParserException
     *             if parser was unable to parse input
     * @throws IllegalArgumentException
     *             if parser was provided with illegal argument
     */
    public static String[] resolveArguments(String input)
            throws ShellParserException, IllegalArgumentException {
        ShellParser parser = new ShellParser(input);

        return parser.getArgumentsList();
    }

    /**
     * Returns passed file flags. Possible flags are:
     * <ol>
     * <li>d for directory or - for file</li>
     * <li>r if allowed to read or - if it's not allowed</li>
     * <li>w if allowed to write or - if it's not allowed</li>
     * <li>x if allowed to execute or - if it's not allowed</li>
     * </ol>
     * 
     * @param f
     *            input file
     * @return string that contains flags for current file
     * @throws IOException
     *             if user provided null reference
     */
    public static String resloveFlags(File f) throws IOException {
        if (f == null) {
            throw new IOException("Cannot read properties of null!");
        }

        StringBuilder flags = new StringBuilder(4);

        flags.append(f.isDirectory() ? 'd' : '-');
        flags.append(f.canRead() ? 'r' : '-');
        flags.append(f.canWrite() ? 'w' : '-');
        flags.append(f.canExecute() ? 'x' : '-');

        return flags.toString();
    }

    /**
     * Creates string filled with symbol. Used where we need specific padding.
     * 
     * @param padding
     *            number of blank spaces that we want to create
     * @param symbol
     *            padding symbol
     * @return string that contains desired number of whitespace characters
     */
    public static String createPadding(int padding, char symbol) {
        if (padding < 1) {
            return "";
        }

        String blanks = String.format("%0" + padding + "d", 0).replace('0',
                symbol);

        return blanks;
    }

    /**
     * Method converts byte array to hexadecimal representation string.
     * 
     * @param inputByte
     *            byte that we are converting to hex string
     * @return String representation of given byte
     */
    public static String bytetohex(byte inputByte) {
        StringBuilder output = new StringBuilder(2);

        byte upper = (byte) ((inputByte >> 4) & LOWER_MASK);
        byte lower = (byte) (inputByte & LOWER_MASK);

        output.append(Integer.toHexString(upper));
        output.append(Integer.toHexString(lower));

        return output.toString();
    }
}
