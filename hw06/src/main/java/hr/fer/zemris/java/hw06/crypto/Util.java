package hr.fer.zemris.java.hw06.crypto;

/**
 * Class provides methods for manipulating strings that are formatted as
 * hexadecimal number sequences. Class provides conversion methods that allows
 * user to create byte array from hexadecimal string and vice versa.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Util {
    /**
     * Used for extracting lower half of byte.
     */
    private static final byte LOWER_MASK = 0x0f;

    /**
     * Converts given string to byte array.
     * 
     * @param hexsequence
     *            string sequence written as hex number @return byte
     *            array @throws
     * @return byte array of input array in dual complement notation
     */
    public static byte[] hextobyte(String hexsequence) {
        if (hexsequence == null) {
            throw new IllegalArgumentException("Input string cannot be null!");
        }
        if ((hexsequence.length() % 2) != 0) {
            throw new IllegalArgumentException(
                    "Input is not dividable with 2.");
        }

        if (hexsequence.length() == 0) {
            return new byte[0];
        }

        char[] inputAsChar = hexsequence.toCharArray();
        byte[] byteOutput = new byte[inputAsChar.length / 2];
        for (int i = 0, j = 0; i < inputAsChar.length; i += 2, j++) {
            try {
                String upper = String.format("#%c", inputAsChar[i]);
                String lower = String.format("#%c", inputAsChar[i + 1]);

                byte lw = Byte.decode(lower);
                byte up = Byte.decode(upper);

                byteOutput[j] = (byte) ((byte) lw | (up << 4));
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException(
                        "Input string contains invalid hexadecimal numbers!");
            }
        }

        return byteOutput;
    }

    /**
     * Method converts byte array to hexadecimal representation string.
     * 
     * @param inputByteArray
     *            array of bytes that we are converting to hex string
     * @return String representation of given byte array
     * @throws IllegalArgumentException
     *             if user passes null
     */
    public static String bytetohex(byte[] inputByteArray) {
        if (inputByteArray == null) {
            throw new IllegalArgumentException("Input array can't be null");
        }
        StringBuilder output = new StringBuilder(inputByteArray.length * 2);

        for (byte b : inputByteArray) {
            byte upper = (byte) ((b >> 4) & LOWER_MASK);
            byte lower = (byte) (b & LOWER_MASK);

            output.append(Integer.toHexString(upper));
            output.append(Integer.toHexString(lower));
        }
        return output.toString();
    }
}
