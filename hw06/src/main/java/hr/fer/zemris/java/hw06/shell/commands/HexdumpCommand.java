package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.ShellUtil;
import hr.fer.zemris.java.hw06.shell.parser.ShellParser;

/**
 * Class models hexdump command. Hexdump command prints on environment standard
 * output. Output of this command is raw file data in hexadecimal format and by
 * its side ANSI representation of those bytes. Character that can't be encoded
 * by ANSI charset is replaced by '.' in output but in raw data value is un
 * changed.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class HexdumpCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "hexdump";
        commandDescription = new ArrayList<>();

        commandDescription.add("Creates hex representation of input file.");
    }

    /**
     * Size of input read buffer.
     */
    private static final int BUFFER_SIZE = 16;

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        Path fileInput;
        // Try to parse arguments
        try {
            ShellParser parser = new ShellParser(arguments);
            String[] argumentList = parser.getArgumentsList();

            if (argumentList.length != 1) {
                env.writeln("Invalid number of arguments.");
                return ShellStatus.CONTINUE;
            }
            fileInput = Paths.get(argumentList[0]).toAbsolutePath();
        } catch (Exception e) {
            env.write(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        try {
            if (!Files.exists(fileInput)) {
                env.writeln("Input file does not exists!");
                return ShellStatus.CONTINUE;
            }

            BufferedInputStream bufferedIs = new BufferedInputStream(
                    Files.newInputStream(fileInput, StandardOpenOption.READ),
                    BUFFER_SIZE);

            byte[] inputBuffer = new byte[BUFFER_SIZE];
            int length;
            int size = 0;
            while ((length = bufferedIs.read(inputBuffer)) > 0) {
                StringBuilder sb = new StringBuilder();
                env.write(String.format("%08X ", size));
                for (int i = 0; i < length; i++) {
                    byte b = inputBuffer[i];
                    char c = (char) b;
                    if (b < 32 || b > 127) {
                        c = '.';
                    }
                    env.write(String.format("%s ", ShellUtil.bytetohex(b)));
                    sb.append(String.format("%c", c));
                }

                if (length != BUFFER_SIZE) {
                    env.write(ShellUtil
                            .createPadding(3 * (BUFFER_SIZE - length), ' '));
                }

                env.write(String.format(" %s ", sb.toString()));
                env.writeln("");
                size += 16;
            }
            bufferedIs.close();
        } catch (Exception e) {
            env.writeln("Error: Can't perform hexdump!" + System.lineSeparator()
                    + e.getMessage());
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
