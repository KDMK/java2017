package hr.fer.zemris.java.hw06.shell.parser;

/**
 * Lexer program is used to build tokens from given input text. TokenTypes for
 * this lexer are defined in @see TokenType enumeration. Language that we are
 * building lexer for consists only of words and control symbols. Lexer stores
 * current token and has methods for generating new tokens.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ShellLexer {
    /**
     * Defines rule set upon which lexer generates tokens.
     */
    private LexerState state;
    /**
     * Array of input text characters for which we are generating tokens.
     */
    private char[] data;
    /**
     * Current token.
     */
    private ShellToken token;
    /**
     * Index of next character for processing.
     */
    private int currentIndex;

    /**
     * Default constructor for constructing lexer.
     *
     * @param text
     *            Input text for which we are generating tokens.
     * @throws IllegalArgumentException
     *             If user passes null reference.
     */
    public ShellLexer(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }
        this.data = text.toCharArray();
        this.currentIndex = 0;
        this.state = LexerState.BASIC;
    }

    /**
     * Generates and return next token from input text.
     *
     * @return Currently generated token
     */
    public ShellToken nextToken() {
        // Check if current token is end of file.
        if (token != null && token.getType() == ShellTokenType.EOF) {
            throw new ShellLexerException("No more tokens in input file.");
        }

        if (state == LexerState.BASIC) {
            return generateTokenBasic();
        } else {
            return generateTokenExtended();
        }
    }

    /**
     * Checks whether next character in input sequence is escaped character.
     *
     * @return <code>true</code> if next character in data array is escaped
     *         character.
     */
    private boolean isEscapedCharacter() {
        // First check if you are in the end of input sequence. Then check if
        // next character is valid escape sequence.
        return (currentIndex < data.length - 1) && data[currentIndex] == '\\'
                && (nextChar() == '"' || nextChar() == '\\');
    }

    /**
     * Skips blanks in character sequence. Characters that are interpreted as
     * blanks are: <code>' ', '\n', '\r', '\n'</code>
     */
    private void skipBlanks() {
        while (currentIndex < data.length) {
            if (isBlank(data[currentIndex])) {
                currentIndex++;
                continue;
            }
            return;
        }
    }

    /**
     * Checks if current character is blank character.
     *
     * @param curCharacter
     *            <code>char</code> value that is checked
     * @return <code>true</code> if next character is blank character
     */
    private boolean isBlank(char curCharacter) {
        return data[currentIndex] == ' ' || data[currentIndex] == '\r'
                || data[currentIndex] == '\n' || data[currentIndex] == '\t';
    }

    /**
     * Determines if current word token is matched fully. Word token ends if
     * next character is blank space, end of input sequence or start of some
     * other token.
     *
     * @return <code>true</code> if token is matched fully
     */
    private boolean wordTokenEnd() {
        return currentIndex == data.length || isBlank(data[currentIndex])
                || data[currentIndex] == '"';
    }

    /**
     * Returns next character in input array. It does not modify currentIndex.
     *
     * @return Next character.
     */
    private char nextChar() {
        return data[currentIndex + 1];
    }

    /**
     * Returns last generated token. This method does not generate next token.
     *
     * @return Last generated token
     */
    public ShellToken getToken() {
        return token;
    }

    /**
     * Sets lexer operation state.
     *
     * @param state
     *            Desired state
     * @see LexerState
     */
    public void setState(LexerState state) {
        if (state == null) {
            throw new IllegalArgumentException("State can't be null.");
        }
        this.state = state;
    }

    /**
     * Generates next token using basic rules.
     *
     * @return Next generated token
     */
    private ShellToken generateTokenBasic() {
        String curInputToken = "";

        while (currentIndex <= data.length) {
            skipBlanks();
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (currentIndex >= data.length && curInputToken.equals("")) {
                token = new ShellToken(ShellTokenType.EOF, null);
                return token;
            }

            if (data[currentIndex] == '"' && curInputToken.isEmpty()) {
                currentIndex++;
                token = new ShellToken(ShellTokenType.SWITCH, null);
                return token;
            }

            curInputToken = curInputToken
                    .concat(Character.toString(data[currentIndex++]));
            // If next character is followed by blank space or there is no
            // characters after it construct Word token. Else continue
            // consuming characters.
            if (wordTokenEnd()) {
                return newWordToken(curInputToken);
            }

        }
        return null;
    }

    /**
     * Generates new token using extended rules.
     *
     * @return Next generated token.
     */
    private ShellToken generateTokenExtended() {
        String curInputToken = "";

        while (currentIndex <= data.length) {
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (currentIndex >= data.length && curInputToken.equals("")) {
                token = new ShellToken(ShellTokenType.EOF, null);
                return token;
            }

            // If it is just skip \
            if (isEscapedCharacter()) {
                currentIndex++;
                if (currentIndex >= data.length - 1) {
                    throw new ShellLexerException("Closing \" not found.");
                }
            }

            if (data[currentIndex] == '"' && curInputToken.isEmpty()) {
                currentIndex++;
                if (currentIndex < data.length
                        && !isBlank(data[currentIndex])) {
                    throw new ShellLexerException(
                            "Expected blank after closing \"");
                }
                token = new ShellToken(ShellTokenType.SWITCH, null);
                return token;
            }

            curInputToken = curInputToken
                    .concat(Character.toString(data[currentIndex++]));

            if (currentIndex == data.length - 1 || data[currentIndex] == '"') {
                return newWordToken(curInputToken);
            }
        }

        return null;
    }

    /**
     * Creates new word token, and sets current token to that token.
     *
     * @param value
     *            Value of word that we are creating.
     * @return New <code>TokenType.WORD</code> token
     */
    private ShellToken newWordToken(String value) {
        token = new ShellToken(ShellTokenType.WORD, value);
        return token;
    }

}
