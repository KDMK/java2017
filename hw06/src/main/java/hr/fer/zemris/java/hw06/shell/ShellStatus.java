package hr.fer.zemris.java.hw06.shell;

/**
 * Defines operation states of implemented shell environment.
 * 
 * @author Matija Bartolac
 *
 */
public enum ShellStatus {
    /**
     * Signals that shell will continue reading commands after current
     * command.
     */
    CONTINUE, 
    /**
     * Signals that shell will exit after executing current command.
     */
    TERMINATE;
}
