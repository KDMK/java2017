package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.ShellUtil;
import hr.fer.zemris.java.hw06.shell.parser.ShellParser;
import hr.fer.zemris.java.hw06.shell.parser.ShellParserException;

/**
 * Writes list of all files in current directory. For each file in directory it
 * displays access permissions, size, creation time and file name.
 * 
 * @author Matija Bartolac
 *
 */
public class LsCommand implements ShellCommand {
    /**
     * Name of this command.
     */
    private final static String commandName;
    /**
     * Short description of command. Each line is single entry in list.
     */
    private final static List<String> commandDescription;

    static {
        commandName = "ls";
        commandDescription = new ArrayList<>();

        commandDescription.add(
                "Prints current folder content with detailed file information.");
    }

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        String dirPath = "";
        try {
            ShellParser parser = new ShellParser(arguments);
            if (parser.getArgumentsList().length != 1) {
                env.writeln("Illegal number of arguments.");
                return ShellStatus.CONTINUE;
            }
            dirPath = parser.getArgumentsList()[0];
        } catch (ShellParserException | IllegalArgumentException e) {
            env.writeln(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        File[] folderContent = null;
        try {
            folderContent = new File(
                    Paths.get(dirPath).toAbsolutePath().toString()).listFiles();
            if (folderContent == null) {
                env.writeln("Cannot find : " + dirPath);
            }
        } catch (Exception e1) {
            env.writeln("Error opening " + dirPath + System.lineSeparator()
                    + e1.getMessage());
            
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            for (File f : folderContent) {
                Path path = Paths.get(f.getAbsolutePath());

                BasicFileAttributeView faView = Files.getFileAttributeView(path,
                        BasicFileAttributeView.class,
                        LinkOption.NOFOLLOW_LINKS);
                BasicFileAttributes attributes = faView.readAttributes();

                FileTime fileTime = attributes.creationTime();
                String formattedDateTime = sdf
                        .format(new Date(fileTime.toMillis()));

                System.out.print(String.format("%4s %10s %s %-10s%n",
                        ShellUtil.resloveFlags(f), Files.size(path),
                        formattedDateTime, f.getName()));
            }
        } catch (IOException e) {
            env.write(e.getMessage());
            return ShellStatus.CONTINUE;
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(commandDescription);
    }

}
