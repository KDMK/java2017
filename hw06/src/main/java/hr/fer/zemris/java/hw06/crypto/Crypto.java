package hr.fer.zemris.java.hw06.crypto;

import static hr.fer.zemris.java.hw06.crypto.Util.bytetohex;
import static hr.fer.zemris.java.hw06.crypto.Util.hextobyte;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Program allows user to encrypt or decrypt given file using the AES
 * cryptoalgorithm and the 128-bit encryption key or calculate and check the
 * SHA-256 file digest. Program takes two(or three) command line arguments:
 * <ul>
 * <li>First argument is operation which we want to perform(checksha, encrypt,
 * decrypt)</li>
 * <li>Second argument is file for which we are performing operation</li>
 * <li>If operation is encryption or decryption third parameter is output
 * file</li>
 * <ul/>
 * <p>
 * User is informed on result of operation on standard output.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Crypto {
    /**
     * Buffer size used for input and output buffers.
     */
    private static final short BUFFER_SIZE = 4096;
    /**
     * Encryption and padding type. Used when constructing {@link Cipher}
     * instances.
     */
    private static final String ENC_ALG_PAD = "AES/CBC/PKCS5Padding";
    /**
     * Algorithm used for calculating checksum.
     */
    private static final String CHEKSHA_ALGORITHM = "SHA-256";

    /**
     * Starting point of program
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 2) {
            String algorithm = args[0];
            String fileName = args[1];

            Scanner is = new Scanner(System.in);
            String expectedChecksum = getInput(
                    String.format(
                            "Please provide expected sha-256 digest for hw06test.bin:%n> "),
                    is);
            is.close();

            String calculatedChecksum = "";
            try {
                calculatedChecksum = checksum(CHEKSHA_ALGORITHM, fileName);
            } catch (IOException e1) {
                System.out.println("Can't open file: " + e1.getMessage());
                System.exit(-3);
            } catch (NoSuchAlgorithmException e2) {
                System.out.println("Invalid algorithm: " + algorithm);
                System.exit(-4);
            }

            if (expectedChecksum.toLowerCase().equals(calculatedChecksum)) {
                System.out.println(String.format(
                        "Digesting completed. Digest of %s matches expected digest.",
                        fileName));
            } else {
                System.out.print(String.format(
                        "Digesting complete. Digest of %s does not match expected "
                                + "digest. Digest was: %s",
                        fileName, calculatedChecksum));
            }

        } else if (args.length == 3) {
            int operation = 0;
            try {
                operation = determineOperation(args[0]);
            } catch (IllegalArgumentException e1) {
                System.out.println(e1.getMessage());
                System.exit(-2);
            }

            Scanner is = new Scanner(System.in);
            String inputFile = args[1];
            String outputFile = args[2];

            String password = getInput(String
                    .format("Please provide password as hex-encoded text (16 bytes, i.e."
                            + " 32 hex-digits):%n> "),
                    is);
            String initVector = getInput(String
                    .format("Please provide initialization vector as hex-encoded text "
                            + "(32 hex-digits):%n> "),
                    is);

            is.close();
            try {
                encryptDecrypt(operation, ENC_ALG_PAD, password, initVector,
                        inputFile, outputFile);
                System.out.println("Done");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Invalid number of arguments!");
            System.exit(-1);
        }

    }

    /**
     * Calculates checksum of input file by specified algorithm.
     * 
     * @param algorithm
     *            used hashing algorithm
     * @param fileName
     *            name of input file
     * @return string representation of calculated hash(hexadecimal)
     * @throws IOException
     *             if file cannot be opened
     * @throws NoSuchAlgorithmException
     *             if user provided unsupported hashing algorithm
     */
    public static String checksum(String algorithm, String fileName)
            throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);

        BufferedInputStream bufferedIs = new BufferedInputStream(
                Files.newInputStream(Paths.get(fileName).toAbsolutePath(),
                        StandardOpenOption.READ),
                BUFFER_SIZE);

        byte[] inputBuffer = new byte[BUFFER_SIZE];
        int readBytes = 0;
        while ((readBytes = bufferedIs.read(inputBuffer)) > 0) {
            md.update(inputBuffer, 0, readBytes);
        }

        bufferedIs.close();

        return bytetohex(md.digest());
    }

    /**
     * Encrypts or decrypts input file.
     * 
     * @param optMode
     *            {@link Cipher#ENCRYPT_MODE} or {@link Cipher#DECRYPT_MODE}
     * @param type
     *            type of encryption algorithm used and padding used
     * @param key
     *            password used for encryption/decryption
     * @param initVector
     *            initialization vector
     * @param inputFileName
     *            file that we want to encrypt/decrypt
     * @param outputFileName
     *            result of encryption/decryption
     * @throws NoSuchAlgorithmException
     *             if user provides unsupported encryption algorithm
     * @throws NoSuchPaddingException
     *             if user provides unsupported padding format
     * @throws InvalidKeyException
     *             if user provides invalid key for specified algorithm
     * @throws InvalidAlgorithmParameterException
     *             if user provides invalid initialization vector
     * @throws IOException
     *             if file cannot be opened for read or write
     * @throws IllegalBlockSizeException
     *             if input file block size does not match expected block
     *             size(at encryption)
     * @throws BadPaddingException
     *             if input file contains padding that is different from
     *             specified
     */
    public static void encryptDecrypt(int optMode, String type, String key,
            String initVector, String inputFileName, String outputFileName)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IOException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec keySpec = new SecretKeySpec(hextobyte(key), "AES");
        AlgorithmParameterSpec paramSpec = new IvParameterSpec(
                hextobyte(initVector));

        Cipher cipher = Cipher.getInstance(type);
        cipher.init(optMode, keySpec, paramSpec);

        Path inputPath = Paths.get(inputFileName).toAbsolutePath();
        Path outputPath = Paths.get(outputFileName).toAbsolutePath();

        StandardOpenOption newFileOpt = Files.exists(outputPath)
                ? StandardOpenOption.WRITE : StandardOpenOption.CREATE_NEW;

        BufferedInputStream bufferedIs = new BufferedInputStream(
                Files.newInputStream(inputPath, StandardOpenOption.READ),
                BUFFER_SIZE);
        BufferedOutputStream bufferedOs = new BufferedOutputStream(
                Files.newOutputStream(outputPath, newFileOpt), BUFFER_SIZE);

        byte[] inputBuffer = new byte[BUFFER_SIZE];
        int readBytes = 0;
        while ((readBytes = bufferedIs.read(inputBuffer)) > 0) {
            bufferedOs.write(cipher.update(inputBuffer, 0, readBytes));
        }
        bufferedOs.write(cipher.doFinal());
        bufferedOs.flush();
        bufferedOs.close();
        bufferedIs.close();
    }

    /**
     * Prompts user for single input parameter and returns value provided
     * through {@link System#in}.
     * 
     * @param message
     *            prompt message
     * @param input
     *            input scanner
     * @return input value
     */
    private static String getInput(String message, Scanner input) {
        System.out.print(message);
        String retVal = input.next();

        return retVal;
    }
    
    /**
     * Determines which operation user demanded. If operation is unsupported
     * throw IllegalOperationException.
     * 
     * @param operation
     *            as string
     * @return {@link Cipher#ENCRYPT_MODE} or {@link Cipher#DECRYPT_MODE}
     * @throws IllegalArgumentException
     *             if operation is unsupported
     */
    private static int determineOperation(String operation) {
        switch (operation.toLowerCase()) {
        case "encrypt":
            return Cipher.ENCRYPT_MODE;
        case "decrypt":
            return Cipher.DECRYPT_MODE;
        default:
            throw new IllegalArgumentException("Unknown command: " + operation);
        }
    }
}
