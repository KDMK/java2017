package hr.fer.zemris.java.hw06.shell;

import java.util.SortedMap;

/**
 * Interface provides contract that every specific command line environment must
 * satisfy in order to be functional.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface Environment {
    /**
     * Returns next line from standard input.
     * 
     * @return next line from environments standard input.
     * @throws ShellIOException
     *             if there was an error reading user input
     */
    String readLine() throws ShellIOException;

    /**
     * Writes formatted text to environments standard output.
     * 
     * @param text
     *            formated string, can contain multiple lines
     * @throws ShellIOException
     *             if there was an error writing data to output
     */
    void write(String text) throws ShellIOException;

    /**
     * Writes single line of formated text to environments standard output.
     * 
     * @param text
     *            formated string
     * @throws ShellIOException
     *             if there was an error writing data to output
     */
    void writeln(String text) throws ShellIOException;

    /**
     * Returns list of all supported commands as sorted map.
     * 
     * @return sorted map of all commands
     */
    SortedMap<String, ShellCommand> commands();

    /**
     * @param symbol
     *            single character that represents multi-line symbol
     */
    void setMultilineSymbol(Character symbol);

    /**
     * @return single character that represents multi-line symbol
     */
    Character getMultilineSymbol();

    /**
     * @return single character that represents prompt symbol.
     */
    Character getPromptSymbol();

    /**
     * @param symbol
     *            new prompt symbol
     */
    void setPromptSymbol(Character symbol);

    /**
     * @return single character that represents that command spreads throughout
     *         multiple lines.
     */
    Character getMorelinesSymbol();

    /**
     * @param symbol
     *            single character that represents that command spreads
     *            throughout multiple lines.
     */
    void setMorelinesSymbol(Character symbol);
}
