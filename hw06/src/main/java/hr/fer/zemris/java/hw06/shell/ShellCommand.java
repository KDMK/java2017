package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * Interface defines methods that every shell command must implement in order to
 * be operable.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface ShellCommand {
    /**
     * Performs given operation.
     * 
     * @param env
     *            current {@link Environment} instance from which command was
     *            called.
     * @param arguments
     *            command arguments
     * @return status code as defined in {@link ShellStatus}
     */
    ShellStatus executeCommand(Environment env, String arguments);

    /**
     * @return command name
     */
    String getCommandName();

    /**
     * @return command description
     */
    List<String> getCommandDescription();
}
