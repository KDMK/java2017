package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.CatCommand;
import hr.fer.zemris.java.hw06.shell.commands.CharsetsCommand;
import hr.fer.zemris.java.hw06.shell.commands.CopyCommand;
import hr.fer.zemris.java.hw06.shell.commands.ExitCommand;
import hr.fer.zemris.java.hw06.shell.commands.HelpCommand;
import hr.fer.zemris.java.hw06.shell.commands.HexdumpCommand;
import hr.fer.zemris.java.hw06.shell.commands.LsCommand;
import hr.fer.zemris.java.hw06.shell.commands.MkdirCommand;
import hr.fer.zemris.java.hw06.shell.commands.SymbolCommand;
import hr.fer.zemris.java.hw06.shell.commands.TreeCommand;

/**
 * Implementation of custom shell. Shell simulates behavior of bash style shell.
 * List of all available commands are available using command <code>help</code>.
 * Program takes no command line arguments. Program is terminated with
 * <code>exit</code> command.
 * 
 * @author Matija Bartolac
 *
 */
public class MyShell implements Environment {
    /**
     * Symbol that represents multi-line command.
     */
    private Character morelines;
    /**
     * Symbol that represents multi-line statement.
     */
    private Character multiline;
    /**
     * Prompt symbol.
     */
    private Character prompt;
    /**
     * Scanner that is used to collect all user input.
     */
    private Scanner inputScanner;
    /**
     * Map of all supported commands.
     */
    private SortedMap<String, ShellCommand> commands;

    /**
     * Default constructor.
     * 
     * @param prompt
     *            default prompt symbol
     * @param morelines
     *            default morelines symbol
     * @param multiline
     *            default multiline symbol
     */
    public MyShell(Character prompt, Character morelines, Character multiline) {
        this.prompt = prompt;
        this.morelines = morelines;
        this.multiline = multiline;
        this.inputScanner = new Scanner(System.in);
        this.commands = new TreeMap<>();

        commands.put("cat", new CatCommand());
        commands.put("charsets", new CharsetsCommand());
        commands.put("exit", new ExitCommand());
        commands.put("ls", new LsCommand());
        commands.put("help", new HelpCommand());
        commands.put("tree", new TreeCommand());
        commands.put("mkdir", new MkdirCommand());
        commands.put("copy", new CopyCommand());
        commands.put("hexdump", new HexdumpCommand());
        commands.put("symbol", new SymbolCommand());
    }

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        Environment env = new MyShell('$', '\\', '|');
        ShellStatus exitStatus = ShellStatus.CONTINUE;

        System.out.println("Welcome to MyShell v1.0");
        while (exitStatus == ShellStatus.CONTINUE) {
            String userInput;
            try {
                userInput = env.readLine();
            } catch (ShellIOException e) {
                env.write(e.getMessage());;
                exitStatus = ShellStatus.TERMINATE;
                continue;
            }

            String[] splitInput = userInput.split("\\s", 2);
            String argumentInput = splitInput.length != 1 ? splitInput[1] : "";

            ShellCommand command = ShellUtil.resolveCommand(splitInput[0], env);
            if (command != null) {
                exitStatus = command.executeCommand(env, argumentInput);
                continue;
            }
            System.out.print(String.format("%c %s%n", env.getPromptSymbol(),
                    "Bad command name."));
        }

        ((MyShell) env).closeInput();
        System.out.println("Goodbye!");
    }

    @Override
    public String readLine() throws ShellIOException {
        StringBuilder inputCommand = new StringBuilder();
        System.out.print(String.format("%c ", this.getPromptSymbol()));

        try {
            while (true) {
                inputCommand.append(inputScanner.nextLine().trim());
                if (inputCommand.charAt(inputCommand.length() - 1) != this
                        .getMorelinesSymbol()) {
                    break;
                }
                inputCommand.deleteCharAt(inputCommand.length() - 1);
                System.out
                        .print(String.format("%c ", this.getMultilineSymbol()));
            }
        } catch (NoSuchElementException | IllegalStateException e) {
            throw new ShellIOException(e.getMessage());
        }

        return inputCommand.toString().trim();
    }

    @Override
    public void write(String text) throws ShellIOException {
        if (text == null) {
            throw new ShellIOException("Cannot print null.");
        }
        System.out.print(text);
        System.out.flush();
    }

    @Override

    public void writeln(String text) throws ShellIOException {
        if (text == null) {
            throw new ShellIOException("Cannot print null.");
        }
        System.out.println(text);
    }

    @Override
    public SortedMap<String, ShellCommand> commands() {
        return Collections.unmodifiableSortedMap(commands);
    }

    @Override
    public void setMultilineSymbol(Character symbol) {
        this.multiline = symbol;
    }

    @Override
    public Character getPromptSymbol() {
        return this.prompt;
    }

    @Override
    public void setPromptSymbol(Character symbol) {
        this.prompt = symbol;

    }

    @Override
    public Character getMorelinesSymbol() {
        return this.morelines;
    }

    @Override
    public void setMorelinesSymbol(Character symbol) {
        this.morelines = symbol;

    }

    @Override
    public Character getMultilineSymbol() {
        return this.multiline;
    }

    // MyShell methods

    /**
     * Closes input scanner.
     */
    public void closeInput() {
        this.inputScanner.close();
    }
}
