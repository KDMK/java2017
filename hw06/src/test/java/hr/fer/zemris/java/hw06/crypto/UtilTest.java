package hr.fer.zemris.java.hw06.crypto;

import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class UtilTest {

    @Test
    public void testHexToByteValid1() {
        byte[] expected = {0b1, (byte) 0b10101110, 0b100010};
        byte[] result = Util.hextobyte("01ae22");
        
        assertArrayEquals(expected, result);
    }
    
    @Test
    public void testHexToByteValid2() {
        byte[] expected = { 0b1, (byte) 0b10101110, 0b100010, 0b01001111,
                (byte) 0b10111000};
        byte[] result = Util.hextobyte("01ae224FB8");
        
        assertArrayEquals(expected, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testHexToByteInvalidLength() {
        byte[] expected = {0b1, (byte) 0b10101110, 0b100010};
        byte[] result = Util.hextobyte("01ae223");
        
        assertArrayEquals(expected, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testHexToByteInvalidCharacter() {
        byte[] expected = {0b1, (byte) 0b10101110, 0b100010};
        byte[] result = Util.hextobyte("01ag22");
        
        assertArrayEquals(expected, result);
    }

    @Test
    public void testByteToHexValid1() {
        String expected = "01ae22";
        byte[] input = {1, -82, 34};
        
        String result = Util.bytetohex(input);
        assertEquals(expected, result);
    }
    
    @Test
    public void testByteToHexValid2() {
        String expected = "01ae224fb8";
        byte[] input = {1, -82, 34, 79, -72};
        
        String result = Util.bytetohex(input);
        assertEquals(expected, result);
    }
}
