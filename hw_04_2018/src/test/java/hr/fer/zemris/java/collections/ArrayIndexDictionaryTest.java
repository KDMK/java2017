package hr.fer.zemris.java.collections;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayIndexDictionaryTest {

    @Test
    public void addToDictionary_shouldSucceed() {
        Dictionary<String, String> phoneBook = new ArrayIndexDictionary<>();

        phoneBook.put("FER", "01/1234-567");
        phoneBook.put("FFZG", "01/7654-321");

        assertEquals("01/1234-567", phoneBook.get("FER"));
        assertEquals("01/7654-321", phoneBook.get("FFZG"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addToDictionary_shouldFail() {
        Dictionary<String, String> phoneBook = new ArrayIndexDictionary<>();
        phoneBook.put(null, "bla");
    }

    @Test
    public void addNullToDictionary_shouldSucceed() {
        Dictionary<String, String> phoneBook = new ArrayIndexDictionary<>();

        phoneBook.put("FER", null);

        assertNull(phoneBook.get("FER"));
    }


}
