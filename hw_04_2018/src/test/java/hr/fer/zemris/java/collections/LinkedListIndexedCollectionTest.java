package hr.fer.zemris.java.collections;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Class that contains test methods for LinkedListIndexedCollection class.
 *
 * @author Matija Bartolac
 *
 */
public class LinkedListIndexedCollectionTest {

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#add(Object)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddNull() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        n.add(null);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#add(Object)}.
     */
    @Test
    public void testAddNormal() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        assertEquals("Crvenkapica", n.get(3));
        assertEquals("Marica", n.get(1));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#contains(Object)}.
     */
    @Test
    public void testContains() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        assertTrue("Crvenkapica", n.contains("Crvenkapica"));
        assertFalse("Trnoruzica", n.contains("Trnoruzica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#toArray()}.
     */
    @Test
    public void testToArray() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);
        String[] expected = new String[]{
                "Ivica", "Marica", "Vuk", "Crvenkapica", "i tri prascica"
        };

        assertArrayEquals(expected, n.toArray());
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#clear()}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testClear() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        n.clear();
        assertEquals(0, n.size());
        n.get(0);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#LinkedListIndexedCollection(hr.fer.zemris.java.custom.collections.Collection)}.
     */
    @Test
    public void testLinkedListIndexedCollectionFromCollection() {
        LinkedListIndexedCollection n1 = new LinkedListIndexedCollection();
        fillList(n1);

        LinkedListIndexedCollection n2 = new LinkedListIndexedCollection(n1);
        assertEquals(n1.get(1), n2.get(1));
        assertEquals(n1.get(4), n2.get(4));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#get(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGet() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        assertEquals("Vuk", n.get(2));
        assertEquals("Crvenkapica", n.get(3));
        n.get(5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#indexOf(Object)}.
     */
    @Test
    public void testIndexOf() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        assertEquals(2, n.indexOf("Vuk"));
        assertEquals(3, n.indexOf("Crvenkapica"));
        assertEquals(-1, n.indexOf("Bakica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#insert(Object, int)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInsertNull() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();

        n.insert(null, 0);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#insert(Object, int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testInsertOutOfBounds() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        n.insert("Bakica", 5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection#remove(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndex() {
        LinkedListIndexedCollection n = new LinkedListIndexedCollection();
        fillList(n);

        n.remove(2);
        assertNotEquals("Vuk", n.get(2));
        n.remove(4);
    }

    /**
     * Fill the collection with some sample elements.
     *
     * @param n Reference to collection
     */
    private void fillList(LinkedListIndexedCollection n) {
        n.add("Ivica");
        n.add("Marica");
        n.add("Vuk");
        n.add("Crvenkapica");
        n.add("i tri prascica");
    }
}
