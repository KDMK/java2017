package hr.fer.zemris.java.collections;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Class that contains test methods for ArrayIndexedCollection class.
 *
 * @author Matija Bartolac
 */
public class ArrayIndexedCollectionTest {

    private ArrayIndexedCollection<String> myCollection;

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#add(Object)}.
     */
    @Test
    public void testAddNull() {
        ArrayIndexedCollection<String> n = new ArrayIndexedCollection<>();
        n.add(null);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#add(Object)}.
     */
    @Test
    public void testAddNormal() {
        ArrayIndexedCollection<String> n = new ArrayIndexedCollection<>();

        n.add("Ivica");
        assertEquals("Ivica", n.get(0));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#get(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOutOfBounds() {
        ArrayIndexedCollection<String> n = new ArrayIndexedCollection<>();

        n.get(5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#get(int)}.
     */
    @Test
    public void testGetNormal() {
        assertEquals("Marica", myCollection.get(1));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#clear()}.
     */
    @Test
    public void testClear() {
        myCollection.clear();
        assertEquals(0, myCollection.size());
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#insert(Object, int)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInsertNull() {
        myCollection.insert(null, 0);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#insert(Object, int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testInsertOutOfBounds() {
        myCollection.insert("Ovo ne stane tu ->", 5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#insert(Object, int)}.
     */
    @Test
    public void testInsertNormal() {
        myCollection.insert("Snjeguljica", 2);
        assertEquals("Snjeguljica", myCollection.get(2));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#indexOf(Object)}.
     */
    @Test
    public void testIndexOfHaveElement() {
        assertEquals(3, myCollection.indexOf("Crvenkapica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#indexOf(Object)}.
     */
    @Test
    public void testIndexOfWithoutElement() {
        assertEquals(-1, myCollection.indexOf("Trnoruzica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#remove(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexOutOfBounds() {
        ArrayIndexedCollection<String> n = new ArrayIndexedCollection<>();
        n.remove(5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#remove(int)}.
     */
    @Test
    public void testRemoveNormal() {
        myCollection.remove(3);

        assertNotEquals("Crvenkapica", myCollection.get(3));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#remove(Object)}.
     */
    @Test()
    public void testRemoveObjectNull() {
        myCollection.remove(null);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#remove(Object)}.
     */
    @Test
    public void testRemoveObjectNotFound() {
        assertFalse(myCollection.remove("Trnoruzica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.collections.ArrayIndexedCollection#remove(Object)}.
     */
    @Test
    public void testRemoveObjectFound() {
        assertTrue(myCollection.remove("Crvenkapica"));
    }

    /**
     * Fill the collection with some sample elements.
     */
    @SuppressWarnings("Duplicates")
    @Before
    public void fillArray() {
        this.myCollection = new ArrayIndexedCollection<>();

        myCollection.add("Ivica");
        myCollection.add("Marica");
        myCollection.add("Vuk");
        myCollection.add("Crvenkapica");
        myCollection.add("i sedam patuljaka");
    }

}
