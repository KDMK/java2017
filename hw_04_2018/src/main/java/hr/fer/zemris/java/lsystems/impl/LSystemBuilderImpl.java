package hr.fer.zemris.java.lsystems.impl;

import hr.fer.zemris.java.lsystems.commands.*;
import hr.fer.zemris.java.math.Vector2DImpl;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;

import java.awt.*;

public class LSystemBuilderImpl implements LSystemBuilder {

    private LSystemImpl lSystem = new LSystemImpl();

    public LSystemBuilderImpl() {
        this.lSystem = new LSystemImpl();
    }

    @Override
    public LSystemBuilder setUnitLength(double unitLength) {
        lSystem.setUnitLength(unitLength);
        return this;
    }

    @Override
    public LSystemBuilder setOrigin(double x, double y) {
        lSystem.setOrigin(new Vector2DImpl(x, y));
        return this;
    }

    @Override
    public LSystemBuilder setAngle(double angle) {
        lSystem.setAngle(angle);
        return this;
    }

    @Override
    public LSystemBuilder setAxiom(String axiom) {
        lSystem.setAxiom(axiom);
        return this;
    }

    @Override
    public LSystemBuilder setUnitLengthDegreeScaler(double unitLengthDegreeScaler) {
        lSystem.setUnitLengthDegreeScaler(unitLengthDegreeScaler);
        return this;
    }

    @Override
    public LSystemBuilder registerProduction(char c, String s) {
        lSystem.getRegisterdActions().put(c, s);
        return this;
    }

    @Override
    public LSystemBuilder registerCommand(char c, String s) {
        Command command = buildCommandFromFormattedInput(s);
        lSystem.getRegisteredCommands().put(c, command);
        return this;
    }

    private Command buildCommandFromFormattedInput(String s) {
        String[] splitted = s.split("\\s");
        switch (splitted[0].toLowerCase()) {
            case "draw":
                return new DrawCommand(Double.parseDouble(splitted[1]));
            case "skip":
                return new SkipCommand(Double.parseDouble(splitted[1]));
            case "scale":
                return new ScaleCommand(Double.parseDouble(splitted[1]));
            case "pop":
                return new PopCommand();
            case "push":
                return new PushCommand();
            case "color":
                // add color parsing from string
                return new ColorCommand(new Color(0, 0, 0));
            case "rotate":
                return new RotateCommand(Double.parseDouble(splitted[1]));
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public LSystemBuilder configureFromText(String[] strings) {
        return null;
    }

    @Override
    public LSystem build() {
        return lSystem;
    }
}
