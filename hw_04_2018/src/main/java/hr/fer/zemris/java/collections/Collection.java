package hr.fer.zemris.java.collections;

/**
 * Class collection represents some general collection of objects.
 *
 * @author Matija Bartolac
 *
 */
public interface Collection<T> {

    /**
     * Checks if collection is empty.
     *
     * @return Returns true if collection contains no objects and false
     *         otherwise
     */
    default boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Returns the number of currently stored objects in collection.
     *
     * @return Size of collection.
     */
    int size();

    /**
     * Adds the given object into collection.
     *
     * @param value
     *            Value of element that we want to store in collection.
     */
    void add(T value);

    /**
     * Checks if given value is in collection.
     *
     * @param value
     *            Value of element that we are looking for.
     * @return True if collection contains given value and false otherwise
     */
    boolean contains(T value);

    /**
     * Checks if given value is in collection and removes it from collection.
     *
     * @param value
     *            Value of element that we want to remove.
     * @return True if collection contains given value, and method removes one
     *         occurrence of it.
     */
    boolean remove(T value);

    /**
     * Gets all collection elements in form of an array. This method never
     * returns null.
     *
     * @return Array of collection elements.
     */
    T[] toArray();

    /**
     * Process each element of collection using given processor. The order in
     * which elements will be sent is undefined in this class.
     *
     * @param processor
     *            Reference to processor used to process elements.
     */
    void forEach(Processor<T> processor);

    /**
     * Adds all elements from given collection to this collection. This other
     * collection remains unchanged.
     *
     * @param other
     *            Reference to collection whose elements we want to append to
     *            this collection.
     */
    default void addAll(Collection<T> other) {
        if (other == null) {
            throw new NullPointerException();
        }

        other.forEach(this::add);
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
