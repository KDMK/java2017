package hr.fer.zemris.java.collections;

/**
 * Implementation of a stack collection. Class provides basic structure and
 * operation of a stack data structure.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class ObjectStack<T> {
    /**
     * Reference to container that stores stack elements.
     */
    private ArrayIndexedCollection<T> stackArray;

    /**
     * Default constructor. It doesn't take any input arguments.
     */
    public ObjectStack() {
        this.stackArray = new ArrayIndexedCollection<>();
    }

    /**
     * Checks if stack is empty.
     *
     * @return True if stack is empty, false otherwise.
     */
    public boolean isEmpty() {
        return stackArray.isEmpty();
    }

    /**
     * Return size of stack.
     *
     * @return Size of stack
     */
    public int size() {
        return stackArray.size();
    }

    /**
     * Pushes given value on the stack. <code>null</code> value is not permitted
     * and function throws IllegalArgumentException if null is passed as an
     * argument.
     *
     * @param value Value that we are inserting on stack.
     */
    public void push(T value) {
        if (value == null) {
            throw new IllegalArgumentException("Null is not valid");
        }
        stackArray.add(value);
    }

    /**
     * Removes last value pushed on stack from stack and returns it. If the
     * stack is empty when method pop is called, the method will throw
     * EmptyStackException.
     *
     * @return Object from the top of the stack.
     */
    public T pop() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }

        T retVal = stackArray.get(stackArray.size() - 1);
        stackArray.remove(stackArray.size() - 1);

        return retVal;
    }

    /**
     * Returns last value from the stack. Returned object is not removed from
     * the stack. If the stack is empty when method pop is called, the method
     * will throw EmptyStackException.
     *
     * @return Object from the top of the stack.
     */
    public T peek() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return stackArray.get(stackArray.size() - 1);
    }

    /**
     * Removes all elements from stack.
     */
    public void clear() {
        stackArray.clear();
    }
}
