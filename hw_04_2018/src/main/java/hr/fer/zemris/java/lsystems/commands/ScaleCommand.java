package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.lsystems.Painter;

public class ScaleCommand implements Command {

    private final double factor;

    public ScaleCommand(double factor) {
        this.factor = factor;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().setTranslationValue(factor);
    }
}
