package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.lsystems.Painter;

public class PopCommand implements Command {

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.popState();
    }
}
