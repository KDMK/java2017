package hr.fer.zemris.java.collections;

public class ArrayIndexDictionary<K, V> implements Dictionary<K, V> {
    private ArrayIndexedCollection<K> keys;
    private ArrayIndexedCollection<V> values;

    public ArrayIndexDictionary() {
        this.keys = new ArrayIndexedCollection<>();
        this.values = new ArrayIndexedCollection<>();
    }

    @Override
    public boolean isEmpty() {
        return keys.isEmpty();
    }

    @Override
    public int size() {
        return keys.size();
    }

    @Override
    public void clear() {
        keys.clear();
        values.clear();
    }

    @Override
    public void put(K key, V value) {
        if (key == null) {
            throw new IllegalArgumentException("Key mustn't be null");
        }

        int keyIndex = keys.indexOf(key);
        if (keyIndex != -1) {
            values.remove(keyIndex);
            values.insert(value, keyIndex);
            return;
        }

        keys.add(key);
        values.add(value);
    }

    @Override
    public V get(K key) {
        int keyIndex = keys.indexOf(key);
        return keyIndex != -1 ? values.get(keyIndex) : null;
    }
}
