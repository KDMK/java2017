package hr.fer.zemris.java.collections;


/**
 * Processor class is used to define desired way of processing collection
 * elements. It contains only process method without any specific
 * implementation. Any class that extends Processor have to give its own
 * implementation of process method.
 *
 * @author Matija Bartolac
 *
 */
public interface Processor<T> {

    /**
     ** Do various processing on given object.
     **
     ** @param value
     *            Instance of object that we want to process.
     */
    void process(T value);
}
