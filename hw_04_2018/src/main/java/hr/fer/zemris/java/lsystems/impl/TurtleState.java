package hr.fer.zemris.java.lsystems.impl;

import hr.fer.zemris.java.math.Vector2D;

import java.awt.*;

public class TurtleState {

    private Vector2D currentPosition;
    private Vector2D currentDirection;
    private Color currentColor;
    private double translationValue;

    public TurtleState(Vector2D currentPosition, Vector2D currentDirection, Color currentColor, double translationValue) {
        this.currentPosition = currentPosition;
        this.currentDirection = currentDirection;
        this.currentColor = currentColor;
        this.translationValue = translationValue;
    }

    public Vector2D getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Vector2D currentPosition) {
        this.currentPosition = currentPosition;
    }

    public Vector2D getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(Vector2D currentDirection) {
        this.currentDirection = currentDirection;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    public double getTranslationValue() {
        return translationValue;
    }

    public void setTranslationValue(double translationValue) {
        this.translationValue = translationValue;
    }
}
