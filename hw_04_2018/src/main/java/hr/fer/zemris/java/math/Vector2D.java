package hr.fer.zemris.java.math;

/**
 * The interface Vector 2 d.
 */
public interface Vector2D {

    /**
     * Gets x.
     *
     * @return the x
     */
    double getX();

    /**
     * Gets y.
     *
     * @return the y
     */
    double getY();

    /**
     * Translates vector for given offset. Translation is done in-place.
     *
     * @param offset the offset
     */
    void translate(Vector2D offset);

    /**
     * Translates vector for given offset. Translation creates new vector.
     *
     * @param offset the offset
     * @return the vector 2 d
     */
    Vector2D translated(Vector2D offset);

    /**
     * Rotates vector for given angle. Rotation is done in-place
     *
     * @param angle the angle
     */
    void rotate(double angle);

    /**
     * Rotates vector for given angle. Rotation creates new vector.
     *
     * @param angle the angle
     * @return the vector 2 d
     */
    Vector2D rotated(double angle);

    /**
     * Scales vector with given scaling factor. Scaling is done in-place.
     *
     * @param scaler the scaler
     */
    void scale(double scaler);

    /**
     * Scales vector with given scaling factor. Scaling creates new vector.
     *
     * @param scaler the scaler
     * @return the vector 2 d
     */
    Vector2D scaled(double scaler);

    /**
     * Creates vector copy.
     *
     * @return the vector 2 d
     */
    Vector2D copy();
}
