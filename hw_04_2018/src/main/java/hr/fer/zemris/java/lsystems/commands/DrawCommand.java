package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.java.lsystems.impl.TurtleState;
import hr.fer.zemris.java.math.Vector2D;
import hr.fer.zemris.lsystems.Painter;

public class DrawCommand implements Command {

    private final double step;

    public DrawCommand(double step) {
        this.step = step;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        TurtleState curState = ctx.getCurrentState();
        Vector2D curPosition = curState.getCurrentPosition();
        Vector2D currentDirection = curState.getCurrentDirection();

        Vector2D move = currentDirection.scaled(step);
        Vector2D newPosition = curPosition.translated(move);

        System.out.println(String.format("CurPoint (%.2f, %.2f)", curPosition.getX(), curPosition.getY()));
        System.out.println(String.format("newPoint (%.2f, %.2f)", newPosition.getX(), newPosition.getY()));

        painter.drawLine(curPosition.getX(), curPosition.getY(), newPosition.getX(), newPosition.getY(), curState.getCurrentColor(), (float) 2);
        curState.setCurrentPosition(newPosition);
    }
}
