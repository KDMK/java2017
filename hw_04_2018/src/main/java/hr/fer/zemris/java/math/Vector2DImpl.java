package hr.fer.zemris.java.math;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

public class Vector2DImpl implements Vector2D {

    private double x;
    private double y;

    public Vector2DImpl(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void translate(Vector2D offset) {
        this.x += offset.getX();
        this.y += offset.getY();
    }

    @Override
    public Vector2D translated(Vector2D offset) {
        Vector2D vectorCopy = copy();
        vectorCopy.translate(offset);

        return vectorCopy;
    }

    @Override
    public void rotate(double angle) {
        this.x = x * cos(toRadians(angle)) - y * sin(toRadians(angle));
        this.y = x * sin(toRadians(angle)) + y * cos(toRadians(angle));
    }

    @Override
    public Vector2D rotated(double angle) {
        Vector2D vectorCopy = copy();
        vectorCopy.rotate(angle);

        return vectorCopy;
    }

    @Override
    public void scale(double scaler) {
        this.x *= scaler;
        this.y *= scaler;
    }

    @Override
    public Vector2D scaled(double scaler) {
        Vector2D vectorCopy = copy();
        vectorCopy.scale(scaler);

        return vectorCopy;
    }

    @Override
    public Vector2D copy() {
        return new Vector2DImpl(this.x, this.y);
    }
}
