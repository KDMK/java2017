package hr.fer.zemris.java.collections;

/**
 * The interface Dictionary models generic key-value store.
 *
 * @param <K> the type used as key in dictionary
 * @param <V> the type parameter used as value in dictionary
 */
public interface Dictionary<K, V> {

    /**
     * Checks if given dictionary is empty..
     *
     * @return <code>true</code> if dictionary is empty, <code>false</code> otherwise
     */
    boolean isEmpty();

    /**
     * Returns current size of the dictionary.
     *
     * @return size of the dictionary
     */
    int size();

    /**
     * Removes all elements from dictionary.
     */
    void clear();

    /**
     * Puts value in the collection. Value is associated with its key. Key mustn't be null. It there was already value
     * associated to key it will be overwritten.
     *
     * @param key   the key of new element
     * @param value the value of new element
     */
    void put(K key, V value);

    /**
     * Retrieves value associated to given key from the dictionary. If there is no value associated to given key method
     * will return null.
     *
     * @param key the key
     * @return the value associated to key given as argument or <code>null</code> if there is no value associated with
     * key it will return null
     */
    V get(K key);
}
