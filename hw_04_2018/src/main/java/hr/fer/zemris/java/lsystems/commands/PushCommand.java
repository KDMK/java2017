package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.java.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.Painter;

public class PushCommand implements Command {

    @Override
    public void execute(Context ctx, Painter painter) {
        TurtleState curState = ctx.getCurrentState();
        TurtleState newState = new TurtleState(curState.getCurrentPosition(), curState.getCurrentDirection(),
                curState.getCurrentColor(), curState.getTranslationValue());

        ctx.pushState(newState);
    }
}
