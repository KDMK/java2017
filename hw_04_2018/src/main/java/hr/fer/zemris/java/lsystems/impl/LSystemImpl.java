package hr.fer.zemris.java.lsystems.impl;

import hr.fer.zemris.java.collections.ArrayIndexDictionary;
import hr.fer.zemris.java.collections.Dictionary;
import hr.fer.zemris.java.lsystems.commands.Command;
import hr.fer.zemris.java.math.Vector2D;
import hr.fer.zemris.java.math.Vector2DImpl;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.Painter;

import java.awt.*;

public class LSystemImpl implements LSystem {

    private static final double DEFAULT_UNIT_LENGTH = 0.1;
    private static final double DEFAULT_UNIT_LENGTH_DEGREE_SCALER = 1;
    private static final Vector2D DEFAULT_ORIGIN = new Vector2DImpl(0, 0);
    private static final double DEFAULT_ANGLE = 0;
    private static final String DEFAULT_AXIOM = "";

    private double unitLength;
    private double unitLengthDegreeScaler;
    private Vector2D origin;
    private double angle;
    private String axiom;

    private Dictionary<Character, Command> registeredCommands;
    private Dictionary<Character, String> registerdActions;

    private Context context;

    public LSystemImpl(double unitLength, double unitLengthDegreeScaler, Vector2D origin, double angle,
                       String axiom, Dictionary<Character, Command> registeredCommands,
                       Dictionary<Character, String> registerdActions, Context context) {
        this.unitLength = unitLength;
        this.unitLengthDegreeScaler = unitLengthDegreeScaler;
        this.origin = origin;
        this.angle = angle;
        this.axiom = axiom;
        this.registeredCommands = registeredCommands;
        this.registerdActions = registerdActions;
        this.context = context;

        Vector2DImpl initDirection = new Vector2DImpl(1, 0);
        initDirection.rotate(angle);
        initDirection.scale(unitLengthDegreeScaler);
        TurtleState initState = new TurtleState(origin, initDirection, Color.BLACK, unitLength);
        context.pushState(initState);
    }

    public LSystemImpl() {
        this(DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_LENGTH_DEGREE_SCALER, DEFAULT_ORIGIN, DEFAULT_ANGLE, DEFAULT_AXIOM,
                new ArrayIndexDictionary<>(), new ArrayIndexDictionary<>(), new Context());
    }

    @Override
    public String generate(int i) {
        String curAxiom = axiom;

        for (int j = 0; j < i; j++) {
            StringBuilder expandedAxiom = new StringBuilder();
            for (int k = 0; k < curAxiom.length(); k++) {
                String production = registerdActions.get(curAxiom.charAt(k));
                if (production != null) {
                    expandedAxiom.append(production);
                    continue;
                }
                expandedAxiom.append(curAxiom.charAt(k));
            }
            curAxiom = expandedAxiom.toString();
        }

        return curAxiom;
    }

    @Override
    public void draw(int i, Painter painter) {
        String expandedAxiom = generate(i);
        char[] actions = expandedAxiom.toCharArray();

        for (Character c : actions) {
            registeredCommands.get(c).execute(context, painter);
        }
    }

    public double getUnitLength() {
        return unitLength;
    }

    public void setUnitLength(double unitLength) {
        this.unitLength = unitLength;
    }

    public double getUnitLengthDegreeScaler() {
        return unitLengthDegreeScaler;
    }

    public void setUnitLengthDegreeScaler(double unitLengthDegreeScaler) {
        this.unitLengthDegreeScaler = unitLengthDegreeScaler;
    }

    public Vector2D getOrigin() {
        return origin;
    }

    public void setOrigin(Vector2D origin) {
        this.origin = origin;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public String getAxiom() {
        return axiom;
    }

    public void setAxiom(String axiom) {
        this.axiom = axiom;
    }

    public Dictionary<Character, Command> getRegisteredCommands() {
        return registeredCommands;
    }

    public void setRegisteredCommands(Dictionary<Character, Command> registeredCommands) {
        this.registeredCommands = registeredCommands;
    }

    public Dictionary<Character, String> getRegisterdActions() {
        return registerdActions;
    }

    public void setRegisterdActions(Dictionary<Character, String> registerdActions) {
        this.registerdActions = registerdActions;
    }
}
