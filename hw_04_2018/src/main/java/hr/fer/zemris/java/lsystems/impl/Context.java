package hr.fer.zemris.java.lsystems.impl;

import hr.fer.zemris.java.collections.ObjectStack;

public class Context {

    private ObjectStack<TurtleState> turtleStack;

    public Context() {
        this.turtleStack = new ObjectStack<>();
    }

    public TurtleState getCurrentState() {
        return turtleStack.peek();
    }

    public void pushState(TurtleState state) {
        turtleStack.push(state);
    }

    public void popState() {
        turtleStack.pop();
    }
}
