package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.lsystems.Painter;

public class RotateCommand implements Command {

    private final double angle;

    public RotateCommand(double angle) {
        this.angle = angle;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().getCurrentDirection().rotate(angle);
    }
}
