/**
 * Package contains classes used to work with basic collections. It contains
 * generic class Collection that we use to blueprint other collection data
 * structures further on. Other than that we have implementations of resizable
 * array collection, linked list collection and stack-like collection. Also,
 * package features class for working with complex numbers.
 *
 * @author Matija Bartolac
 * @version 1.0
 *
 */
package hr.fer.zemris.java.collections;
