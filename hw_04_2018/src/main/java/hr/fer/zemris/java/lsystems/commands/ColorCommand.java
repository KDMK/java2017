package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.java.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.Painter;

import java.awt.Color;

public class ColorCommand implements Command {

    private final Color color;

    public ColorCommand(Color color) {
        this.color = color;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().setCurrentColor(color);
    }
}
