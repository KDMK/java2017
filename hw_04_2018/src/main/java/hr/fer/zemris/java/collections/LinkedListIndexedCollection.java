package hr.fer.zemris.java.collections;

/**
 * Implementation of linked list collection of objects. Linked list collection
 * extends {@link Collection}. List consists of ListNodes. Duplicate elements
 * are allowed and storage of null elements is not allowed. You can create list
 * from some other list or as completely new object.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
@SuppressWarnings("Duplicates")
public class LinkedListIndexedCollection<T> implements Collection<T> {
    /**
     * Current size of collection; number of nodes in list.
     */
    private int size;
    /**
     * Reference to first first node of the linked list.
     */
    private ListNode<T> first;
    /**
     * Reference to last node of the linked list.
     */
    private ListNode<T> last;

    /**
     * ListNode used for storing list elements.
     *
     * @author Matija Bartolac
     * @version v1.0
     */
    private final class ListNode<T> {
        /**
         * Value that ListNode is holding.
         */
        private T value;

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            ListNode<T> other = (ListNode<T>) obj;
            if (!getOuterType().equals(other.getOuterType())) {
                return false;
            }
            if (value == null) {
                return other.value == null;
            } else return value.equals(other.value);
        }

        /**
         * Reference to previous ListNode.
         */
        private ListNode<T> previous;
        /**
         * Reference to next ListNode.
         */
        private ListNode<T> next;

        /**
         * Default constructor for List node.
         *
         * @param value    Value of an object that list node is holding.
         * @param previous Reference to previous list node.
         * @param next     Reference to next list node.
         */
        private ListNode(T value, ListNode<T> previous, ListNode<T> next) {
            this.value = value;
            this.previous = previous;
            this.next = next;
        }

        /**
         * Returns type of outer class.
         *
         * @return Type of outer class type.
         */
        private LinkedListIndexedCollection getOuterType() {
            return LinkedListIndexedCollection.this;
        }
    }

    /**
     * Default constructor. It doesn't take any parameters. Creates empty list.
     */
    public LinkedListIndexedCollection() {
        this.size = 0;
        this.first = null;
        this.first = null;
    }

    /**
     * This constructor takes reference to some other collection and creates
     * list from its elements.
     *
     * @param other Reference to collection which elements are copied into this
     *              newly constructed collection.
     */
    public LinkedListIndexedCollection(Collection<T> other) {
        this();
        this.addAll(other);
    }

    // Helper methods

    /**
     * Checks if Object value is same class as the objects already stored in
     * elements array. It will also return true if array is empty, because then
     * it's irrelevant which type is elements in array.
     *
     * @param value Reference to an object.
     * @return True if value is the same class as the elements in array or array
     * is empty.
     */
    private boolean isNotSameClass(Object value) {
        return (size != 0 && !first.value.getClass().isInstance(value));
    }

    /**
     * Helper function used to get to element at index. Starts from beginning.
     *
     * @param index Index of wanted element.
     * @return Object at given index.
     */
    private ListNode<T> goToIndex(int index) {
        ListNode<T> current = first;

        while (index > 0) {
            index--;
            current = current.next;
        }
        return current;
    }

    /**
     * Helper function used to get to element at index. Starts from end.
     *
     * @param index Index of wanted element.
     * @return Object at given index.
     */
    private ListNode<T> goToIndexReverse(int index) {
        ListNode<T> current = last;

        while (index < size - 1) {
            index++;
            current = current.previous;
        }
        return current;
    }

    /**
     * Add the given object into this collection at the end of collection. Newly
     * added element becomes the element at the biggest index. Method refuses to
     * add null as element by throwing IllegalArgumentException.
     *
     * @see hr.fer.zemris.java.collections.Collection#add(Object)
     */
    @Override
    public void add(T value) {
        if (value == null) {
            throw new IllegalArgumentException("Cannot add null!");
        }

        // Check if type of value is same type as objects already in list
        // -- NOT CHECKING HERE --
        if (isNotSameClass(value)) {
            throw new IllegalArgumentException(
                    "Value does not match type of stored elements!");
        }

        ListNode<T> newNode = new ListNode<>(value, null, null);

        // Check if list is empty: if true point first and last to newNode.
        // List is not empty, add element after the last.
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            newNode.previous = last;
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    @Override
    public boolean contains(T value) {
        if (value == null) {
            return false;
        }

        ListNode<T> current = first;
        while (current != null) {
            if (current.value.equals(value)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean remove(T value) {
        return false;
    }

    @Override
    public T[] toArray() {
        Object[] outputArray = new Object[size]; // this should be written using reflection
        ListNode<T> current = first;
        int i = 0;

        while (current != null) {
            outputArray[i++] = current.value;
            current = current.next;
        }

        return (T[]) outputArray;
    }

    @Override
    public void forEach(Processor<T> processor) {
        ListNode<T> current = first;

        while (current != null) {
            processor.process(current.value);
            current = current.next;
        }
    }

    @Override
    public void clear() {
        ListNode<T> current = first;

        // Makni referencu na prethodni clan, skoci na slijedeci
        while (current != null) {
            current.previous = null;
            current = current.next;
        }

        first = null;
        last = null;
        size = 0;
    }

    /**
     * Returns the object that is stored in linked list at position index. Valid
     * indexes are 0 to size-1. If index is out of bounds throw exception.
     *
     * @param index Index of an element that we are want to get.
     * @return Object at index.
     */
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds!");
        }

        // Complexity of get operation shouldn't be greater than n/2 + 1. That
        // means that we have to find is index near end or start of list and
        // travel to that element accordingly.
        if (index <= size / 2) {
            return goToIndex(index).value;
        } else {
            return goToIndexReverse(index).value;
        }
    }

    /**
     * Searches the collection and returns the index of the first occurrence of
     * the given value or -1 if the value is not found.
     *
     * @param value Value of object that we are searching.
     * @return Index if element is found or -1 if it isn't.
     */
    public int indexOf(T value) {
        int index = 0;

        ListNode<T> current = first;

        while (current != null) {
            if (current.value.equals(value)) {
                return index;
            }
            index++;
            current = current.next;
        }

        return -1;
    }

    /**
     * Inserts (does not overwrite) the given value at the given position in
     * linked-list. The legal positions are 0 to size. If position is invalid
     * IllegalArgumentExcepiton is thrown. If you are trying to add object that
     * is not same type as objects currently in list throw
     * IllegalArgumentExpression.
     *
     * @param value    Value that we want to insert in our list.
     * @param position Index in which we want to add element.
     */
    public void insert(T value, int position) {
        if (position == 0 && size == 0) {
            add(value);
            return;
        }

        if (position < 0 || position >= size) {
            throw new IndexOutOfBoundsException("Index out bounds!");
        }

        if (isNotSameClass(value)) {
            throw new IllegalArgumentException(
                    "Value does not match type of stored elements!");
        }

        // List is empty or we are adding to end: delegate job to add function
        if (position == size - 1) {
            add(value);
        } else if (position == 0) {
            // Add in front
            first = new ListNode<>(value, null, first);
            size++;
        } else {
            // Add somewhere in the middle
            ListNode<T> currentPosNode = goToIndex(position);
            currentPosNode.previous = new ListNode<>(value, currentPosNode.previous,
                    currentPosNode);
            size++;
        }
    }

    /**
     * Removes element at specified index from collection. Legal indexes are 0
     * to size - 1. In case of invalid index it throws IndexOutOfBounds
     * exception.
     *
     * @param index Index of element that we want to remove from list.
     */
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out bounds!");
        }

        if (index == 0) {
            first = first.next;
            first.previous.next = null;
            first.previous = null;
        } else if (index == size - 1) {
            last = last.previous;
            last.next.previous = null;
            last.next = null;
        } else {
            ListNode<T> element = goToIndex(index);
            // Remove references to itself
            element.previous.next = element.next;
            element.next.previous = element.previous;
            // Remove his references to other nodes
            element.next = null;
            element.previous = null;
        }
        size--;
    }
}
