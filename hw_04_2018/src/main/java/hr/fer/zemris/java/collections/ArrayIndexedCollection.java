package hr.fer.zemris.java.collections;

import java.lang.reflect.Array;

/**
 * Class implements resizable array-backed collection of objects. It extends
 * {@link Collection} class. Duplicate elements are allowed and storage of null
 * elements is not. You can create collection with predefined size, you can
 * specify initial size of collection or you can create new collection from
 * elements of some other collection.
 *
 * @author Matija Bartolac
 * @version 1.0
 */
@SuppressWarnings("ALL")
public class ArrayIndexedCollection<T> implements Collection<T> {
    /**
     * Current size of collection (number of elements actually stored).
     */
    private int size;
    /**
     * Current capacity of allocated array of object references.
     */
    private int capacity;
    /**
     * An array of object references which length is determined by capacity
     * variable.
     */
    private T[] elements;
    /**
     * Default initial capacity of array collection. Used if user didn't provide
     * initial array capacity.
     */
    private static final int INIT_CAPACITY = 16;

    /**
     * Constructor for creating collection of given initial capacity. If initial
     * capacity is less than one IllegalArgumentException is thrown.
     *
     * @param initialCapacity Size of collection that we want to create.
     */
    public ArrayIndexedCollection(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException(
                    "Size of array can't be less than one.");
        }
        this.size = 0;
        this.capacity = initialCapacity;
        this.elements = createGenericArray(initialCapacity);
    }

    /**
     * Creates array of generic elements.
     *
     * @param capacity of requested array
     * @return generic array of requested capacity
     */
    private T[] createGenericArray(int capacity) {
        return (T[]) Array.newInstance(Object.class, capacity);
    }

    /**
     * Default constructor. It takes no arguments. Default size used for
     * creating array(if user did not specify) is defined by INIT_CAPACITY.
     */
    public ArrayIndexedCollection() {
        this(INIT_CAPACITY);
    }

    /**
     * Constructor for creating an array from existing array.
     *
     * @param other Reference to an array from which we are creating new
     *              collection.
     */
    public ArrayIndexedCollection(ArrayIndexedCollection<T> other) {
        this(other.size);
        this.addAll(other);
    }

    /**
     * Constructor for creating an array from existing array with specifying
     * initial size. Use it if you want to crate array that is initially larger
     * than array from whose elements you want to create array. If you want to
     * create exact same array(copy of other) use of constructor that takes only
     * other array reference is recommended.
     *
     * @param other           Reference to array from which we are creating new collection.
     * @param initialCapacity Initial capacity of array that we are creating.
     */
    public ArrayIndexedCollection(ArrayIndexedCollection<T> other,
                                  int initialCapacity) {
        this(initialCapacity);
        addAll(other);
    }

    // Helper methods

    /**
     * Resizes collection to double capacity.
     */
    private void resizeArray() {
        // Resize capacity and create new, larger array
        capacity *= 2;
        elements = java.util.Arrays.copyOf(elements, capacity);
    }

    /**
     * Checks if Object value is same class as the objects already stored in
     * elements array. It will also return true if array is empty, because then
     * it's irrelevant which type is elements in array.
     *
     * @param value Reference to an object.
     * @return True if value is the same class as the elements in array or array
     * is empty.
     */
//    private boolean isNotSameClass(T value) {
//        return (size != 0 && !elements[0].getClass().isInstance(value));
//    }

    // First empty methods
    // Average complexity of this method is O(1).

    /**
     * Method throws IllegalArgumentException if user tries to add null. Note
     * that first insert element will determine which type of object will be
     * stored in array.
     *
     * @see hr.fer.zemris.java.custom.collections.Collection#add(Object)
     */
    @Override
    public void add(T value) {
        // Check if the value matches the class of elements that are already in
        // collection
//        if (isNotSameClass(value)) {
//            throw new IllegalArgumentException(
//                    "Value does not match type of stored elements!");
//        }


        if (size == capacity) {
            resizeArray();
        }

        elements[size] = value;
        size++;
    }

    @Override
    public void forEach(Processor<T> processor) {
        for (T element : elements) {
            if (element == null) {
                continue;
            }
            processor.process(element);
        }
    }

    @Override
    public void clear() {
        size = 0;
        java.util.Arrays.fill(elements, null);
        elements = createGenericArray(capacity);
    }

    /**
     * Returns the object that is stored in backing array at position index.
     *
     * @param index Index of an element we want to get.
     * @return Element of collection at given index.
     */
    // Average complexity is O(1)
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds.");
        }
        return elements[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T[] toArray() {
        return java.util.Arrays.copyOfRange(elements, 0, size);
    }

    @Override
    public boolean contains(T value) {
        return indexOf(value) != -1;
    }

    /**
     * Insert(does not overwrite) the given value at the given position in
     * array.
     *
     * @param value    Value of element that we want to insert in array.
     * @param position Position where we want to insert element.
     */
    // Average complexity of this method is O(n)
    public void insert(T value, int position) {
        // Call add method if array is empty and user tries to add element to
        // index zero.
        if (size == 0 && position == 0) {
            add(value);
            return;
        }

        if (position < 0 || position >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds!");
        }

        // Check if the value matches the class of elements that are already in
        // collection
//        if (isNotSameClass(value)) {
//            throw new IllegalArgumentException(
//                    "Value does not match type of stored elements!");
//        }

        // Check array is already full. If true, rezize array.
        if (size == capacity) {
            resizeArray();
        }

        // "Shift" elements in array one place to the right, and place element
        // in desired position
        System.arraycopy(elements, position, elements, position + 1,
                (size - position));
        elements[position] = value;
    }

    /**
     * Searches the collection and returns the index of the first occurrence of
     * the given value or -1 if the value is not found. If called with null will
     * throw illegal argument exception.
     *
     * @param value Value of object whose index we are looking for.
     * @return Index of found object or -1 if not found.
     */
    public int indexOf(T value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Removes element at specified index from collection. Legal indexes are 0
     * to size-1. If index is out of bounds method throws indexOutOfBounds
     * exception.
     *
     * @param index Index of an element that we want to remove.
     */
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds!");
        }

        System.arraycopy(elements, index + 1, elements, index,
                (size - index) - 1);
        elements[size - 1] = null;
        size--;
    }

    @Override
    public boolean remove(T value) {
        int index = indexOf(value);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }
}
