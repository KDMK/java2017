package hr.fer.zemris.java.lsystems.commands;

import hr.fer.zemris.java.lsystems.impl.Context;
import hr.fer.zemris.lsystems.Painter;

public interface Command {

    void execute(Context ctx, Painter painter);
}
