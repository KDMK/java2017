<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8">
    <title>HW-13 webpage</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
        body {
	        background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                : session.getAttribute("pickedBgCol")%>;
        }

        .row h1 {
	        text-align: center;
        }

        .block {
            width: 100%;
        }
        </style>
</head>
<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="./" class="brand-logo">Home</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
				<li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="colors.jsp">Background color chooser</a></li>
                <li><a href="stories/funny.jsp">Funny story</a></li>
                <li><a href="report.jsp">OS usage report</a></li>
                <li><a href="appinfo.jsp">Server runtime</a></li>
                <li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i
				class="material-icons">menu</i></a>
		</div>
	</nav>

	<div class="container">
		<div class="row">
		    <br>
			<h1>HW13 welcome page</h1>
		    <br>
		</div>
		<div class="section">

			<!--   Icon Section   -->
			<div class="row">
				<div class="col s12 m6">
					<div class="icon-block">
						<h2 class="center light-blue-text">
							<i class="material-icons">flash_on</i>
						</h2>
						<h5 class="center">How to get around</h5>

						<p class="light">For navigating through page you can click on
							any link in navigation bar. If you want to get back to this page
							you can click on Home button any time and from any page.</p>
					</div>
				</div>

				<div class="col s12 m6">
					<div class="icon-block">
						<h2 class="center light-blue-text">
							<i class="material-icons">assessment</i>
						</h2>
						<h5 class="center">What is this form here for?</h5>

						<p class="light">You can get table of all sin and cos from
							given range from this from. Valid range for starting and ending
							angles are 0 to 360 degrees.</p>
					</div>
				</div>

			</div>
			<hr>
			<br>

			<div class="row">
				<form style="padding:20px; margin-top:15px;" class="col s12" action="trigonometric" method="GET">
					<div class="input-field col s6">
					    <i class="material-icons prefix">call_made</i>
						<input type="number" name="a" min="0" max="360" step="1" value="0">
						<label for="a">Starting angle:</label>
					</div>
					<div class="input-field col s6">
						<i class="material-icons prefix">call_received</i>
						<input type="number" name="b" min="0" max="360" step="1" value="360"><br>
						<label for="b">End angle:</label>
					</div>

					<div class="input-field col s6">
					   <button class="btn waves-effect waves-light btn-large block" type="submit" value="Create table">Submit<i class="material-icons right">send</i></button>
					</div>

					<div class="input-field col s6">
						<button class="btn waves-effect waves-light btn-large block" type="reset" value="Reset">Reset<i class="material-icons right">replay</i></button>
					</div>
				</form>
			</div>

		</div>

		<!--Import jQuery before materialize.js-->
		<script type="text/javascript"
			src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type="text/javascript" src="js/init.js"></script>
</body>
</html>
