<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page
	import="javax.servlet.jsp.JspWriter, java.io.*, java.util.*, java.text.SimpleDateFormat"%>

<%!
    /**
     * Function calculates time difference from server startup to now and formats
     * output as: "DD days, HH hours, mm minutes, ss seconds, ms miliseconds"
     *
     * @return formated time difference
     */
    private String printDate() {
        long initTime = (Long) getServletContext().getAttribute("initTime");
        long nowMils = System.currentTimeMillis();

        long timeDiff = nowMils - initTime;

        long milisecs = timeDiff % 1000;
        long secs = (timeDiff / 1000) % 60;
        long mins = ((timeDiff / (1000 * 60)) % 60);
        long hours = ((timeDiff / (1000 * 60 * 60)) % 24);
        long days = ((timeDiff / (1000 * 60 * 60 * 24)) % 365);

        return String.format(
                "%d days, %d hours, %d minutes, %d seconds, %d miliseconds%n",
                days, hours, mins, secs, milisecs);
    }%>

<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
        body {
	        background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                : session.getAttribute("pickedBgCol")%>;
        }

        .row h1 {
	        margin-top: 3.5em;
	        margin-bottom: 1.5em;
	        text-align: center;
        }

        .block {
	        width: 100%;
        }
</style>
</head>

<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="./" class="brand-logo">Home</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i
				class="material-icons">menu</i></a>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<h1>Server running time</h1>
		</div>
		<div class="row">
			<p style="font-size:2em; text-align: center;">
				Server is running for :
				<%=printDate()%>
			</p>
		</div>
	</div>

    <!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
</body>
