<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
    <title>Color chooser</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
    body {
	    background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
            : session.getAttribute("pickedBgCol")%>;
    }

    .row h1 {
        margin-top: 2.5em;
        margin-bottom: 2.5em;
	       text-align: center;
    }

    .block {
	    width: 100%;
    }
    </style>
</head>

<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="./" class="brand-logo">Home</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i
				class="material-icons">menu</i></a>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<h1>Set background color</h1>
		</div>
		<div class="row">
			<div class="col s6 m3">
				<a class="btn waves-effect waves-light btn-large block"
					href="<%=request.getContextPath()%>/setColor?color=white">WHITE</a>
			</div>
			<div class="col s6 m3">
				<a class="btn waves-effect waves-light btn-large block"
					href="<%=request.getContextPath()%>/setColor?color=red">RED</a>
			</div>
			<div class="col s6 m3">
				<a class="btn waves-effect waves-light btn-large block"
					href="<%=request.getContextPath()%>/setColor?color=green">GREEN</a>
			</div>
			<div class="col s6 m3">
				<a class="btn waves-effect waves-light btn-large block"
					href="<%=request.getContextPath()%>/setColor?color=cyan">CYAN</a>
			</div>
		</div>
	</div>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
</body>
