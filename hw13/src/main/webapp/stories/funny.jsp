<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.awt.Color"%>

<%!
    /**
     * Produces random color.
     *
     * @return string representation of color in format #rrggbb
     */
    private String getColor() {
        int r = (int) (Math.random() * 255);
        int g = (int) (Math.random() * 255);
        int b = (int) (Math.random() * 255);

        return String.format("#%02X%02X%02X", r, g, b);
    }%>

<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"
	media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
    .story {
	    color: <%=getColor()%>;
    }

    body {
	    background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
            : session.getAttribute("pickedBgCol")%>;
        }

    .row h1 {
        margin-top: 1em;
        margin-bottom: 0.75em;
	    text-align: center;
    }

    .block {
	    width: 100%;
    }
</style>
</head>

<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="../" class="brand-logo">Home</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="../colors.jsp">Background color chooser</a></li>
				<li><a href="../stories/funny.jsp">Funny story</a></li>
				<li><a href="../report.jsp">OS usage report</a></li>
				<li><a href="../appinfo.jsp">Server runtime</a></li>
				<li><a href="../powers?a=1&b=100&n=3">Create XLS</a></li>
				<li><a href="/webapp2/glasanje">Voting</a></li>
                <li><a href="/webapp2/glasanje-rezultati">Voting results</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="../colors.jsp">Background color chooser</a></li>
				<li><a href="../stories/funny.jsp">Funny story</a></li>
				<li><a href="../report.jsp">OS usage report</a></li>
				<li><a href="../appinfo.jsp">Server runtime</a></li>
				<li><a href="../powers?a=1&b=100&n=3">Create XLS</a></li>
				<li><a href="/webapp2/glasanje">Voting</a></li>
                <li><a href="/webapp2/glasanje-rezultati">Voting results</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i
				class="material-icons">menu</i></a>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<h1>Short funny story</h1>
		</div>
		<p class="story">A couple in their nineties are both having
			problems remembering things. They decide to go to the doctor for a
			checkup. The doctor tells them that they're physically okay, but they
			might want to start writing things down to help them remember. Later
			that night while watching TV, the old man gets up from his chair. His
			wife asks, "Where are you going?" "To the kitchen," he replies. "Will
			you get me a bowl of ice cream?" "Sure." "Don't you think you should
			write it down so you can remember it?" she asks. "No, I can remember
			it." "Well, I'd like some strawberries on top, too. You'd better
			write it down, because you know you'll forget it." He says, "I can
			remember that! You want a bowl of ice cream with strawberries." "I'd
			also like whipped cream. I'm certain you'll forget that, so you'd
			better write it down!" she retorts. Irritated, he says, "I don't need
			to write it down, I can remember it! Leave me alone! Ice cream with
			strawberries and whipped cream -- I got it, for goodness sake!" Then
			he grumbles into the kitchen. After about 20 minutes the old man
			returns from the kitchen and hands his wife a plate of bacon and
			eggs. She stares at the plate for a moment and says... "Where's my
			toast?</p>

		<div class="row"></div>
	</div>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript" src="../js/init.js"></script>
</body>
