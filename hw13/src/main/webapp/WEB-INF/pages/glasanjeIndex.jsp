<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="./css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style type="text/css">
body {
	background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                    : session.getAttribute("pickedBgCol")%>;
}
</style>
</head>
<body>
    <nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="./" class="brand-logo">Home</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="colors.jsp">Background color chooser</a></li>
				<li><a href="stories/funny.jsp">Funny story</a></li>
				<li><a href="report.jsp">OS usage report</a></li>
				<li><a href="appinfo.jsp">Server runtime</a></li>
				<li><a href="powers?a=1&b=100&n=3">Create XLS</a></li>
                <li><a href="glasanje">Voting</a></li>
                <li><a href="glasanje-rezultati">Voting results</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i
				class="material-icons">menu</i></a>
		</div>
	</nav>

    <div class="container">
        <h1>Vote for your favorite band:</h1>
        <p>What is your favorite band from this top list? Click on band link to vote!</p>
            <div class="collection">
                <a class="collection-item" href="glasanje-glasaj?id=1">The Beatles</a>
                <a class="collection-item" href="glasanje-glasaj?id=2">The Platters</a>
                <a class="collection-item" href="glasanje-glasaj?id=3">The Beach Boys</a>
                <a class="collection-item" href="glasanje-glasaj?id=4">The Four Seasons</a>
                <a class="collection-item" href="glasanje-glasaj?id=5">The Marcels</a>
                <a class="collection-item" href="glasanje-glasaj?id=6">The Everly Brothers</a>
                <a class="collection-item" href="glasanje-glasaj?id=7">The Mamas And The Papas</a>
            </div>
    </div>


    <!--Import jQuery before materialize.js-->
        <script type="text/javascript"
            src="js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/materialize.min.js"></script>
        <script type="text/javascript" src="./js/init.js"></script>
</body>
</html>
