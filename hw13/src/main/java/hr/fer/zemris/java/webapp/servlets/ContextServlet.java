package hr.fer.zemris.java.webapp.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This class represents ServletContextListener and it is used to store server
 * initialization time. We can use that information later to calculate server
 * running time.
 * 
 * @author Matija Bartolac
 *
 */
public class ContextServlet implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("initTime",
                System.currentTimeMillis());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
