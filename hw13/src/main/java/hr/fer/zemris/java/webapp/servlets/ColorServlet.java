package hr.fer.zemris.java.webapp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Color servlet sets background color of all pages.
 * 
 * @author Matija Bartolac
 *
 */
public class ColorServlet extends HttpServlet {
    /**
     * Serialization version id
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Object colorName = req.getParameter("color");
        String stylePath = getServletContext().getRealPath("/")
                + "/backgroundStyle.css";
        resp.getWriter().println(stylePath);

        if (colorName instanceof String) {
            req.getSession().setAttribute("pickedBgCol", colorName);
            resp.sendRedirect("/webapp2/colors.jsp");
        }
    }
}
