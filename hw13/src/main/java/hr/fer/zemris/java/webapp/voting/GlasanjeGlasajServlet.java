package hr.fer.zemris.java.webapp.voting;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet processes user vote and updates votes in file. After updating is
 * finished user is redirected to results page.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {
    /**
     * Default serial version uid
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Map<String, Integer> votes = readVotes(req.getServletContext()
                .getRealPath("/WEB-INF/glasanje-rezultati.txt"));

        if (votes == null) {
            req.setAttribute("errorMsg",
                    "There is currently error with vopting system. Sorry for inconvenience. Please try again later");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        String id = req.getParameter("id");
        resp.getWriter()
                .append("Requested band : " + id + System.lineSeparator());
        if (id != null) {
            resp.getWriter().append("Got in here.");
            Integer curVoteCount = votes.get(id);
            votes.replace(id, curVoteCount + 1);
        }

        writeVotes(req.getServletContext()
                .getRealPath("WEB-INF/glasanje-rezultati.txt"), votes);

        req.setAttribute("votes", votes);
        resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
    }

    /**
     * Writes current votes to file.
     * 
     * @param realPath
     *            real path to file
     * @param votes
     *            Map of all votes
     * @throws IOException
     *             if there is error writing to output stream
     */
    private void writeVotes(String realPath, Map<String, Integer> votes)
            throws IOException {
        Path filePath = Paths.get(realPath);

        BufferedWriter owriter = Files.newBufferedWriter(filePath,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

        for (Entry<String, Integer> ent : votes.entrySet()) {
            owriter.write(
                    String.format("%s\t%d", ent.getKey(), ent.getValue()));
            owriter.newLine();
        }

        owriter.flush();
        owriter.close();
    }

    /**
     * Reads votes from text file to map.
     * 
     * @param path
     *            to file
     * @return mapped values contained in textual file
     * @throws IOException
     *             If error occurs while reading from file
     */
    protected static Map<String, Integer> readVotes(String path)
            throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path));
        Map<String, Integer> votes = new LinkedHashMap<>();

        for (String line : lines) {
            String[] splittedLine = line.split("\t");

            if (splittedLine.length != 2) {

                return null;
            }

            votes.put(splittedLine[0].trim(),
                    Integer.parseInt(splittedLine[1].trim()));
        }

        return votes;
    }
}
