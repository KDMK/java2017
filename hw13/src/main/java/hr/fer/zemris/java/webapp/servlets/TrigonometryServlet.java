package hr.fer.zemris.java.webapp.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Trigonometry servlet is used to generate sine and cosine values for angles
 * from interval given through request parameters. When data is generated values
 * are passed to trigonometric.jsp to render.
 * 
 * @author Matija Bartolac
 *
 */
public class TrigonometryServlet extends HttpServlet {
    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Model of data that we are passing to user.
     * 
     * @author Matija Bartolac
     *
     */
    public static class SinAndCos {
        /**
         * Angle value
         */
        private double angle;
        /**
         * Sine value
         */
        private double sin;
        /**
         * Cosine value
         */
        private double cos;

        /**
         * Default constructor
         * 
         * @param angle value
         * @param sin value
         * @param cos value
         */
        public SinAndCos(double angle, double sin, double cos) {
            this.angle = angle;
            this.sin = sin;
            this.cos = cos;
        }

        /**
         * @return the angle value
         */
        public double getAngle() {
            return angle;
        }

        /**
         * @return the sine value
         */
        public double getSin() {
            return sin;
        }

        /**
         * @return the cosine value
         */
        public double getCos() {
            return cos;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Integer a = 0, b = 360, step = 1;

        try {
            a = Integer.valueOf(req.getParameter("a"));
        } catch (Exception ignorable) {
        }

        try {
            b = Integer.valueOf(req.getParameter("b"));
        } catch (Exception ignorable) {
        }

        if (a > b) {
            Integer tmp = a;
            a = b;
            b = tmp;
        }

        if (b > a + 720) {
            b = a + 720;
        }

        List<SinAndCos> retList = new ArrayList<>();
        for (; a < b + step; a += step) {
            Double sinValue = Math.sin(Math.toRadians(a));
            Double cosValue = Math.cos(Math.toRadians(a));

            retList.add(new SinAndCos(a, sinValue, cosValue));
        }

        req.setAttribute("retList", retList);
        req.getRequestDispatcher("WEB-INF/pages/trigonometric.jsp").forward(req,
                resp);
    }
}
