package hr.fer.zemris.java.webapp.voting;

import static hr.fer.zemris.java.webapp.voting.GlasanjeGlasajServlet.readVotes;
import static hr.fer.zemris.java.webapp.voting.GlasanjeServlet.bandEntries;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import hr.fer.zemris.java.webapp.voting.GlasanjeServlet.BandEntry;

/**
 * This servlet generates pie chart in form of a .png image. Pie chart shows
 * votes distribution for bands that participate in voting.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeGrafikaServlet extends HttpServlet {
    /**
     * Default serial version uid
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates dataset for pie chart.
     * 
     * @param bandPath
     *            location of bands list file
     * @param votePath
     *            location of vote list file
     * @return PieDataset of bands and their respective votes
     * @throws IOException
     *             If any error occurs while reading files
     * @throws ServletException
     *             If there was an error in servlet
     */
    private PieDataset createDataset(String bandPath, String votePath)
            throws IOException, ServletException {
        Map<String, Integer> votes = readVotes(votePath);
        List<BandEntry> bands = bandEntries(bandPath);

        DefaultPieDataset result = new DefaultPieDataset();

        for (BandEntry band : bands) {
            result.setValue(band.getBandName(), votes.get(band.getId()));
        }

        return result;
    }

    /**
     * Creates chart object from given data set.
     * 
     * @param dataset
     *            from which we are creating chart
     * @param title
     *            title of this chart
     * @return JFreeChart instance
     */
    private JFreeChart createChart(PieDataset dataset, String title) {
        JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true,
                true, false);

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(0);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(1.0f);

        return chart;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String bandPath = req.getServletContext()
                .getRealPath("WEB-INF/glasanje-definicija.txt");
        String votePath = req.getServletContext()
                .getRealPath("WEB-INF/glasanje-rezultati.txt");

        PieDataset dataset = createDataset(bandPath, votePath);
        JFreeChart chart = createChart(dataset, "Band votes");

        BufferedImage bim = chart.createBufferedImage(700, 350);

        resp.setContentType("image/png");

        ImageIO.write(bim, "png", resp.getOutputStream());
        resp.getOutputStream().flush();
    }

}
