package hr.fer.zemris.java.webapp.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Powers servlet gets three arguments and based on their values generates excel
 * document which contains numbers from [a,b] interval raised to the power n.
 * Each sheet in excel document represents n-th power. Valid range for a and b
 * is [-100, 100], and [1,5] for n. If given parameters are invalid user is
 * redirected to error page.
 * 
 * @author Matija Bartolac
 *
 */
public class PowersServlet extends HttpServlet {

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Integer a = null, b = null, n = null;

        try {
            a = Integer.parseInt(req.getParameter("a"));
        } catch (NumberFormatException | NullPointerException ex) {
            req.getSession().setAttribute("errorMsg",
                    "Error: " + ex.getMessage());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        try {
            b = Integer.parseInt(req.getParameter("b"));
        } catch (NumberFormatException | NullPointerException ex) {
            req.getSession().setAttribute("errorMsg",
                    "Error: " + ex.getMessage());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        try {
            n = Integer.parseInt(req.getParameter("n"));
        } catch (NumberFormatException | NullPointerException ex) {
            req.getSession().setAttribute("errorMsg",
                    "Error: " + ex.getMessage());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        if (!validArg(a, -100, 100)) {
            req.getSession().setAttribute("errorMsg",
                    "Error: a is out of range - " + a.intValue());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }
        if (!validArg(b, -100, 100)) {
            req.getSession().setAttribute("errorMsg",
                    "Error: b is out of range - " + b.intValue());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }
        if (!validArg(n, 1, 5)) {
            req.getSession().setAttribute("errorMsg",
                    "Error: n is out of range - " + n.intValue());
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        if (a > b) {
            Integer tmp = a;
            a = b;
            b = tmp;
        }

        createXls(a, b, n, resp, req);
    }

    /**
     * Checks if tested argument is in valid range
     * 
     * @param a
     *            number that we are testing
     * @param lowerBound
     *            lower bound of interval
     * @param upperBound
     *            upper bound of interval
     * @return true if number is in interval, false otherwise
     */
    private boolean validArg(Integer a, int lowerBound, int upperBound) {
        return (a <= upperBound) && (a >= lowerBound);
    }

    /**
     * Creates XLS document using the given parameters
     * 
     * @param a
     *            interval lower bound
     * @param b
     *            interval upper bound
     * @param n
     *            max power
     * @param resp
     *            HTTP response
     * @param req
     *            HTTP request
     * @throws IOException
     *             If there is an error opening file or writing to output
     *             stream.
     * @throws ServletException
     *             if servlet encounters any kind of problem
     */
    public static void createXls(int a, int b, int n, HttpServletResponse resp,
            HttpServletRequest req) throws IOException, ServletException {
        HSSFWorkbook hwb = null;

        try {
            String fileName = "powers.xls";
            hwb = new HSSFWorkbook();

            for (int i = 1; i <= n; i++) {
                HSSFSheet sheet = hwb.createSheet("x^" + i);

                for (int j = 0, start = a; start <= b; start++, j++) {
                    HSSFRow row = sheet.createRow(j);
                    row.createCell(0).setCellValue(start);
                    row.createCell(1).setCellValue(Math.pow(start, i));
                }
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            hwb.write(bos);
            byte[] outArray = bos.toByteArray();

            resp.setContentType("application/ms-excel");
            resp.setContentLength(outArray.length);
            resp.setHeader("Expires:", "0");
            resp.setHeader("Content-Disposition",
                    "attachment; filename=" + fileName + ".xls");
            OutputStream outStream = resp.getOutputStream();
            outStream.write(outArray);
            outStream.flush();

            return;
        } catch (Exception ex) {
            req.getSession().setAttribute("errorMsg",
                    "Error: cannot create xls.");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        } finally {
            hwb.close();
        }
    }
}
