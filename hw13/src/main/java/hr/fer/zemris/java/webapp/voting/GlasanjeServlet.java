package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet is used to load all entries that participates in voting. After
 * bands are loaded they are passed as session parameters to other pages to
 * render.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeServlet extends HttpServlet {
    /**
     * Default serial version id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Model of a band entry that participates in voting process.
     * 
     * @author Matija Bartolac
     *
     */
    public static class BandEntry {
        /**
         * Band id.
         */
        private String id;
        /**
         * Name of the band.
         */
        private String bandName;
        /**
         * Link to the band song.
         */
        private String songLink;

        /**
         * Default constructor.
         * 
         * @param id
         *            of song
         * @param bandName
         *            the name of the band
         * @param songLink
         *            link to the band song
         */
        public BandEntry(String id, String bandName, String songLink) {
            this.id = id;
            this.bandName = bandName;
            this.songLink = songLink;
        }

        /**
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * @return the bandName
         */
        public String getBandName() {
            return bandName;
        }

        /**
         * @return the songLink
         */
        public String getSongLink() {
            return songLink;
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String fileName = req.getServletContext()
                .getRealPath("/WEB-INF/glasanje-definicija.txt");

        List<BandEntry> bands = bandEntries(fileName);

        if (bands == null) {
            req.setAttribute("errorMsg",
                    "There is currently error with vopting system. Sorry for inconvenience. Please try again later");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        req.setAttribute("bands", bands);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp")
                .forward(req, resp);
    }

    /**
     * Loads all band entries from text file to list.
     * 
     * @param fileName
     *            name of the file that contains data
     * @return List of band entries
     * @throws IOException
     *             if there was an error reading from file
     * @throws ServletException
     *             if any kind of exception occurs in servlet
     */
    public static List<BandEntry> bandEntries(String fileName)
            throws IOException, ServletException {
        List<String> lines = Files.readAllLines(Paths.get(fileName));
        List<BandEntry> bands = new ArrayList<>();

        for (String line : lines) {
            String[] splittedLine = line.split("\t");

            if (splittedLine.length != 3) {

                return null;
            }

            bands.add(new BandEntry(splittedLine[0].trim(),
                    splittedLine[1].trim(), splittedLine[2].trim()));
        }

        return bands;
    }
}
