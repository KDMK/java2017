package hr.fer.zemris.java.webapp.servlets;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * Reporter servlet generates pie chart in form of a .png image. Pie chart shows
 * usage statistics for three most popular operating system.
 * 
 * @author Matija Bartolac
 *
 */
public class ReporterServlet extends HttpServlet {
    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates data set for chart.
     * 
     * @return PieDataset filled with desired values.
     */
    private PieDataset createDataset() {
        DefaultPieDataset result = new DefaultPieDataset();
        result.setValue("Linux", 29);
        result.setValue("OSx", 20);
        result.setValue("Windows", 51);

        return result;
    }

    /**
     * Creates chart object from given data set.
     * 
     * @param dataset
     *            from which we are creating chart
     * @param title
     *            title of this chart
     * @return JFreeChart instance
     */
    private JFreeChart createChart(PieDataset dataset, String title) {
        JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true,
                true, false);

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(0);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);

        return chart;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        PieDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset, "OS usage");

        BufferedImage bim = chart.createBufferedImage(500, 270);

        resp.setContentType("image/png");

        ImageIO.write(bim, "png", resp.getOutputStream());
        resp.getOutputStream().flush();
    }
}
