CREATE TABLE Polls
(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
title VARCHAR(150) NOT NULL,
message CLOB(2048) NOT NULL
);

ALTER TABLE Polls ALTER COLUMN id RESTART WITH 1;

INSERT INTO Polls(title, message) 
 VALUES ('Vote for your favorite band:', 
         'What is your favorite band from this top list? Click on band link to vote!');
         
INSERT INTO Polls(title, message) 
 VALUES ('Rock bands top list:', 
         'Vote for your favorite!');