<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="hr.fer.zemris.java.p12.model.*" %>

<html>
<head>
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="../css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style type="text/css">
body {
	background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                    : session.getAttribute("pickedBgCol")%>;
}
</style>
</head>
<body>
    <nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="/webapp-baza/index.html" class="brand-logo">Home</a>
		</div>
	</nav>

    <div class="container">
        <h1><%= ((PollEntry) request.getAttribute("poll")).getTitle() %></h1>
        <p><%= ((PollEntry) request.getAttribute("poll")).getMessage() %></p>
            <div class="collection">
                <c:forEach var="entry" items="${bands}">
                    <a class="collection-item" href="/webapp-baza/servleti/glasanje-glasaj?id=<%= request.getParameter("pollID") %>&title=<%= ((PollOption)pageContext.getAttribute("entry")).getOptionTitle().replaceAll("\\s", "\\+") %>">${entry.optionTitle}</a>
                </c:forEach>
            </div>
    </div>


    <!--Import jQuery before materialize.js-->
        <script type="text/javascript"
            src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/materialize.min.js"></script>
        <script type="text/javascript" src="../js/init.js"></script>
</body>
</html>
