<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page
	import="java.util.Set, hr.fer.zemris.java.webapp.voting.GlasanjeRezultatiServlet.*"%>

<%!
    /**
     * Function gets winner song. If there are multiple entires with same number of votes
     * function gets all entires with same number of votes. 
     *
     * @return top band song wrapped in html list items
     */
    @SuppressWarnings("unchecked")
    private String getWinnerSong() {
        Set<VoteResult> results = (Set<VoteResult>) getServletContext()
                .getAttribute("finalResults");
        Object[] array = results.toArray();

        StringBuilder output = new StringBuilder();

        String bandName = ((VoteResult) array[0]).getBandName();
        String bandSong = ((VoteResult) array[0]).getBandSong();
        int bandVoteCount = ((VoteResult) array[0]).getVoteCount();
        String youtubeID = bandSong.split("=")[1];

        output.append(String.format(
                "<li>%s<br><iframe width=\"80%%\" height=\"50%%\" src=\"https://www.youtube.com/embed/%s\" frameborder=\"0\" allowfullscreen></iframe></li>",
                bandName,youtubeID));

        for(int i = 1; i < array.length; i++) {
            if (((VoteResult) array[i]).getVoteCount() != bandVoteCount) break;

            bandName = ((VoteResult) array[i]).getBandName();
            bandSong = ((VoteResult) array[i]).getBandSong();
            bandVoteCount = ((VoteResult) array[i]).getVoteCount();
            youtubeID = bandSong.split("=")[1];

            output.append(String.format(
                    "<li>%s<br><iframe width=\"80%%\" height=\"50%%\"src=\"https://www.youtube.com/embed/%s\" frameborder=\"0\" allowfullscreen></iframe></li>%n",
                    bandName,youtubeID));
        }

        return output.toString();
    }%>


<html>
<head>

<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="../css/materialize.min.css"
	media="screen,projection" />

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style type="text/css">
table.rez td {
	text-align: center;
}

body {
	background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                    : session.getAttribute("pickedBgCol")%>;
}
.row {
    margin-top: 20px;
    padding-top:10px;
    padding-bottom:10px;
    text-align: center;
}

</style>
</head>
<body>
    <nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="/webapp-baza/index.html" class="brand-logo">Home</a>
		</div>
	</nav>

    <div class="container">
        <div class="row">
            <h1>Voting results</h1>
            <p>These are voting results.</p>
            <table border="1" class="rez">
                <thead>
                    <tr>
                        <th style="text-align:center;">Band</th>
                        <th style="text-align:center;">Number of votes</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="entry" items="${finalResults}">
                        <tr>
                            <td>${entry.bandName}</td>
                            <td>${entry.voteCount}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="divider"></div>
        <div class="row">
            <h2>Graphical representation of votes</h2>
            <img alt="Pie-chart" src="/webapp-baza/servleti/glasanje-grafika" width="70%" />
        </div>
        <div class="divider"></div>
        <div class="row">
            <h2>Results in XLS format</h2>
            <p>
                Results in XLS format are available <a href="/webapp-baza/servleti/glasanje-xls">here</a>
            </p>
        </div>
        <div class="divider"></div>
        <div class="row">
            <h2>Other</h2>
            <p>Example songs of winning band(s):</p>
            <ul>
                <%= getWinnerSong()%>
            </ul>
        </div>
    </div>

    <!--Import jQuery before materialize.js-->
        <script type="text/javascript"
            src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/materialize.min.js"></script>
        <script type="text/javascript" src="../js/init.js"></script>
</body>
</html>
