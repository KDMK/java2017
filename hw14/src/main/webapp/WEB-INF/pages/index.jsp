<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8">
    <title>HW-13 webpage</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"
	media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
        body {
	        background-color: <%=session.getAttribute("pickedBgCol") == null ? "white"
                : session.getAttribute("pickedBgCol")%>;
        }

        .row h1 {
	        text-align: center;
        }

        .block {
            width: 100%;
        }
        </style>
</head>
<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="/webapp-baza/index.html" class="brand-logo">Home</a>
		</div>
	</nav>

	<div class="container">
		<div class="row">
		    <br>
			<h1>HW13 welcome page</h1>
		    <br>
		</div>
	   
	    <div class="row">
	       <div class="collection">
                <c:forEach var="entry" items="${polls}">
                    <a class="collection-item" href="/webapp-baza/servleti/glasanje?pollID=${entry.id}">${entry.title}</a>
                </c:forEach>
            </div>
	    </div>
        

    </div>

		<!--Import jQuery before materialize.js-->
		<script type="text/javascript"
			src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/materialize.min.js"></script>
		<script type="text/javascript" src="../js/init.js"></script>
</body>
</html>
