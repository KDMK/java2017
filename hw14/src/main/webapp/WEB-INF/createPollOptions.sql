CREATE TABLE PollOptions
(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
optionTitle VARCHAR(100) NOT NULL,
optionLink VARCHAR(150) NOT NULL,
pollID BIGINT,
votesCount BIGINT,
FOREIGN KEY (pollID) REFERENCES Polls(id)
);

ALTER TABLE PollOptions ALTER COLUMN id RESTART WITH 1;

INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Beatles', 'https://www.youtube.com/watch?v=z9ypq6_5bsg',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Platters', 'https://www.youtube.com/watch?v=H2di83WAOhU',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Beach boys', 'https://www.youtube.com/watch?v=2s4slliAtQU',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Four Seasons', 'https://www.youtube.com/watch?v=y8yvnqHmFds',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Marcels', 'https://www.youtube.com/watch?v=qoi3TH59ZEs',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Everly Brothers', 'https://www.youtube.com/watch?v=tbU3zdAgiX8',
          1, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('The Mamas And the Papas', 'https://www.youtube.com/watch?v=N-aK6JnyFmk',
          1, 0);
          
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Led Zeppelin', 'https://www.youtube.com/watch?v=HQmmM_qwG4k',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Journey', 'https://www.youtube.com/watch?v=PBEXSiFzOfU',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Sabaton', 'https://www.youtube.com/watch?v=iLTpKs2ylP4',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('AC/DC', 'https://www.youtube.com/watch?v=gEPmA3USJdI',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Iron Maiden', 'https://www.youtube.com/watch?v=NGN9_kYs88Q',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Metallica', 'https://www.youtube.com/watch?v=OIh3nO6-V_A',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Black Sabbath', 'https://www.youtube.com/watch?v=LQUXuQ6Zd9w',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Motorhead', 'https://www.youtube.com/watch?v=3VNUyjRRjxM',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Blue oyster cult', 'https://www.youtube.com/watch?v=ClQcUyhoxTg',
          2, 0);
INSERT INTO PollOptions(optionTitle, optionLink, pollID, votesCount)
  VALUES ('Lynyrd Skynyrd', 'https://www.youtube.com/watch?v=ye5BuYf8q4o',
          2, 0);