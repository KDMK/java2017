package hr.fer.zemris.java.webapp.voting;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.webapp.voting.GlasanjeRezultatiServlet.VoteResult;

/**
 * This servlet generates xls with voting results. When xls is generated user is
 * prompted to save it. If any error occurs user is redirected to error page.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeXlsServlet extends HttpServlet {
    /**
     * Default serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        @SuppressWarnings("unchecked")
        Set<VoteResult> results = (Set<VoteResult>) getServletContext()
                .getAttribute("finalResults");
        try {
            String fileName = "voting-results.xls";
            @SuppressWarnings("resource")
            HSSFWorkbook hwb = new HSSFWorkbook();

            HSSFSheet sheet = hwb.createSheet("results");

            int i = 0;
            for (VoteResult vote : results) {
                HSSFRow row = sheet.createRow(i++);
                row.createCell(0).setCellValue(vote.getBandName());
                row.createCell(1).setCellValue(vote.getVoteCount());
                row.createCell(2).setCellValue(vote.getBandSong());
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            hwb.write(bos);
            byte[] outArray = bos.toByteArray();

            resp.setContentType("application/ms-excel");
            resp.setContentLength(outArray.length);
            resp.setHeader("Expires:", "0");
            resp.setHeader("Content-Disposition",
                    "attachment; filename=" + fileName);
            OutputStream outStream = resp.getOutputStream();
            outStream.write(outArray);
            outStream.flush();

            return;
        } catch (Exception ex) {
            req.getSession().setAttribute("errorMsg",
                    "Error: cannot create xls.");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }
    }
}
