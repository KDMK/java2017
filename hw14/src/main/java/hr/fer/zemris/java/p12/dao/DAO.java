package hr.fer.zemris.java.p12.dao;

import java.util.List;

import hr.fer.zemris.java.p12.model.PollEntry;
import hr.fer.zemris.java.p12.model.PollOption;

/**
 * Interface that connects data storage layer and presentation layer.
 * 
 * @author Matija Bartolac
 *
 */
public interface DAO {
    /**
     * Grabs all existing poll entries from database.
     * 
     * @return list of all polls
     * @throws DAOException
     *             in case of error
     */
    public List<PollEntry> getAllPolls() throws DAOException;

    /**
     * Grabs poll entry with given id. If entry does not exists it return null
     * <code>null</code>.
     * 
     * @param pollId
     *            id of requested poll
     * @return Poll entry with given id
     * @throws DAOException
     *             in case of error
     */
    public PollEntry getPoll(long pollId) throws DAOException;

    /**
     * Grabs all poll options for given pollId. If such does not exists it
     * returns null;
     * 
     * @param pollId
     *            id of requested poll
     * @return List of all Poll options for given pollId
     * @throws DAOException
     *             in case of error
     */
    public List<PollOption> getAllPollOptions(long pollId) throws DAOException;

    /**
     * Grabs poll entry with given id. If entry does not exists it return null
     * <code>null</code>.
     * 
     * @param entryTitle
     *            title of requested poll
     * @param id
     *            of requested poll Option
     * @return Poll entry with given id
     * @throws DAOException
     *             in case of error
     */
    public PollOption getPollOption(String entryTitle, long id)
            throws DAOException;

    /**
     * Grabs poll entry with given id and register new vote.
     * 
     * @param entryTitle
     *            title of requested poll
     * @param id
     *            of requested poll Option
     * @return Poll entry with given id
     * @throws DAOException
     *             in case of error
     */
    public PollOption registerVote(String entryTitle, long id)
            throws DAOException;
}