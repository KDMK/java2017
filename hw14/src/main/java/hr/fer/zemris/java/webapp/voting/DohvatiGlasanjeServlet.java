package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollEntry;

/**
 * This servlet is used to load all entries that participates in voting. After
 * bands are loaded they are passed as session parameters to other pages to
 * render.
 * 
 * @author Matija Bartolac
 *
 */
public class DohvatiGlasanjeServlet extends HttpServlet {
    /**
     * Default serial version id.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        List<PollEntry> polls = DAOProvider.getDao().getAllPolls();
        
        if (polls == null) {
            req.setAttribute("errorMsg",
                    "There is currently error with voting system. Sorry for inconvenience. Please try again later");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        req.setAttribute("polls", polls);
        req.getRequestDispatcher("/WEB-INF/pages/index.jsp")
                .forward(req, resp);
    }
}
