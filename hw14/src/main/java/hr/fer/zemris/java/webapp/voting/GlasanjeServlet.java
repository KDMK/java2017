package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollEntry;
import hr.fer.zemris.java.p12.model.PollOption;

/**
 * This servlet is used to load all entries that participates in voting. After
 * bands are loaded they are passed as session parameters to other pages to
 * render.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeServlet extends HttpServlet {
    /**
     * Default serial version id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Model of a band entry that participates in voting process.
     * 
     * @author Matija Bartolac
     *
     */
    public static class BandEntry {
        /**
         * Band id.
         */
        private String id;
        /**
         * Name of the band.
         */
        private String bandName;
        /**
         * Link to the band song.
         */
        private String songLink;

        /**
         * Default constructor.
         * 
         * @param id
         *            of song
         * @param bandName
         *            the name of the band
         * @param songLink
         *            link to the band song
         */
        public BandEntry(String id, String bandName, String songLink) {
            this.id = id;
            this.bandName = bandName;
            this.songLink = songLink;
        }

        /**
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * @return the bandName
         */
        public String getBandName() {
            return bandName;
        }

        /**
         * @return the songLink
         */
        public String getSongLink() {
            return songLink;
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long id = Long.valueOf(req.getParameter("pollID"));
        
        PollEntry poll = DAOProvider.getDao().getPoll(1);
        List<PollOption> bands = DAOProvider.getDao().getAllPollOptions(id);
        
        if (bands == null) {
            req.setAttribute("errorMsg",
                    "There is currently error with voting system. Sorry for inconvenience. Please try again later");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        req.setAttribute("bands", bands);
        req.setAttribute("poll", poll);
        getServletContext().setAttribute("poll", poll);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp")
                .forward(req, resp);
    }
}
