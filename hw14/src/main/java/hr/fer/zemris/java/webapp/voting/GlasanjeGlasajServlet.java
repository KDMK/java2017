package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollOption;

/**
 * Servlet processes user vote and updates votes in file. After updating is
 * finished user is redirected to results page.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {
    /**
     * Default serial version uid
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String title = req.getParameter("title");
        long id = Long.valueOf(req.getParameter("id"));

        DAOProvider.getDao().registerVote(title, id);
        List<PollOption> votes = DAOProvider.getDao().getAllPollOptions(id);

        getServletContext().setAttribute("votes", votes);
        resp.sendRedirect(req.getContextPath() + "/servleti/glasanje-rezultati");
    }
}
