package hr.fer.zemris.java.p12;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import com.mchange.v2.log.log4j.Log4jMLog;

/**
 * This class models web listener that is used to initialize all parameters that
 * are required in order to communicate with database.
 * 
 * @author Matija Bartolac
 *
 */
@WebListener
public class Initialization implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Properties dbProperties = new Properties();
        String uri = sce.getServletContext()
                .getRealPath("WEB-INF/dbsettings.properties");
        try {
            dbProperties.load(Files.newInputStream(Paths.get(uri),
                    StandardOpenOption.READ));
        } catch (IOException e) {
            System.out.println("Cannot open " + uri);
            System.exit(-1);
        }

        String connectionURL = String.format(
                "jdbc:derby://%s:%s/%s;user=%s;password=%s",
                dbProperties.getProperty("host"),
                dbProperties.getProperty("port"),
                dbProperties.getProperty("name"),
                dbProperties.getProperty("user"),
                dbProperties.getProperty("password"));
        
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
        } catch (PropertyVetoException e1) {
            throw new RuntimeException(
                    "Error while initializing database pool.", e1);
        }
        cpds.setJdbcUrl(connectionURL);
        sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);

        try (Connection conn = cpds.getConnection()) {
            checkTable("Polls", conn, sce.getServletContext()
                    .getRealPath("WEB-INF/createPolls.sql"));
            checkTable("PollOptions", conn, sce.getServletContext()
                    .getRealPath("WEB-INF/createPollOptions.sql"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }

    /**
     * Checks if given table exists in database. If not it creates it. Method
     * requires input path to .sql file that contains definition of table.
     * 
     * @param tablename
     *            name of table
     * @param conn
     *            connection to database
     * @param fileName
     *            path to file that contains .sql for creating database
     * @return true if table exists, false otherwise
     * @throws SQLException
     *             if there was an error performing operations on table
     * 
     */
    public boolean checkTable(String tablename, Connection conn,
            String fileName) throws SQLException {
        if (conn != null) {
            DatabaseMetaData dbmd = conn.getMetaData();
            ResultSet rs = dbmd.getTables(null, null, tablename.toUpperCase(),
                    null);
            if (rs.next()) {
                Log4jMLog.info("Table " + rs.getString("TABLE_NAME")
                        + " already exists !!");
            } else {
                try (Scanner sc = new Scanner(new File(fileName))) {
                    while (sc.useDelimiter(";").hasNext()) {
                        String curStatement = sc.useDelimiter(";").next();
                        PreparedStatement statement = conn
                                .prepareStatement(curStatement);
                        statement.execute();
                    }
                } catch (IOException | SQLException e) {
                    Log4jMLog.info("Cannot create table " + tablename);
                    Log4jMLog.info(e.getMessage());
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ComboPooledDataSource cpds = (ComboPooledDataSource) sce
                .getServletContext().getAttribute("hr.fer.zemris.dbpool");
        if (cpds != null) {
            try {
                DataSources.destroy(cpds);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}