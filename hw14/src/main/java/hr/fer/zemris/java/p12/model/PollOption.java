package hr.fer.zemris.java.p12.model;

/**
 * Model of poll option.
 * 
 * @author Matija Bartolac
 *
 */
public class PollOption {
    /**
     * Id of poll option
     */
    private long id;
    /**
     * Title of poll option
     */
    private String optionTitle;
    /**
     * Link to band song
     */
    private String optionLink;
    /**
     * Id of poll that contains this poll option.
     */
    private long pollID;
    /**
     * Number of votes
     */
    private int votesCount;

    /**
     * Default constructor.
     */
    public PollOption() {
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the optionTitle
     */
    public String getOptionTitle() {
        return optionTitle;
    }

    /**
     * @param optionTitle
     *            the optionTitle to set
     */
    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }

    /**
     * @return the optionLink
     */
    public String getOptionLink() {
        return optionLink;
    }

    /**
     * @param optionLink
     *            the optionLink to set
     */
    public void setOptionLink(String optionLink) {
        this.optionLink = optionLink;
    }

    /**
     * @return the pollID
     */
    public long getPollID() {
        return pollID;
    }

    /**
     * @param pollID
     *            the pollID to set
     */
    public void setPollID(long pollID) {
        this.pollID = pollID;
    }

    /**
     * @return the votesCount
     */
    public int getVotesCount() {
        return votesCount;
    }

    /**
     * @param votesCount
     *            the votesCount to set
     */
    public void setVotesCount(int votesCount) {
        this.votesCount = votesCount;
    }

}
