package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet is used to load all entries that participates in voting. After
 * bands are loaded they are passed as session parameters to other pages to
 * render.
 * 
 * @author Matija Bartolac
 *
 */
public class RedirectServlet extends HttpServlet {
    /**
     * Default serial version id.
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath() + "/servleti/index.html");
    }
}
