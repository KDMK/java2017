package hr.fer.zemris.java.p12.dao;

/**
 * Exception that can be thrown while using DAO object.
 * 
 * @author Matija Bartolac
 *
 */
@SuppressWarnings("javadoc")
// Metoda iz upute, valjda ne treba pisati javadoc na ovo :)
public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DAOException() {
	}

	public DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}
}