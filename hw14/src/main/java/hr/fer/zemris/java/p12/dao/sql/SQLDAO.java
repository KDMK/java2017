package hr.fer.zemris.java.p12.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOException;
import hr.fer.zemris.java.p12.model.PollEntry;
import hr.fer.zemris.java.p12.model.PollOption;

/**
 * SQLDAO implemntes subsystem DAO using SQL. DAO provides user way to
 * communicate with DB. This implementation gets connections to database using
 * {@link SQLConnectionProvider}.
 * 
 * @author Matija Bartolac
 */
public class SQLDAO implements DAO {

    @Override
    public List<PollEntry> getAllPolls() throws DAOException {
        List<PollEntry> entries = new ArrayList<>();
        Connection con = SQLConnectionProvider.getConnection();
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement(
                    "SELECT id, title FROM Polls ORDER BY id");
            try {
                ResultSet rs = pst.executeQuery();
                try {
                    while (rs != null && rs.next()) {
                        PollEntry entry = new PollEntry();
                        entry.setId(rs.getLong(1));
                        entry.setTitle(rs.getString(2));
                        entries.add(entry);
                    }
                } finally {
                    try {
                        rs.close();
                    } catch (Exception ignorable) {
                    }
                }
            } finally {
                try {
                    pst.close();
                } catch (Exception ignorable) {
                }
            }
        } catch (Exception ex) {
            throw new DAOException("Error fetching all polls.", ex);
        }
        return entries;
    }

    @Override
    public PollEntry getPoll(long id) throws DAOException {
        PollEntry entry = null;
        Connection con = SQLConnectionProvider.getConnection();
        PreparedStatement pst = null;
        try {
            pst = con
                    .prepareStatement("SELECT id, title, message FROM Polls WHERE id=?");
            pst.setLong(1, Long.valueOf(id));
            try {
                ResultSet rs = pst.executeQuery();
                try {
                    if (rs != null && rs.next()) {
                        entry = new PollEntry();
                        entry.setId(rs.getLong(1));
                        entry.setTitle(rs.getString(2));
                        entry.setMessage(rs.getString(3));
                    }
                } finally {
                    try {
                        rs.close();
                    } catch (Exception ignorable) {
                    }
                }
            } finally {
                try {
                    pst.close();
                } catch (Exception ignorable) {
                }
            }
        } catch (Exception ex) {
            throw new DAOException(
                    "Error getting poll entry. " + ex.getMessage(), ex);
        }
        return entry;
    }

    @Override
    public List<PollOption> getAllPollOptions(long id) throws DAOException {
        List<PollOption> pollOptions = new ArrayList<>();
        Connection con = SQLConnectionProvider.getConnection();
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement(
                    "SELECT id, optionTitle, optionLink, pollID, votesCount FROM PollOptions WHERE pollID=?");
            pst.setLong(1, Long.valueOf(id));
            try {
                ResultSet rs = pst.executeQuery();
                try {
                    while (rs != null && rs.next()) {
                        PollOption option = new PollOption();
                        option.setId(rs.getLong(1));
                        option.setOptionTitle(rs.getString(2));
                        option.setOptionLink(rs.getString(3));
                        option.setPollID(rs.getLong(4));
                        option.setVotesCount(rs.getInt(5));
                        pollOptions.add(option);
                    }
                } finally {
                    try {
                        rs.close();
                    } catch (Exception ignorable) {
                    }
                }
            } finally {
                try {
                    pst.close();
                } catch (Exception ignorable) {
                }
            }
        } catch (Exception ex) {
            throw new DAOException("Error getting poll opitons list.", ex);
        }
        return pollOptions;
    }

    @Override
    public PollOption getPollOption(String entryTitle, long id)
            throws DAOException {
        PollOption option = null;
        Connection con = SQLConnectionProvider.getConnection();
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement(
                    "SELECT id, optionTitle, optionLink, pollID, votesCount FROM PollOptions WHERE optionTitle=? AND id=?");
            pst.setString(1, entryTitle);
            pst.setLong(2, Long.valueOf(id));
            try {
                ResultSet rs = pst.executeQuery();
                try {
                    if (rs != null && rs.next()) {
                        option = new PollOption();
                        option.setId(rs.getLong(1));
                        option.setOptionTitle(rs.getString(2));
                        option.setOptionLink(rs.getString(3));
                        option.setPollID(rs.getLong(4));
                        option.setVotesCount(rs.getInt(5));
                    }
                } finally {
                    try {
                        rs.close();
                    } catch (Exception ignorable) {
                    }
                }
            } finally {
                try {
                    pst.close();
                } catch (Exception ignorable) {
                }
            }
        } catch (Exception ex) {
            throw new DAOException("Error getting poll option with title "
                    + String.valueOf(entryTitle), ex);
        }
        return option;
    }

    @Override
    public PollOption registerVote(String entryTitle, long id)
            throws DAOException {
        PollOption option = null;
        Connection con = SQLConnectionProvider.getConnection();
        PreparedStatement pst = null;
        
        System.out.println(entryTitle);
        System.out.println(id);
        try {
            pst = con.prepareStatement(
                    "UPDATE PollOptions SET votesCount=votesCount+1 WHERE optionTitle=? AND pollID=?");
            pst.setString(1, entryTitle);
            pst.setLong(2, Long.valueOf(id));

            int numberOfAffectedRows = pst.executeUpdate();
            System.out
                    .println("Number of updated rows: " + numberOfAffectedRows);

        } catch (Exception ex) {
            throw new DAOException("Error fetching table from database.", ex);
        } finally {
            try {
                pst.close();
            } catch (Exception ignorable) {
            }
        }
        return option;
    }
}