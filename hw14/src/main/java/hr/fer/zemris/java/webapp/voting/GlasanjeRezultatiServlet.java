package hr.fer.zemris.java.webapp.voting;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.p12.model.PollOption;

/**
 * This servlet generates data that is used to render voting results.
 * 
 * @author Matija Bartolac
 *
 */
public class GlasanjeRezultatiServlet extends HttpServlet {
    /**
     * Default serial version uid
     */
    private static final long serialVersionUID = 1L;

    /**
     * Model of single voting result entry
     * 
     * @author Matija Bartolac
     *
     */
    public static class VoteResult implements Comparable<VoteResult> {
        /**
         * Name of the band.
         */
        private String bandName;
        /**
         * Number of votes.
         */
        private int voteCount;
        /**
         * Link to the band song.
         */
        private String bandSong;

        /**
         * Default constructor.
         * 
         * @param bandName
         *            name of the band
         * @param voteCount
         *            number of votes
         * @param bandSong
         *            link to the band song
         */
        public VoteResult(String bandName, int voteCount, String bandSong) {
            super();
            this.bandName = bandName;
            this.voteCount = voteCount;
            this.bandSong = bandSong;
        }

        /**
         * @return the bandName
         */
        public String getBandName() {
            return bandName;
        }

        /**
         * @return the voteCount
         */
        public int getVoteCount() {
            return voteCount;
        }

        /**
         * @return the voteCount
         */
        public String getBandSong() {
            return bandSong;
        }

        @Override
        public int compareTo(VoteResult o) {
            if (this.voteCount < o.voteCount) return 1;
            if (this.voteCount > o.voteCount) return -1;
            return 1;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        @SuppressWarnings("unchecked")
        List<PollOption> bands = (List<PollOption>) getServletContext()
                .getAttribute("votes");
        
        if (bands == null) {
            req.setAttribute("errorMsg",
                    "There is currently error with voting system. Sorry for inconvenience. Please try again later");
            req.getRequestDispatcher("WEB-INF/error.jsp").forward(req, resp);
            return;
        }

        Set<VoteResult> finalResults = new TreeSet<>();
        for (PollOption band : bands) {
            String title = band.getOptionTitle();
            String link = band.getOptionLink();
            int votes = band.getVotesCount();

            finalResults.add(new VoteResult(title, votes, link));
        }

        req.getServletContext().setAttribute("finalResults", finalResults);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req,
                resp);
    }
}
