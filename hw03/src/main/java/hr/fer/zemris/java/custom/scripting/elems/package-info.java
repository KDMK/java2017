/**
 * Elements package contains element types used to build expressions.
 * Expressions are valid variable/constant types in language for which we are
 * building parser.
 *
 * @author Matija Bartolac
 *
 */
package hr.fer.zemris.java.custom.scripting.elems;
