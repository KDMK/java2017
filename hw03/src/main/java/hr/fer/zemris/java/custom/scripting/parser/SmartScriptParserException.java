package hr.fer.zemris.java.custom.scripting.parser;

/**
 * SmartScriptParser exception class models exception that can occur in
 * {@link hr.fer.zemris.java.custom.scripting.parser Parser} and it's specific
 * to parsing.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class SmartScriptParserException extends RuntimeException {
    /**
     * Serializable class version number.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Does not take any arguments.
     */
    public SmartScriptParserException() {
        super();
    }

    /**
     * Constructor that accepts message.
     *
     * @param message
     *            Error message that will be displayed.
     */
    public SmartScriptParserException(String message) {
        super(message);
    }

    /**
     * Constructor that creates exception using cause object.
     *
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public SmartScriptParserException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor that creates exception using cause and string.
     *
     * @param message
     *            Error message that will be displayed.
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public SmartScriptParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
