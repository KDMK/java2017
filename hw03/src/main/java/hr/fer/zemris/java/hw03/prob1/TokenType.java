package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration of token types present in language for which we are building
 * lexer.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public enum TokenType {
    /**
     * Symbols end of file. There are no more tokens further.
     */
    EOF,
    /**
     * Every sequence of characters(one or more) upon which method
     * <code>Character.isLetter(char)</code> returns <code>true</code>.
     */
    WORD,
    /**
     * Every sequence of digits(one or more) that can be represented as
     * <code>Long</code> type.
     */
    NUMBER,
    /**
     * Every single character that is left when we remove words, numbers, and
     * white spaces.
     */
    SYMBOL;
}
