package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models string expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementString extends Element {
    /**
     * Value of string element.
     */
    private String value;

    /**
     * Default constructor. Creates new string element with given name.
     *
     * @param value
     *            of variable.
     */
    public ElementString(String value) {
        this.value = value;
    }

    @Override
    public String asText() {
        return String.format("\"%s\"", value);
    }

    /**
     * Returns value of this string element.
     *
     * @return <code>String</code> value.
     */
    public String getValue() {
        return value;
    }
}
