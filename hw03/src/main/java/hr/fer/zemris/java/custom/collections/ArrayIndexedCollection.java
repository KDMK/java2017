package hr.fer.zemris.java.custom.collections;

/**
 * Class implements resizable array-backed collection of objects. It extends
 * {@link Collection} class. Duplicate elements are allowed and storage of null
 * elements is not. You can create collection with predefined size, you can
 * specify initial size of collection or you can create new collection from
 * elements of some other collection.
 *
 * @author Matija Bartolac
 * @version 1.0
 *
 */
public class ArrayIndexedCollection extends Collection {
    /**
     * Current size of collection (number of elements actually stored).
     */
    private int size;
    /**
     * Current capacity of allocated array of object references.
     */
    private int capacity;
    /**
     * An array of object references which length is determined by capacity
     * variable.
     */
    private Object[] elements;
    /**
     * Default initial capacity of array collection. Used if user didn't provide
     * initial array capacity.
     */
    private static final int INIT_CAPACITY = 16;

    /**
     * Constructor for creating collection of given initial capacity. If initial
     * capacity is less than one IllegalArgumentException is thrown.
     *
     * @param initialCapacity
     *            Size of collection that we want to create.
     */
    public ArrayIndexedCollection(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException(
                    "Size of array can't be less than one.");
        }
        this.size = 0;
        this.capacity = initialCapacity;
        this.elements = new Object[capacity];
    }

    /**
     * Default constructor. It takes no arguments. Default size used for
     * creating array(if user did not specify) is defined by INIT_CAPACITY.
     */
    public ArrayIndexedCollection() {
        this(INIT_CAPACITY);
    }

    /**
     * Constructor for creating an array from existing array.
     *
     * @param other
     *            Reference to an array from which we are creating new
     *            collection.
     */
    public ArrayIndexedCollection(ArrayIndexedCollection other) {
        this(other.size);
        this.addAll(other);
    }

    /**
     * Constructor for creating an array from existing array with specifying
     * initial size. Use it if you want to crate array that is initially larger
     * than array from whose elements you want to create array. If you want to
     * create exact same array(copy of other) use of constructor that takes only
     * other array reference is recommended.
     *
     * @param other
     *            Reference to array from which we are creating new collection.
     * @param initialCapacity
     *            Initial capacity of array that we are creating.
     */
    public ArrayIndexedCollection(ArrayIndexedCollection other,
            int initialCapacity) {
        this(initialCapacity);
        addAll(other);
    }

    // Helper methods
    /**
     * Resizes collection to double capacity.
     */
    private void resizeArray() {
        // Resize capacity and create new, larger array
        capacity *= 2;
        elements = java.util.Arrays.copyOf(elements, capacity);
    }

    /**
     * Checks if Object value is same class as the objects already stored in
     * elements array. It will also return true if array is empty, because then
     * it's irrelevant which type is elements in array.
     *
     * @param value
     *            Reference to an object.
     * @return True if value is the same class as the elements in array or array
     *         is empty.
     */
    // NOT USED IN THIS EXAMPLE
    @SuppressWarnings("unused")
    private boolean isNotSameClass(Object value) {
        return (size != 0 && !elements[0].getClass().isInstance(value));
    }

    // First empty methods
    // Average complexity of this method is O(1).
    /**
     * Method throws IllegalArgumentException if user tries to add null.
     *
     * @see hr.fer.zemris.java.custom.collections.Collection#add(java.lang.Object)
     */
    @Override
    public void add(Object value) {
        if (value == null) {
            throw new IllegalArgumentException("Cannot add null!");
        }

        // Check if the value matches the class of elements that are already in
        // collection -- NOT CHECKING HERE --
        /*
         * if (isNotSameClass(value)) { throw new IllegalArgumentException(
         * "Value does not match type of stored elements!"); }
         */

        if (size == capacity) {
            resizeArray();
        }

        elements[size] = value;
        size++;
    }

    @Override
    public void forEach(Processor processor) {
        if (processor == null) {
            throw new IllegalArgumentException("Processor can't be null!");
        }
        for (Object element : elements) {
            if (element == null) {
                break;
            }
            processor.process(element);
        }
    }

    @Override
    public void clear() {
        // Set size to 0, put all objects to null, then allocate new memory for
        // new empty array
        size = 0;
        java.util.Arrays.fill(elements, null);
    }

    /**
     * Returns the object that is stored in backing array at position index.
     *
     * @param index
     *            Index of an element we want to get.
     * @return Element of collection at given index.
     */
    // Average complexity is O(1)
    public Object get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds.");
        }
        return elements[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray() {
        return java.util.Arrays.copyOfRange(elements, 0, size);
    }

    @Override
    public boolean contains(Object value) {
        if (indexOf(value) == -1) {
            return false;
        }
        return true;
    }

    /**
     * Insert(does not overwrite) the given value at the given position in
     * array.
     *
     * @param value
     *            Value of element that we want to insert in array.
     * @param position
     *            Position where we want to insert element.
     */
    // Average complexity of this method is O(n)
    public void insert(Object value, int position) {
        if (value == null) {
            throw new IllegalArgumentException("Cannot add null!");
        }

        // Call add method if array is empty and user tries to add element to
        // index zero.
        if (size == 0 && position == 0) {
            add(value);
            return;
        }

        if (position < 0 || position >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds!");
        }

        // Check if the value matches the class of elements that are already in
        // collection -- NOT CHECKING HERE --
        // if (isNotSameClass(value)) {
        // throw new IllegalArgumentException(
        // "Value does not match type of stored elements!");
        // }

        // Check array is already full. If true, rezize array.
        if (size == capacity) {
            resizeArray();
        }

        // "Shift" elements in array one place to the right, and place element
        // in desired position
        System.arraycopy(elements, position, elements, position + 1,
                (size - position));
        elements[position] = value;
        size++;
    }

    /**
     * Searches the collection and returns the index of the first occurrence of
     * the given value or -1 if the value is not found. If called with null will
     * throw illegal argument exception.
     *
     * @param value
     *            Value of object whose index we are looking for.
     * @return Index of found object or -1 if not found.
     */
    public int indexOf(Object value) {
        if (value == null) {
            throw new IllegalArgumentException("Null can't be in array.");
        }
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Removes element at specified index from collection. Legal indexes are 0
     * to size-1. If index is out of bounds method throws indexOutOfBounds
     * exception.
     *
     * @param index
     *            Index of an element that we want to remove.
     */
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index is out of bounds!");
        }

        System.arraycopy(elements, index + 1, elements, index,
                (size - index) - 1);
        elements[size - 1] = null;
        size--;
    }

    @Override
    public boolean remove(Object value) {
        int index = indexOf(value);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }
}
