/**
 * Package contains usage example of SmartScriptParser.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
package hr.fer.zemris.java.hw03;
