/**
 * Parser for structured document format described in homework assignment.
 *
 * @author Matija Bartolac
 *
 */
package hr.fer.zemris.java.custom.scripting.parser;
