package hr.fer.zemris.java.hw03.prob1;

/**
 * Lexer program is used to build tokens from given input text. TokenTypes for
 * this lexer are defined in @see TokenType enumeration. Language that we are
 * building lexer for consists of numbers, strings and symbols. Lexer stores
 * current token and has methods for generating new tokens.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class Lexer {
    /**
     * Defines rule set upon which lexer generates tokens.
     */
    private LexerState state;
    /**
     * Array of input text characters for which we are generating tokens.
     */
    private char[] data;
    /**
     * Current token.
     */
    private Token token;
    /**
     * Index of next character for processing.
     */
    private int currentIndex;
    /**
     * Number of digits that largest long value contains.
     */
    private static final int MAX_LONG_DIGITS = 19;

    /**
     * Default constructor for constructing lexer.
     *
     * @param text Input text for which we are generating tokens.
     * @throws IllegalArgumentException If user passes null reference.
     */
    public Lexer(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }
        this.data = text.toCharArray();
        this.currentIndex = 0;
        this.state = LexerState.BASIC;
    }

    /**
     * Generates and return next token from input text.
     *
     * @return Currently generated token
     */
    public Token nextToken() {
        // Check if current token is end of file.
        if (token != null && token.getType() == TokenType.EOF) {
            throw new LexerException("No more tokens in input file.");
        }

        if (state == LexerState.BASIC) {
            return generateTokenBasic();
        } else {
            return generateTokenExtended();
        }
    }

    /**
     * Checks whether next character in input sequence is escaped character.
     *
     * @return <code>true</code> if next character in data array is escaped
     * character.
     */
    private boolean isEscapedCharacter() {
        // First check if you are in the end of input sequence. Then check if
        // next character is valid escape sequence.
        return (currentIndex < data.length - 1)
                && (Character.isDigit(nextChar()) || nextChar() == '\\');
    }

    /**
     * Skips blanks in character sequence. Characters that are interpreted as
     * blanks are: <code>' ', '\n', '\r', '\n'</code>
     */
    private void skipBlanks() {
        while (currentIndex < data.length) {
            if (isBlank(data[currentIndex])) {
                currentIndex++;
                continue;
            }
            return;
        }
    }

    /**
     * Checks if current character is blank character.
     *
     * @param curCharacter <code>char</code> value that is checked
     * @return <code>true</code> if next character is blank character
     */
    private boolean isBlank(char curCharacter) {
        return data[currentIndex] == ' ' || data[currentIndex] == '\r'
                || data[currentIndex] == '\n' || data[currentIndex] == '\t';
    }

    /**
     * Determines if current word token is matched fully. Word token ends if
     * next character is blank space, end of input sequence or start of some
     * other token.
     *
     * @return <code>true</code> if token is matched fully
     */
    private boolean wordTokenEnd() {
        return currentIndex == data.length || isBlank(data[currentIndex])
                || (!Character.isLetter(data[currentIndex])
                && data[currentIndex] != '\\');
    }

    /**
     * Determines if current number token is matched fully. Number token ends if
     * next character is blank space, end of input sequence or start of some
     * other token.
     *
     * @return <code>true</code> if token is matched fully
     */
    private boolean numberTokenEnd() {
        return (currentIndex == data.length || isBlank(data[currentIndex]))
                || !Character.isDigit(data[currentIndex]);
    }

    /**
     * Returns next character in input array. It does not modify currentIndex.
     *
     * @return Next character.
     */
    private char nextChar() {
        return data[currentIndex + 1];
    }

    /**
     * Returns last generated token. This method does not generate next token.
     *
     * @return Last generated token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Sets lexer operation state.
     *
     * @param state Desired state
     * @see LexerState
     */
    public void setState(LexerState state) {
        if (state == null) {
            throw new IllegalArgumentException("State can't be null.");
        }
        this.state = state;
    }

    /**
     * Generates next token using basic rules.
     *
     * @return Next generated token
     */
    private Token generateTokenBasic() {
        String curInputToken = "";

        while (currentIndex <= data.length) {
            skipBlanks();
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (isEOFToken(curInputToken)) {
                token = new Token(TokenType.EOF, null);
                return token;
            }

            // If next character is letter consume it.
            if (isNextCharLetter()) {
                curInputToken = getNextLetter(curInputToken);

                // If next character is followed by blank space or there is no
                // characters after it construct Word token. Else continue
                // consuming characters.
                if (wordTokenEnd()) {
                    return newWordToken(curInputToken);
                }
                continue;
            }

            // If next character is digit consume it.
            if (Character.isDigit(data[currentIndex])) {
                curInputToken = curInputToken
                        .concat(Character.toString(data[currentIndex++]));

                // Check if current input can fit in Long type variable. Parsing
                // is not necessary if number has less than 18 digits - largest
                // number that can be stored is 9223372036854775807
                if (curInputToken.length() >= MAX_LONG_DIGITS) {
                    try {
                        Long.parseLong(curInputToken);
                    } catch (NumberFormatException e) {
                        throw new LexerException(
                                "Can't store number in variable of type Long");
                    }
                }

                if (numberTokenEnd()) {
                    return newNumberToken(curInputToken);
                }
                continue;
            }

            // If next character is not a number or a string, interpret it as a
            // symbol.
            return newSymbolToken(data[currentIndex++]);
        }

        return null;
    }

    private boolean isNextCharLetter() {
        return Character.isLetter(data[currentIndex])
                || data[currentIndex] == '\\';
    }

    private boolean isEOFToken(String curInputToken) {
        return currentIndex >= data.length && curInputToken.equals("");
    }

    private String getNextLetter(String curInputToken) {
        // Check if next character is escape character. If not valid
        // escape character throw exception.
        if (data[currentIndex] == '\\') {
            if (!isEscapedCharacter()) {
                throw new LexerException("Invalid escape sequence!");
            }

            currentIndex++;
        }

        curInputToken = curInputToken
                .concat(Character.toString(data[currentIndex++]));
        return curInputToken;
    }

    /**
     * Generates new token using extended rules.
     *
     * @return Next generated token.
     */
    private Token generateTokenExtended() {
        String curInputToken = "";

        while (currentIndex <= data.length) {
            skipBlanks();
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (isEOFToken(curInputToken)) {
                token = new Token(TokenType.EOF, null);
                return token;
            }

            curInputToken = curInputToken
                    .concat(Character.toString(data[currentIndex++]));

            // If consumed symbol was # return symbol
            if (curInputToken.equals("#")) {
                return newSymbolToken(curInputToken.charAt(0));
            }

            // If next symbol is # return matched string
            if (currentIndex == data.length - 1 || isBlank(data[currentIndex])
                    || data[currentIndex] == '#') {
                return newWordToken(curInputToken);
            }
        }

        return null;
    }

    /**
     * Creates new symbol token, and sets current token to that token.
     *
     * @param value Value of token that we are creating.
     * @return New <code>TokenType.SYMBOL</code> token.
     */
    private Token newSymbolToken(char value) {
        token = new Token(TokenType.SYMBOL, value);
        return token;
    }

    /**
     * Creates new word token, and sets current token to that token.
     *
     * @param value Value of word that we are creating.
     * @return New <code>TokenType.WORD</code> token
     */
    private Token newWordToken(String value) {
        token = new Token(TokenType.WORD, value);
        return token;
    }

    /**
     * Creates new number token, and sets current token to that token.
     *
     * @param value Value of number that we are creating.
     * @return New <code>TokenType.NUMBER</code> token
     */
    private Token newNumberToken(String value) {
        token = new Token(TokenType.NUMBER, Long.parseLong(value));
        return token;
    }
}
