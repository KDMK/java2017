package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration defines lexer operation states.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public enum LexerState {
    /**
     * Rule set for processing text.
     */
    TEXT,
    /**
     * Rule set for processing tags.
     */
    TAG
}
