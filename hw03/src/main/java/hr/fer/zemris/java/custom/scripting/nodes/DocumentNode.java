package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Node that represents an entire document.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class DocumentNode extends Node {
}
