package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * Node that represent a single for-loop construct.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ForLoopNode extends Node {
    /**
     * Variable name that is used as iterator in for loop.
     */
    private ElementVariable variable;
    /**
     * Start point of for loop.
     */
    private Element startExpression;
    /**
     * End point of for loop.
     */
    private Element endExpression;
    /**
     * Increment step. Cannot be null.
     */
    private Element stepExpression;

    /**
     * @return the variable
     */
    public ElementVariable getVariable() {
        return variable;
    }

    /**
     * @return the startExpression
     */
    public Element getStartExpression() {
        return startExpression;
    }

    /**
     * @return the endExpression
     */
    public Element getEndExpression() {
        return endExpression;
    }

    /**
     * @return the stepExpression
     */
    public Element getStepExpression() {
        return stepExpression;
    }

    /**
     * Default constructor.
     *
     * @param variable Variable that we use to iterate in loop
     * @param startExpression Start value of variable
     * @param endExpression End value of variable
     * @param stepExpression Incrementation step
     */
    public ForLoopNode(ElementVariable variable, Element startExpression,
            Element endExpression, Element stepExpression) {
        super();
        if (variable == null || startExpression == null || endExpression == null) {
            throw new IllegalArgumentException("Parameter cannot be null;");
        }
        this.variable = variable;
        this.startExpression = startExpression;
        this.endExpression = endExpression;
        this.stepExpression = stepExpression;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("{$ FOR ");  
        sb.append(variable.asText()).append(" ");
        sb.append(startExpression.asText()).append(" ");
        sb.append(endExpression.asText()).append(" ");
        if (stepExpression != null) {
            sb.append(stepExpression.asText()).append(" ");
        }
        sb.append("$}");
        
        return sb.toString();
    }
}
