/**
 * Lexical analyzer used to generate tokens for SmartScript language.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
package hr.fer.zemris.java.hw03.prob1;
