package hr.fer.zemris.java.hw03.prob1;

/**
 * Token class models basic lexical analysis object - token. Token groups one or
 * more consecutive characters from input text. Token is immutable object.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class Token {
    /**
     * Type of stored token as described in @see TokenType.
     */
    private final TokenType type;
    /**
     * Value of token.
     */
    private final Object value;

    /**
     * Default Token constructor. It creates new token based on token type and
     * value of passed object.
     *
     * @param type
     *            <code>TokenType</code>
     * @param value
     *            Value of token
     * @throws IllegalArgumentException
     *             If any of arguments is <code>null</code>
     */
    public Token(TokenType type, Object value) {
        if (type == null) {
            throw new IllegalArgumentException("Token type can't be null!");
        }
        this.type = type;
        this.value = value;
    }

    /**
     * Returns type of token. Valid types for tokens are defined in
     * {@link TokenType}.
     *
     * @return the type of token.
     */
    public TokenType getType() {
        return type;
    }

    /**
     * Returns the value of token. Value can be
     * <code>Integer, String, Character</code> or <code>null</code>, depending
     * on a type of this token.
     *
     * @return the value of token.
     */
    public Object getValue() {
        return value;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + type.hashCode();
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Token)) {
            return false;
        }
        Token other = (Token) obj;
        if (type != other.type) {
            return false;
        }
        if (value == null) {
            return other.value == null;
        } else {
            return value.equals(other.value);
        }
    }

}
