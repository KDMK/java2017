package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration defines lexer operation states.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public enum LexerState {
    /**
     * Basic lexer state. When lexer is in this state it differs symbols,
     * numbers and strings TokenTypes.
     */
    BASIC,
    /**
     * Extended lexer state. When lexer is in this state it treats all input
     * sequences as strings.
     */
    EXTENDED
}
