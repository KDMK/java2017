package hr.fer.zemris.java.hw03;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Program tests implementation of developed parser.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class SmartScriptTester {

    /**
     * Starting point of our program.
     *
     * @param args
     *            Command line arguments; path to parser input files.
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid number of arguments! Aborting...");
            System.exit(-1);
        }

        String basePath = new File(args[0])
                .getAbsolutePath();
        String docBody = null;

        try {
            docBody = new String(Files.readAllBytes(Paths.get(basePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        SmartScriptParser parser = null;
        try {
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException ex) {
            System.out.println("Unable to parse document!");
            System.exit(-1);
        }

        DocumentNode document = parser.getDocumentNode();
        String originalDocumentBody = createOriginalDocumentBody(document);
        System.out.println(originalDocumentBody);

        System.out.println();
        System.out.println();
        System.out.println();

        SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
        DocumentNode document2 = parser2.getDocumentNode();
        String sameDocumentBody = createOriginalDocumentBody(document2);
        System.out.println(sameDocumentBody);
    }

    /**
     * Method travels through built syntax tree and rebuilds original document
     * structure.
     *
     * @param document
     *            Top level DocumentNode of parsed document
     * @return Reconstructed string of original document
     */
    public static String createOriginalDocumentBody(Node document) {
        StringBuilder dom = new StringBuilder();

        if (document.numberOfChilderen() != 0) {
            for (int i = 0, numOfChildren = document
                    .numberOfChilderen(); i < numOfChildren; i++) {
                dom.append(document.getChild(i).toString());

                if (document.getChild(i).numberOfChilderen() != 0) {
                    dom.append(
                            createOriginalDocumentBody(document.getChild(i)));
                }
            }
        }
        return dom.toString();
    }
}
