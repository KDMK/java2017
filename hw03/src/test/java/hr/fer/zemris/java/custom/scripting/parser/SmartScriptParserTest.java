package hr.fer.zemris.java.custom.scripting.parser;

import static hr.fer.zemris.java.hw03.SmartScriptTester.createOriginalDocumentBody;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;

public class SmartScriptParserTest {

    @Test
    public void testNormalFile() {
        SmartScriptParser parser = parse("doc1.txt");

        DocumentNode document = parser.getDocumentNode();
        String originalDocumentBody = createOriginalDocumentBody(document);

        assertEquals(document.numberOfChilderen(), 4);
        assertEquals(document.getChild(1).getClass(), ForLoopNode.class);
        assertEquals(document.getChild(1).numberOfChilderen(), 4);

        // Grab @sin element from second echo node in second for loop
        Element testTag = ((EchoNode) document.getChild(3).getChild(3))
                .getElements()[3];
        assertEquals(testTag.getClass(), ElementFunction.class);
        assertEquals(testTag.asText(), "@sin");
    }

    @Test
    public void testConsistency() {
        SmartScriptParser parser = parse("doc1.txt");

        DocumentNode document = parser.getDocumentNode();
        String originalDocumentBody = createOriginalDocumentBody(document);

        SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
        DocumentNode document2 = parser2.getDocumentNode();
        String rebuiltDocumentBody = createOriginalDocumentBody(document2);

        assertEquals(originalDocumentBody, rebuiltDocumentBody);
    }

    @Test
    public void testForTwoParameters() {
        SmartScriptParser parser = parse("doc2.txt");

        DocumentNode document = parser.getDocumentNode();

        Node forLoop = document.getChild(1);

        assertEquals(forLoop.getClass(), ForLoopNode.class);
        assertEquals(((ForLoopNode) forLoop).getStartExpression().asText(),
                "1");
        assertEquals(((ForLoopNode) forLoop).getEndExpression().asText(), "10");
        assertEquals(((ForLoopNode) forLoop).getStepExpression(), null);
    }

    @Test(expected = SmartScriptParserException.class)
    public void testForTooFewParameters() {
        SmartScriptParser parser = parse("doc3.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testForTooManyParameters() {
        SmartScriptParser parser = parse("doc4.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testForWrongParameters() {
        SmartScriptParser parser = parse("doc5.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testForOpenTagEnd() {
        SmartScriptParser parser = parse("doc6.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testForDoubleTagStart() {
        SmartScriptParser parser = parse("doc7.txt");
    }

    @Test
    public void testNestedLoop() {
        SmartScriptParser parser = parse("doc8.txt");

        DocumentNode document = parser.getDocumentNode();
        String docBody = createOriginalDocumentBody(document);

        assertEquals(document.numberOfChilderen(), 4);
        assertEquals(document.getChild(1).getClass(), ForLoopNode.class);
        assertEquals(document.getChild(1).numberOfChilderen(), 6);

        Node forLoop = document.getChild(1);
        assertEquals(forLoop.getClass(), ForLoopNode.class);
    }

    @Test(expected = SmartScriptParserException.class)
    // Missing end tag
    public void testOpenForLoop() {
        SmartScriptParser parser = parse("doc9.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testInvalidFunctionName() {
        SmartScriptParser parser = parse("doc10.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testInvalidTagName() {
        SmartScriptParser parser = parse("doc11.txt");
    }

    @Test(expected = SmartScriptParserException.class)
    public void testInvalidVariableName() {
        SmartScriptParser parser = parse("doc12.txt");
    }

    @Test
    public void testEmptyTag() {
        SmartScriptParser parser = parse("doc13.txt");

        DocumentNode document = parser.getDocumentNode();
        assertEquals(document.numberOfChilderen(), 6);

        Node emptyTag = document.getChild(3);
        assertEquals(emptyTag.getClass(), EchoNode.class);
        assertEquals(((EchoNode) emptyTag).getElements().length, 0);
    }

    // =========================================================================
    // Helper methods
    // =========================================================================


    /**
     * Loads file and copies content to string.
     *
     * @param filename
     *            Name of file which content we want to test
     * @return String representation of all file content
     */
    private String loader(String filename) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Path file = Paths.get("src/main/resources/" + filename).toAbsolutePath();

        try (InputStream is = new FileInputStream(file.toFile())) {
            byte[] buffer = new byte[1024];
            while (true) {
                int read = is.read(buffer);
                if (read < 1) {
                    break;
                }
                bos.write(buffer, 0, read);
            }
            return new String(bos.toByteArray(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Builds parser and parses document that is provided.
     *
     * @param docName
     *            Name of document which we want to parse
     * @return Built syntax tree
     */
    private SmartScriptParser parse(String docName) {
        String docBody = loader(docName);

        try {
            return new SmartScriptParser(docBody);
        } catch (SmartScriptParserException ex) {
            System.out.println("Unable to parse document!");
            ex.printStackTrace();
            throw ex;
        }
    }
}
