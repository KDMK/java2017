package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class LexerTest {

    @Test
    public void testNotNull() {
        Lexer lexer = new Lexer("");

        assertNotNull("Token was expected but null was returned.",
                lexer.nextToken());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullInput() {
        // must throw!
        new Lexer(null);
    }

    @Test
    public void testEmpty() {
        Lexer lexer = new Lexer("");

        assertEquals("Empty input must generate only EOF token.", TokenType.EOF,
                lexer.nextToken().getType());
    }

    @Test
    public void testGetReturnsLastNext() {
        // Calling getToken once or several times after calling nextToken must
        // return each time what nextToken returned...
        Lexer lexer = new Lexer("");

        Token token = lexer.nextToken();
        assertEquals("getToken returned different token than nextToken.", token,
                lexer.getToken());
        assertEquals("getToken returned different token than nextToken.", token,
                lexer.getToken());
    }

    @Test(expected = LexerException.class)
    public void testRadAfterEOF() {
        Lexer lexer = new Lexer("");

        // will obtain EOF
        lexer.nextToken();
        // will throw!
        lexer.nextToken();
    }

    @Test
    public void testOnlySpacesText() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("   \r\n\t    ");

        Token[] correctData = {new Token(TokenType.TEXT, "   \r\n\t    "),
                new Token(TokenType.EOF, null)};

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testNormalText() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text!");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text!"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testNormalTextSpaces() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("T   his is s a\nmp \tle text!");

        Token[] correctData = {
                new Token(TokenType.TEXT, "T   his is s a\nmp \tle text!"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testNormalTextEscapeLegal() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("T   hi\\\\s i\\{s s a\nmp \tle text!");

        Token[] correctData = {
                new Token(TokenType.TEXT, "T   hi\\s i{s s a\nmp \tle text!"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test(expected = LexerException.class)
    public void testNormalTextEscapeIllegal() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("T   hi\\3s i\\{s s a\nmp \tle text!");

        Token[] correctData = {
                new Token(TokenType.TEXT, "T   hi\\s i{s s a\nmp \tle text!"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    // =========================================================================
    // TAG MODE AND MIXED
    // =========================================================================
    @Test(expected = IllegalArgumentException.class)
    public void testNullState() {
        new Lexer("").setState(null);
    }

    @Test
    public void testNotNullInExtended() {
        Lexer lexer = new Lexer("");
        lexer.setState(LexerState.TAG);

        assertNotNull("Token was expected but null was returned.",
                lexer.nextToken());
    }

    @Test
    public void testEmptyInExtended() {
        Lexer lexer = new Lexer("");
        lexer.setState(LexerState.TAG);

        assertEquals("Empty input must generate only EOF token.", TokenType.EOF,
                lexer.nextToken().getType());
    }

    @Test
    public void testGetReturnsLastNextInExtended() {
        // Calling getToken once or several times after calling nextToken must
        // return each time what nextToken returned...
        Lexer lexer = new Lexer("");
        lexer.setState(LexerState.TAG);

        Token token = lexer.nextToken();
        assertEquals("getToken returned different token than nextToken.", token,
                lexer.getToken());
        assertEquals("getToken returned different token than nextToken.", token,
                lexer.getToken());
    }

    @Test(expected = LexerException.class)
    public void testRadAfterEOFInExtended() {
        Lexer lexer = new Lexer("");
        lexer.setState(LexerState.TAG);

        // will obtain EOF
        lexer.nextToken();
        // will throw!
        lexer.nextToken();
    }

    @Test
    public void testEmptyTagMixed() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text. {$= $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.TAG_NAME, "="),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testCustomTagMixed() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text. {$name $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testFunctionTagMixed() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text. {$name @see $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testStringTagMixed() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text. {$name @see \"Mike\" $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.STRING_CONSTANT, "Mike"),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testStringTagValidEscape() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"M\\\\ik\\\"e\" $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.STRING_CONSTANT, "M\\ik\"e"),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test(expected = LexerException.class)
    public void testStringTagInvalidEscape() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"M\\7ik\\\"e\" $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.STRING_CONSTANT, "M\\ik\"e"),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testIntValid() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer("This is sample text. {$name @see 342 -45 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(342)),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(-45)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test(expected = LexerException.class)
    // Number is to big to store in int.
    public void testIntInvalid() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see 3425432675423547675434354 -45 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(342)),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(-45)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testIntInStringValid() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"342\" -45 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(342)),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(-45)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    // NOTE: See buildNumber javadoc
    public void testIntAsString() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"342a\" -45 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.STRING_CONSTANT, "342a"),
                new Token(TokenType.INT_CONSTANT, Integer.valueOf(-45)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    @Test
    // NOTE: See buildNumber javadoc
    public void testDoubleValid() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"34.2\" -4.5 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.DOUBLE_CONSTANT, Double.valueOf(34.2)),
                new Token(TokenType.DOUBLE_CONSTANT, Double.valueOf(-4.5)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    public void testDoubleAsString() {
        // When input is only of spaces, tabs, newlines, etc...
        Lexer lexer = new Lexer(
                "This is sample text. {$name @see \"638a334.2\" -4.5 $}");

        Token[] correctData = {
                new Token(TokenType.TEXT, "This is sample text. "),
                new Token(TokenType.TAG_START, "{$"),
                new Token(TokenType.NAME, "name"),
                new Token(TokenType.FUNCTION, "@see"),
                new Token(TokenType.STRING_CONSTANT, "638a334.2"),
                new Token(TokenType.DOUBLE_CONSTANT, Double.valueOf(-4.5)),
                new Token(TokenType.TAG_END, "$}"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, correctData);
    }

    // =========================================================================
    // TAG MODE MIXED *END* =
    // =========================================================================

    // Helper method for checking if lexer generates the same stream of tokens
    // as the given stream.
    private void checkTokenStream(Lexer lexer, Token[] correctData) {
        int counter = 0;
        for (Token expected : correctData) {
            Token actual = lexer.nextToken();
            String msg = "Checking token " + counter + ":";
            assertEquals(msg, expected.getType(), actual.getType());
            assertEquals(msg, expected.getValue(), actual.getValue());
            counter++;
        }
    }
}
