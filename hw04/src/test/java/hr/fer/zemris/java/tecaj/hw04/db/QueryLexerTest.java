package hr.fer.zemris.java.tecaj.hw04.db;

import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class QueryLexerTest {

    @Test
    public void testEmptyQuery() {
        QueryLexer lex = new QueryLexer("");

        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }

    @Test(expected = QueryLexerException.class)
    public void testNextAfterEmpty() {
        QueryLexer lex = new QueryLexer("");

        lex.nextToken();
        lex.nextToken();
    }

    @Test
    public void testBlankQuery() {
        QueryLexer lex = new QueryLexer("     \t\n");

        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }

    @Test
    public void testFieldToken() {
        QueryLexer lex = new QueryLexer("fieldName");

        QueryToken cur = lex.nextToken();

        assertEquals(QueryTokenType.FIELD, cur.getType());
        assertEquals("fieldName", cur.getValue());
        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }

    @Test(expected = QueryLexerException.class)
    public void testInvalidFieldToken() {
        QueryLexer lex = new QueryLexer("fieldN\"ame");

        lex.nextToken();
    }

    @Test
    public void testOperatorsToken() {
        QueryLexer lex1 = new QueryLexer("<");
        QueryLexer lex2 = new QueryLexer("<=");
        QueryLexer lex3 = new QueryLexer(">");
        QueryLexer lex4 = new QueryLexer(">=");
        QueryLexer lex5 = new QueryLexer("=");
        QueryLexer lex6 = new QueryLexer("!=");
        QueryLexer lex7 = new QueryLexer("LIKE");

        QueryToken oper1 = lex1.nextToken();
        QueryToken oper2 = lex2.nextToken();
        QueryToken oper3 = lex3.nextToken();
        QueryToken oper4 = lex4.nextToken();
        QueryToken oper5 = lex5.nextToken();
        QueryToken oper6 = lex6.nextToken();
        QueryToken oper7 = lex7.nextToken();

        assertEquals(QueryTokenType.OPERATION, oper1.getType());
        assertEquals("<", oper1.getValue());
        assertEquals(QueryTokenType.END, lex1.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper2.getType());
        assertEquals("<=", oper2.getValue());
        assertEquals(QueryTokenType.END, lex2.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper3.getType());
        assertEquals(">", oper3.getValue());
        assertEquals(QueryTokenType.END, lex3.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper4.getType());
        assertEquals(">=", oper4.getValue());
        assertEquals(QueryTokenType.END, lex4.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper5.getType());
        assertEquals("=", oper5.getValue());
        assertEquals(QueryTokenType.END, lex5.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper6.getType());
        assertEquals("!=", oper6.getValue());
        assertEquals(QueryTokenType.END, lex6.nextToken().getType());

        assertEquals(QueryTokenType.OPERATION, oper7.getType());
        assertEquals("LIKE", oper7.getValue());
        assertEquals(QueryTokenType.END, lex7.nextToken().getType());
    }

    @Test(expected = QueryLexerException.class)
    public void testInvalidOperatorToken() {
        QueryLexer lex = new QueryLexer("!");

        lex.nextToken();
    }

    @Test
    public void testStringToken() {
        QueryLexer lex = new QueryLexer("\"Ivica\"");

        QueryToken cur = lex.nextToken();

        assertEquals(QueryTokenType.STRING, cur.getType());
        assertEquals("Ivica", cur.getValue());
        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }
    
    @Test(expected = QueryLexerException.class)
    public void testStringTokenError() {
        QueryLexer lex = new QueryLexer("\"Ivica");

        QueryToken cur = lex.nextToken();

        assertEquals(QueryTokenType.STRING, cur.getType());
        assertEquals("Ivica", cur.getValue());
        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }
    
    @Test
    public void testMultipleToken() {
        QueryLexer lex = new QueryLexer("fieldName = \"Ivica\"");

        QueryToken cur = lex.nextToken();

        assertEquals(QueryTokenType.FIELD, cur.getType());
        assertEquals("fieldName", cur.getValue());
        
        cur = lex.nextToken();
        assertEquals(QueryTokenType.OPERATION, cur.getType());
        assertEquals("=", cur.getValue());
        
        cur = lex.nextToken();
        assertEquals(QueryTokenType.STRING, cur.getType());
        assertEquals("Ivica", cur.getValue());
        
        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }
    
    @Test(expected = QueryLexerException.class)
    public void testMultipleTokenError() {
        QueryLexer lex = new QueryLexer("fieldName =! \"Ivica\"");

        QueryToken cur = lex.nextToken();

        assertEquals(QueryTokenType.FIELD, cur.getType());
        assertEquals("fieldName", cur.getValue());
        
        cur = lex.nextToken();
        assertEquals(QueryTokenType.OPERATION, cur.getType());
        assertEquals("=", cur.getValue());
        
        cur = lex.nextToken();
        assertEquals(QueryTokenType.STRING, cur.getType());
        assertEquals("Ivica", cur.getValue());
        
        assertEquals(QueryTokenType.END, lex.nextToken().getType());
    }
}
