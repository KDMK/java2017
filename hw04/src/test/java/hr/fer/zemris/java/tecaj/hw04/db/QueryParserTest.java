package hr.fer.zemris.java.tecaj.hw04.db;

import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class QueryParserTest {

    /**
     * Test if expected and result expression are equal.
     * 
     * @param expected
     *            expected conditional expression
     * @param result
     *            result conditional expression
     */
    private void checkExpression(ConditionalExpression expected,
            ConditionalExpression result) {
        assertEquals(expected.getComparisonOperator(),
                result.getComparisonOperator());
        assertEquals(expected.getFieldGetter(), result.getFieldGetter());
        assertEquals(expected.getStringLiteral(), result.getStringLiteral());
    }

    @Test
    public void testEmptyExpression() {
        QueryParser parser = new QueryParser("");

        assertEquals(0, parser.getQuery().size());
    }

    @Test
    public void testCorrectExpression1() {
        QueryParser parser = new QueryParser("jmbag=\"0000000034\"");

        ConditionalExpression expected = new ConditionalExpression(
                FieldValueGetters.JMBAG, "0000000034",
                ComparisonOperators.EQUALS);

        checkExpression(expected, parser.getQuery().get(0));
    }

    @Test
    public void testCorrectExpression2() {
        QueryParser parser = new QueryParser("firstName<=\"Ivica\"");

        ConditionalExpression expected = new ConditionalExpression(
                FieldValueGetters.FIRST_NAME, "Ivica",
                ComparisonOperators.LESS_OR_EQUALS);

        checkExpression(expected, parser.getQuery().get(0));
    }

    @Test
    public void testCorrectExpression3() {
        QueryParser parser = new QueryParser("lastName LIKE \"Tolić\"");

        ConditionalExpression expected = new ConditionalExpression(
                FieldValueGetters.LAST_NAME, "Tolić", ComparisonOperators.LIKE);

        checkExpression(expected, parser.getQuery().get(0));
    }

    @Test(expected = QueryParserException.class)
    public void testInvalidExpression1() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser("lastNa\"me LIKE \"Tolić\"");
    }

    @Test(expected = QueryParserException.class)
    public void testInvalidExpression2() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser("lastNameLIKE \"Tolić\"");
    }

    @Test(expected = QueryParserException.class)
    public void testInvalidExpression3() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser("lastName LIKE\"Tolić\"");
    }

    @Test(expected = QueryParserException.class)
    public void testInvalidExpression4() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser("lastName LIkE \"Tolić\"");
    }

    @Test
    public void testCorrectMutliExpression1() {
        QueryParser parser = new QueryParser(
                "firstName<=\"Ivica\" AND lastName=\"Kićmanović\"");

        ConditionalExpression[] expected = {
                new ConditionalExpression(FieldValueGetters.FIRST_NAME, "Ivica",
                        ComparisonOperators.LESS_OR_EQUALS),
                new ConditionalExpression(FieldValueGetters.LAST_NAME,
                        "Kićmanović", ComparisonOperators.EQUALS) };

        checkExpression(expected[0], parser.getQuery().get(0));
        checkExpression(expected[1], parser.getQuery().get(1));
    }

    @Test
    public void testCorrectMutliExpression2() {
        QueryParser parser = new QueryParser(
                "jmbag!=\"0036783456\" AND lastName=\"Kićmanović\"");

        ConditionalExpression[] expected = {
                new ConditionalExpression(FieldValueGetters.JMBAG, "0036783456",
                        ComparisonOperators.NOT_EQUALS),
                new ConditionalExpression(FieldValueGetters.LAST_NAME,
                        "Kićmanović", ComparisonOperators.EQUALS) };

        checkExpression(expected[0], parser.getQuery().get(0));
        checkExpression(expected[1], parser.getQuery().get(1));
    }

    @Test
    public void testCorrectMutliExpression3() {
        QueryParser parser = new QueryParser(
                "jmbag!=\"0036783456\" AND lastName=\"Kićmanović\" AND firstName=\"Ivica\"");

        ConditionalExpression[] expected = {
                new ConditionalExpression(FieldValueGetters.JMBAG, "0036783456",
                        ComparisonOperators.NOT_EQUALS),
                new ConditionalExpression(FieldValueGetters.LAST_NAME,
                        "Kićmanović", ComparisonOperators.EQUALS),
                new ConditionalExpression(FieldValueGetters.FIRST_NAME,
                        "Ivica", ComparisonOperators.EQUALS)};

        checkExpression(expected[0], parser.getQuery().get(0));
        checkExpression(expected[1], parser.getQuery().get(1));
        checkExpression(expected[2], parser.getQuery().get(2));
    }
    
    @Test(expected = QueryParserException.class)
    public void testIncorrectMutliExpression1() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser(
                "jmbag!=\"0036783456\" AND lastNme=\"Kićmanović\" AND firstName=\"Ivica\"");
    }
    
    @Test(expected = QueryParserException.class)
    public void testIncorrectMutliExpression2() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser(
                "jmbag!=\"0036783456\" AN lastName=\"Kićmanović\" AND firstName=\"Ivica\"");
    }
    
    @Test(expected = QueryParserException.class)
    public void testIncorrectMutliExpression3() {
        @SuppressWarnings("unused")
        QueryParser parser = new QueryParser(
                "jmbag!=\"0036783456\" AND \"Kićmanović\"=lastName AND firstName=\"Ivica\"");
    }
}
