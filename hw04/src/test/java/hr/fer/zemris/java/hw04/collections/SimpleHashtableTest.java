package hr.fer.zemris.java.hw04.collections;

import static org.junit.Assert.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.junit.Test;

import hr.fer.zemris.java.hw04.collections.SimpleHashtable.TableEntry;

@SuppressWarnings("javadoc")
public class SimpleHashtableTest {

    /**
     * Creates new hashtable and fills it with test data.
     *
     * @return filled hash table
     */
    private SimpleHashtable<String, Integer> fillTable() {
        SimpleHashtable<String, Integer> hashtable = new SimpleHashtable<>();
        // fill data:
        hashtable.put("Ivana", 2);
        hashtable.put("Ante", 2);
        hashtable.put("Jasna", 2);
        hashtable.put("Kristina", 5);
        hashtable.put("Mirjana", 5);

        return hashtable;
    }

    @Test
    public void testNormalAdd() {
        SimpleHashtable<String, Integer> hashtable = fillTable();

        assertEquals(5, hashtable.size());
    }

    @Test
    public void testDoubleAdd() {
        SimpleHashtable<String, Integer> hashtable = fillTable();
        hashtable.put("Ivana", 3);

        assertEquals(5, hashtable.size());
        assertEquals(Integer.valueOf(3), hashtable.get("Ivana"));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void testConcurrentModification() {
        SimpleHashtable<String, Integer> hashtable = fillTable();

        for (SimpleHashtable.TableEntry<String, Integer> i : hashtable) {
            hashtable.remove(i.getKey());
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testIllegalStateModification() {
        SimpleHashtable<String, Integer> hashtable = fillTable();
        Iterator<TableEntry<String, Integer>> i = hashtable.iterator();

        while (i.hasNext()) {
            @SuppressWarnings("unused")
            SimpleHashtable.TableEntry<String, Integer> pair = i.next();
            i.remove();
            i.remove();
        }
    }

    @Test
    public void testOutput() {
        SimpleHashtable<String, Integer> hashtable = fillTable();
        assertEquals("[Mirjana=5, Ivana=2, Kristina=5, Ante=2, Jasna=2]",
                hashtable.toString());
    }
}
