package hr.fer.zemris.java.tecaj.hw04.db;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class StudentDatabaseTest {
    /**
     * Loads database to list of strings. Each list entry is one line in input
     * file.
     * 
     * @param filename
     *            of database that we want to load.
     * @return List that contains each line stored separately
     */
    private StudentDatabase loadDatabase(String filename) {
        String basePath = new File("src/main/resources/" + filename)
                .getAbsolutePath();
        List<String> input = null;

        try {
            input = Files.readAllLines(Paths.get(basePath),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }

        return new StudentDatabase(input);
    }

    /**
     * Compares given student records.
     * 
     * @param expected
     *            table entry
     * @param result
     *            real table entry
     */
    private void compareTableEntries(StudentRecord expected,
            StudentRecord result) {
        assertEquals(expected.getFirstName(), result.getFirstName());
        assertEquals(expected.getLastName(), result.getLastName());
        assertEquals(expected.getJmbag(), result.getJmbag());
        assertEquals(expected.getFinalGrade(), result.getFinalGrade());
    }

    /**
     * Check if result of query matches expected values.
     * 
     * @param expected
     *            array of expected student records
     * @param result
     *            list of queried student records
     */
    private void checkQueryResult(StudentRecord[] expected,
            List<StudentRecord> result) {
        assertEquals(expected.length, result.size());
        for (int i = 0; i < expected.length; i++) {
            compareTableEntries(expected[i], result.get(i));
        }
    }

    @Test
    public void testCreateDatabase() {
        StudentDatabase db = loadDatabase("database.txt");
        assertEquals(63, db.getTableDb().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDatabaseWrongFormat() {
        @SuppressWarnings("unused")
        StudentDatabase db = loadDatabase("database1.txt");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDatabaseDoubleJmbag() {
        @SuppressWarnings("unused")
        StudentDatabase db = loadDatabase("database2.txt");
    }

    @Test
    public void testSingleCondQuery() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("jmbag=\"0000000002\"");
        StudentRecord expected = new StudentRecord("0000000002", "Bakamović",
                "Petra", "3");
        StudentRecord result;

        if (parser.isDirectQuery()) {
            result = db.forJMBAG(parser.getQueriedJMBAG());
            compareTableEntries(expected, result);
        } else {
            throw new IllegalStateException("This should not be thrown!");
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testSingleCondQueryWrong() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("jmbag<\"0000000002\"");
        StudentRecord expected = new StudentRecord("0000000001", "Akšamović",
                "Marin", "2");
        StudentRecord result;

        result = db.forJMBAG(parser.getQueriedJMBAG());
        compareTableEntries(expected, result);
    }

    @Test
    public void testSingleCondQueryLess() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("jmbag<\"0000000003\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000001", "Akšamović", "Marin", "2"),
                new StudentRecord("0000000002", "Bakamović", "Petra", "3") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testSingleCondQueryLessOrEquals() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("jmbag<=\"0000000003\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000001", "Akšamović", "Marin", "2"),
                new StudentRecord("0000000002", "Bakamović", "Petra", "3"),
                new StudentRecord("0000000003", "Bosnić", "Andrea", "4") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);

    }

    @Test
    public void testSingleCondQueryGreater() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("jmbag>\"0000000060\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000061", "Vukojević", "Renato", "2"),
                new StudentRecord("0000000062", "Zadro", "Kristijan", "3"),
                new StudentRecord("0000000063", "Žabčić", "Željko", "4") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testSingleCondQueryGreaterOrEquals() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("lastName>=\"Z\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000007", "Čima", "Sanjin", "4"),
                new StudentRecord("0000000008", "Ćurić", "Marko", "5"),
                new StudentRecord("0000000054", "Šamija", "Pavle", "3"),
                new StudentRecord("0000000055", "Šimunov", "Ivan", "4"),
                new StudentRecord("0000000056", "Šimunović", "Veljko", "5"),
                new StudentRecord("0000000057", "Širanović", "Hrvoje", "2"),
                new StudentRecord("0000000058", "Šoić", "Vice", "3"),
                new StudentRecord("0000000059", "Štruml", "Marin", "4"),
                new StudentRecord("0000000062", "Zadro", "Kristijan", "3"),
                new StudentRecord("0000000063", "Žabčić", "Željko", "4") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testSingleCondQueryEquals() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("firstName=\"Ivan\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000006", "Cvrlje", "Ivan", "3"),
                new StudentRecord("0000000035", "Marić", "Ivan", "4"),
                new StudentRecord("0000000044", "Pilat", "Ivan", "5"),
                new StudentRecord("0000000047", "Rakipović", "Ivan", "4"),
                new StudentRecord("0000000055", "Šimunov", "Ivan", "4") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testSingleCondQueryNotEquals() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("firstName!=\"Ivan\"");

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        assertEquals(58, result.size());
    }

    @Test
    public void testSingleCondQueryLike() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser("firstName LIKE \"J*p\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000038", "Markotić", "Josip", "3"),
                new StudentRecord("0000000052", "Slijepčević", "Josip", "5") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testMultipleQuery1() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser(
                "jmbag > \"0000000050\" AND firstName LIKE \"I*\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000055", "Šimunov", "Ivan", "4"),
                new StudentRecord("0000000060", "Vignjević", "Irena", "5") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testMultipleQuery2() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser(
                "jmbag > \"0000000050\" AND firstName LIKE \"I*\""
                        + " AND firstName = \"Luka\"");
        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        assertEquals(0, result.size());
    }

    @Test
    public void testMultipleQuery3() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser(
                "firstName LIKE \"*na\" AND jmbag < \"0000000030\"");
        StudentRecord[] expected = {
                new StudentRecord("0000000023", "Kalvarešin", "Ana", "4") };

        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        checkQueryResult(expected, result);
    }

    @Test
    public void testMultipleQuery4() {
        StudentDatabase db = loadDatabase("database.txt");
        QueryParser parser = new QueryParser(
                "firstName LIKE \"T*\"" + " AND firstName = \"Luka\"");
        List<StudentRecord> result = db
                .filter(new QueryFilter(parser.getQuery()));

        assertEquals(0, result.size());
    }

}
