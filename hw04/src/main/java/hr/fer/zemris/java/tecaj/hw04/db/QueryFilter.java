package hr.fer.zemris.java.tecaj.hw04.db;

import java.util.List;

/**
 * Query filter class models behavior of SQL type query, but with limited query
 * options. Query only supports linking conditions with AND operator. OR
 * operator or grouping with parenthesis are not supported in this
 * implementation.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class QueryFilter implements IFilter {
    /**
     * Query filters copy of query conditions.
     */
    private List<ConditionalExpression> conditionList;

    /**
     * Default constructor.
     *
     * @param conditionList
     *            list of all conditions that tested StudentRecord must satisfy
     *            in order to be accepted.
     */
    public QueryFilter(List<ConditionalExpression> conditionList) {
        this.conditionList = conditionList;
    }

    @Override
    public boolean accepts(StudentRecord record) {
        for (ConditionalExpression ex : conditionList) {
            IFieldValueGetter field = ex.getFieldGetter();
            IComparisonOperator operation = ex.getComparisonOperator();

            if (!operation.satisfied(field.get(record), ex.getStringLiteral())) {
                return false;
            }
        }
        return true;
    }
}
