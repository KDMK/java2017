package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Models complete expression for creating queries. Each expression consists of
 * its operation type and variables which we are comparing.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ConditionalExpression {
    /**
     * Field of student record upon which comparison is done.
     */
    private IFieldValueGetter field;
    /**
     * Comparison operation that we are performing on given input values.
     */
    private IComparisonOperator operation;
    /**
     * Value that we are comparing db elements with.
     */
    private String testValue;

    /**
     * Default constructor. It creates expression that we can use to get desired
     * table entries from database.
     *
     * @param field
     *            which we are testing.
     * @param testValue
     *            value that we compare given record to.
     * @param operation
     *            comparison operation that we are performing.
     */
    public ConditionalExpression(IFieldValueGetter field, String testValue,
            IComparisonOperator operation) {
        this.field = field;
        this.operation = operation;
        this.testValue = testValue;
    }

    /**
     * @return Comparison {@link ConditionalExpression#operation operator} of
     *         expression.
     */
    public IComparisonOperator getComparisonOperator() {
        return this.operation;
    }

    /**
     * @return the {@link ConditionalExpression#testValue testValue}.
     */
    public String getStringLiteral() {
        return this.testValue;
    }

    /**
     * @return the {@link ConditionalExpression#field field}.
     */
    public IFieldValueGetter getFieldGetter() {
        return this.field;
    }
}
