package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Filter interface describe method for creating element filters. Element
 * filters gives us easier way to select only elements that fulfill some given
 * condition.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public interface IFilter {
    /**
     * Test passed student record if it satisfies comparison condition.
     *
     * @param record
     *            StudenRecord that we are testing
     * @return True if comparison condition is satisfied, false if it is not
     */
    boolean accepts(StudentRecord record);
}
