package hr.fer.zemris.java.tecaj.hw04.db;

import java.util.ArrayList;
import java.util.List;

/**
 * Class contains parser for processing user input. It evaluates input text into
 * valid expressions. At the end of parsing list of all valid expressions is
 * stored for user to execute them.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class QueryParser {
    /**
     * Last processed query expressions.
     */
    private List<ConditionalExpression> expressions;
    /**
     * Stores state of expressions list. Used to determine if we need to
     * initialize new list or we can use existing(after we clear it);
     */
    private boolean expressionsInitialized;

    /**
     * Empty constructor. It doesn't take any arguments.
     */
    public QueryParser() {
        parseQuery("");
    }

    /**
     * Default constructor. It takes query string and tries to parse it. If
     * parsing was successful expressions are stored in
     *
     * @param query string of input.
     */
    public QueryParser(String query) {
        try {
            parseQuery(query);
        } catch (QueryLexerException | QueryParserException ex) {
            throw new QueryParserException(ex.getMessage());
        }
    }

    /**
     * @return list of all conditional expressions from query.
     */
    public List<ConditionalExpression> getQuery() {
        return expressions;
    }

    /**
     * Method checks if current query is in format jmbag="...". Method returns
     * true just if there is only one comparison in query and it is in desired
     * format.
     *
     * @return True if parsed query is direct query otherwise false.
     */
    public boolean isDirectQuery() {
        if (expressions.size() == 1) {
            ConditionalExpression query = expressions.get(0);
            return query.getComparisonOperator() == ComparisonOperators.EQUALS
                    && query.getFieldGetter() == FieldValueGetters.JMBAG;
        }
        return false;
    }

    /**
     * Method returns jmbag of given query. Method is allowed only when
     * expression is direct query.
     *
     * @return jmbag of direct query
     * @throws IllegalStateException
     *             if query is not direct query.
     */
    public String getQueriedJMBAG() {
        if (!isDirectQuery()) {
            throw new IllegalStateException(
                    "Contained query is not direct query, cannot get JMBAG!");
        }
        return expressions.get(0).getStringLiteral();
    }

    // =========================================================================
    // Helper methods
    // =========================================================================

    /**
     * Parses given query. After method is successfully done given expression is
     * stored and can be evaluated.
     *
     * @param query
     *            user input
     * @throws QueryParserException
     *             if there is error in input query.
     */
    public void parseQuery(String query) {
        if (expressionsInitialized) {
            expressions.clear();
        } else {
            expressions = new ArrayList<>();
        }

        QueryLexer lexer = new QueryLexer(query);
        QueryToken currentToken = lexer.nextToken();

        while (currentToken.getType() != QueryTokenType.END) {
            // Next token type is FIELD, check if it is followed with OPERATOR
            // token, than STRING and END or AND after last member of an
            // expression.
            if (currentToken.getType() == QueryTokenType.FIELD) {
                String fieldName = currentToken.getValue();

                // Grab next and check if it is operation
                currentToken = lexer.nextToken();
                if (currentToken.getType() != QueryTokenType.OPERATION) {
                    throw new QueryParserException(
                            "Invalid expression after token: " + fieldName);
                }
                String operation = currentToken.getValue();

                // Grab next and check if it is string
                currentToken = lexer.nextToken();
                if (currentToken.getType() != QueryTokenType.STRING) {
                    throw new QueryParserException(
                            "Invalid expression after token: " + operation);
                }
                String testValue = currentToken.getValue();

                // Grab next token and check if it is end or and operator
                currentToken = lexer.nextToken();
                if (currentToken.getType() != QueryTokenType.END
                        && currentToken.getType() != QueryTokenType.AND) {
                    throw new QueryParserException(
                            "Invalid expression after token: " + testValue);
                }

                expressions.add(
                        generateExpression(fieldName, testValue, operation));
                // Move to next expression if current token is AND token
                if (currentToken.getType() == QueryTokenType.AND) {
                    currentToken = lexer.nextToken();
                }
                continue;
            }
            throw new QueryParserException(
                    "Invalid expression at token: " + currentToken.getValue());
        }
    }

    /**
     * Method builds expression from given parameters. Method is provided with
     * string values needed to build full expression.
     *
     * @param fieldName
     *            on which we are doing query.
     * @param testValue
     *            that we test in query.
     * @param operator
     *            operation symbol.
     * @return conditional expression ready for evaluation.
     */
    private ConditionalExpression generateExpression(String fieldName,
            String testValue, String operator) {
        IFieldValueGetter field = generateFieldGetter(fieldName);
        IComparisonOperator operation = generateOperation(operator);
        return new ConditionalExpression(field, testValue, operation);
    }

    /**
     * Generates appropriate FieldValueGetter based on filed name.
     *
     * @param fieldName
     *            on which we are doing query.
     * @return proper IFieldValueGetter
     * @throws QueryParserException
     *             field name does not exist in our table.
     */
    private IFieldValueGetter generateFieldGetter(String fieldName) {
        switch (fieldName) {
        case "lastName":
            return FieldValueGetters.LAST_NAME;
        case "firstName":
            return FieldValueGetters.FIRST_NAME;
        case "jmbag":
            return FieldValueGetters.JMBAG;
        default:
            throw new QueryParserException("Invalid field name!");
        }
    }

    /**
     * Generates appropriate ComparisonOperator based on operator.
     *
     * @param operator
     *            for desired operation..
     * @return proper IComparisonOperator.
     * @throws QueryParserException
     *             if operation is not supported.
     */
    private IComparisonOperator generateOperation(String operator) {
        switch (operator.toLowerCase()) {
        case "like":
            return ComparisonOperators.LIKE;
        case "=":
            return ComparisonOperators.EQUALS;
        case "!=":
            return ComparisonOperators.NOT_EQUALS;
        case "<":
            return ComparisonOperators.LESS;
        case "<=":
            return ComparisonOperators.LESS_OR_EQUALS;
        case ">":
            return ComparisonOperators.GREATER;
        case ">=":
            return ComparisonOperators.GREATER_OR_EQUALS;
        default:
            throw new QueryParserException("Unsupported operation!");
        }
    }
}
