package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Interface represents a strategy upon which we are building specific
 * student record field value getter.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface IFieldValueGetter {
    /**
     * Method returns value of designated property from given record.
     *
     * @param record from which we are grabbing desired property.
     * @return Value of desired property
     */
    String get(StudentRecord record);
}
