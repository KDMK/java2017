package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Token class represents tokens from our query input. It groups characters in
 * valid token types. All tokens are immutable.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class QueryToken {
    /**
     * Type of stored QueryToken as described in {@link QueryTokenType}.
     *
     */
    private final QueryTokenType type;
    /**
     * Value of QueryToken.
     */
    private final String value;

    /**
     * Default QueryToken constructor. It creates new QueryToken based on
     * QueryToken type and value of passed string.
     *
     * @param type
     *            <code>QueryTokenType</code>
     * @param value
     *            Value of QueryToken
     * @throws IllegalArgumentException
     *             If any of arguments is <code>null</code>
     */
    public QueryToken(QueryTokenType type, String value) {
        if (type == null) {
            throw new IllegalArgumentException(
                    "QueryToken type can't be null!");
        }
        this.type = type;
        this.value = value;
    }

    /**
     * Returns type of QueryToken. Valid types for QueryTokens are defined in
     * {@link QueryTokenType}.
     *
     * @return the type of QueryToken.
     */
    public QueryTokenType getType() {
        return type;
    }

    /**
     * Returns the value of QueryToken. Value string with content depending on a
     * type of this QueryToken.
     *
     * @return the value of QueryToken.
     */
    public String getValue() {
        return value;
    }
}
