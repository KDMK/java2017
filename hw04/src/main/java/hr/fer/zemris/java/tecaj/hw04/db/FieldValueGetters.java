package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Models getters for student entries. 
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class FieldValueGetters {
    /**
     * Models getter for first name.
     */
    public static final IFieldValueGetter FIRST_NAME;
    /**
     * Models getter for last name.
     */
    public static final IFieldValueGetter LAST_NAME;
    /**
     * Models getter for jmbag.
     */
    public static final IFieldValueGetter JMBAG;

    static {
        FIRST_NAME = (record) -> record.getFirstName();
        LAST_NAME = (record) -> record.getLastName();
        JMBAG = (record) -> record.getJmbag();
    }
}
