package hr.fer.zemris.java.hw04.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class represents hash table that provides storage for ordered pairs formatted
 * as (key, value). First parameter is Key type and second parameter is value
 * type. Type of keys and values can be any class type extended from
 * {@link Object}. Key can never be null.
 *
 * @param <K> Key type
 * @param <V> Value type
 * @author Matija Bartolac
 * @version v1.0
 */
public class SimpleHashtable<K, V>
        implements Iterable<SimpleHashtable.TableEntry<K, V>> {
    /**
     * Default hash table size.
     */
    private static final int DEF_HASHTABLE_SIZE = 16;
    /**
     * Optimal fill ratio of hash table.
     */
    private static final double OPT_FILL = 0.75;
    /**
     * Array used to store Table entries.
     */
    private TableEntry<K, V>[] table;
    /**
     * Number of elements stored in hash table.
     */
    private int size;
    /**
     * Tracks modifications on collection.
     */
    private int modificationCount;

    /**
     * Single data entry. TableEntry holds key and value pair as well as
     * reference to next TableEntry in same slot.
     *
     * @param <K> parameter type used for key
     * @param <V> parameter type used for value
     * @author Matija Bartolac
     * @version v1.0
     */
    public static class TableEntry<K, V> {
        /**
         * Key of stored pair.
         */
        private K key;
        /**
         * Value of stored pair.
         */
        private V value;
        /**
         * Reference to next TableEntry in same slot.
         */
        private TableEntry<K, V> next;

        /**
         * Default constructor.
         *
         * @param key   Key in pair.
         * @param value Value in pair.
         * @param next  Reference to next pair in list.
         */
        public TableEntry(K key, V value, TableEntry<K, V> next) {
            if (key == null) {
                throw new IllegalArgumentException("Key cannot be null!");
            }
            this.key = key;
            this.value = value;
            this.next = next;
        }

        /**
         * @return Key of pair
         */
        public K getKey() {
            return key;
        }

        /**
         * @return Value of pair
         */
        public V getValue() {
            return value;
        }

        /**
         * @param value value of pair that we want to set.
         */
        public void setValue(V value) {
            this.value = value;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((key == null) ? 0 : key.hashCode());
            result = prime * result + ((value == null) ? 0 : value.hashCode());
            return result;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof TableEntry)) {
                return false;
            }
            TableEntry<K, V> other = (TableEntry<K, V>) obj;
            if (key == null) {
                if (other.key != null) {
                    return false;
                }
            } else if (!key.equals(other.getKey())) {
                return false;
            }
            if (value == null) {
                return other.value == null;
            } else return value.equals(other.getValue());
        }
    }

    /**
     * Default constructor. Creates hash table with n slots(Defined in
     * DEF_HASHTABLE_SIZE).
     */
    @SuppressWarnings("unchecked")
    public SimpleHashtable() {
        this.table = (TableEntry<K, V>[]) new TableEntry[DEF_HASHTABLE_SIZE];
    }

    /**
     * Default constructor. Size of hash table is determined by passed size
     * value. Real size of hash table is first bigger or equal as size power of
     * number two.
     *
     * @param capacity Number of slots that we want to create.
     */
    @SuppressWarnings("unchecked")
    public SimpleHashtable(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Size can't be less than one");
        }

        double twoPow = Math.ceil((Math.log(capacity) / Math.log(2)));
        capacity = (int) Math.pow(2, twoPow);

        this.table = (TableEntry<K, V>[]) new TableEntry[capacity];
    }

    /**
     * Puts element in hashtable. If element with given key already exists in
     * table it will replace its value.
     *
     * @param key   Key of element that we want to store
     * @param value Value of element that we want to store
     */
    public void put(K key, V value) {
        if (key == null) {
            throw new IllegalArgumentException("Key can't be null.");
        }

        int slot = getSlot(key);

        if (table[slot] == null) {
            insertNew(key, value, slot, null);
            return;
        }

        TableEntry<K, V> cur = table[slot];
        cur = updateExisting(key, value, cur);
        if (cur == null) return;

        insertNew(key, value, slot, cur);
    }

    /**
     * Updates exitsing value if it's present in current collection. If value with given key doesn't exists method will
     * return reference to last element of array after which we will insert new value.
     *
     * @param key   table entry key
     * @param value of the table entry
     * @param cur   reference to element after which we will insert new one
     * @return null if value is updated or reference to last element in list if element with given key wasn't found
     * in list
     */
    private TableEntry<K, V> updateExisting(K key, V value, TableEntry<K, V> cur) {
        while (cur.next != null) {
            if (cur.getKey().equals(key)) {
                cur.setValue(value);
                return null;
            }
            cur = cur.next;
        }
        return cur;
    }

    private void insertNew(K key, V value, int slot, TableEntry<K, V> entry) {
        TableEntry<K, V> kvTableEntry = new TableEntry<>(key, value, null);

        if (entry == null) {
            table[slot] = kvTableEntry;
        } else {
            entry.next = kvTableEntry;
        }

        size++;
        modificationCount++;

        if (updateFill()) {
            reallocateTable();
        }
    }

    /**
     * Gets element value which is stored with provided key. If such element is
     * not present in hash table it returns null.
     *
     * @param key Key of entry that we want to get
     * @return Value of desired table entry
     */
    public V get(Object key) {
        if (key == null) {
            return null;
        }

        int slot = getSlot(key);
        TableEntry<K, V> cur = table[slot];

        return findValueInList(key, cur);
    }

    private V findValueInList(Object key, TableEntry<K, V> cur) {
        while (cur != null) {
            if (cur.getKey().equals(key)) {
                return cur.getValue();
            }
            cur = cur.next;
        }
        return null;
    }

    /**
     * @return Number of elements in hashtable
     */
    public int size() {
        return this.size;
    }

    /**
     * Checks if table entry with given key exists in hash table.
     *
     * @param key Key of table entry that we are looking for
     * @return True if table entry with given key exists in hash table, false if
     * doesn't.
     */
    public boolean containsKey(Object key) {
        return key != null && get(key) != null;
    }

    /**
     * Checks if desired value exists in hash table.
     *
     * @param value Value that we want to check.
     * @return True if value is in hash table, false if it isn't.
     */
    public boolean containsValue(Object value) {
        for (TableEntry<K, V> entry : table) {
            TableEntry<K, V> cur = entry;
            while (cur != null) {
                if (cur.getValue() == value || cur.getValue().equals(value)) {
                    return true;
                }
                cur = cur.next;
            }
        }
        return false;
    }

    /**
     * Removes element with given key from hashtable. If such element does not
     * exist it does nothing.
     *
     * @param key Key of entry that we want to remove
     */
    public void remove(Object key) {
        if (key == null) {
            return;
        }

        int slot = getSlot(key);
        // remove from start
        if (table[slot].getKey().equals(key)) {
            table[slot] = table[slot].next;
            // Remove from any other position in list
        } else {
            TableEntry<K, V> cur = table[slot];
            // Search slot for given key
            while (cur.next != null && !cur.next.getKey().equals(key)) {
                cur = cur.next;
            }
            // Key is not in slot, do nothing.
            if (cur.next == null) {
                return;
            }
            cur.next = cur.next.next;
        }

        size--;
        modificationCount++;
    }

    /**
     * @return True if hash table doesn't contain any elements.
     */
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * Removes all table entries from table.
     */
    public void clear() {
        for (int i = 0; i < table.length; i++) {
            TableEntry<K, V> cur = table[i];
            while (cur != null) {
                TableEntry<K, V> next = cur.next;
                cur.next = null;
                cur = next;
                size--;
            }
            table[i] = null;
        }
        modificationCount++;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (TableEntry<K, V> aTable : table) {
            TableEntry<K, V> cur = aTable;
            while (cur != null) {
                sb.append(cur.getKey().toString())
                        .append("=")
                        .append(cur.getValue().toString())
                        .append(", ");
                cur = cur.next;
            }
        }
        if (sb.charAt(sb.length() - 1) == ' ') {
            sb.replace(sb.length() - 2, sb.length(), "]");
        } else {
            sb.append(']');
        }

        return sb.toString();
    }

    // Helper method

    /**
     * Calculates slot for given key.
     *
     * @param key Key of table entry that we are storing/getting.
     * @return Reference to first element in slot where given key should be
     * stored.
     */
    private int getSlot(Object key) {
        return Math.abs(key.hashCode() % table.length);
    }

    /**
     * Updates fill ratio of table and signals user if fill ratio is greater
     * than desired.
     *
     * @return True if fill ratio is passed {@link SimpleHashtable#OPT_FILL
     * OPT_FILL.}
     */
    private boolean updateFill() {
        double filled = (double) size / (double) table.length;
        return (Double.compare(filled, OPT_FILL) >= 0);
    }

    /**
     * Reallocates table to double of present size.
     */
    @SuppressWarnings("unchecked")
    private void reallocateTable() {
        TableEntry<K, V>[] oldTable = table;
        table = (TableEntry<K, V>[]) new TableEntry[oldTable.length * 2];
        size = 0;

        // Process all elements from old table, put them in new and remove all
        // references to them(for garbage collector)
        for (int i = 0; i < oldTable.length; i++) {
            TableEntry<K, V> cur = oldTable[i];
            while (cur != null) {
                put(cur.getKey(), cur.getValue());
                TableEntry<K, V> next = cur.next;
                cur.next = null;
                cur = next;
            }
            oldTable[i] = null;
        }
    }

    @Override
    public Iterator<TableEntry<K, V>> iterator() {
        return new IteratorImpl();
    }

    /**
     * Class models the iterator for simple hash table collection.
     *
     * @author Matija Bartolac
     * @version v1.0
     */
    private class IteratorImpl
            implements Iterator<SimpleHashtable.TableEntry<K, V>> {
        /**
         * Current position in {@link SimpleHashtable#table table}.
         */
        private int curLevel;
        /**
         * Last returned element.
         */
        private TableEntry<K, V> current;
        /**
         * Flag that indicates that object is modified(removed) since last call
         * of next().
         */
        private boolean lastMod;
        /**
         * Iterators copy of modificationCount. Used to track concurrent
         * modifications.
         */
        private int expectedModCount;

        /**
         * Default constructor. Current modification count is stored in iterator
         * for tracking changes made outside iterator(during iterations).
         */
        IteratorImpl() {
            this.expectedModCount = modificationCount;
        }

        @Override
        public boolean hasNext() {
            return ((current != null && current.next != null)
                    || (curLevel < table.length && !isEmpty()));
        }

        @Override
        public TableEntry<K, V> next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            checkConcurrentMod();

            while (hasNext()) {
                if (current == null) {
                    current = table[curLevel++];
                } else {
                    current = current.next;
                }
                if (current != null) {
                    break;
                }
            }

            lastMod = false;
            return current;
        }

        @Override
        public void remove() {
            if (lastMod) {
                throw new IllegalStateException();
            }

            checkConcurrentMod();

            SimpleHashtable.this.remove(current.getKey());
            lastMod = true;
            expectedModCount = modificationCount;
        }

        /**
         * Checks if modifications have been made outside of iterator during the
         * iteration.
         *
         * @throws ConcurrentModificationException If modifications have been made
         */
        void checkConcurrentMod() {
            if (expectedModCount != SimpleHashtable.this.modificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
