package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Model of a single student record. Student record class models single entity
 * in StudentFinalGrades table(if we look at our database as relational
 * database). Jmbag is unique for every student and its also primary key in our
 * table. Only students grade is modifiable.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class StudentRecord {
    /**
     * Students unique ID number.
     */
    private String jmbag;
    /**
     * Students first name.
     */
    private String firstName;
    /**
     * Students last name.
     */
    private String lastName;
    /**
     * Students final grade.
     */
    private String finalGrade;

    /**
     * @return the finalGrade
     */
    public String getFinalGrade() {
        return finalGrade;
    }

    /**
     * @param finalGrade
     *            the value of grade to set
     */
    public void setFinalGrade(String finalGrade) {
        this.finalGrade = finalGrade;
    }

    /**
     * @return the jmbag
     */
    public String getJmbag() {
        return jmbag;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Default constructor.
     *
     * @param jmbag
     *            Students unique identification number
     * @param firstName
     *            Students name
     * @param lastName
     *            Students last name
     * @param finalGrade
     *            Students final grade
     * @throws IllegalArgumentException
     *             if any given parameter is null.
     */
    public StudentRecord(String jmbag, String lastName, String firstName,
            String finalGrade) {
        if (jmbag == null || lastName == null || firstName == null
                || finalGrade == null) {
            throw new IllegalArgumentException(
                    "None of student record arguments can be null.");
        }
        this.jmbag = jmbag;
        this.lastName = lastName;
        this.firstName = firstName;
        this.finalGrade = finalGrade;
    }

    @Override
    public String toString() {
        return jmbag + " " + lastName + " " + firstName;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof StudentRecord)) {
            return false;
        }
        StudentRecord other = (StudentRecord) obj;
        if (jmbag == null) {
            return other.jmbag == null;
        } else return jmbag.equals(other.jmbag);
    }
}
