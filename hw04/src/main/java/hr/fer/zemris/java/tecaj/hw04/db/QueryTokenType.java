package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Enumeration of token types present in queries.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public enum QueryTokenType {
    /**
     * Student record filed on which we are doing query.
     */
    FIELD,
    /**
     * Comparison operation.
     */
    OPERATION,
    /**
     * Text between double quotation marks.
     */
    STRING,
    /**
     * End of query.
     */
    END,
    /**
     * And operator.
     */
    AND;
}
