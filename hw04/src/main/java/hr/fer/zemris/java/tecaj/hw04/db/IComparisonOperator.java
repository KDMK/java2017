package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * Interface represents a strategy upon which we are building specific
 * comparison operators.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public interface IComparisonOperator {
    /**
     * Check if given values satisfy desired condition.
     *
     * @param value1 expected value
     * @param value2 real value
     * @return True if expected value satisfies condition
     */
    boolean satisfied(String value1, String value2);
}
