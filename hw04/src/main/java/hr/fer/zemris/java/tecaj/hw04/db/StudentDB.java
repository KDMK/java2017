package hr.fer.zemris.java.tecaj.hw04.db;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Class used for testing functionality of database. Program takes one argument
 * - location of database.txt. database.txt contains dump of StudentGrade table
 * (from some other database). Program reconstructs table in object modeled DB
 * and provides user with basic query funcitonality. Program takes user input
 * from {@link System#in} and output result on {@link System#out}.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class StudentDB {
    /**
     * Starting point of program.
     *
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid number of arguments! Aborting...");
            System.exit(1);
        }

        String basePath = new File(args[0]).getAbsolutePath();
        List<String> lines = null;

        try {
            lines = Files.readAllLines(Paths.get(basePath),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(2);
        }

        StudentDatabase db = null;
        try {
            db = new StudentDatabase(lines);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.exit(3);
        }

        System.out.println("Welcome to DBEngine!");
        System.out.println(
                "For search type query followed by list of conditions linked "
                        + "with operator AND. For end type: kraj.");
        System.out.println(
                "Valid search fields: firstName, lastName, jmbag.");

        Scanner inputScanner = new Scanner(System.in);
        String input;
        QueryParser parser = new QueryParser();

        while (true) {
            System.out.print(String.format("> "));
            input = inputScanner.nextLine();

            if (input.equalsIgnoreCase("kraj")) {
                break;
            }

            // check if query is valid
            if (!input.matches("\\s*query\\s.+")) {
                System.out.println("Invalid command!");
                continue;
            }
            input = input.replaceAll("query", "");

            try {
                parser.parseQuery(input);
            } catch (QueryParserException ex) {
                System.out.println(ex.getMessage());
                continue;
            }

            if (parser.isDirectQuery()) {
                System.out.println("Using index for record retrieval.");
                StudentRecord student = db.forJMBAG(parser.getQueriedJMBAG());
                if (student == null) {
                    System.out.println("Records selected: 0");
                    continue;
                }
                System.out.println(StudentDatabase.printStudent(student));
            } else {
                List<StudentRecord> queryResult = db
                        .filter(new QueryFilter(parser.getQuery()));
                System.out.print(StudentDatabase.printTable(queryResult));
            }
        }
        inputScanner.close();
        System.out.println("Goodbye!");
    }
}
