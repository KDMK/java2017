package hr.fer.zemris.java.tecaj.hw04.db;

import java.util.ArrayList;
import java.util.List;

/**
 * Query lexer builds tokens from given input query. Valid token types for query
 * are defined in {@link QueryTokenType}. Last processed token is stored
 * internally.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class QueryLexer {
    /**
     * Query text.
     */
    private char[] inputQuery;
    /**
     * Last processed token returned to user.
     */
    private QueryToken curToken;
    /**
     * Index of next character for processing.
     */
    private int currentIndex;
    /**
     * List of valid operators. Note: combined operators are checked in lexer!
     */
    private static List<Character> operators;

    static {
        operators = new ArrayList<>(4);
        operators.add('=');
        operators.add('!');
        operators.add('<');
        operators.add('>');
    }

    /**
     * Default constructor.
     *
     * @param input text of a query.
     */
    public QueryLexer(String input) {
        if (input == null) {
            throw new IllegalArgumentException(
                    "Null is not valid input argument!");
        }
        this.inputQuery = input.toCharArray();
    }

    /**
     * Generates and return next token from input query.
     *
     * @return Currently generated token.
     */
    public QueryToken nextToken() {
        if (curToken != null && curToken.getType() == QueryTokenType.END) {
            throw new QueryLexerException("No more tokens in input file.");
        }
        return generateToken();
    }

    /**
     * Groups next n characters in text token.
     *
     * @return Next token from query input.
     */
    private QueryToken generateToken() {
        StringBuilder curInputToken = new StringBuilder();

        if (currentIndex <= inputQuery.length) {
            skipBlanks();
            if (currentIndex >= inputQuery.length
                    && curInputToken.toString().equals("")) {
                curToken = new QueryToken(QueryTokenType.END, null);
                return curToken;
            }
            char curInputCharacter = inputQuery[currentIndex++];

            throwIfInvalidDiffOperator(curInputCharacter);
            if (operators.contains(curInputCharacter)) {
                return processOperator(curInputToken, curInputCharacter);
            }

            if (curInputCharacter == '"') {
                return buildString(curInputToken);
            }

            if (Character.isLetter(curInputCharacter)) {
                curInputToken.append(curInputCharacter);
                buildKeyword(curInputToken);

                curToken = generateKeywordToken(curInputToken.toString());
                return curToken;
            }
            throw new QueryLexerException(
                    "Invalid input token: " + curInputToken.toString());
        }
        return null;
    }

    private QueryToken processOperator(StringBuilder curInputToken, char curInputCharacter) {
        curInputToken.append(curInputCharacter);
        if (currentIndex < inputQuery.length && inputQuery[currentIndex] == '='
                && (curInputCharacter == '<' || curInputCharacter == '>' || curInputCharacter == '!')) {
            curInputToken.append(inputQuery[currentIndex++]);
        }

        skipBlanks();
        if (currentIndex < inputQuery.length && inputQuery[currentIndex] != '"') {
            throw new QueryLexerException("Invalid token " + inputQuery[currentIndex] + " at position " + (currentIndex));
        }

        curToken = new QueryToken(QueryTokenType.OPERATION, curInputToken.toString());
        return curToken;
    }

    private void throwIfInvalidDiffOperator(char curInputCharacter) {
        if (curInputCharacter == '!' && (currentIndex >= inputQuery.length || inputQuery[currentIndex] != '=')) {
            throw new QueryLexerException(
                    "Invalid token !" + curInputCharacter + " at position "
                            + (currentIndex - 1));
        }
    }

    /**
     * Builds string token value. Valid escape sequences in strings are \" and
     * \\.
     *
     * @param curInputToken String builder that stores current token that we are
     *                      processing.
     * @throws QueryLexerException if input contains invalid escape sequences or if quote marks
     *                             are not closed.
     */
    private QueryToken buildString(StringBuilder curInputToken) {
        while (currentIndex < inputQuery.length) {
            if (inputQuery[currentIndex] == '\"') {
                currentIndex++;
                curToken = new QueryToken(QueryTokenType.STRING, curInputToken.toString());
                return curToken;
            }

            curInputToken.append(inputQuery[currentIndex++]);
        }
        throw new QueryLexerException("Closing double qoute mark not found!");
    }

    /**
     * Builds keyword token value. Valid keyword are field names, "AND" and
     * "LIKE". All keywords are case insensitive.
     *
     * @param curInputToken String builder that stores current token that we are
     *                      processing.
     * @throws QueryLexerException if input is not valid keyword.
     */
    private void buildKeyword(StringBuilder curInputToken) {
        while (currentIndex < inputQuery.length
                && !Character.isWhitespace(inputQuery[currentIndex])
                && !operators.contains(inputQuery[currentIndex])) {
            if (Character.isLetterOrDigit(inputQuery[currentIndex])
                    || inputQuery[currentIndex] == '_') {
                curInputToken.append(inputQuery[currentIndex++]);
            } else {
                throw new QueryLexerException(
                        "Invalid field name: " + curInputToken.toString());
            }
        }
    }

    /**
     * Skips blanks in character sequence. Characters that are interpreted as
     * blanks are: <code>' ', '\n', '\r', '\n'</code>
     */
    private void skipBlanks() {
        while (currentIndex < inputQuery.length) {
            if (isBlank(inputQuery[currentIndex])) {
                currentIndex++;
                continue;
            }
            return;
        }
    }

    /**
     * Builds and returns correct token base on passed token name.
     *
     * @param tokenValue value of token.
     * @return desired Token.
     */
    private QueryToken generateKeywordToken(String tokenValue) {
        if (tokenValue.equals("LIKE")) {
            curToken = new QueryToken(QueryTokenType.OPERATION, tokenValue);
            return curToken;
        }
        if (tokenValue.equalsIgnoreCase("and")) {
            curToken = new QueryToken(QueryTokenType.AND, tokenValue);
            return curToken;
        }
        return new QueryToken(QueryTokenType.FIELD, tokenValue);
    }

    /**
     * Checks if current character is blank character.
     *
     * @param curCharacter <code>char</code> value that is checked
     * @return <code>true</code> if next character is blank character
     */
    private boolean isBlank(char curCharacter) {
        return curCharacter == ' ' || curCharacter == '\r'
                || curCharacter == '\n' || curCharacter == '\t';
    }
}
