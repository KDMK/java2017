package hr.fer.zemris.java.tecaj.hw04.db;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw04.collections.SimpleHashtable;

/**
 * Model of a simple student database. Student database models a table(if we
 * look on it as relational database) of students with their final grades.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class StudentDatabase {
    /**
     * Indexed collection of student records.
     */
    private List<StudentRecord> tableDb;
    /**
     * Hashed collection of student records. Used when we want to query by
     * jmbag.
     */
    private SimpleHashtable<String, StudentRecord> jmbagIndexDb;

    /**
     * @return reference to list that contains all databases table entry.
     */
    public List<StudentRecord> getTableDb() {
        return this.tableDb;
    }

    /**
     * Default constructor. Creates new indexed collection and hashed(by JMBAG)
     * collection from data given as an argument.
     *
     * @param data
     *            Student records in textual format.
     */
    public StudentDatabase(List<String> data) {
        tableDb = new ArrayList<>();
        jmbagIndexDb = new SimpleHashtable<>();

        for (String entry : data) {
            String[] student = entry.split("\t");
            if (student.length != 4) {
                throw new IllegalArgumentException(
                        "Wrong input line format: " + entry);
            }
            // 0 -> JMBAG , 1 -> lastName, 2 -> firstName, 3 -> finalGrade
            StudentRecord newStudent = new StudentRecord(student[0], student[1], student[2], student[3]);
            // Check if jmbag is already in DB; If it does, only update its
            // value
            if (jmbagIndexDb.containsKey(student[0])) {
                throw new IllegalArgumentException("Jmbag already in table!");
            }
            tableDb.add(newStudent);
            jmbagIndexDb.put(newStudent.getJmbag(), newStudent);
        }
    }

    /**
     * Returns student with given jmbag. If jmbag doesn't exist in database
     * method returns null.
     *
     * @param jmbag
     *            of desired student.
     * @return StudentRecord with given jmbag or <code>null</code> if it doesn't
     *         exist.
     */
    public StudentRecord forJMBAG(String jmbag) {
        return jmbagIndexDb.get(jmbag);
    }

    /**
     * Get all student records from database which complies with desired filter.
     *
     * @param filter
     *            IFilter interface instance
     * @return All StudentRecords that comply with filter
     */
    public List<StudentRecord> filter(IFilter filter) {
        List<StudentRecord> retList = new ArrayList<>();

        for (StudentRecord r : tableDb) {
            if (filter.accepts(r)) {
                retList.add(r);
            }
        }

        return retList;
    }

    /**
     * Creates formatted output for single student entry.
     *
     * @param student
     *            single student entry
     * @return formatted output
     */
    public static String printStudent(StudentRecord student) {
        ArrayList<StudentRecord> singleStudent = new ArrayList<>();
        singleStudent.add(student);

        String output = printTable(singleStudent);
        singleStudent.clear();
        singleStudent = null;

        return output;
    }

    /**
     * Formats table and returns string representation of table.
     *
     * @param table
     *            which we want to print.
     * @return String with formatted table.
     */
    public static String printTable(List<StudentRecord> table) {
        StringBuilder sb = new StringBuilder();
        if (table.size() != 0) {
            int maxNameLen = 0;
            int maxLastNameLen = 0;
            int maxJmbagLen = 0;
            int maxGradeLen = 0;

            // Not efficient but its working
            for (StudentRecord r : table) {
                if (r.getFirstName().length() > maxNameLen) {
                    maxNameLen = r.getFirstName().length();
                }
                if (r.getLastName().length() > maxLastNameLen) {
                    maxLastNameLen = r.getLastName().length();
                }
                if (r.getJmbag().length() > maxJmbagLen) {
                    maxJmbagLen = r.getJmbag().length();
                }
                if (r.getFinalGrade().length() > maxGradeLen) {
                    maxGradeLen = r.getFinalGrade().length();
                }
            }

            // Create all formats
            String headerNameField = String.format("%0" + maxNameLen + "d", 0)
                    .replaceAll("0", "=");
            String headerLastNameField = String
                    .format("%0" + maxLastNameLen + "d", 0)
                    .replaceAll("0", "=");

            String nameFieldFormat = "%-" + maxNameLen + "s";
            String lastNameFieldFormat = "%-" + maxLastNameLen + "s";
            String jmbagFieldFormat = "%-" + maxJmbagLen + "s";
            String finalGradeFieldFormat = "%-" + maxGradeLen + "s";

            String tableRowFormat = "| " + jmbagFieldFormat + " | "
                    + lastNameFieldFormat + " | " + nameFieldFormat + " | "
                    + finalGradeFieldFormat + " |%n";
            String tableHeaderFooter = "+============+=" + headerLastNameField
                    + "=+=" + headerNameField + "=+===+"
                    + System.lineSeparator();

            // Generate output
            sb.append(tableHeaderFooter);
            for (StudentRecord r : table) {
                sb.append(String.format(tableRowFormat, r.getJmbag(),
                        r.getLastName(), r.getFirstName(), r.getFinalGrade()));
            }
            sb.append(tableHeaderFooter);
        }
        sb.append("Record selected: ").append(table.size()).append(System.lineSeparator());

        return sb.toString();
    }

    @Override
    public String toString() {
        return printTable(tableDb);
    }
}
