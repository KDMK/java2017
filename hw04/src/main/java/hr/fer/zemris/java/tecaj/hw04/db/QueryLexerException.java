package hr.fer.zemris.java.tecaj.hw04.db;

/**
 * QueryLexerException exception class models exception that can occur in
 * {@link hr.fer.zemris.java.tecaj.hw04.db.QueryParser Parser} and it's specific
 * to lexical analysis.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class QueryLexerException extends RuntimeException{
    /**
     * Serializable class version number.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Does not take any arguments.
     */
    public QueryLexerException() {
        super();
    }

    /**
     * Constructor that accepts message.
     *
     * @param message
     *            Error message that will be displayed.
     */
    public QueryLexerException(String message) {
        super(message);
    }

    /**
     * Constructor that creates exception using cause object.
     *
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public QueryLexerException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor that creates exception using cause and string.
     *
     * @param message
     *            Error message that will be displayed.
     * @param cause
     *            <code>Throwable</code> that caused exception
     */
    public QueryLexerException(String message, Throwable cause) {
        super(message, cause);
    }
}
