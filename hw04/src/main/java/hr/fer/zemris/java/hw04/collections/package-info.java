/**
 * User implementations of standard Java Collection API data structures.
 */
package hr.fer.zemris.java.hw04.collections;
