package hr.fer.zemris.java.tecaj.hw04.db;

import java.util.regex.Pattern;

/**
 * Class contains definition and functionality of supported operations.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class ComparisonOperators {
    /**
     * Defines operator for less than operation.
     */
    public static final IComparisonOperator LESS;
    /**
     * Defines operator for less or equals than operation.
     */
    public static final IComparisonOperator LESS_OR_EQUALS;
    /**
     * Defines operator for greater than operation.
     */
    public static final IComparisonOperator GREATER;
    /**
     * Defines operator for less greater or equals operation.
     */
    public static final IComparisonOperator GREATER_OR_EQUALS;
    /**
     * Defines operator for equals operation.
     */
    public static final IComparisonOperator EQUALS;
    /**
     * Defines operator for not equals operation.
     */
    public static final IComparisonOperator NOT_EQUALS;
    /**
     * Defines operator for like operation.
     */
    public static final IComparisonOperator LIKE;

    static {
        LESS = (value1, value2) -> value1.compareTo(value2) < 0;
        LESS_OR_EQUALS = (value1, value2) -> value1.compareTo(value2) <= 0;
        GREATER = (value1, value2) -> value1.compareTo(value2) > 0;
        GREATER_OR_EQUALS = (value1, value2) -> value1.compareTo(value2) >= 0;
        EQUALS = (value1, value2) -> value1.equals(value2);
        NOT_EQUALS = (value1, value2) -> !value1.equals(value2);
        LIKE = (value1, value2) -> {
            value2 = value2.replaceFirst("\\*", ".*");
            return Pattern.matches(value2, value1);
        };
    }
}
