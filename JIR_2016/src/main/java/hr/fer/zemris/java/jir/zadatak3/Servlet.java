package hr.fer.zemris.java.jir.zadatak3;

import hr.fer.zemris.java.jir.zadatak1.Analizator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by matija on 9/1/17.
 */
@WebServlet({"/", "/index.jsp"})
public class Servlet extends HttpServlet {
    private List<String> selledItems;

    @Override
    public void init() throws ServletException {
        Analizator analyser = new Analizator();
        Path servletContextPath = Paths.get(getServletContext().getRealPath("."));
        analyser.process(Paths.get(servletContextPath.toString() + "/WEB-INF/inputData").toFile());

        getServletContext().setAttribute("analizator", analyser);

        this.selledItems = analyser.getAllProducts();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("selledItems", selledItems);

        req.getRequestDispatcher("WEB-INF/index.jsp").forward(req,resp);
    }
}
