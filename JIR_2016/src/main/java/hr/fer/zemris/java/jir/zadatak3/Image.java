package hr.fer.zemris.java.jir.zadatak3;

import hr.fer.zemris.java.jir.zadatak2.MyBarChart;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * Created by matija on 9/3/17.
 */
@WebServlet("/image")
public class Image extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Map<String, Integer> displayData = (Map<String, Integer>) getServletContext().getAttribute
                ("displayData");
        int totalCount = (Integer) getServletContext().getAttribute("totalCount");
        String selectedItem = (String) getServletContext().getAttribute("selectedItem");

        BufferedImage image = createGraphics(displayData, totalCount, selectedItem);

        resp.setContentType("image/png");
        ImageIO.write(image, "png", resp.getOutputStream());
    }

    private BufferedImage createGraphics(Map<String, Integer> data, int totalItems, String
            selectedItem) {
        BufferedImage outImage = new BufferedImage(500, 500, BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g2d = (Graphics2D) outImage.getGraphics();
        MyBarChart.paintGraph(500, 500, g2d, data, totalItems, selectedItem);

        return outImage;
    }
}
