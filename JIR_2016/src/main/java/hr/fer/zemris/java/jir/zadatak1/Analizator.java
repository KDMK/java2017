package hr.fer.zemris.java.jir.zadatak1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by matija on 8/31/17.
 */
public class Analizator {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private List<AnalyzerFileVisitor> analyzedStructures;
    private List<String> allDates;
    private List<String> allProducts;

    public Analizator() {
        this.analyzedStructures = new ArrayList<>();
        this.allProducts = new ArrayList<>();
        this.allDates = new ArrayList<>();
    }

    public void process(File inputDirectory) {
        if(!inputDirectory.isDirectory()) {
            System.out.println("[ERROR] Input path is not directory.");
            System.exit(-1);
        }

        File[] inputDirectoryContent = inputDirectory.listFiles();
        for(File file: inputDirectoryContent) {
            if(file.isDirectory()) {
                AnalyzerFileVisitor analyser = new AnalyzerFileVisitor(file.getName());

                try {
                    Files.walkFileTree(file.toPath(), analyser);
                } catch (IOException e) {
                    System.out.println("[ERROR] Unexpected error while traversing file tree. " +
                                       e.getMessage());
                    continue;
                }

                allDates.add(file.getName());
                analyser.getSelledItems().forEach((key , value) -> {
                    if(!allProducts.contains(key)) {
                        allProducts.add(key);
                    }
                });

                analyzedStructures.add(analyser);
            }
        }

        allDates.sort((o1, o2) -> {
            Date date1 = null;
            Date date2 = null;

            try {
                date1 = dateFormat.parse(o1);
                date2 = dateFormat.parse(o2);
            } catch (ParseException e) {
                System.out.println("[ERROR] Cannot parse date " + e.getMessage());
                System.exit(-1);
            }

            return date1.compareTo(date2);
        });
    }

    private void printOutput(File outputDirectory) {
        if(!outputDirectory.exists()) {
            if(!outputDirectory.mkdir()) {
                System.out.println("[ERROR] Cannot create " + outputDirectory.getAbsolutePath());
                System.exit(-1);
            }
        }

        if(!outputDirectory.isDirectory()) {
            System.out.println("[ERROR] Output path is not directory.");
            System.exit(-1);
        }

        for(AnalyzerFileVisitor visitedFile: this.analyzedStructures) {
            File outputLogFile = createFile(outputDirectory.toString() + "/" + visitedFile.getDirecotoryName() +
                                            ".log");
            if(outputLogFile == null) return;

            File outputInfoFile = createFile(outputDirectory.toString() + "/" + visitedFile.getDirecotoryName() +
                                             ".info");
            if(outputInfoFile == null) return;

            writeToFile(outputLogFile, visitedFile.toLogString());
            writeToFile(outputInfoFile, visitedFile.toInfoString());
        }
    }

    public static void main(String[] args) {
        if(args.length != 2) {
            System.out.println("[ERROR] Two arguments were expected.");
            System.exit(-1);
        }

        File inputDirectory = Paths.get(args[0]).toAbsolutePath().toFile();
        File outputDirectory = Paths.get(args[1]).toAbsolutePath().toFile();

        Analizator analyser = new Analizator();
        analyser.process(inputDirectory);
        analyser.printOutput(outputDirectory);

        System.out.println(analyser.allDates.toString());
        System.out.println(analyser.allProducts.toString());

        System.out.println("[INFO] Succesfully created all logs.");
    }

    private static File createFile(String fileName) {
        File outputLogFile = new File(fileName);

        if(!outputLogFile.exists()) {
            try {
                System.out.println("[INFO] Creating file " + outputLogFile.getName());
                outputLogFile.createNewFile();
            } catch (IOException e) {
                System.out.println("[ERROR] Cannot create " +
                                   e.getMessage());
                return null;
            }
        }

        return  outputLogFile;
    }

    private static void writeToFile(File outputFile, String outputData) {
        try(OutputStream fos = new FileOutputStream(outputFile, false)) {
            System.out.println("[INFO] Writing data to file...");
            fos.write(outputData.getBytes(StandardCharsets.UTF_8));
            fos.flush();
        } catch (IOException e) {
            System.out.println("[ERROR] Cannot find " +
                               e.getMessage());
            return;
        }

        System.out.println("[INFO] Succesfully created " + outputFile.getName());
    }

    public List<String> getAllDates() {
        return allDates;
    }

    public List<String> getAllProducts() {
        return allProducts;
    }

    public List<AnalyzerFileVisitor> getAnalyzedStructures() {
        return analyzedStructures;
    }
}
