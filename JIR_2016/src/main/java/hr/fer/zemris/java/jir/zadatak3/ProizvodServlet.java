package hr.fer.zemris.java.jir.zadatak3;

import hr.fer.zemris.java.jir.zadatak1.Analizator;
import hr.fer.zemris.java.jir.zadatak2.MyBarChart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by matija on 9/3/17.
 */

@WebServlet("/proizvod")
public class ProizvodServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Analizator analyser = (Analizator) getServletContext().getAttribute("analizator");
        String selectedItem = req.getParameter("selectedItem");
        int[] totalCount = {0};

        Map<String, Integer> displayData = MyBarChart.createDisplayData(selectedItem, analyser
                .getAnalyzedStructures(), totalCount);

        getServletContext().setAttribute("displayData", displayData);
        getServletContext().setAttribute("totalCount", totalCount[0]);
        getServletContext().setAttribute("selectedItem", selectedItem);

        req.setAttribute("itemData", displayData);
        req.setAttribute("item", selectedItem);

        req.getRequestDispatcher("WEB-INF/prikaz.jsp").forward(req, resp);
    }

}
