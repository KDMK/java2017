package hr.fer.zemris.java.jir.zadatak1;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created by matija on 8/31/17.
 */
public class AnalyzerFileVisitor extends SimpleFileVisitor<Path> {
    private HashMap<String, Integer> selledItems;
    private List<String> allFilesLog;
    private String directoryName;

    public String getDirecotoryName() {
        return directoryName;
    }

    public Map<String, Integer> getSelledItems() {
        return Collections.unmodifiableMap(this.selledItems);
    }

    public AnalyzerFileVisitor(String directoryName) {
        this.selledItems = new HashMap<>();
        this.allFilesLog = new ArrayList<>();
        this.directoryName = directoryName;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        List<String> inputFileLines = Files.readAllLines(file);

        for(String line: inputFileLines) {
            String[] arguments = line.split("\t");
            if(arguments.length != 2) continue;

            String item = arguments[0].trim();
            Integer itemSellCount = null;
            try {
                itemSellCount = Integer.parseInt(arguments[1]);
            } catch (NumberFormatException ignorable) {
                String errorMessage = String.format("[ERROR] In file %s - Cannot parse line: " +
                                                    "\"%s\"", file.getFileName(), line);

                System.out.println(errorMessage);
                continue;
            }

            allFilesLog.add(line);
            selledItems.put(item, selledItems.getOrDefault(item, 0) + itemSellCount);
        }

        return FileVisitResult.CONTINUE;
    }

    public String toInfoString() {
        StringBuilder sb = new StringBuilder();

        for(Map.Entry selledItem: selledItems.entrySet()) {
            String formattedData = String.format("%s\t%d%n", selledItem.getKey(), selledItem
                    .getValue());
            sb.append(formattedData);
        }

        return sb.toString();
    }

    public String toLogString() {
        StringBuilder sb = new StringBuilder();

        for(String line: allFilesLog) {
            sb.append(line + System.lineSeparator());
        }

        return sb.toString();
    }
}
