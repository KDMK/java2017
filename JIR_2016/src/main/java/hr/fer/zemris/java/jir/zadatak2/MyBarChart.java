package hr.fer.zemris.java.jir.zadatak2;

import hr.fer.zemris.java.jir.zadatak1.Analizator;
import hr.fer.zemris.java.jir.zadatak1.AnalyzerFileVisitor;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by matija on 9/1/17.
 */
public class MyBarChart extends JComponent {
    private static Color[] colors = {Color.blue, Color.green, Color.pink, Color.red, Color
            .yellow};

    private String activeItem;
    private Analizator analyser;

    public void setActiveItem(Object item) {
        if(item instanceof String) {
            if(activeItem != null && activeItem.equals(item)) return;
            this.activeItem = (String)item;
        }

        repaint();
    }

    public void setAnalyser(Analizator analyser) {
        this.analyser = analyser;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fm = g2d.getFontMetrics();

        if(activeItem == null) {
            g2d.setColor(Color.white);
            g2d.fillRect(0,0,getWidth(),getHeight());
            g2d.setColor(Color.black);
            g2d.drawString("Nothing to draw.",
                         getWidth()/2 - fm.stringWidth("Nothing to draw.") / 2,getHeight()/2);
            return;
        }

        if(analyser != null) {
            int[] totalItemsCount = { 0 };
            List<AnalyzerFileVisitor> analysedFiles = analyser.getAnalyzedStructures();
            Map<String, Integer> displayData = createDisplayData(activeItem, analysedFiles, totalItemsCount);

            paintGraph(getWidth(), getHeight(), g2d, displayData, totalItemsCount[0], activeItem);
        }
    }

    @Override
    public void repaint() {
        Graphics g = getGraphics();

        g.setColor(Color.BLUE);
        paint(g);
    }

    public static Map<String, Integer> createDisplayData(String item, List<AnalyzerFileVisitor>
            analysedFiles, int[] totalItemsCount){
        Map<String, Integer> displayData = new HashMap<>();

        for(AnalyzerFileVisitor file: analysedFiles) {
            Integer numOfItems = file.getSelledItems().get(item);
            if (numOfItems == null) continue;

            totalItemsCount[0] += numOfItems;
            displayData.put(file.getDirecotoryName(), numOfItems);
        }

        return displayData;
    }

    public static void paintGraph(int width, int height, Graphics2D g2d, Map<String, Integer>
            displayData, int
            totalItemsCount, String label) {
        FontMetrics fm = g2d.getFontMetrics();
        double drawableHeightBlock = height / (double)totalItemsCount;

        int i = 0;
        int y = height;
        for(Map.Entry<String, Integer> data: displayData.entrySet()) {
            int curHeight = (int) Math.round(drawableHeightBlock * data.getValue());
            y -= curHeight;
            g2d.setColor(MyBarChart.colors[i++ % colors.length]);
            g2d.fillRect(0, y, width, curHeight);

            String curLabel = String.format("%s  number of selled %s is %d", label, data.getKey(),
                                            data.getValue());
            g2d.setColor(Color.BLACK);
            g2d.drawString(curLabel, width/2 - fm.stringWidth(curLabel) / 2,y + curHeight/2);
        }
    }
}
