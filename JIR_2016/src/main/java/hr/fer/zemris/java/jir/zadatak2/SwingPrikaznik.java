package hr.fer.zemris.java.jir.zadatak2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by matija on 8/31/17.
 */
@SuppressWarnings("ALL")
public class SwingPrikaznik extends JFrame {
    private WindowButtons buttons;

    JList items;
    JList dates;
    MyBarChart chart;

    public SwingPrikaznik() {
        // enable anti-aliased text:
        System.setProperty("awt.useSystemAAFontSettings","on");
        System.setProperty("swing.aatext", "true");

        initGUI();
    }

    private void initGUI() {
        this.buttons = new WindowButtons(this);
        createMenuBar();
        createLayout();

        setTitle("Swing prikaznik");
        setSize(800,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }


    private void createMenuBar() {
        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        file.add(buttons.openMenuItem);
        file.addSeparator();
        file.add(buttons.exitMenuItem);

        menubar.add(file);

        setJMenuBar(menubar);
    }

    private void createLayout() {
        this.items = new JList(new DefaultListModel());
        this.dates = new JList(new DefaultListModel());
        this.chart = new MyBarChart();

        items.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dates.disable();

        items.addListSelectionListener(e -> {
            chart.setActiveItem(((JList)e.getSource()).getSelectedValue());
        });

        JPanel content = new JPanel();
        JScrollPane itemsScrollPane = new JScrollPane(items);
        JScrollPane datesScrollPane = new JScrollPane(dates);

        BorderLayout windowLayout = new BorderLayout();
        content.setLayout(windowLayout);
        content.add(itemsScrollPane, windowLayout.WEST);
        content.add(datesScrollPane, windowLayout.EAST);
        content.add(chart, windowLayout.CENTER);

        setContentPane(content);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            SwingPrikaznik window = new SwingPrikaznik();
            window.setVisible(true);
        });
    }
}
