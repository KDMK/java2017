package hr.fer.zemris.java.jir.zadatak2;

import hr.fer.zemris.java.jir.zadatak1.Analizator;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by matija on 8/29/17.
 */
public class Actions {
    private SwingPrikaznik windowReference;
    private Analizator analyser;

    public Actions(SwingPrikaznik windowReference) {
        this.windowReference = windowReference;
    }

    @SuppressWarnings("Duplicates")
    public ActionListener loadFile = e -> {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(Paths.get(".").toFile().getAbsoluteFile());
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int status = fileChooser.showDialog(null,"Load file");

        if(status == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "No files selected.");
            return;
        }

        if(status == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "An error has occured while opening " +
                                                           "file", "Error!", JOptionPane
                    .ERROR_MESSAGE);
        }

        analyser = new Analizator();
        try {
            analyser.process(fileChooser.getSelectedFile());
        } catch (Exception e1) {
            JOptionPane.showMessageDialog(windowReference, "An error occured while trying to " +
                                                           "parse input file.", "Error!",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        DefaultListModel datesModel = createDefaultListModel(analyser.getAllDates());
        DefaultListModel productsModel = createDefaultListModel(analyser.getAllProducts());

        windowReference.dates.setModel(datesModel);
        windowReference.items.setModel(productsModel);
        windowReference.chart.setAnalyser(analyser);
    };

    private DefaultListModel createDefaultListModel(List myList) {
        DefaultListModel myListModel = new DefaultListModel();

        myList.forEach(o -> {
            myListModel.addElement(o);
        });

        return myListModel;
    }

    public ActionListener exitProgram = (e) -> {
        System.exit(0);
    };

}
