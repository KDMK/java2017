<%--
  Created by IntelliJ IDEA.
  User: matija
  Date: 9/2/17
  Time: 11:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
    <h2> Title </h2>
    <form name="f1" action="proizvod" method="GET">
        <select name="selectedItem">
            <c:forEach var="item" items="${selledItems}">
                <option value="${item}">${item}</option>
            </c:forEach>
        </select>
        <button>Submit</button>
    </form>
</body>
</html>
