<%--
  Created by IntelliJ IDEA.
  User: matija
  Date: 9/2/17
  Time: 11:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<h2> ${item} </h2>

    <table>
        <tr>
            <th>Date</th>
            <th>Number of selled items</th>
        </tr>
            <c:forEach var="item" items="${itemData}">
                <tr>
                    <td>${item.key}</td>
                    <td>${item.value}</td>
                </tr>
            </c:forEach>

    </table>
    <br>
    <br>
    <img src="image" />
    <a href="/analizator">Back</a>
</form>
</body>
</html>
