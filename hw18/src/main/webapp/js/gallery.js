var curLocation;
var picBlock = document.getElementById("picContainer");

function addButton(name) {
  //Create an input type dynamically.
  var element = document.createElement("a");
  //Assign different attributes to the element.
  element.innerHTML = toTitleCase(name);
  element.name = name;
  element.onclick = function() {
		curLocation = name;
		getImages(name);
  };

  var btnBlock = document.getElementById("buttonBlock");
  //Append the element in page (in span).
	let listItem = document.createElement("li");
	listItem.appendChild(element);
  btnBlock.appendChild(listItem);
}

function removeChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild);
	}
}

function addImageThumbnail(base, location, tag) {
	var element = document.createElement("img");

	element.src = base + location;
	element.width = 150;
	element.height = 150;
	element.classList.add("imageSmall");

	element.onclick = () => {
		removeChildren(picBlock);

		var picture = document.createElement("img");
		picture.id = "displayedImage";
		picture.src = './images/' + location;
		picture.classList.add("imageLarge")
		picture.onclick = () => {
			getImages(curLocation);
		};

		picBlock.appendChild(picture);
		$("#displayedImage").hide().appendTo("#picContainer").fadeIn(1000);

		getImageInfo(location);
	}

	picBlock.appendChild(element);
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function getImageInfo(location) {
	$.ajax({
		url: 'http://localhost:8080/jsaplikacija/servlets/img/' + location,
		type: 'GET',
		dataType: 'JSON',
		success: (data) => {
			var description = document.createElement("p");
			var tags = document.createElement("p");

			description.innerHTML = data.description;
			description.classList.add("text");
			tags.innerHTML = data.tags;
			tags.classList.add("text");

			picBlock.appendChild(description);
			picBlock.appendChild(tags);
		}
	});
}

function getImages(tag) {
	$.ajax({
		url: 'http://localhost:8080/jsaplikacija/servlets/tags/' + tag,
		type: 'GET',
		dataType: 'JSON',
		success: (data) => {
			// First remove old children
			removeChildren(picBlock);

			data.forEach((element) => {
				addImageThumbnail("./thumbs/", element.location, name);
			})
		}
	});
}

window.onload = () => {
	$.ajax({
		url: 'http://localhost:8080/jsaplikacija/servlets/tags',
		type: 'GET',
		dataType: 'JSON',
		success: (data) => {
			data.forEach((element) => {
				addButton(element);
			})
		}
	});
}
