package hr.fer.zemris.jsdemo.servlets;

/**
 * Model of photol
 * 
 * @author Matija Bartolac
 *
 */
public class Photo {
	/**
	 * Name of image file.
	 */
	private String location;
	/**
	 * Short description.
	 */
	private String description;
	/**
	 * Array of image tags.
	 */
	private String[] tags;

	/**
	 * Default constructor.
	 * 
	 * @param location image file location
	 * @param description image description
	 * @param tags array of image tags
	 */
	public Photo(String location, String description, String ... tags) {
		this.location = location;
		this.description = description;
		this.tags = tags;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return array of tags
	 */
	public String[] getTags() {
		return tags;
	}
}
