package hr.fer.zemris.jsdemo.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet returns all tags that are present on all images(without duplicates).
 * Tags are returned as json object.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet(urlPatterns = { "/servlets/tags" })
public class GetTagsServlet extends HttpServlet {
    /**
     * Relative path to file that contains pictures data
     */
    private static final String desc = "/WEB-INF/opisnik.txt";

    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings({ "unchecked", "deprecation" })
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Set<String> allTags;
        List<Photo> photos;

        Object obj = req.getSession().getAttribute("tags");
        if (obj != null) {
            allTags = (Set<String>) obj;
            photos = (List<Photo>) req.getSession().getAttribute("photos");
        } else {
            allTags = new HashSet<>();
            photos = new ArrayList<>();

            Path p = Paths.get(req.getRealPath(desc));
            List<String> lines = Files.readAllLines(p);

            if ((lines.size() % 3) != 0) {
                System.out.println("Wrong descriptor.");
            }

            for (int i = 0; i + 3 < lines.size(); i += 3) {
                String location = lines.get(i);
                String description = lines.get(i + 1);
                String[] tags = lines.get(i + 2).split(",\\s*");

                for (String tag : tags) {
                    allTags.add(tag);
                }

                Photo newPhoto = new Photo(location, description, tags);
                photos.add(newPhoto);
            }

            req.getSession().setAttribute("tags", allTags);
            req.getSession().setAttribute("photos", photos);
        }

        resp.setContentType("application/json;charset=UTF-8");

        Gson gson = new Gson();
        String[] tagsArray = new String[allTags.size()];
        String jsonText = gson.toJson(allTags.toArray(tagsArray));

        resp.getWriter().write(jsonText);

        resp.getWriter().flush();
    }

}
