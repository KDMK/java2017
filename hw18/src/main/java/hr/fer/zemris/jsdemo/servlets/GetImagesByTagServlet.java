package hr.fer.zemris.jsdemo.servlets;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet returns thumbnails of images that corresponds to requested tag.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet(urlPatterns = { "/servlets/tags/*" })
public class GetImagesByTagServlet extends HttpServlet {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Path to icon images.
     */
    private static final String basePath = "thumbs/";
    /**
     * Path to full images.
     */
    private static final String imagesPath = "images/";

    @SuppressWarnings({ "unchecked", "deprecation" })
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        List<Photo> photos = (List<Photo>) req.getSession()
                .getAttribute("photos");
        List<Photo> requestedPhotos = new ArrayList<>();

        String tag = req.getPathInfo().substring(1);

        photos.forEach((e) -> {
            for (String s : e.getTags()) {
                if (s.equalsIgnoreCase(tag)) {
                    requestedPhotos.add(e);
                    break;
                }
            }
        });

        Path thumbsRoot = Paths.get(req.getRealPath(basePath));
        Path imagesRoot = Paths.get(req.getRealPath(imagesPath));

        if (!thumbsRoot.toFile().exists()) {
            thumbsRoot.toFile().mkdir();
        }

        requestedPhotos.forEach((e) -> {
            Path p = thumbsRoot.resolve(e.getLocation());

            if (!p.toFile().exists()) {
                BufferedImage largeImage;
                try {
                    largeImage = ImageIO
                            .read(imagesRoot.resolve(e.getLocation()).toFile());
                } catch (IOException e1) {
                    return;
                }

                Image smallImage = largeImage.getScaledInstance(150, 150,
                        Image.SCALE_SMOOTH);
                BufferedImage bufImage = convertToBufferedImage(smallImage);

                try {
                    ImageIO.write(bufImage, p.toString().split("\\.")[1],
                            p.toFile());
                } catch (IOException e1) {
                    e1.printStackTrace();
                    return;
                }
            }
        });

        resp.setContentType("application/json;charset=UTF-8");

        Gson gson = new Gson();
        String jsonText = gson.toJson(requestedPhotos.toArray());

        resp.getWriter().write(jsonText);

        resp.getWriter().flush();
    }

    /**
     * Converts image object to buffered image object.
     * 
     * @param image
     *            reference to image object
     * @return new BuffredImage
     */
    public static BufferedImage convertToBufferedImage(Image image) {
        BufferedImage newImage = new BufferedImage(image.getWidth(null),
                image.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g = newImage.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return newImage;
    }

}
