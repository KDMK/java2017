package hr.fer.zemris.jsdemo.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet for getting image data from server. Image data is passed as json
 * object.
 * 
 * @author Matija Bartolac
 *
 */
@WebServlet(urlPatterns = { "/servlets/img/*" })
public class GetImageServlet extends HttpServlet {
    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        @SuppressWarnings("unchecked")
        List<Photo> photos = (List<Photo>) req.getSession()
                .getAttribute("photos");
        Photo requestedPhoto = null;

        String tag = req.getPathInfo().substring(1);

        for (Photo p : photos) {
            if (p.getLocation().equalsIgnoreCase(tag)) {
                requestedPhoto = p;
                break;
            }
        }

        resp.setContentType("application/json;charset=UTF-8");

        Gson gson = new Gson();
        String jsonText = gson.toJson(requestedPhoto);

        resp.getWriter().write(jsonText);

        resp.getWriter().flush();
    }
}
