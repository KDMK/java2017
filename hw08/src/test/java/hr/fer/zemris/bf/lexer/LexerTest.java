package hr.fer.zemris.bf.lexer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class LexerTest {

    @Test
    public void testNotNull() {
        Lexer lexer = new Lexer("");

        assertNotNull("Token was expected but null was returned.",
                lexer.nextToken());
    }

    @Test(expected = LexerException.class)
    public void testNullInput() {
        new Lexer(null);
    }

    @Test
    public void testEmpty() {
        Lexer lexer = new Lexer("");

        assertEquals("Empty input must generate only EOF token.", TokenType.EOF,
                lexer.nextToken().getTokenType());
    }

    @Test(expected = LexerException.class)
    public void testReadAfterEOF() {
        Lexer lexer = new Lexer("");

        // will obtain EOF
        lexer.nextToken();
        // will throw!
        lexer.nextToken();
    }

    @Test
    public void testValidInput1() {
        Lexer lexer = new Lexer("false");

        Token[] expected = { new Token(TokenType.CONSTANT, false),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput2() {
        Lexer lexer = new Lexer("true");

        Token[] expected = { new Token(TokenType.CONSTANT, true),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput3() {
        Lexer lexer = new Lexer("true and false");

        Token[] expected = { new Token(TokenType.CONSTANT, true),
                new Token(TokenType.OPERATOR, "and"),
                new Token(TokenType.CONSTANT, false),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput4() {
        Lexer lexer = new Lexer("A and false");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "and"),
                new Token(TokenType.CONSTANT, false),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput5() {
        Lexer lexer = new Lexer("A * b");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "and"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput6() {
        Lexer lexer = new Lexer("A or b");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "or"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput7() {
        Lexer lexer = new Lexer("A + b");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "or"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput8() {
        Lexer lexer = new Lexer("A xor b");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "xor"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput9() {
        Lexer lexer = new Lexer("A :+: b");

        Token[] expected = { new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "xor"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput10() {
        Lexer lexer = new Lexer("not b");

        Token[] expected = { new Token(TokenType.OPERATOR, "not"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput11() {
        Lexer lexer = new Lexer("(A xor b)");

        Token[] expected = { new Token(TokenType.OPEN_BRACKET, '('),
                new Token(TokenType.VARIABLE, "A"),
                new Token(TokenType.OPERATOR, "xor"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.CLOSED_BRACKET, ')'),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput12() {
        Lexer lexer = new Lexer("(A_D :+: b) or nOt (c and d)");

        Token[] expected = { new Token(TokenType.OPEN_BRACKET, '('),
                new Token(TokenType.VARIABLE, "A_D"),
                new Token(TokenType.OPERATOR, "xor"),
                new Token(TokenType.VARIABLE, "B"),
                new Token(TokenType.CLOSED_BRACKET, ')'),
                new Token(TokenType.OPERATOR, "or"),
                new Token(TokenType.OPERATOR, "not"),
                new Token(TokenType.OPEN_BRACKET, '('),
                new Token(TokenType.VARIABLE, "C"),
                new Token(TokenType.OPERATOR, "and"),
                new Token(TokenType.VARIABLE, "D"),
                new Token(TokenType.CLOSED_BRACKET, ')'),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput14() {
        Lexer lexer = new Lexer("1");

        Token[] expected = { new Token(TokenType.CONSTANT, true),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test
    public void testValidInput15() {
        Lexer lexer = new Lexer("01");

        Token[] expected = { new Token(TokenType.CONSTANT, true),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test(expected = LexerException.class)
    public void testValidInput16() {
        @SuppressWarnings("unused")
        Lexer lexer = new Lexer("0.0");
    }

    @Test
    public void testValidInput13() {
        Lexer lexer = new Lexer("0");

        Token[] expected = { new Token(TokenType.CONSTANT, false),
                new Token(TokenType.EOF, null) };

        checkTokenStream(lexer, expected);
    }

    @Test(expected = LexerException.class)
    public void testInvalidInput1() {
        Lexer lexer = new Lexer("12");

        lexer.nextToken();
    }

    @Test(expected = LexerException.class)
    public void testInvalidInput2() {
        Lexer lexer = new Lexer("-VAR");

        lexer.nextToken();
    }

    @Test(expected = LexerException.class)
    public void testInvalidInput3() {
        Lexer lexer = new Lexer("var-var");

        lexer.nextToken();
    }

    @Test(expected = LexerException.class)
    public void testInvalidInput4() {
        Lexer lexer = new Lexer("-12");

        lexer.nextToken();
    }

    @Test(expected = LexerException.class)
    public void testInvalidInput5() {
        Lexer lexer = new Lexer("=");

        lexer.nextToken();
    }

    // Helper method for checking if lexer generates the same stream of tokens
    // as the given stream.
    private void checkTokenStream(Lexer lexer, Token[] correctData) {
        int counter = 0;
        for (Token expected : correctData) {
            Token actual = lexer.nextToken();
            String msg = "Checking token " + counter + ":";
            assertEquals(msg, expected.getTokenType(), actual.getTokenType());
            assertEquals(msg, expected.getValue(), actual.getValue());
            counter++;
        }
    }

}
