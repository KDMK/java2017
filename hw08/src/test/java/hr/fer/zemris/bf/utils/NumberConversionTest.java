package hr.fer.zemris.bf.utils;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class NumberConversionTest {

    @Test
    public void testValid1() {
        byte[] expected = { 1, 1 };
        byte[] result = Util.indexToByteArray(3, 2);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid2() {
        byte[] expected = { 0, 0, 1, 1 };
        byte[] result = Util.indexToByteArray(3, 4);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid3() {
        byte[] expected = { 0, 0, 0, 0, 1, 1 };
        byte[] result = Util.indexToByteArray(3, 6);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid4() {
        byte[] expected = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 };
        byte[] result = Util.indexToByteArray(-2, 32);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid5() {
        byte[] expected = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 };
        byte[] result = Util.indexToByteArray(-2, 16);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid6() {
        byte[] expected = { 1, 0, 1, 1, 0 };
        byte[] result = Util.indexToByteArray(54, 5);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid7() {
        byte[] expected = { 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1 };
        byte[] result = Util.indexToByteArray(4865, 16);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid8() {
        byte[] expected = { 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1 };
        byte[] result = Util.indexToByteArray(4865, 13);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid9() {
        byte[] expected = { 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1 };
        byte[] result = Util.indexToByteArray(-765, 12);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testValid10() {
        byte[] expected = { 0, 0, 0, 0, 0, 0, 1, 1 };
        byte[] result = Util.indexToByteArray(-765, 8);

        assertArrayEquals(expected, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidValid1() {
        byte[] expected = { 0, 0, 0, 0, 0, 0, 1, 1 };
        byte[] result = Util.indexToByteArray(-765, 33);

        assertArrayEquals(expected, result);
    }

}
