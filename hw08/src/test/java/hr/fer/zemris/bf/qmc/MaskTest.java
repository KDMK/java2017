package hr.fer.zemris.bf.qmc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class MaskTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidConstructor1() {
        @SuppressWarnings("unused")
        Mask n = new Mask(null, new HashSet<Integer>(), false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidConstructor2() {
        byte[] b = { 1, 2, 3 };
        @SuppressWarnings("unused")
        Mask n = new Mask(b, null, false);
    }

    @Test
    public void testEquals() {
        byte[] mask = { 0, 1, 1, 1 };
        Set<Integer> indexes = new TreeSet<>();
        indexes.add(7);

        Mask m1 = new Mask(mask, indexes, false);
        Mask m2 = new Mask(mask, indexes, false);

        assertEquals(m1, m2);
    }

    @Test
    public void testNotEquals() {
        byte[] mask1 = { 0, 1, 1, 1 };
        byte[] mask2 = { 0, 1, 1, 0 };
        Set<Integer> indexes1 = new TreeSet<>();
        Set<Integer> indexes2 = new TreeSet<>();
        indexes1.add(7);
        indexes2.add(6);

        Mask m1 = new Mask(mask1, indexes1, false);
        Mask m2 = new Mask(mask2, indexes2, false);

        assertNotEquals(m1, m2);
    }

    @Test
    public void countOnes() {
        byte[] mask = { 0, 1, 1, 1 };
        Set<Integer> indexes = new TreeSet<>();
        indexes.add(7);

        Mask m1 = new Mask(mask, indexes, false);

        assertEquals(3, m1.countOfOnes());
    }

    @Test
    public void testCombineWithCombinable() {
        byte[] mask1 = { 0, 1, 1, 1 };
        byte[] mask2 = { 0, 1, 1, 0 };
        Set<Integer> indexes1 = new TreeSet<>();
        Set<Integer> indexes2 = new TreeSet<>();
        indexes1.add(7);
        indexes2.add(6);

        Mask m1 = new Mask(mask1, indexes1, false);
        Mask m2 = new Mask(mask2, indexes2, false);

        Mask m3 = m1.combineWith(m2).get();

        byte[] mask = { 0, 1, 1, 2 };
        Set<Integer> indexes = new TreeSet<>();
        indexes.add(7);
        indexes.add(6);
        Mask m4 = new Mask(mask, indexes, false);

        assertEquals(m3, m4);
    }

    @Test
    public void testCombineWithCombinable2() {
        byte[] mask1 = { 0, 1, 1, 1, 0 };
        byte[] mask2 = { 0, 1, 1, 1, 1 };
        Set<Integer> indexes1 = new TreeSet<>();
        Set<Integer> indexes2 = new TreeSet<>();
        indexes1.add(14);
        indexes2.add(15);

        Mask m1 = new Mask(mask1, indexes1, false);
        Mask m2 = new Mask(mask2, indexes2, false);

        Mask m3 = m1.combineWith(m2).get();

        byte[] mask = { 0, 1, 1, 1, 2 };
        Set<Integer> indexes = new TreeSet<>();
        indexes.add(14);
        indexes.add(15);
        Mask m4 = new Mask(mask, indexes, false);

        assertEquals(m3, m4);
    }
    
    @Test
    public void testCombineWithNotCombinable() {
        byte[] mask1 = { 0, 1, 1, 0, 0 };
        byte[] mask2 = { 0, 1, 1, 1, 1 };
        Set<Integer> indexes1 = new TreeSet<>();
        Set<Integer> indexes2 = new TreeSet<>();
        indexes1.add(12);
        indexes2.add(15);

        Mask m1 = new Mask(mask1, indexes1, false);
        Mask m2 = new Mask(mask2, indexes2, false);

        Mask m3 = m1.combineWith(m2).orElse(null);

        assertEquals(m3, null);
    }
}
