package hr.fer.zemris.bf.qmc;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.zemris.bf.utils.Util;

/**
 * Instances of this class represents specification of(maybe incomplete) boolean
 * products.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Mask {
    /**
     * Mask of current input minterm.
     */
    private byte[] mask;
    /**
     * Covered minterms.
     */
    private TreeSet<Integer> minterms;
    /**
     * Is product normal or it's don't care.
     */
    private boolean dontCare;
    /**
     * Marks if minterm is combined minterm.
     */
    private boolean combined;
    /**
     * Hash code of mask;
     */
    private int hash;

    /**
     * Constructor builds single minterm/don't-care entry.
     * 
     * @param index
     *            in truth table of desired input combination
     * @param numberOfVariables
     *            number of truth table variables
     * @param dontCare
     *            is current mask minterm or don't care
     */
    public Mask(int index, int numberOfVariables, boolean dontCare) {
        this.mask = Util.indexToByteArray(index, numberOfVariables);
        this.dontCare = dontCare;
        minterms = new TreeSet<Integer>();
        minterms.add(index);
        this.hash = Arrays.hashCode(mask);
    }

    /**
     * Constructs group of minterms/don't-cares
     * 
     * @param values
     *            mask value.
     * @param indexes
     *            index of all minterms/don't-cares with given mask.
     * @param dontCare
     *            is entry don't care or minterm.
     */
    public Mask(byte[] values, Set<Integer> indexes, boolean dontCare) {
        if (values == null) {
            throw new IllegalArgumentException("Values can't be null!");
        }
        if (indexes == null) {
            throw new IllegalArgumentException("Indexes can't be null!");
        }

        this.mask = Arrays.copyOf(values, values.length);
        this.minterms = new TreeSet<Integer>(indexes);
        this.dontCare = dontCare;
        this.hash = Arrays.hashCode(mask);
    }

    /**
     * @return if minterm is already combined or it's original
     */
    public boolean isCombined() {
        return this.combined;
    }

    /**
     * @param combined
     *            sets minterm status to combined if used in any step of
     *            Quine-McCluskey algorithm
     */
    public void setCombined(boolean combined) {
        this.combined = combined;
    }

    /**
     * @return true if mask is don't care, false if it is minterm
     */
    public boolean isDontCare() {
        return this.dontCare;
    }

    /**
     * @return Set of indexes that correspond stored mask.
     */
    public Set<Integer> getIndexes() {
        return Collections.unmodifiableSet(minterms);
    }

    /**
     * @return number of 1's in current mask.
     */
    public int countOfOnes() {
        int ones = 0;
        for (byte b : mask) {
            if (b == 1) ones++;
        }
        return ones;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (byte b : mask) {
            sb.append(b == 2 ? "-" : b);
        }

        sb.append(dontCare ? " D " : " . ");
        sb.append(combined ? "* " : "  ");

        sb.append('[');
        for (Integer i : minterms) {
            sb.append(String.format("%d, ", i));
        }
        sb.replace(sb.length() - 2, sb.length(), "]");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        return this.hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Mask)) {
            return false;
        }

        Mask other = (Mask) obj;
        if (other.hash != this.hash) {
            return false;
        }

        return Arrays.equals(this.mask, other.mask);
    }

    /**
     * Combines two masks into one if they are combinable by simplification
     * rule.
     * 
     * @param other
     *            mask
     * @return empty optional if masks can't combine or Optional containing
     *         combined values if they can combine.
     */
    public Optional<Mask> combineWith(Mask other) {
        if (other == null) {
            throw new IllegalArgumentException("Cannot combine with null!");
        }
        if (this.mask.length != other.mask.length) {
            throw new IllegalArgumentException("Masks are different length!");
        }

        // Maske se razlikuju u duljini
        if (this.countOfOnes() != (other.countOfOnes() + 1)
                && this.countOfOnes() != (other.countOfOnes() - 1)) {
            return Optional.empty();
        }

        if (this.equals(other)) {
            return Optional.of(this);
        }

        int difference = -1;
        for (int i = 0; i < mask.length; i++) {
            // Masks don't have don't cares in same position -> return empty
            if (this.mask[i] == 2 && other.mask[i] != 2) {
                return Optional.empty();
            }
            // Masks differ at more than one place -> return empty
            if ((this.mask[i] != other.mask[i]) && difference >= 0) {
                return Optional.empty();
            }
            if (this.mask[i] != other.mask[i]) difference = i;
        }

        byte[] newMask = Arrays.copyOf(mask, mask.length);
        newMask[difference] = 2;
        TreeSet<Integer> newMinterms = new TreeSet<>();
        newMinterms.addAll(minterms);
        newMinterms.addAll(other.getIndexes());

        return Optional.of(new Mask(newMask, newMinterms,
                this.isDontCare() && other.isDontCare()));
    }
}
