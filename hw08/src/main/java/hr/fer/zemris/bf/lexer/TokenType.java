package hr.fer.zemris.bf.lexer;

/**
 * Enumeration of token types present in language for which we are building
 * lexer.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public enum TokenType {
    /**
     * Symbols end of expression. There are no more tokens further.
     */
    EOF,
    /**
     * Every sequence of characters which starts with letter after which is zero
     * or more letters, numbers or underscore.
     */
    VARIABLE,
    /**
     * Every sequence that starts with number, and after which is zero or more
     * digits.
     */
    CONSTANT,
    /**
     * One of supported operators. Supported operations are: and, xor, or, not.
     */
    OPERATOR,
    /**
     * Token type for opening bracket (
     */
    OPEN_BRACKET,
    /**
     * Token type for closing bracket )
     */
    CLOSED_BRACKET;
}
