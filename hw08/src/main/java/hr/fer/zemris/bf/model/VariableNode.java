package hr.fer.zemris.bf.model;

/**
 * Class models expression variable.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class VariableNode implements Node {
    /**
     * Name of current variable node.
     */
    private String name;

    /**
     * Default constructor.
     * 
     * @param name
     *            of variable
     */
    public VariableNode(String name) {
        this.name = name;
    }

    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * @return name of this variable.
     */
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
