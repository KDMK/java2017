package hr.fer.zemris.bf.model;

import java.util.List;
import java.util.function.BinaryOperator;

/**
 * Class models binary operator node. Binary operators in this grammars are:
 * <code>or, and, xor</code>.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class BinaryOperatorNode implements Node{
    /**
     * Name of operation.
     */
    private String name;
    /**
     * List of children.
     */
    private List<Node> children;
    /**
     * operation that is performed.
     */
    private BinaryOperator<Boolean> operator;
    
    
    /**
     * Default constructor.
     * 
     * @param name of current operator node
     * @param children list of children upon which we are performing operations
     * @param operator operation that we want to perform
     */
    public BinaryOperatorNode(String name, List<Node> children,
            BinaryOperator<Boolean> operator) {
        this.name = name;
        this.children = children;
        this.operator = operator;
    }


    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }
    
    /**
     * @return name of operation
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return list of all children that operator contains
     */
    public List<Node> getChildren() {
        return children;
    }
    
    /**
     * @return operation
     */
    public BinaryOperator<Boolean> getOperator() {
        return operator;
    }
}
