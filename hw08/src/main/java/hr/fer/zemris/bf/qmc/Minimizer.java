package hr.fer.zemris.bf.qmc;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import hr.fer.zemris.bf.model.BinaryOperatorNode;
import hr.fer.zemris.bf.model.ConstantNode;
import hr.fer.zemris.bf.model.Node;
import hr.fer.zemris.bf.model.UnaryOperatorNode;
import hr.fer.zemris.bf.model.VariableNode;

/**
 * Logic function minimizer using Quine-McCluskey algorithm with Pyne-McCluskey
 * approach.
 * 
 * @author Matija Bartolac
 *
 */
public class Minimizer {
    /**
     * Marks that function is tautology.
     */
    private boolean isTautology;
    /**
     * Marks that function is contradiction.
     */
    private boolean isContradiction;
    /**
     * Input mintermSet.
     */
    private Set<Integer> mintermSet;
    /**
     * Input dontCareSet.
     */
    private Set<Integer> dontCareSet;
    /**
     * Set of primary implicants masks.
     */
    private Set<Mask> primCover;
    /**
     * Set of minimal forms.
     */
    private List<Set<Mask>> minimalForms;
    /**
     * List of expression variables.
     */
    private List<String> variables;
    /**
     * Logger level of logging.
     */
    private static final Logger LOG = Logger.getLogger("hr.fer.zemris.bf.qmc");

    /**
     * Default constructor.
     * 
     * @param mintermSet
     *            input minterm set
     * @param dontCareSet
     *            input don't care set
     * @param variables
     *            list of expression variables
     * @throws IllegalArgumentException
     *             if mintermSet and dontCareSet overlap or if any of them
     *             contains illegal values;
     */
    public Minimizer(Set<Integer> mintermSet, Set<Integer> dontCareSet,
            List<String> variables) {
        validateInput(mintermSet, dontCareSet,
                (int) Math.pow(2, variables.size()));
        this.mintermSet = mintermSet;
        this.dontCareSet = dontCareSet;
        this.variables = variables;
        // All ones, tautology
        if ((mintermSet.size() + dontCareSet.size()) == Math.pow(2,
                variables.size())) {
            isTautology = true;
            return;
        }
        // All zeroes, contradiction
        if ((mintermSet.size() + dontCareSet.size()) == 0) {
            isContradiction = true;
            return;
        }
        this.primCover = findPrimaryImplicants();
        this.minimalForms = chooseMinimalCover(primCover);
    }

    /**
     * @return List of all minimal forms of input expression.
     */
    public List<Set<Mask>> getMinimalForms() {
        return this.minimalForms;
    }

    /**
     * Checks if minterms and dontcares sets overlap, and if any of them
     * contains illegal value.
     * 
     * @param mintermSet
     *            input set of all minterms
     * @param dontCareSet
     *            input set of all dontcares
     * @param truthTableSize
     *            largest minterm index that can sets can contain
     * @throws IllegalArgumentException
     *             if sets overlap or if sets contain illegal values.
     */
    private void validateInput(Set<Integer> mintermSet,
            Set<Integer> dontCareSet, int truthTableSize) {
        for (Integer minterm : mintermSet) {
            if (dontCareSet.contains(minterm)) {
                throw new IllegalArgumentException(
                        "Minterm set and don't care set overlap.");
            }
            if (minterm > truthTableSize) {
                throw new IllegalArgumentException(
                        "Minterm table contains illegal value: " + minterm);
            }
        }

        for (Integer dontcare : dontCareSet) {
            if (dontcare > truthTableSize) {
                throw new IllegalArgumentException(
                        "Minterm table contains illegal value: " + dontcare);
            }
        }
    }

    /**
     * Method does first step of Pyne-McCluskey algorithm. It creates set of all
     * primary implicants.
     * 
     * @return all primary implicants.
     */
    private Set<Mask> findPrimaryImplicants() {
        TreeMap<Integer, CopyOnWriteArrayList<Mask>> implicants = createGroups();
        Set<Mask> outputSet = new LinkedHashSet<>();
        boolean combinedSomething = true;

        while (combinedSomething) {
            combinedSomething = false;
            List<Mask> newPrimary = new ArrayList<>();

            removeEmptyKeys(implicants);
            // Unset combined
            implicants.forEach((key, value) -> {
                value.forEach(m -> {
                    m.setCombined(false);
                });
            });

            List<Integer> keys = new ArrayList<>(implicants.keySet());
            List<Mask> removeList = new ArrayList<>();
            List<Mask> addList = new ArrayList<>();

            for (int i = 0, size = keys.size() - 1; i < size; i++) {
                CopyOnWriteArrayList<Mask> curList = implicants
                        .get(keys.get(i));
                CopyOnWriteArrayList<Mask> nextList = implicants
                        .get(keys.get(i + 1));

                for (Mask curMask : curList) {
                    for (Mask nextMask : nextList) {
                        Optional<Mask> combinedMask = curMask
                                .combineWith(nextMask);
                        // Two entries combined. Add combined values to table
                        // and remove values used for combinations.
                        if (combinedMask.isPresent()) {
                            combinedSomething = true;
                            curMask.setCombined(true);
                            nextMask.setCombined(true);
                            Mask newMask = combinedMask.get();

                            // Add it to remove list but don't delete it yet!
                            removeList.add(nextMask);

                            addList.add(newMask);
                            continue;
                        }
                    }
                    if (!curMask.isCombined() && !curMask.isDontCare()
                            && !curMask.isDontCare()) {
                        newPrimary.add(curMask);
                        removeList.add(curMask);
                    } else {
                        removeList.add(curMask);
                    }
                }
            }
            // Didn't combine anything -> if any implicants left they are
            // primary
            if (!combinedSomething) {
                for (List<Mask> l : implicants.values()) {
                    for (Mask m : l) {
                        newPrimary.add(m);
                    }
                }
            }

            logProgress(implicants);
            logPrimary(newPrimary);

            if (!newPrimary.isEmpty()) {
                outputSet.addAll(newPrimary);
            }
            // Remove implicants used in combinations and add new implicants
            removeCombinedAndPrimary(implicants, removeList);
            addAllCombined(implicants, addList);
        }

        LOG.log(Level.FINE, "");
        LOG.log(Level.FINE, "Primary implicants:");

        for (Mask m : outputSet) {
            LOG.log(Level.FINE, m.toString());
        }
        return outputSet;
    }

    /**
     * Method performs second step of Pyne-McCluskey algorithm. It build minimal
     * forms based on input primary implicants.
     * 
     * @param primCover
     *            set of all primary implicants
     * @return List of all minimal forms.
     */
    private List<Set<Mask>> chooseMinimalCover(Set<Mask> primCover) {
        // Izgradi polja implikanata i minterma (rub tablice):
        Mask[] implicants = primCover.toArray(new Mask[primCover.size()]);
        Integer[] minterms = mintermSet.toArray(new Integer[mintermSet.size()]);
        // Mapiraj minterm u stupac u kojem se nalazi:
        Map<Integer, Integer> mintermToColumnMap = new HashMap<>();
        for (int i = 0; i < minterms.length; i++) {
            Integer index = minterms[i];
            mintermToColumnMap.put(index, i);
        }
        // Napravi praznu tablicu pokrivenosti:
        boolean[][] table = buildCoverTable(implicants, minterms,
                mintermToColumnMap);

        // Donji redak tablice: koje sam minterme pokrio?
        boolean[] coveredMinterms = new boolean[minterms.length];
        // Pronađi primarne implikante...
        Set<Mask> importantSet = selectImportantPrimaryImplicants(implicants,
                mintermToColumnMap, table, coveredMinterms);

        LOG.log(Level.FINE, "");
        LOG.log(Level.FINE, "Important primary implicants are:");
        for (Mask m : importantSet) {
            LOG.log(Level.FINE, m.toString());
        }

        // Izgradi funkciju pokrivenosti:
        List<Set<BitSet>> pFunction = buildPFunction(table, coveredMinterms);
        // Pronađi minimalne dopune:
        Set<BitSet> minset = findMinimalSet(pFunction);
        // Izgradi minimalne zapise funkcije:
        List<Set<Mask>> minimalForms = new ArrayList<>();
        LOG.log(Level.FINE, "");
        LOG.log(Level.FINE, "Minimal forms of function:");
        int index = 1;
        for (BitSet bs : minset) {
            Set<Mask> set = new LinkedHashSet<>(importantSet);
            bs.stream().forEach(i -> set.add(implicants[i]));
            minimalForms.add(set);
            LOG.log(Level.FINE,
                    String.format("%d. %s", index++, set.toString()));
        }

        return minimalForms;
    }

    /**
     * Function checks which of given primary implicants are necessary to build
     * minimal expression.
     * 
     * @param pFunction
     *            input pFunction in form of products
     * @return Minimal set of implicants needed to build minimal expression.
     */
    private Set<BitSet> findMinimalSet(List<Set<BitSet>> pFunction) {
        // Pretvori u p-funkciju u sumu produkata
        while (pFunction.size() > 1) {
            Set<BitSet> firstSet = pFunction.remove(0);
            Set<BitSet> secondSet = pFunction.remove(0);
            Set<BitSet> newSet = new LinkedHashSet<BitSet>();

            for (BitSet first : firstSet) {
                for (BitSet second : secondSet) {
                    BitSet temp = new BitSet();
                    temp.or(first);
                    temp.or(second);
                    newSet.add(temp);
                }
            }
            pFunction.add(0, newSet);
        }

        LOG.log(Level.FINER, "After converting to sum of products:");
        LOG.log(Level.FINER, pFunction.toString());
        LOG.log(Level.FINER, "");

        int smallest = Integer.MAX_VALUE;
        for (BitSet b : pFunction.get(0)) {
            if (b.cardinality() < smallest) {
                smallest = b.cardinality();
            }
        }

        Set<BitSet> minimalSet = new LinkedHashSet<BitSet>();
        for (BitSet b : pFunction.get(0)) {
            if (b.cardinality() == smallest) {
                minimalSet.add(b);
            }
        }

        LOG.log(Level.FINER, "Minimal coverages needs also:");
        LOG.log(Level.FINER, minimalSet.toString());

        return minimalSet;
    }

    /**
     * Builds pFunction based on remaining uncovered minterms.
     * 
     * @param table
     *            coverage table
     * @param coveredMinterms
     *            already covered minterms in previous steps of algorithm
     * @return pFunction
     */
    private List<Set<BitSet>> buildPFunction(boolean[][] table,
            boolean[] coveredMinterms) {
        List<Set<BitSet>> PFunction = new ArrayList<>();

        for (int col = 0; col < coveredMinterms.length; col++) {
            if (coveredMinterms[col]) continue;
            Set<BitSet> term = new HashSet<>();
            for (int row = 0; row < table.length; row++) {
                if (table[row][col]) {
                    BitSet implicant = new BitSet();
                    implicant.set(row);
                    term.add(implicant);
                }
            }
            PFunction.add(term);
        }

        LOG.log(Level.FINER, "");
        LOG.log(Level.FINER, "p funkcija je:");
        LOG.log(Level.FINER, PFunction.toString());
        LOG.log(Level.FINER, "");

        return PFunction;
    }

    /**
     * Method finds all important primary implicants. Important primary
     * implicants are those which are only implicants to cover some minterm.
     * 
     * @param implicants
     *            all implicants
     * @param mintermToColumnMap
     *            auxiliary map that holds information about minterm position in
     *            table
     * @param table
     *            coverage table
     * @param coveredMinterms
     *            Necessary primary implicants
     * @return Set of Important primary implicants.
     */
    private Set<Mask> selectImportantPrimaryImplicants(Mask[] implicants,
            Map<Integer, Integer> mintermToColumnMap, boolean[][] table,
            boolean[] coveredMinterms) {
        Set<Mask> importantPrimary = new HashSet<>();
        for (int col = 0; col < mintermToColumnMap.size(); col++) {
            int counter = 0;
            Mask newImportantPrimary = null;
            for (int row = 0; row < implicants.length; row++) {
                if (table[row][col]) {
                    counter++;
                    newImportantPrimary = implicants[row];
                }
            }
            if (counter == 1) {
                importantPrimary.add(newImportantPrimary);
                for (Integer i : newImportantPrimary.getIndexes()) {
                    Integer tableIndex = mintermToColumnMap.get(i);

                    if (tableIndex != null) {
                        coveredMinterms[tableIndex] = true;
                    }
                }
            }
        }

        return importantPrimary;
    }

    /**
     * Builds coverage table needed in some steps of algorithm.
     * 
     * @param implicants
     *            all implicants
     * @param minterms
     *            all minterms
     * @param mintermToColumnMap
     *            auxiliary map that holds information about minterm position in
     *            table
     * @return coverage table
     */
    private boolean[][] buildCoverTable(Mask[] implicants, Integer[] minterms,
            Map<Integer, Integer> mintermToColumnMap) {
        boolean[][] coverTable = new boolean[implicants.length][minterms.length];

        for (int i = 0; i < implicants.length; i++) {
            for (Integer index : implicants[i].getIndexes()) {
                Integer tableIndex = mintermToColumnMap.get(index);

                if (tableIndex != null) {
                    coverTable[i][tableIndex] = true;
                }
            }
        }

        return coverTable;
    }

    /**
     * Method used to log primary implicants when found.
     * 
     * @param outputSet
     *            set of implicants
     */
    private void logPrimary(List<Mask> outputSet) {
        if (outputSet.isEmpty()) return;

        for (Mask m : outputSet) {
            LOG.log(Level.FINEST,
                    String.format("Found primary implicant: %s", m));
        }

    }

    /**
     * Method logs progress in process of finding primary implicants.
     * 
     * @param implicants
     *            all implicants
     */
    private void logProgress(
            TreeMap<Integer, CopyOnWriteArrayList<Mask>> implicants) {
        LOG.log(Level.FINER, "Table column:");
        LOG.log(Level.FINER, "=========================================");

        for (Integer i : implicants.keySet()) {
            for (Mask m : implicants.get(i)) {
                LOG.log(Level.FINER, m.toString());
            }
            LOG.log(Level.FINER, "----------------------------------------");
        }
        LOG.log(Level.FINER, "");
    }

    // Razvrstaj implikante u grupe po broju jedinica.
    /**
     * Method builds map based on number of 1's in all given minterms and don't
     * cares.
     * 
     * @return Map of implicants sorted by number of 1's
     */
    private TreeMap<Integer, CopyOnWriteArrayList<Mask>> createGroups() {
        TreeMap<Integer, CopyOnWriteArrayList<Mask>> out = new TreeMap<Integer, CopyOnWriteArrayList<Mask>>();

        for (Integer i : mintermSet) {
            Mask newMask = new Mask(i, variables.size(), false);
            if (!out.containsKey(newMask.countOfOnes())) {
                CopyOnWriteArrayList<Mask> t = new CopyOnWriteArrayList<>();
                t.add(newMask);
                out.put(newMask.countOfOnes(), t);
                continue;
            }
            out.get(newMask.countOfOnes()).add(newMask);
        }

        for (Integer i : dontCareSet) {
            Mask newMask = new Mask(i, variables.size(), true);
            if (!out.containsKey(newMask.countOfOnes())) {
                CopyOnWriteArrayList<Mask> t = new CopyOnWriteArrayList<>();
                t.add(newMask);
                out.put(newMask.countOfOnes(), t);
                continue;
            }
            out.get(newMask.countOfOnes()).add(newMask);
        }

        return out;
    }

    /**
     * Removes all empty entries in implicants map. Used if first part of
     * algorithm.
     * 
     * @param input
     *            map of implicants mapped by number of 1's
     */
    private void removeEmptyKeys(
            TreeMap<Integer, CopyOnWriteArrayList<Mask>> input) {
        List<Integer> removeList = new ArrayList<>();
        for (Integer i : input.keySet()) {
            if (input.get(i).isEmpty()) {
                removeList.add(i);
            }
        }

        for (Integer i : removeList) {
            input.remove(i);
        }
    }

    /**
     * Removes all combined and primary implicants from implicants table.
     * 
     * @param implicants
     *            map of implicants
     * @param rmList
     *            implicants to remove
     */
    private void removeCombinedAndPrimary(
            TreeMap<Integer, CopyOnWriteArrayList<Mask>> implicants,
            List<Mask> rmList) {
        for (Mask m : rmList) {
            implicants.get(m.countOfOnes()).remove(m);
        }
    }

    /**
     * Adds newly combined implicants to implicants table.
     * 
     * @param implicants
     *            map of implicants
     * @param addList
     *            new implicants
     */
    private void addAllCombined(
            TreeMap<Integer, CopyOnWriteArrayList<Mask>> implicants,
            List<Mask> addList) {
        for (Mask m : addList) {
            if (implicants.get(m.countOfOnes()).indexOf(m) != -1) continue;
            if (!implicants.containsKey(m.countOfOnes())) {
                CopyOnWriteArrayList<Mask> t = new CopyOnWriteArrayList<>();
                t.add(m);
                implicants.put(m.countOfOnes(), t);
                continue;
            }
            implicants.get(m.countOfOnes()).add(m);
        }
    }

    /**
     * Builds all minimal expressions and returns them as list.
     * 
     * @return list of all minimal forms as boolean expressions
     */
    public List<Node> getMinimalFormsAsExpressions() {
        List<Node> outputList = new ArrayList<>();
        if (isTautology) {
            outputList.add(new ConstantNode((byte) 1));
            return outputList;
        }
        if (isContradiction) {
            outputList.add(new ConstantNode((byte) 0));
            return outputList;
        }

        for (Set<Mask> m : minimalForms) {
            if (m.size() == 1) {
                // only one Node
                m.forEach(mask -> {
                    outputList
                            .add(buildProduct(mask.toString().substring(0, 4)));
                });
                continue;
            }

            List<Node> children = new ArrayList<>();
            m.forEach(mask -> {
                children.add(buildProduct(mask.toString().substring(0, 4)));
            });

            Node newMinimalForm = new BinaryOperatorNode("or", children,
                    (a1, a2) -> a1 | a2);
            outputList.add(newMinimalForm);
        }
        return outputList;
    }

    /**
     * Builds all minimal expressions as strings.
     * 
     * @return list of minimal expressions as strings.
     */
    public List<String> getMinimalFormsAsString() {
        List<String> outputMinimalExpressions = new ArrayList<>();
        List<Node> expression = this.getMinimalFormsAsExpressions();
        if (isTautology || isContradiction) {
            outputMinimalExpressions
                    .add(((ConstantNode) expression.get(0)).getValue() == 1
                            ? "true" : "false");
        }

        for (Node nd : expression) {
            StringBuilder sb = new StringBuilder();

            if (nd instanceof BinaryOperatorNode) {
                for (Node nd1 : ((BinaryOperatorNode) nd).getChildren()) {
                    if (nd1 instanceof BinaryOperatorNode) {
                        Iterator<Node> iter = ((BinaryOperatorNode) nd1)
                                .getChildren().iterator();
                        while (iter.hasNext()) {
                            Node curElem = iter.next();
                            sb.append(resolveUnaryOrVar(curElem));
                            if (iter.hasNext()) sb.append(" AND ");
                        }
                    } else {
                        sb.append(resolveUnaryOrVar(nd1));
                    }
                    sb.append(" OR ");
                }
                sb.replace(sb.length() - 4, sb.length(), "");
                outputMinimalExpressions.add(sb.toString());
            } else {
                sb.append(resolveUnaryOrVar(nd));
            }
        }

        return outputMinimalExpressions;
    }

    /**
     * Return string that represents input node.
     * 
     * @param nd
     *            expression node
     * @return string representation of that node.
     */
    private static String resolveUnaryOrVar(Node nd) {
        if (nd instanceof UnaryOperatorNode) {
            return String.format("NOT %s",
                    ((UnaryOperatorNode) nd).getChild().toString());
        } else {
            return nd.toString();
        }
    }

    /**
     * Builds expression from given input string.
     * 
     * @param product
     *            truth table row in form of product
     * @return new node representing current product
     */
    private Node buildProduct(String product) {
        List<Node> children = new ArrayList<>();

        int i = 0;
        for (char c : product.toCharArray()) {
            if (c == '1') {
                children.add(new VariableNode(variables.get(i)));
            } else if (c == '0') {
                children.add(new UnaryOperatorNode("not",
                        new VariableNode(variables.get(i)), a -> !a));
            }
            i++;
        }
        if (i == 1) {
            return children.get(0);
        } else {
            return new BinaryOperatorNode("and", children, (a1, a2) -> a1 & a2);
        }
    }
}
