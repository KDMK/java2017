package hr.fer.zemris.bf.model;

/**
 * Interface models public method which every node must implement in order to
 * work with {@link NodeVisitor}
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface Node {
    /**
     * Method performs action described within provided visitor instance.
     * 
     * @param visitor
     *            instance of visitor that we are using
     */
    void accept(NodeVisitor visitor);
}
