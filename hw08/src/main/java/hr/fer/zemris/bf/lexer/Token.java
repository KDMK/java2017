package hr.fer.zemris.bf.lexer;

/**
 * Token class models basic lexical analysis object - token. Token groups one or
 * more consecutive characters from input text. Token is immutable object.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class Token {
    /**
     * Type of stored token as described in @see TokenType.
     */
    private final TokenType tokenType;
    /**
     * Value of token.
     */
    private final Object tokenValue;

    /**
     * Default Token constructor. It creates new token based on token type and
     * value of passed object.
     *
     * @param tokenType
     *            <code>TokenType</code>
     * @param tokenValue
     *            Value of token
     * @throws IllegalArgumentException
     *             If any of arguments is <code>null</code>
     */
    public Token(TokenType tokenType, Object tokenValue) {
        if (tokenType == null) {
            throw new IllegalArgumentException("Token type can't be null!");
        }
        this.tokenType = tokenType;
        this.tokenValue = tokenValue;
    }

    /**
     * Returns type of token. Valid types for tokens are defined in
     * {@link TokenType}.
     *
     * @return the type of token.
     */
    public TokenType getTokenType() {
        return tokenType;
    }

    /**
     * Returns the value of token. Value can be <code>String or Boolean</code>,
     * depending on a type of this token.
     *
     * @return the value of token.
     */
    public Object getValue() {
        return tokenValue;
    }

    public String toString() {
        StringBuilder out = new StringBuilder();

        out.append(String.format("Type: %s", tokenType.name()));
        if (tokenValue != null) {
            out.append(String.format(", Value: %s, Value is instance of: %s",
                    tokenValue.toString(), tokenValue.getClass().getName()));
        } else {
            out.append(", Value: null");
        }
        return out.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((tokenType == null) ? 0 : tokenType.hashCode());
        result = prime * result
                + ((tokenValue == null) ? 0 : tokenValue.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Token)) {
            return false;
        }
        Token other = (Token) obj;
        if (tokenType != other.tokenType) {
            return false;
        }
        if (tokenValue == null) {
            if (other.tokenValue != null) {
                return false;
            }
        } else if (!tokenValue.equals(other.tokenValue)) {
            return false;
        }
        return true;
    }
}
