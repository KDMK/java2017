package hr.fer.zemris.bf.demo;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.bf.parser.Parser;
import hr.fer.zemris.bf.qmc.Minimizer;
import hr.fer.zemris.bf.utils.Util;

/**
 * Test program for parser and minimizer of boolean expressions. Program takes
 * input function in form:
 * <p>
 * <code> f(var1, var2) = minterm_expression | dontcare_expression
 * <p>
 * minterm_expression can be passed as boolean expression or list of indexes as
 * follows: <code>[0, 3, 4]</code>. Same applies for dontcare_expression.
 * dontcare_expression is optional. Program terminates on user input of
 * quit(case insensitive).
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class QMC {
    /**
     * Pattern for matching minterms/dontcares in implicants brackets.
     */
    private static Pattern mintermBracket = Pattern.compile("\\[.*\\]");
    /**
     * Pattern for matching variables in function arguments brackets.
     */
    private static Pattern vars = Pattern.compile("\\(.*\\)");

    /**
     * @param args
     *            command line arguments; not used;
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("> ");
            String nextLine = input.nextLine();
            if (nextLine.equalsIgnoreCase("quit")) break;

            // see if there is input function
            String[] function = nextLine.split("=");
            if (function.length != 2) {
                System.out.println("Error at input!");
                continue;
            }
            
            if(!Character.isLetter(function[0].trim().charAt(0))) {
                System.out.println("Illegal function name!");
                continue;
            }
            
            // extract variables
            Matcher m = vars.matcher(function[0].trim());
            String variables = "";
            List<String> variablesList;
            if (m.find()) {
                variables = m.group(0);
                variables = variables.replace('(', ' ');
                variables = variables.replace(')', ' ');
                variables = variables.trim();
                variablesList = Arrays.asList(variables.split(","));
            } else {
                System.out.println("Variables not valid!");
                continue;
            }

            String[] minDcs = function[1].split("\\|");

            if (minDcs.length > 2 || minDcs.length <= 0) {
                System.out.println("Bad expression format!");
                continue;
            }

            Set<Integer> dontCares = new TreeSet<>();
            if (minDcs.length == 2) {
                try {
                    dontCares = getImplicants(variablesList, minDcs[1]);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    continue;
                }
            }

            Set<Integer> minterms = null;
            try {
                minterms = getImplicants(variablesList, minDcs[0]);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }          

            Minimizer min = null;
            try {
                min = new Minimizer(minterms, dontCares, variablesList);
            } catch (Exception e) {
                System.out.println(e.getMessage() + " " + e.getClass());
                continue;
            }

            int i = 1;
            for (String s : min.getMinimalFormsAsString()) {
                System.out.print(String.format("%d. %s%n", i++, s));
            }
        }

        input.close();
    }

    /**
     * Method returns set of implicants as indexes in truth table.
     * 
     * @param variables
     *            list of variables
     * @param expression
     *            expression as string
     * @return set of implicants
     */
    private static Set<Integer> getImplicants(List<String> variables,
            String expression) {
        Matcher m = mintermBracket.matcher(expression);
        if (m.find()) {
            expression = expression.replace('[', ' ');
            expression = expression.replace(']', ' ');
            expression = expression.trim();
            Set<Integer> dontcares = new TreeSet<>();
            for (String c : expression.split(",")) {
                dontcares.add(Integer.valueOf(c.trim()));
            }
            return dontcares;
        }
        Parser p = new Parser(expression);

        return Util.toSumOfMinterms(variables, p.getExpression());
    }

}
