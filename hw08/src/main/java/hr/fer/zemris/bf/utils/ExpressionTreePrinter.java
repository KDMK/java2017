package hr.fer.zemris.bf.utils;

import hr.fer.zemris.bf.model.BinaryOperatorNode;
import hr.fer.zemris.bf.model.ConstantNode;
import hr.fer.zemris.bf.model.Node;
import hr.fer.zemris.bf.model.NodeVisitor;
import hr.fer.zemris.bf.model.UnaryOperatorNode;
import hr.fer.zemris.bf.model.VariableNode;

/**
 * Creates textual representation of built syntax tree.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ExpressionTreePrinter implements NodeVisitor{
    /**
     * Indentation of current level.
     */
    private int indentation;
    
    @Override
    public void visit(ConstantNode node) {
        System.out.println(createPadding(indentation, ' ') + node.getValue());
    }

    @Override
    public void visit(VariableNode node) {
        System.out.println(createPadding(indentation, ' ') + node.getName());        
    }

    @Override
    public void visit(UnaryOperatorNode node) {
        System.out.println(createPadding(indentation, ' ') + node.getName());
        indentation += 2;
        node.getChild().accept(this);
        indentation -= 2;
    }

    @Override
    public void visit(BinaryOperatorNode node) {
        System.out.println(createPadding(indentation, ' ') + node.getName());
        indentation += 2;
        for(Node n : node.getChildren()) {
            n.accept(this);
        }
        indentation -= 2;
    }
    
    /**
     * Creates string filled with symbol. Used where we need specific padding.
     * 
     * @param padding
     *            number of blank spaces that we want to create
     * @param symbol
     *            padding symbol
     * @return string that contains desired number of whitespace characters
     */
    public static String createPadding(int padding, char symbol) {
        if (padding < 1) {
            return "";
        }

        String blanks = String.format("%0" + padding + "d", 0).replace('0',
                symbol);

        return blanks;
    }
}
