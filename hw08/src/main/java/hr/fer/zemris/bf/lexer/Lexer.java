package hr.fer.zemris.bf.lexer;

/**
 * Lexer program is used to build tokens from given input text. TokenTypes for
 * this lexer are defined in {@link TokenType} enumeration. Language that we are
 * building lexer is language that we use to write boolean expressions. All
 * standard rules for boolean functions applies. If operator is provided as
 * symbol it is transformed to appropriate "word operator"(xor, or...).
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Lexer {
    /**
     * Expression from which lexer extract tokens.
     */
    private char[] expression;
    /**
     * Current token.
     */
    private Token token;
    /**
     * Index of next character for processing.
     */
    private int currentIndex;

    /**
     * Default constructor.
     * 
     * @param expression
     *            that user provided.
     * @throws LexerException
     *             if user provides null as an argument to constructor
     */
    public Lexer(String expression) {
        if (expression == null) {
            throw new LexerException("Cannot parse null!");
        }
        this.expression = expression.toCharArray();
        this.currentIndex = 0;
    }

    /**
     * Generates and return next token from input text.
     *
     * @return Currently generated token
     */
    public Token nextToken() {
        if (token != null && token.getTokenType() == TokenType.EOF) {
            throw new LexerException("No more tokens!");
        }
        StringBuilder curInputToken = new StringBuilder();

        while (currentIndex <= expression.length) {
            skipBlanks();
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (currentIndex >= expression.length
                    && curInputToken.toString().equals("")) {
                token = new Token(TokenType.EOF, null);
                return token;
            }
            char curInputCharacter = expression[currentIndex++];

            // Found open bracket.
            if (curInputCharacter == '(') {
                token = new Token(TokenType.OPEN_BRACKET, curInputCharacter);
                return token;
            }
            // Found closed bracket.
            if (curInputCharacter == ')') {
                token = new Token(TokenType.CLOSED_BRACKET, curInputCharacter);
                return token;
            }

            // Current token is symbol token
            if (isOperator(curInputCharacter)) {
                curInputToken.append(curInputCharacter);
                if (curInputCharacter == ':') {
                    resolveXOROperator(curInputToken);
                }
                token = resolveIdentifier(curInputToken.toString());
                return token;
            }

            // Check if current character is number(include negative number)
            if (Character.isDigit(curInputCharacter)) {
                curInputToken.append(curInputCharacter);
                Boolean value = buildNumber(curInputToken);

                token = new Token(TokenType.CONSTANT, value);
                return token;
            }

            // If following characters are valid variable name or operator(word)
            // group them and create token.
            if (Character.isLetter(curInputCharacter)) {
                curInputToken.append(curInputCharacter);
                buildIdentifier(curInputToken);
                token = resolveIdentifier(curInputToken.toString());
                return token;
            }
            throw new LexerException("Invalid token: "
                    + curInputToken.append(curInputCharacter).toString());
        }
        return null;
    }

    // ========================================================================
    // Helper methods
    // ========================================================================

    /**
     * Skips blanks in character sequence. Characters that are interpreted as
     * blanks are: <code>' ', '\n', '\r', '\n'</code>
     */
    private void skipBlanks() {
        while (currentIndex < expression.length) {
            if (isBlank(expression[currentIndex])) {
                currentIndex++;
                continue;
            }
            return;
        }
    }

    /**
     * Checks if current character is blank character.
     *
     * @param curCharacter
     *            <code>char</code> value that is checked
     * @return <code>true</code> if next character is blank character
     */
    private boolean isBlank(char curCharacter) {
        return curCharacter == ' ' || curCharacter == '\r'
                || curCharacter == '\n' || curCharacter == '\t';
    }

    /**
     * Builds identifier token value. Valid identifiers are those starting with
     * letter, followed by zero or more letters, digits or underscores.
     *
     * @param curInputToken
     *            String builder that stores current token that we are
     *            processing.
     * @throws LexerException
     *             if input is not valid function name.
     */
    private void buildIdentifier(StringBuilder curInputToken) {
        while (currentIndex < expression.length
                && !Character.isWhitespace(expression[currentIndex])
                && expression[currentIndex] != ')'
                && expression[currentIndex] != '('
                && !isOperator(expression[currentIndex])) {
            if (Character.isLetterOrDigit(expression[currentIndex])
                    || expression[currentIndex] == '_') {
                curInputToken.append(expression[currentIndex++]);
            } else {
                throw new LexerException("Invalid identifier!");
            }
        }
    }

    /**
     * Builds Number token value. Return boolean value of input number.
     *
     * @param curInputToken
     *            String builder that stores current token that we are
     *            processing
     * @return appropriate boolean value of input number
     * @throws LexerException
     *             if number format is not supported or if input number is
     *             different from 1 or 0
     */
    private Boolean buildNumber(StringBuilder curInputToken) {
        while (currentIndex < expression.length
                && (!Character.isWhitespace(expression[currentIndex])
                        && expression[currentIndex] != ')'
                        && expression[currentIndex] != '(')) {
            if (Character.isDigit(expression[currentIndex])) {
                curInputToken.append(expression[currentIndex++]);
                continue;
            }
            throw new LexerException("Illegal number format.");
        }

        try {
            int i = Integer.parseInt(curInputToken.toString());

            if (i == 0) return false;
            else if (i == 1) return true;
            else {
                throw new LexerException(String.format("Unexpected number: %s.",
                        curInputToken.toString()));
            }
        } catch (NumberFormatException e) {
            throw new LexerException(String.format(
                    "Cannot parse %s as integer!", curInputToken.toString()));
        }
    }

    /**
     * Builds appropriate operator token based on input string.
     * 
     * @param identifier
     *            string representation of identifier
     * @return {@link TokenType#OPERATOR} or {@link TokenType#VARIABLE} with
     *         appropriate value
     */
    private Token resolveIdentifier(String identifier) {
        if (identifier.equalsIgnoreCase("xor") || identifier.equals(":+:")) {
            return new Token(TokenType.OPERATOR, "xor");
        } else if (identifier.equalsIgnoreCase("not")
                || identifier.equals("!")) {
            return new Token(TokenType.OPERATOR, "not");
        } else if (identifier.equalsIgnoreCase("or")
                || identifier.equals("+")) {
            return new Token(TokenType.OPERATOR, "or");
        } else if (identifier.equalsIgnoreCase("and")
                || identifier.equals("*")) {
            return new Token(TokenType.OPERATOR, "and");
        } else if (identifier.equalsIgnoreCase("true")) {
            return new Token(TokenType.CONSTANT, Boolean.valueOf(true));
        } else if (identifier.equalsIgnoreCase("false")) {
            return new Token(TokenType.CONSTANT, Boolean.valueOf(false));
        } else {
            return new Token(TokenType.VARIABLE, identifier.toUpperCase());
        }
    }

    /**
     * Checks if current input symbol is supported.
     * 
     * @param symbol
     *            current input character
     * @return true if operator is supported, false otherwise
     */
    private boolean isOperator(Character symbol) {
        return (symbol == '!' || symbol == '*' || symbol == '+'
                || symbol == ':');
    }

    /**
     * Checks if operator in string is symbol for string.
     * 
     * @param sb
     *            input string builder
     */
    private void resolveXOROperator(StringBuilder sb) {
        if (currentIndex >= expression.length) {
            throw new LexerException("Invalid token : " + sb.toString());
        }

        char inputChar = expression[currentIndex++];
        if (inputChar == '+' && currentIndex < expression.length) {
            sb.append(inputChar);
            inputChar = expression[currentIndex++];
            if (inputChar != ':') {
                throw new LexerException(
                        "Invalid token : " + sb.append(inputChar).toString());
            }
            sb.append(inputChar);
            return;
        }
        throw new LexerException(
                "Invalid token : " + sb.append(inputChar).toString());
    }
}
