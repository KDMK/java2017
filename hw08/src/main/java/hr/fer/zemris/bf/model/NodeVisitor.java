package hr.fer.zemris.bf.model;

/**
 * Visitor interface that allows us to create visitors for boolean expression
 * model.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface NodeVisitor {
    /**
     * Visitor for {@link ConstantNode}.
     * 
     * @param node
     *            to visit
     */
    void visit(ConstantNode node);

    /**
     * Visitor for {@link VariableNode}.
     * 
     * @param node
     *            to visit
     */
    void visit(VariableNode node);

    /**
     * Visitor for {@link UnaryOperatorNode}.
     * 
     * @param node
     *            to visit
     */
    void visit(UnaryOperatorNode node);

    /**
     * Visitor for {@link BinaryOperatorNode}.
     * 
     * @param node
     *            to visit
     */
    void visit(BinaryOperatorNode node);
}
