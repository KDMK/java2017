package hr.fer.zemris.bf.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import hr.fer.zemris.bf.model.BinaryOperatorNode;
import hr.fer.zemris.bf.model.ConstantNode;
import hr.fer.zemris.bf.model.Node;
import hr.fer.zemris.bf.model.NodeVisitor;
import hr.fer.zemris.bf.model.UnaryOperatorNode;
import hr.fer.zemris.bf.model.VariableNode;

/**
 * Class models evaluator that evaluates boolean expression from parsed syntax
 * tree.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
/**
 * @author Matija
 *
 */
public class ExpressionEvaluator implements NodeVisitor {
    /**
     * Input variable values.
     */
    private boolean[] values;
    /**
     * Input variables with their position in {@link ExpressionEvaluator#values}
     */
    private Map<String, Integer> position;
    /**
     * Stack used for storing result of operation.
     */
    private Stack<Boolean> stack = new Stack<>();

    /**
     * Default constructor.
     * 
     * @param variables
     *            list of input variables
     * @throws IllegalArgumentException
     *             if null is passed as argument.
     */
    public ExpressionEvaluator(List<String> variables) {
        if (variables == null) {
            throw new IllegalArgumentException("null is not valid argument!");
        }

        this.position = new HashMap<>();
        this.stack = new Stack<>();
        this.values = new boolean[variables.size()];

        int i = 0;
        for (String var : variables) {
            position.put(var, i++);
        }
    }

    /**
     * @param values
     *            of input variables
     * @throws IllegalArgumentException
     *             if user passed more values than there is variables in this
     *             ExpressionEvaluator
     */
    public void setValues(boolean[] values) {
        if (values.length != this.values.length) {
            throw new IllegalArgumentException("Array lengths don't match");
        }
        this.values = values;
        this.start();
    }

    @Override
    public void visit(ConstantNode node) {
        stack.push(node.getValue() == 1);
    }

    @Override
    public void visit(VariableNode node) {
        if (!position.containsKey(node.getName())) {
            throw new IllegalStateException(
                    "Unknown variable: " + node.getName());
        }
        stack.push(values[position.get(node.getName())]);
    }

    @Override
    public void visit(UnaryOperatorNode node) {
        node.getChild().accept(this);

        boolean curValue = stack.pop();
        stack.push(node.getOperator().apply(curValue));
    }

    @Override
    public void visit(BinaryOperatorNode node) {
        int children = 0;
        for (Node child : node.getChildren()) {
            child.accept(this);
            children++;
        }
        while (children > 0) {
            boolean arg1 = stack.pop();
            boolean arg2 = stack.pop();
            boolean result = node.getOperator().apply(arg1, arg2);

            stack.push(result);
            children -= 2;
        }
    }

    /**
     * Prepares evaluator for next operation.
     */
    public void start() {
        this.stack.clear();
    }

    /**
     * @return result of evaluated expression.
     * @throws IllegalStateException
     *             if stack contains more than one value.
     */
    public boolean getResult() {
        if (stack.size() != 1) {
            throw new IllegalStateException("Stack contains multiple values!");
        }
        return stack.peek();
    }
}
