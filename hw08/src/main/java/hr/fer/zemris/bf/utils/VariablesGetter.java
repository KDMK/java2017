package hr.fer.zemris.bf.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import hr.fer.zemris.bf.model.BinaryOperatorNode;
import hr.fer.zemris.bf.model.ConstantNode;
import hr.fer.zemris.bf.model.Node;
import hr.fer.zemris.bf.model.NodeVisitor;
import hr.fer.zemris.bf.model.UnaryOperatorNode;
import hr.fer.zemris.bf.model.VariableNode;

/**
 * Class models a visitor that tracks all variables in given expression.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class VariablesGetter implements NodeVisitor {
    /**
     * Set that contains all variables in program.
     */
    private TreeSet<String> variables;
    
    /**
     * Default constructor.
     */
    public VariablesGetter() {
        this.variables = new TreeSet<>();
    }
    
    /**
     * @return all variable from expression as list.
     */
    public List<String> getVariables() {
        return new ArrayList<String>(variables);
    }
    
    
    @Override
    public void visit(ConstantNode node) {
        return;
    }

    @Override
    public void visit(VariableNode node) {
        variables.add(node.getName());        
    }

    @Override
    public void visit(UnaryOperatorNode node) {
        node.getChild().accept(this);
    }

    @Override
    public void visit(BinaryOperatorNode node) {
        for(Node n : node.getChildren()) {
            n.accept(this);
        }
    }
}
