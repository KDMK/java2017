/**
 * Nodes contained in this package are used to build syntax tree.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
package hr.fer.zemris.java.custom.scripting.nodes;
