package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class models web worker which, when requested, generates table of all passed
 * parameters.
 * 
 * @author Matija Bartolac
 *
 */
public class EchoParams implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {
        context.setMimeType("text/html");
        context.write("<table><tr><th>Name</th><th>Value</th></tr>");

        for (String name : context.getParameterNames()) {
            String value = context.getParameter(name);

            context.write(
                    "<tr><td>" + name + "</td><td>" + value + "</td></tr>");
        }

        context.write("</table>");
    }

}
