package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;

/**
 * Node that represent a command which generates some textual output
 * dynamically.
 *
 * @author Matija Bartolac
 *
 */
public class EchoNode extends Node {
    /**
     * Echo elements.
     */
    private Element[] elements;

    /** 
     * @return the elements
     */
    public Element[] getElements() {
        return elements;
    }

    /**
     * Default constructor.
     *
     * @param elements Array of elements that echo node that we build contains
     */
    public EchoNode(Element[] elements) {
        super();
        this.elements = elements;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{$= ");
        
        for(Element e : elements) {
            sb.append(e.asText() + " ");
        }
        
        sb.append("$}");
        
        return sb.toString();
    }

    @Override
    public void accept(INodeVisitor visitor) {
        visitor.visitEchoNode(this);
    }
}
