package hr.fer.zemris.java.custom.scripting.parser;

import java.util.Arrays;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.lexer.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;

/**
 * Parser for described document format(in homework assignment). Parser takes
 * input text, constructs syntax tree and checks whether input is conforming to
 * the rules of used grammar.
 *
 * @author Matija Bartolac
 *
 */
public class SmartScriptParser {
    /**
     * Top level node containing all other nodes.
     */
    private DocumentNode docNode;

    /**
     * Constructor for parser. Constructor takes document text and create syntax
     * tree if document is correctly parsed.
     *
     * @param docBody
     *            Whole document content in form of a string
     */
    public SmartScriptParser(String docBody) {
        ObjectStack documentStack = new ObjectStack();
        docNode = new DocumentNode();
        documentStack.push(docNode);

        try {
            parseDocument(docBody, documentStack);
        } catch (LexerException | IllegalArgumentException ex) {
            throw new SmartScriptParserException(ex.getMessage());
        }
        if (documentStack.isEmpty()
                || !(documentStack.peek() instanceof DocumentNode)) {
            throw new SmartScriptParserException();
        }
    }

    /**
     * Function travels through input text and builds syntax tree. If error
     * occurred it throws SmartScriptParserException.
     *
     * @param docBody
     *            Whole document content in form of a string
     * @param docStack
     *            Stack that we use to test if document is correctly formatted
     */
    private void parseDocument(String docBody, ObjectStack docStack) {
        Lexer lexer = new Lexer(docBody);
        Token currentToken = lexer.nextToken();

        while (currentToken.getType() != TokenType.EOF) {
            if (currentToken.getType() == TokenType.TEXT) {
                // You have text node. Add it to children nodes on top of the
                // stack.
                TextNode newTextNode = new TextNode(
                        (String) currentToken.getValue());

                ((Node) docStack.peek()).addChildNode(newTextNode);
                currentToken = lexer.nextToken();
                continue;
            }
            if (currentToken.getType() == TokenType.TAG_START) {
                currentToken = lexer.nextToken();
                // Token ends without any content in. throw error.
                if (currentToken.getType() == TokenType.TAG_END) {
                    throw new SmartScriptParserException();
                }
                // Check if current token is FOR loop element
                if (currentToken.getType() == TokenType.NAME) {
                    String tokenName = (String) currentToken.getValue();
                    if (tokenName.equalsIgnoreCase("for")) {
                        ElementVariable variable;
                        Element startExpression;
                        Element endExpression;

                        currentToken = lexer.nextToken();

                        if (!(currentToken.getType() == TokenType.NAME)) {
                            throw new SmartScriptParserException();
                        }
                        variable = new ElementVariable(
                                (String) currentToken.getValue());

                        currentToken = lexer.nextToken();
                        startExpression = createForLoopMember(currentToken);

                        currentToken = lexer.nextToken();
                        endExpression = createForLoopMember(currentToken);

                        currentToken = lexer.nextToken();

                        if (currentToken.getType() == TokenType.TAG_END) {
                            ForLoopNode newLoop = new ForLoopNode(variable,
                                    startExpression, endExpression, null);
                            ((Node) docStack.peek()).addChildNode(newLoop);
                            docStack.push(newLoop);
                            currentToken = lexer.nextToken();
                            continue;
                        }

                        Element stepExpression = createForLoopMember(
                                currentToken);
                        currentToken = lexer.nextToken();

                        if (currentToken.getType() == TokenType.TAG_END) {
                            ForLoopNode newLoop = new ForLoopNode(variable,
                                    startExpression, endExpression,
                                    stepExpression);
                            ((Node) docStack.peek()).addChildNode(newLoop);
                            docStack.push(newLoop);
                            currentToken = lexer.nextToken();
                            continue;
                        }
                    }
                    // Check if token is end token
                    if (tokenName.equalsIgnoreCase("end")) {
                        currentToken = lexer.nextToken();
                        if (currentToken.getType() != TokenType.TAG_END) {
                            throw new SmartScriptParserException();
                        }

                        // ADD END NODE AS TEXT TO DOM TREE.
                        TextNode newTextNode = new TextNode("{$END$}");
                        ((Node) docStack.peek()).addChildNode(newTextNode);

                        docStack.pop();

                        if (docStack.isEmpty()) {
                            throw new SmartScriptParserException();
                        }
                        currentToken = lexer.nextToken();
                        continue;
                    }
                }
                // Check echo token
                if (currentToken.getType() == TokenType.TAG_NAME) {
                    currentToken = lexer.nextToken();
                    ArrayIndexedCollection echoMembers = new ArrayIndexedCollection();

                    while (currentToken.getType() != TokenType.TAG_END) {
                        if (currentToken.getType() == TokenType.EOF) {
                            throw new SmartScriptParserException(
                                    "Closing tag not found;");
                        }
                        echoMembers.add(createEchoMember(currentToken));
                        currentToken = lexer.nextToken();
                    }

                    Element[] echoMembersArray = Arrays.copyOf(
                            echoMembers.toArray(), echoMembers.size(),
                            Element[].class);

                    EchoNode newEchoNode = new EchoNode(echoMembersArray);
                    ((Node) docStack.peek()).addChildNode(newEchoNode);
                    currentToken = lexer.nextToken();
                    continue;
                }
                throw new SmartScriptParserException("Invalid tag name!");
            }
        }
    }

    /**
     * Returns top level document node.
     *
     * @return Top level document node
     */
    public DocumentNode getDocumentNode() {
        return docNode;
    }

    // Helper methods.
    /**
     * Checks if current token is valid for loop element. Return new Element
     * object if it is.
     *
     * @param currentToken
     *            current token in input field
     * @return For loop member element.
     */
    private Element createForLoopMember(Token currentToken) {
        switch (currentToken.getType()) {
        case INT_CONSTANT:
            return new ElementConstantInteger((int) currentToken.getValue());
        case DOUBLE_CONSTANT:
            return new ElementConstantDouble((double) currentToken.getValue());
        case NAME:
            return new ElementVariable((String) currentToken.getValue());
        default:
            throw new SmartScriptParserException(
                    "Invalid start expression FOR loop argument");
        }
    }

    /**
     * Checks if current token is valid echo element. Return new Element object
     * if it is.
     *
     * @param currentToken
     *            current token in input field
     * @return Echo member element.
     */
    private Element createEchoMember(Token currentToken) {
        switch (currentToken.getType()) {
        case INT_CONSTANT:
            return new ElementConstantInteger((int) currentToken.getValue());
        case DOUBLE_CONSTANT:
            return new ElementConstantDouble((double) currentToken.getValue());
        case NAME:
            return new ElementVariable((String) currentToken.getValue());
        case FUNCTION:
            return new ElementFunction((String) currentToken.getValue());
        case OPERATOR:
            return new ElementOperator((String) currentToken.getValue());
        case STRING_CONSTANT:
            return new ElementString((String) currentToken.getValue());
        default:
            throw new SmartScriptParserException(
                    "Invalid start expression FOR loop argument");
        }
    }
}
