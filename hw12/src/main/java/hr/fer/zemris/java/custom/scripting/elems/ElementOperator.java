package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models symbol expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementOperator extends Element {
    /**
     * Value of stored operator.
     */
    private String symbol;

    /**
     * Default constructor. Creates new operator element from given symbol.
     *
     * @param symbol
     *            single character passed as a <code>String</code>
     */
    public ElementOperator(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Returns value of operator element.
     *
     * @return <code>String</code> value of element.
     */
    public String getSymbol() {
        return symbol;
    }

    @Override
    public String asText() {
        return symbol;
    }
}
