package hr.fer.zemris.java.custom.scripting.demo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * SmartScriptEngine first demo application. It takes one command line argument,
 * path to input textual file that contains script in smartScriptEngine format.
 * 
 * @author Matija Bartolac
 *
 */
public class SmartScriptEngineDemo {

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid number of arguments! Aborting...");
            System.exit(-1);
        }

        String basePath = new File(args[0]).getAbsolutePath();
        String docBody = null;

        try {
            docBody = new String(Files.readAllBytes(Paths.get(basePath)));
        } catch (IOException e) {
            System.out.println(
                    "File: \"" + e.getMessage() + "\" doesn't exists.");
            return;
        }

        SmartScriptParser parser = null;
        try {
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException ex) {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }

        Map<String, String> parameters = new HashMap<String, String>();
        Map<String, String> persistentParameters = new HashMap<String, String>();
        List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();

        parameters.put("broj", "4");

        new SmartScriptEngine(parser.getDocumentNode(), new RequestContext(
                System.out, parameters, persistentParameters, null, cookies))
                        .execute();
    }
}
