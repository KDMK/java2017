package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;;

/**
 * This server uses cookie based authentication. Class models cookie factory.
 * 
 * @author Matija Bartolac
 *
 */
public class RequestContext {
    /**
     * Request context cookie class models single cookie that is used for user
     * authentication on server. For each session user cookie is generated and
     * saved in order to authorize user and determine which access rights he or
     * she has.
     * 
     * @author Matija Bartolac
     *
     */
    public static class RCCookie {
        /**
         * Name of this cookie.
         */
        private String name;
        /**
         * Value of this cookie.
         */
        private String value;
        /**
         * Domain to which this cookie belongs.
         */
        private String domain;
        /**
         * Path to resource under set domain.
         */
        private String path;
        /**
         * Maximum age of cookie. Cookie becomes invalid after that period.
         */
        private Integer maxAge;
        /**
         * Marks if cookie is HTTP only.
         */
        private boolean HTTPonly;

        /**
         * Default constructor.
         * 
         * @param name
         *            of this cookie
         * @param value
         *            value of this cookie
         * @param maxAge
         *            maximum age of cookie
         * @param path
         *            path to resource
         * @param domain
         *            that created this cookie
         * @param HTTPonly
         *            flag that tells if this cookie is http only
         */
        public RCCookie(String name, String value, Integer maxAge, String path,
                String domain, boolean HTTPonly) {
            super();
            this.name = name;
            this.value = value;
            this.domain = domain;
            this.maxAge = maxAge;
            this.path = path;
            this.HTTPonly = HTTPonly;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the value
         */
        public String getValue() {
            return value;
        }

        /**
         * @return the domain
         */
        public String getDomain() {
            return domain;
        }

        /**
         * @return the maxAge
         */
        public Integer getMaxAge() {
            return maxAge;
        }

        /**
         * @return the path
         */
        public String getPath() {
            return path;
        }

        /**
         * @return the HTTPonly
         */
        public boolean getHTTPonly() {
            return this.HTTPonly;
        }
    }

    /**
     * Output stream used to write data.
     */
    private OutputStream outputStream;
    /**
     * Character set used in this document.
     */
    private Charset charset;
    /**
     * Signals if header is generated.
     */
    private boolean headerGenerated;

    /**
     * Encoding of document. Used to set charset(user doesn't have direct access
     * to charset variable).
     */
    private String encoding;
    /**
     * Response status code.
     */
    private int statusCode;
    /**
     * Response status text.
     */
    private String statusText;
    /**
     * MIME type of document in response.
     */
    private String mimeType;
    /**
     * Map of parameters.
     */
    private Map<String, String> parameters;
    /**
     * Map of temporary parameters.
     */
    private Map<String, String> temporaryParameters;
    /**
     * Map of persistent parameters.
     */
    private Map<String, String> persistentParameters;
    /**
     * List of all output cookies.
     */
    private List<RCCookie> outputCookies;
    /**
     * Additional header parameters list.
     */
    private List<String> additionalHeaderParameters;
    /**
     * Request dispatcher.
     */
    private IDispatcher dispatcher;

    /**
     * Map of all valid mime types.
     */
    // NOTE: I'm using this kind of map here because I want fast search.
    private static HashMap<String, Byte> validMimeTypes;
    /**
     * Pattern that specifies format of each mime type entry.
     */
    private static Pattern mimePattern = Pattern
            .compile("[a-zA-Z\\-]*/[a-zA-Z\\-0-9\\.\\+]*");

    // Za recenzente: napravio sam ovo prije nego sam procitao cetvrti zadatak i
    // vidio da se u mime.properties treba nalaziti popis dozvoljenih mime
    // tipova. Smatram da ovo ne smeta ovdje, tako da necu brisati i mjenjati
    // kod.

    // File mimeTypes.txt contains all supported mime types. Every line is
    // checked if its valid mime type format.
    static {
        try {
            Path p = Paths.get("src/main/resources/mimeTypes.txt")
                    .toAbsolutePath();
            List<String> lines = Files.readAllLines(p);
            validMimeTypes = new HashMap<>((int) (lines.size() * 1.30));

            for (String line : lines) {
                if (!mimePattern.matcher(line.trim()).matches()) {
                    System.out.println(
                            "Error: " + line + " is not valid MIME type.");
                }
                validMimeTypes.put(line.trim().toLowerCase(), null);
            }
        } catch (IOException e) {
            System.out.println("Error: Cannot read file " + e.getMessage());
            System.exit(-1);
        }

    }

    /**
     * Default constructor. If user provide null values for parameters,
     * persistent parameters or output cookies they are by default set to empty
     * maps and list. Default encoding is set to UTF-8, status code and message
     * to 200 OK, and mime type "text/html".
     * 
     * @param outputStream
     *            output stream in which we write our data
     * @param parameters
     *            parameters map
     * @param persistentParameters
     *            persistent parameters map
     * @param temporaryParameters
     *            temporary parameters map
     * @param outputCookies
     *            output cookies list
     * @throws IllegalArgumentException
     *             if outputStream is null
     */
    public RequestContext(OutputStream outputStream,
            Map<String, String> parameters,
            Map<String, String> persistentParameters,
            Map<String, String> temporaryParameters,
            List<RCCookie> outputCookies) {
        if (outputStream == null) {
            throw new IllegalArgumentException(
                    "Error: output stream mustn't be null");
        }

        this.encoding = "UTF-8";
        this.statusCode = 200;
        this.statusText = "OK";
        this.mimeType = "text/html";

        this.outputStream = outputStream;
        this.parameters = (parameters == null) ? new HashMap<>() : parameters;
        this.persistentParameters = (persistentParameters == null)
                ? new HashMap<>() : persistentParameters;

        this.outputCookies = (outputCookies == null) ? new ArrayList<>()
                : outputCookies;

        this.temporaryParameters = (temporaryParameters == null)
                ? new HashMap<>() : temporaryParameters;
        this.additionalHeaderParameters = new ArrayList<>();
    }

    /**
     * Extended constructor. It takes one additional parameter - reference to
     * dispatcher thread.
     * 
     * @param outputStream
     *            output stream in which we write our data
     * @param parameters
     *            parameters map
     * @param persistentParameters
     *            persistent parameters map
     * @param temporaryParameters
     *            temporary parameters map
     * @param outputCookies
     *            output cookies list
     * @param dispatcher
     *            dispatching thread
     * @throws IllegalArgumentException
     *             if outputStream is null
     */
    public RequestContext(OutputStream outputStream,
            Map<String, String> parameters,
            Map<String, String> persistentParameters,
            Map<String, String> temporaryParameters,
            List<RCCookie> outputCookies, IDispatcher dispatcher) {
        this(outputStream, parameters, persistentParameters,
                temporaryParameters, outputCookies);
        this.dispatcher = dispatcher;
    }

    /**
     * @param encoding
     *            the encoding to set
     */
    public void setEncoding(String encoding) {
        if (headerGenerated) {
            throw new RuntimeException(
                    "Error: changing encoding after header is written is not permitted.");
        }
        this.encoding = encoding;
    }

    /**
     * Returns this dispatcher.
     * 
     * @return this dispatcher
     */
    public IDispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * @param statusCode
     *            the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        if (headerGenerated) {
            throw new RuntimeException(
                    "Error: changing status code after header is written is not permitted.");
        }
        this.statusCode = statusCode;
    }

    /**
     * @param statusText
     *            the statusText to set
     */
    public void setStatusText(String statusText) {
        if (headerGenerated) {
            throw new RuntimeException(
                    "Error: changing status text after header is written is not permitted.");
        }
        this.statusText = statusText;
    }

    /**
     * @param mimeType
     *            the mimeType to set
     * @throws IllegalArgumentException
     *             if user passed unsupported mime type
     * @throws RuntimeException
     *             if user tries to change mime type after header was written
     */
    public void setMimeType(String mimeType) {
        if (headerGenerated) {
            throw new RuntimeException(
                    "Error: changing mime type after header is written is not permitted.");
        }
        if (!validMimeTypes.containsKey(mimeType)) {
            throw new IllegalArgumentException(
                    "Error: unusupported mime type: " + mimeType);
        }
        this.mimeType = mimeType;
    }

    /**
     * @param parameters
     *            the parameters to set
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    /**
     * @param temporaryParameters
     *            the temporaryParameters to set
     */
    public void setTemporaryParameters(
            Map<String, String> temporaryParameters) {
        this.temporaryParameters = temporaryParameters;
    }

    /**
     * @param persistentParameters
     *            the persistentParameters to set
     */
    public void setPersistentParameters(
            Map<String, String> persistentParameters) {
        this.persistentParameters = persistentParameters;
    }

    /**
     * @param outputCookies
     *            the outputCookies to set
     */
    public void setOutputCookies(List<RCCookie> outputCookies) {
        if (headerGenerated) {
            throw new RuntimeException(
                    "Error: changing output cookies after header is written is not permitted.");
        }
        this.outputCookies = outputCookies;
    }

    /**
     * Method retrieves value from parameters map. If no entry exists it returns
     * null.
     * 
     * @param name
     *            of given key
     * @return parameter associated to requested name if exists, null otherwise
     */
    public String getParameter(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: name in parametres map can't be null");
        }
        return parameters.get(name);
    }

    /**
     * Method generates unmodifiable map of all keys from parameters map.
     * 
     * @return map of all parameters name
     */
    public Set<String> getParameterNames() {
        return Collections.unmodifiableSet(parameters.keySet());
    }

    /**
     * Method that retrieves value from persistentParameters map.
     * 
     * @param name
     *            key of entry
     * @throws IllegalArgumentException
     *             if passed argument is null
     * @return value associated to name or null if that value doesn't exists
     */
    public String getPersistentParameter(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: name in persistent parametres map can't be null");
        }
        return persistentParameters.get(name);
    }

    /**
     * Method that retrieves names of all parameters in persistent parameters
     * map.
     * 
     * @return unmodifiable set of all parameter names
     */
    public Set<String> getPersistentParameterNames() {
        return Collections.unmodifiableSet(persistentParameters.keySet());
    }

    /**
     * Method that stores a value to persistentParameters map.
     * 
     * @param name
     *            key in map.
     * @param value
     *            value of parameter.
     * @throws IllegalArgumentException
     *             if name of value is null
     */
    public void setPersistentParameter(String name, String value) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: cannot set persistent parameter with name null!");
        }
        if (value == null) {
            throw new IllegalArgumentException(
                    "Error: cannot set value to null.");
        }

        persistentParameters.put(name, value);
    }

    /**
     * Method that removes a value from persistentParameters map.
     * 
     * @param name
     *            of entry to remove
     */
    public void removePersistentParameter(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: map doesn't containt any null elemtnts.");
        }
        if (persistentParameters.remove(name) == null) {
            System.out.println(
                    "Nothing removed, map doesn't containt parameter with given name.");
        }
    }

    /**
     * Method that retrieves value from temporaryParameters map.
     * 
     * @param name
     *            key of entry
     * @throws IllegalArgumentException
     *             if passed argument is null
     * @return value associated to name or null if that value doesn't exists
     */
    public String getTemporaryParameter(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: name in persistent parametres map can't be null");
        }

        return temporaryParameters.get(name);
    }

    /**
     * Method that retrieves names of all parameters in temporary parameters
     * map.
     * 
     * @return unmodifiable set of all parameter names
     */
    public Set<String> getTemporaryParameterNames() {
        return Collections.unmodifiableSet(temporaryParameters.keySet());
    }

    /**
     * Method that stores a value to temporaryParameters map.
     * 
     * @param name
     *            key in map.
     * @param value
     *            value of parameter.
     * @throws IllegalArgumentException
     *             if name of value is null
     */
    public void setTemporaryParameter(String name, String value) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: cannot set persistent parameter with name null!");
        }
        if (value == null) {
            throw new IllegalArgumentException(
                    "Error: cannot set value to null.");
        }

        temporaryParameters.put(name, value);
    }

    /**
     * Method that removes a value from temporaryParameters map.
     * 
     * @param name
     *            of entry to remove
     */
    public void removeTemporaryParameter(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Error: map doesn't containt any null elemtnts.");
        }
        if (temporaryParameters.remove(name) == null) {
            System.out.println(
                    "Nothing removed, map doesn't containt parameter with given name.");
        }
    }

    /**
     * Writes byte array into set output stream. When first time called it
     * prints header before other data.
     * 
     * @param data
     *            which we are writing
     * @return reference to this context object
     * @throws IOException
     *             if there was error writing data to output stream.
     */
    public RequestContext write(byte[] data) throws IOException {
        if (!headerGenerated) {
            String header = buildHeader();
            outputStream.write(header.getBytes(StandardCharsets.ISO_8859_1));
        }

        outputStream.write(data);
        outputStream.flush();

        return this;
    }

    /**
     * Writes text into set output stream. When first time called it prints
     * header before other data.
     * 
     * @param text
     *            which we are writing
     * @return reference to this context object
     * @throws IOException
     *             if there was error writing data to output stream.
     */
    public RequestContext write(String text) throws IOException {
        if (!headerGenerated) {
            String header = buildHeader();
            outputStream.write(header.getBytes(StandardCharsets.ISO_8859_1));
        }

        outputStream.write(text.getBytes(charset));
        outputStream.flush();

        return this;
    }

    /**
     * Adds parameter to additional header parameters list.
     * 
     * @param parameter
     *            header parameter
     */
    protected void addAdditionalHeaderParameter(String parameter) {
        this.additionalHeaderParameters.add(parameter);
    }

    /**
     * Builds response header. Standard response header is in following format:
     * <p>
     * <code>
     * HTTP ver statusCode statusText </br>
     * Content-Type: mimeType </br>
     * [Set-Cookie: name="value"; Domain=domain; Path=path; Max-Age=maxAge]*</br>
     * </br>
     * </code>
     * 
     * @return header in form of a string.
     */
    private String buildHeader() {
        this.charset = Charset.forName(encoding);

        StringBuilder header = new StringBuilder();
        header.append("HTTP/1.1 " + statusCode + " " + statusText + "\r\n");
        header.append("Content-Type: " + mimeType);

        if (mimeType.startsWith("text/")) {
            header.append("; charset=" + encoding);
        }
        header.append("\r\n");

        if (!outputCookies.isEmpty()) {
            generateCookies(header);
        }

        for (String param : additionalHeaderParameters) {
            header.append(param + "\r\n");
        }

        header.append("\r\n");

        headerGenerated = true;

        return header.toString();
    }

    /**
     * Generates header entry for each cookie in cookies list.
     * 
     * @param header
     *            stringBuilder object to which we are appending cookies
     */
    private void generateCookies(StringBuilder header) {
        for (RCCookie cookie : outputCookies) {
            header.append(String.format("Set-Cookie: %s=\"%s\"",
                    cookie.getName(), cookie.getValue()));
            if (cookie.getDomain() != null) {
                header.append("; Domain=" + cookie.getDomain());
            }
            if (cookie.getPath() != null) {
                header.append("; Path=" + cookie.getPath());
            }
            if (cookie.getMaxAge() != null) {
                header.append("; Max-Age=" + cookie.getMaxAge());
            }
            if (cookie.getHTTPonly()) {
                header.append("; HttpOnly");
            }
            header.append("\r\n");
        }
    }

    /**
     * Adds new cookie to outputCookies list.
     * 
     * @param rcCookie
     *            new cookie
     */
    public void addRCCookie(RCCookie rcCookie) {
        if (rcCookie == null) {
            throw new IllegalArgumentException("Error: cookie cannot be null!");
        }

        outputCookies.add(rcCookie);
    }
}
