package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Node that represents a piece of textual data.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class TextNode extends Node {
    /**
     * Text content of node.
     */
    private String text;

    /**
     * Default constructor.
     *
     * @param text
     *            Text of node that we are creating.
     */
    public TextNode(String text) {
        this.text = text;
    }

    /**
     * Returns text content of TextNode.
     *
     * @return content text of node
     */
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }
    
    @Override
    public void accept(INodeVisitor visitor) {
        visitor.visitTextNode(this);
    }
}
