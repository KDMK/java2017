package hr.fer.zemris.java.webserver;

/**
 * Models dispatcher interface. Class that implements this interface is
 * obligatory to provide method that will determines what to do with each type
 * of request to server.
 * 
 * @author Matija Bartolac
 *
 */
public interface IDispatcher {
    /**
     * Dispatches job to appropriate thread.
     * 
     * @param urlPath
     *            path of requested resource/service
     * @throws Exception
     *             if any error occurs during dispatching or processing request.
     */
    void dispatchRequest(String urlPath) throws Exception;
}
