package hr.fer.zemris.java.webserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.workers.EchoParams;
import hr.fer.zemris.java.webserver.workers.SumWorker;

/**
 * SmartHttpServer models simple web server implementation. This server provides
 * basic functionality(just GET method) extended with SmartScriptEngine. On
 * server startup server is automatically configured. Initial server parameters
 * are given in server.properties file.
 * 
 * @author Matija Bartolac
 *
 */
public class SmartHttpServer {
    /**
     * Default buffer size for reading files.
     */
    public static final int BUFFER_SIZE = 4096;
    /**
     * Root address
     */
    private String address;
    /**
     * Port on which server is listening.
     */
    private int port;
    /**
     * Number of worker threads.
     */
    private int workerThreads;
    /**
     * Time interval after which session resets.
     */
    private int sessionTimeout;
    /**
     * Tracked sessions.
     */
    private Map<String, SessionMapEntry> sessions = new HashMap<String, SmartHttpServer.SessionMapEntry>();
    /**
     * Random number generator for generating session id's.
     */
    private Random sessionRandom = new Random();
    /**
     * List of allowed mime types.
     */
    private Map<String, String> mimeTypes = new HashMap<String, String>();
    /**
     * Reference to server thread.
     */
    private ServerThread serverThread;
    /**
     * Thread pool of all worker threads
     */
    private ExecutorService threadPool;
    /**
     * Root path to files on physical storage.
     */
    private Path documentRoot;
    /**
     * String representation of properties file that contains workers.
     */
    private String workersPath;
    /**
     * Map of all workers.
     */
    private Map<String, IWebWorker> workersMap;

    /**
     * Default constructor.
     * 
     * @param configFileName
     *            name of file that contains parameters needed for initial
     *            server setup
     */
    public SmartHttpServer(String configFileName) {
        Properties properties = loadProperties(configFileName);
        resolveProperties(properties);

        this.serverThread = new ServerThread();
    }

    /**
     * Load properties from .properties file into properties map.
     * 
     * @param configFileName
     *            name of relevant configuration file
     * @return Properties map
     */
    private Properties loadProperties(String configFileName) {
        Path p = Paths.get(configFileName).toAbsolutePath();

        if (!p.toFile().exists()) {
            throw new IllegalArgumentException(
                    "Error: " + p.toString() + " doesn't exist");
        }

        Properties props = new Properties();

        try {
            props.load(Files.newInputStream(p));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    "Error: Cannot read " + p.toString());
        }
        return props;
    }

    /**
     * Sets basic server parameters. If there was error reading properties from
     * properties map appropriate message is shown and execution is terminated.
     * 
     * @param properties
     *            name of file that contains server parameters
     */
    private void resolveProperties(Properties properties) {
        // Set listening address
        this.address = properties.getProperty("server.address");

        // Set listening port
        try {
            this.port = Integer.parseInt(properties.getProperty("server.port"));
        } catch (NumberFormatException e) {
            System.out.println("Error: " + e.getMessage());
            System.exit(-1);
        }

        // Set number of worker threads and initialize ExecutorService
        this.workerThreads = Integer
                .parseInt(properties.getProperty("server.workerThreads"));

        // Set document root path on disk
        this.documentRoot = Paths
                .get(properties.getProperty("server.documentRoot"))
                .toAbsolutePath();

        // read and set mime types
        readMimeTypes(properties.getProperty("server.mimeConfig"));

        // Set session timeout
        try {
            this.sessionTimeout = Integer
                    .parseInt(properties.getProperty("session.timeout"));
        } catch (NumberFormatException e) {
            System.out.println("Error: " + e.getMessage());
            System.exit(-1);
        }

        // Read workers path
        this.workersPath = properties.getProperty("server.workers");
        getWorkers(workersPath);
    }

    /**
     * Gets workers from properties file and initializes them.
     * 
     * @param workers
     *            name of .properties file that contains worker parameters
     */
    private void getWorkers(String workers) {
        Properties workerProps = loadProperties(workers);
        this.workersMap = new HashMap<>();

        for (Object worker : workerProps.keySet()) {
            String workerClassName = workerProps.getProperty((String) worker);

            try {
                Class<?> newClass = this.getClass().getClassLoader()
                        .loadClass(workerClassName);

                Object newWorkerInstance = newClass.newInstance();

                IWebWorker iww = (IWebWorker) newWorkerInstance;

                workersMap.put((String) worker, iww);
            } catch (ClassNotFoundException | SecurityException
                    | InstantiationException | IllegalAccessException
                    | IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    /**
     * Reads mime types from properties file and puts them into mime types list.
     * 
     * @param property
     *            name of properties file from which we load mimeTypes
     */
    private void readMimeTypes(String property) {
        Properties properties = loadProperties(property);

        for (Object mimeType : properties.keySet()) {
            mimeTypes.put((String) mimeType,
                    properties.getProperty((String) mimeType));
        }
    }

    /**
     * Method starts server.
     */
    protected synchronized void start() {
        this.threadPool = Executors.newFixedThreadPool(workerThreads);
        this.serverThread.run();
    }

    /**
     * Stops server.
     */
    protected synchronized void stop() {
        this.serverThread.endExecution();

        while (serverThread.isAlive()) {
            try {
                this.serverThread.join();
            } catch (Exception ignorable) {
            }
        }

        threadPool.shutdown();
    }

    /**
     * Auxiliary data structure for storing specific session data.
     * 
     * @author Matija Bartolac
     *
     */
    private static class SessionMapEntry {
        /**
         * Session ID.
         */
        private String sid;
        /**
         * Time until this session is valid
         */
        long validUntil;
        /**
         * Map for storing session parameters.
         */
        Map<String, String> map;

        /**
         * Default constructor.
         * 
         * @param sid
         *            session id
         * @param validUntil
         *            time until this session is valid.
         * @param map
         *            for storing session parameters.
         */
        public SessionMapEntry(String sid, long validUntil,
                ConcurrentHashMap<String, String> map) {
            this.sid = sid;
            this.validUntil = validUntil;
            this.map = map;
        }

        /**
         * Checks if cookie is still valid.
         * 
         * @param time
         *            now
         * @param timeout
         *            cookie timeout
         * @return true if cookie is still valid, false otherwise
         */
        public boolean isValid(long time, int timeout) {
            return (time - timeout) < validUntil;
        }

        /**
         * Updates session valid time.
         * 
         * @param sessionTimeout
         *            session timeout time.
         */
        public void updateValid(int sessionTimeout) {
            Calendar now = Calendar.getInstance();
            now.add(Calendar.SECOND, sessionTimeout);

            this.validUntil = now.getTimeInMillis();
        }

        /**
         * @return this session sid
         */
        @SuppressWarnings("unused")
        public String getSID() {
            return this.sid;
        }

        /**
         * Used to initialize new map when valid time has expired and we want to
         * reset session.
         */
        private void newMap() {
            this.map = new ConcurrentHashMap<>();
        }
    }

    /**
     * Class models server thread.
     * 
     * @author Matija Bartolac
     *
     */
    protected class ServerThread extends Thread {
        /**
         * Flag used to control server state(running/end).
         */
        private boolean running = true;

        /**
         * Method used for termination of server thread.
         */
        protected void endExecution() {
            this.running = false;
        }

        @Override
        public void run() {
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket();
                serverSocket
                        .bind(new InetSocketAddress((InetAddress) null, port));
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
                System.exit(-1);
            }

            while (running) {
                Socket client = null;
                try {
                    client = serverSocket.accept();
                } catch (IOException e) {
                    System.out.println("Error: " + e.getMessage());
                    System.exit(-1);
                }

                ClientWorker cw = new ClientWorker(client);

                threadPool.submit(cw);
            }
        }
    }

    /**
     * Thread models workers whose job is to process clients requests.
     * 
     * @author Matija Bartolac
     *
     */
    private class ClientWorker implements Runnable, IDispatcher {
        /**
         * Numeric value of letter A
         */
        private static final int MIN_CHAR = 65;
        /**
         * Numeric value of letter Z
         */
        private static final int MAX_CHAR = 90;

        /**
         * Connection socket.
         */
        private Socket csocket;
        /**
         * Socket input stream.
         */
        private PushbackInputStream istream;
        /**
         * Socket output stream.
         */
        private OutputStream ostream;
        /**
         * HTTP protocol version
         */
        @SuppressWarnings("unused")
        private String version;
        /**
         * HTTP method
         */
        @SuppressWarnings("unused")
        private String method;
        /**
         * Map of parameters.
         */
        private Map<String, String> params = new HashMap<String, String>();
        /**
         * Map of temporary parameters
         */
        private Map<String, String> tempParams = new HashMap<String, String>();
        /**
         * Map of permanent parameters
         */
        private Map<String, String> permPrams = new HashMap<String, String>();
        /**
         * List of output cookies.
         */
        private List<RCCookie> outputCookies = new ArrayList<RequestContext.RCCookie>();
        /**
         * Server ID.
         */
        @SuppressWarnings("unused")
        private String SID;
        /**
         * Private context used by this client.
         */
        private RequestContext context;

        /**
         * Default constructor.
         * 
         * @param csocket
         *            client socket
         */
        public ClientWorker(Socket csocket) {
            super();
            this.csocket = csocket;

        }

        /**
         * Builds random SID value from upper-case letters of English alphabet.
         * 
         * @return random SID
         */
        private String buildRandomSID() {
            StringBuilder randSID = new StringBuilder();

            for (int i = 0; i < 20; i++) {
                char c = (char) (SmartHttpServer.this.sessionRandom
                        .nextInt((MAX_CHAR - MIN_CHAR) + 1) + MIN_CHAR);

                randSID.append(c);
            }

            return randSID.toString();
        }

        /**
         * Gets context.
         * 
         * @return this client request context
         */
        private RequestContext getContext() {
            if (this.context == null) {
                this.context = new RequestContext(this.ostream, this.params,
                        this.permPrams, this.tempParams, this.outputCookies,
                        this);
            }
            return this.context;
        }

        public void dispatchRequest(String urlPath) throws Exception {
            internalDispatchRequest(urlPath, false);
        }

        /**
         * Dispatches request to appropriate processing method based on given
         * urlPath
         * 
         * @param urlPath
         *            path to resource
         * @param directCall
         *            flag that signalizes if it is direct call of this method
         *            or
         *            {@link ClientWorker#internalDispatchRequest(String, boolean)
         *            internalDispatchRequest} called it.
         * @throws Exception
         *             if there was any error while processing request
         */
        public void internalDispatchRequest(String urlPath, boolean directCall)
                throws Exception {
            // Direct call to private path
            if (urlPath.startsWith(documentRoot.toString() + "/private")
                    && directCall) {
                generateErrorResponse(404, "Not found.");
            }

            // Calling script
            if (urlPath.endsWith(".smscr")) {
                executeScript(urlPath);
                return;
            }

            // Calling calc script. Pass job to SumWorker and then call script
            // which will generate HTML output.
            if (urlPath.startsWith("/calc")) {

                IWebWorker sumWorker = new SumWorker();
                sumWorker.processRequest(getContext());
                dispatchRequest(
                        documentRoot.toString() + "/private/calc.smscr");
                return;
            }

            // Call echo worker
            if (urlPath.startsWith("/ext/EchoParams")) {
                IWebWorker echoWorker = new EchoParams();
                echoWorker.processRequest(getContext());
                ostream.close();
                return;
            }

            // Determine is worker from workersMap and call it
            for (String wkPath : workersMap.keySet()) {
                if (urlPath.startsWith(wkPath)) {
                    workersMap.get(wkPath).processRequest(getContext());
                    ostream.close();
                    return;
                }
            }

            // If non of upper code ran try to get file.
            getFileFromServer(urlPath);
        }

        /**
         * Reads script from file, parses it and executes it.
         * 
         * @param urlPath
         *            path to script file on physical storage
         * @throws IOException
         *             if script cannot be read or there was an error while
         *             writing to output stream.
         */
        private void executeScript(String urlPath) throws IOException {
            String docBody = null;

            try {
                docBody = new String(Files.readAllBytes(Paths.get(urlPath)));
            } catch (IOException e) {
                System.out.println(
                        "File: \"" + e.getMessage() + "\" doesn't exists.");
                return;
            }

            SmartScriptParser parser = null;
            try {
                parser = new SmartScriptParser(docBody);
            } catch (SmartScriptParserException ex) {
                System.out.println(ex.getMessage());
                System.exit(-1);
            }

            new SmartScriptEngine(parser.getDocumentNode(), getContext())
                    .execute();

            ostream.close();
        }

        /**
         * Resolves request if user requested file.
         * 
         * @param urlPath
         *            resolved url to path on physical storage.
         */
        private void getFileFromServer(String urlPath) {
            Path requestedPath = Paths.get(urlPath);
            File requestedFile = requestedPath.toFile();

            String extension = requestedFile.getName().split("\\.")[1];
            // else extract file extension
            // find in mimeTypes map appropriate mimeType for current file
            // extension
            // (you filled that map during the construction of SmartHttpServer
            // from mime.properties)
            // if no mime type found, assume application/octet-stream
            String mimeType = mimeTypes.get(extension);
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }

            // create a rc = new RequestContext(...); set mime-type; set status
            // to 200
            RequestContext rc = getContext();
            rc.setMimeType(mimeType);
            rc.setStatusCode(200);
            rc.setStatusText("OK");

            try {
                writeFile(requestedPath, mimeType, rc);
                ostream.close();
                istream.close();
            } catch (IOException e) {
                System.out.println("Error: cannot write response.");
                return;
            } finally {
            }
        }

        @Override
        public void run() {
            // obtain input stream from socket and wrap it to pushback input
            // stream
            // obtain output stream from socket
            try {
                istream = new PushbackInputStream(csocket.getInputStream());
                ostream = new BufferedOutputStream(csocket.getOutputStream());
            } catch (IOException e) {
                System.out.println("Error: cannot open " + e.getMessage());
            }

            // Then read complete request header from your client in separate
            // method...
            List<String> request = readRequest(istream);
            // If header is invalid (less then a line at least) return response
            // status 400
            if (request == null) {
                generateErrorResponse(400, "Bad request");
                return;
            }

            checkSession(request);

            String firstLine = request.get(0);
            // Extract (method, requestedPath, version) from firstLine
            // if method not GET or version not HTTP/1.0 or HTTP/1.1 return
            // response status 400
            String params = decomposeFirstLine(firstLine);
            if (params == null) {
                generateErrorResponse(400, "Bad request");
                return;
            }

            // (path, paramString) = split requestedPath to path and
            // parameterString
            String path;
            String paramString = null;

            String[] splittedParams = params.split("[?]");
            if (splittedParams.length <= 0 || splittedParams.length > 2) {
                generateErrorResponse(400, "Bad request");
                return;
            } else if (splittedParams.length == 2) {
                paramString = splittedParams[1];
            }

            path = splittedParams[0];

            // parseParameters(paramString); ==> your method to fill map
            // parameters
            if (!parseParameters(paramString)) {
                generateErrorResponse(400, "Bad request");
                return;
            }

            // requestedPath = resolve path with respect to documentRoot
            // if requestedPath is not below documentRoot, return response
            // status 403 forbidden
            // when we introduced workers: first check if path is valid worker
            // if it is ignore this block
            if (!workersMap.containsKey(path)
                    && !path.startsWith("/ext/EchoParams")
                    && !path.startsWith("/calc")) {
                path = resolveFilePath(path);

                if (path == null) {
                    return;
                }
            }

            try {
                internalDispatchRequest(path, true);
            } catch (Exception e1) {
                System.out.println("Error: " + e1.getMessage());
            }
        }

        /**
         * Resolves requested file path to absolute path on physical storage.
         * 
         * @param path
         *            relative path to resource
         * @return file path to resource or null if error response was generated
         */
        private String resolveFilePath(String path) {
            path = path.substring(1);

            Path requestedPath = documentRoot.resolve(path);
            if (!requestedPath.startsWith(documentRoot)) {
                generateErrorResponse(403, "Forbidden!");
                return null;
            }

            // check if requestedPath exists, is file and is readable; if
            // not,
            // return status 404
            File requestedFile = requestedPath.toFile();
            if (!requestedFile.exists()) {
                generateErrorResponse(404, "Not found.");
                return null;
            }
            if (!requestedFile.canRead()) {
                generateErrorResponse(404, "Not found.");
                return null;
            }

            path = requestedFile.getAbsolutePath();
            return path;
        }

        /**
         * Checks if session provided cookies, and stores them if they were
         * present in received header.
         * 
         * @param request
         *            header
         */
        private void checkSession(List<String> request) {
            RCCookie sidCandidate = null;
            String domain = null;

            for (String line : request) {
                if (line.startsWith("Host: ")) {
                    line = line.substring(6);
                    String[] splittedHost = line.split(":");
                    domain = splittedHost[0];
                    continue;
                }
                if (!line.startsWith("Cookie:")) continue;

                line = line.substring(7);

                String[] params = line.split(";");
                String name = null;
                String value = null;
                String path = null;
                Integer maxAge = null;
                boolean httpOnly = false;

                for (String param : params) {
                    String[] splitted = param.trim().split("[=]");

                    if (splitted.length != 2) {
                        System.out.println("Warning: invalid cookie - " + line);
                        break;
                    }

                    if (!splitted[0].equals("sid")) break;

                    switch (splitted[0]) {
                    case "sid":
                        name = "sid";
                        value = splitted[1].replaceAll("\"", "");
                        continue;
                    case "Domain":
                        domain = splitted[1];
                        continue;
                    case "Path":
                        path = splitted[1];
                        continue;
                    case "HttpOnly":
                        httpOnly = true;
                        continue;
                    case "Max-Age":
                        maxAge = Integer.parseInt(splitted[1]);
                        continue;
                    }
                }

                if (name.equals("sid")) {
                    sidCandidate = new RCCookie(name, value, maxAge, path,
                            domain, httpOnly);
                }
            }

            synchronized (this) {
                SessionMapEntry storedSession = null;

                if (sidCandidate == null) {
                    sidCandidate = new RCCookie("sid", buildRandomSID(), null,
                            "/", (domain == null) ? address : domain, true);
                    outputCookies.add(sidCandidate);

                    Calendar validLength = Calendar.getInstance();
                    validLength.add(Calendar.SECOND, sessionTimeout);

                    storedSession = new SessionMapEntry(sidCandidate.getValue(),
                            validLength.getTimeInMillis(),
                            new ConcurrentHashMap<String, String>());
                    sessions.put(sidCandidate.getValue(), storedSession);
                }

                storedSession = sessions.get(sidCandidate.getValue());
                Calendar now = Calendar.getInstance();

                if (storedSession == null) {
                    outputCookies.add(sidCandidate);

                    now.add(Calendar.SECOND, sessionTimeout);

                    storedSession = new SessionMapEntry(sidCandidate.getValue(),
                            now.getTimeInMillis(),
                            new ConcurrentHashMap<String, String>());
                    sessions.put(sidCandidate.getValue(), storedSession);

                } else {
                    if (!storedSession.isValid(now.getTimeInMillis(),
                            sessionTimeout)) {
                        storedSession.newMap();
                    }

                    storedSession.updateValid(sessionTimeout);
                }

                this.SID = sidCandidate.getValue();
                this.permPrams = storedSession.map;
            }
        }

        /**
         * Writes requested file to output stream.
         * 
         * @param requestedPath
         *            path to requested file
         * @param mimeType
         *            mime type of requested file
         * @param rc
         *            Request context
         * @throws IOException
         *             if input file is not readable or error occurs while
         *             writing to socket.
         */
        private void writeFile(Path requestedPath, String mimeType,
                RequestContext rc) throws IOException {

            if (mimeType.startsWith("text/")) {
                List<String> lines = Files.readAllLines(requestedPath,
                        StandardCharsets.UTF_8);
                rc.setEncoding("UTF-8");
                for (String line : lines) {
                    rc.write(line);
                }
            } else {
                BufferedInputStream bis = new BufferedInputStream(
                        Files.newInputStream(requestedPath));
                Long fileSize = requestedPath.toFile().length();
                rc.addAdditionalHeaderParameter("Content-Length: " + fileSize);
                byte[] buffer = new byte[BUFFER_SIZE];
                while (true) {
                    int i = bis.read(buffer);
                    if (i < 1) break;
                    rc.write(buffer);
                }
                bis.close();
            }
        }

        /**
         * Parses parameters list and adds them to request parameters list.
         * 
         * @param paramString
         *            text that contains parameters
         * @return true if parameters were parsed succesfully, false otherwise.
         */
        private boolean parseParameters(String paramString) {
            if (paramString == null) return true;
            String[] parameterPairs = paramString.split("[&]");

            for (String pair : parameterPairs) {
                String[] keyValue = pair.split("[=]");
                if (keyValue.length != 2) {
                    System.out.println("Error: invalid parameter - " + pair);
                    return false;
                }
                this.params.put(keyValue[0], keyValue[1]);
            }

            return true;
        }

        /**
         * Generates error response and writes it to output stream.
         * 
         * @param errorCode
         *            HTTP status code of error
         * @param errorText
         *            Text representation of error
         */
        private void generateErrorResponse(int errorCode, String errorText) {
            RequestContext invalid = getContext();
            invalid.setStatusCode(errorCode);
            invalid.setStatusText(errorText);

            try {
                invalid.write(errorText + "\r\n");
                ostream.close();
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }

        /**
         * Slices first line of request and sets method, HTTP version and
         * returns path/parameters string.
         * 
         * @param firstLine
         *            first line of request
         * @return path/parameters string or null if method or http version are
         *         invalid
         */
        private String decomposeFirstLine(String firstLine) {
            String[] decomposedLine = firstLine.trim().split(" ");

            if (decomposedLine.length != 3) {
                System.out.println("Error: " + firstLine + " is not valid.");
                return null;
            }

            if (decomposedLine[0].trim().equals("GET")) {
                this.method = "GET";
            } else {
                System.out
                        .println("Error: invalid method " + decomposedLine[0]);
                return null;
            }

            if (decomposedLine[2].matches("HTTP/1\\.[01]")) {
                this.version = decomposedLine[2];
            }

            return decomposedLine[1];
        }

        /**
         * Reads request from input stream
         * 
         * @param cis
         *            socket input stream
         * @return list of all lines from request
         */
        private List<String> readRequest(PushbackInputStream cis) {
            List<String> requestHeader = new ArrayList<>();

            BufferedReader r = new BufferedReader(
                    new InputStreamReader(cis, StandardCharsets.ISO_8859_1));

            try {
                String firstLine = r.readLine();
                System.out.println(
                        "Zahtjev" + System.lineSeparator() + firstLine);
                if (firstLine.trim().length() == 0) {
                    System.out.println(
                            "Error: invalid header - at least one line is expected.");
                    return null;
                }
                requestHeader.add(firstLine);

                String newLine = r.readLine();
                while (newLine != null) {
                    System.out.println(newLine);
                    if (newLine.trim().length() == 0) break;
                    requestHeader.add(newLine.trim());
                    newLine = r.readLine();
                }
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
                return null;
            }

            return requestHeader;
        }
    }

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        SmartHttpServer server = new SmartHttpServer("server.properties");

        server.start();
    }
}