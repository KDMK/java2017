package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Models visitors for all node types defined in
 * {@link hr.fer.zemris.java.custom.scripting.nodes}
 * 
 * @author Matija Bartolac
 *
 */
public interface INodeVisitor {
    /**
     * Visitor for textual node.
     * 
     * @param node which we want to visit
     */
    public void visitTextNode(TextNode node);

    /**
     * Visitor for ForLoop node.
     * 
     * @param node which we want to visit
     */
    public void visitForLoopNode(ForLoopNode node);

    /**
     * Visitor for Echo node.
     * 
     * @param node which we want to visit
     */
    public void visitEchoNode(EchoNode node);

    /**
     * Visitor for Document node.
     * 
     * @param node which we want to visit
     */
    public void visitDocumentNode(DocumentNode node);
}
