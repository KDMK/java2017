package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Base Elements class. Every element is derived from this class.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class Element {
    /**
     * Function prints out value of current element as string.
     *
     * @return <code>String</code> value of element.
     */
    public String asText() {
        return "";
    }
}
