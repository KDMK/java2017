package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.regex.Pattern;

/**
 * Lexer program is used to build tokens from given input text. TokenTypes for
 * this lexer are defined in @see TokenType enumeration. Language that we are
 * building lexer for consists of numbers, strings and symbols. Lexer stores
 * current token and has methods for generating new tokens.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Lexer {
    /**
     * Defines rule set upon which lexer generates tokens.
     */
    private LexerState state;
    /**
     * Array of input text characters for which we are generating tokens.
     */
    private char[] data;
    /**
     * Current token.
     */
    private Token token;
    /**
     * Index of next character for processing.
     */
    private int currentIndex;

    /**
     * Default constructor for constructing lexer.
     *
     * @param text
     *            Input text for which we are generating tokens.
     * @throws IllegalArgumentException
     *             If user passes null reference.
     */
    public Lexer(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }
        this.data = text.toCharArray();
        this.currentIndex = 0;
        this.state = LexerState.TEXT;
    }

    /**
     * Returns last generated token. This method does not generate next token.
     *
     * @return Last generated token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Generates and return next token from input text.
     *
     * @return Currently generated token
     */
    public Token nextToken() {
        // Check if current token is end of file.
        if (token != null && token.getType() == TokenType.EOF) {
            throw new LexerException("No more tokens in input file.");
        }
        if (state == LexerState.TEXT) {
            return generateTokenText();
        } else {
            return generateTokenTag();
        }
    }

    /**
     * Sets lexer state. Valid lexer states are defined in {@link LexerState}.
     *
     * @param state
     *            <code>LexerState</code>
     */
    public void setState(LexerState state) {
        if (state == null) {
            throw new IllegalArgumentException("Lexer state can't be null.");
        }
        this.state = state;
    }

    /**
     * Checks whether next character in input sequence is escaped character in
     * String token.
     *
     * @return <code>true</code> if next character in data array is escaped
     *         character.
     */
    private boolean isEscapedInString() {
        // First check if you are in the end of input sequence. Then check if
        // next character is valid escape sequence.
        return (currentIndex < data.length - 1)
                && (data[currentIndex + 1] == '\\'
                        || data[currentIndex + 1] == '\"'
                        || data[currentIndex + 1] == 'n'
                        || data[currentIndex + 1] == 't'
                        || data[currentIndex + 1] == 'r');
    }

    /**
     * Checks whether next character in input sequence is escaped character in
     * Text token.
     *
     * @return <code>true</code> if next character in data array is escaped
     *         character.
     */
    private boolean isEscapedInText() {
        // First check if you are in the end of input sequence. Then check if
        // next character is valid escape sequence.
        return (currentIndex < data.length - 1)
                && (data[currentIndex] == '\\' || data[currentIndex] == '{');
    }

    /**
     * Skips blanks in character sequence. Characters that are interpreted as
     * blanks are: <code>' ', '\n', '\r', '\n'</code>
     */
    private void skipBlanks() {
        while (currentIndex < data.length) {
            if (isBlank(data[currentIndex])) {
                currentIndex++;
                continue;
            }
            return;
        }
    }

    /**
     * Checks if current character is blank character.
     *
     * @param curCharacter
     *            <code>char</code> value that is checked
     * @return <code>true</code> if next character is blank character
     */
    private boolean isBlank(char curCharacter) {
        return curCharacter == ' ' || curCharacter == '\r'
                || curCharacter == '\n' || curCharacter == '\t';
    }

    /**
     * Groups next n characters in text token.
     *
     * @return Next token in character input.
     */
    private Token generateTokenText() {
        StringBuilder curInputToken = new StringBuilder();

        while (currentIndex <= data.length) {
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (currentIndex >= data.length
                    && curInputToken.toString().equals("")) {
                token = new Token(TokenType.EOF, null);
                return token;
            }
            char curInputCharacter = data[currentIndex++];

            // Take first character or and determine is it text or tag.
            if (isBlank(curInputCharacter) || curInputCharacter != '{') {
                // If this is escaped character add it to the string.
                if (curInputCharacter == '\\') {
                    if (isEscapedInText()) {
                        // I know it's stupid but it does the magic.
                        if (data[currentIndex] == '\\') {
                            curInputToken.append('\\');
                        }
                        curInputCharacter = data[currentIndex++];
                    } else {
                        throw new LexerException(
                                "Illegal escape character in text.");
                    }
                }

                curInputToken.append(curInputCharacter);

                if (currentIndex >= data.length || data[currentIndex] == '{') {
                    token = new Token(TokenType.TEXT, curInputToken.toString());
                    return token;
                }
                continue;
            }
            // If this is escaped character add it to the string.

            if (curInputCharacter == '{') {
                curInputToken.append(curInputCharacter);
                curInputToken.append(data[currentIndex++]);
                token = new Token(TokenType.TAG_START,
                        curInputToken.toString());
                state = LexerState.TAG;
                return token;
            }
        }

        return null;
    }

    /**
     * Groups next n characters in text token.
     *
     * @return Next token in character input.
     */
    private Token generateTokenTag() {
        // curInputToken is used to store largest valid token that we read from
        // data array
        StringBuilder curInputToken = new StringBuilder();

        while (currentIndex <= data.length) {
            skipBlanks();
            // There is no more characters in data array. Generate EOF token and
            // return null.
            if (currentIndex >= data.length
                    && curInputToken.toString().equals("")) {
                token = new Token(TokenType.EOF, null);
                return token;
            }
            char curInputCharacter = data[currentIndex++];

            // Expect tag end after this.
            if (curInputCharacter == '$') {
                if (currentIndex >= data.length || data[currentIndex] != '}') {
                    throw new LexerException("Invalid closing tag");
                }
                curInputToken.append(curInputCharacter);
                curInputToken.append(data[currentIndex++]);
                token = new Token(TokenType.TAG_END, curInputToken.toString());
                state = LexerState.TEXT;
                return token;
            }

            // If current input character is =, and followed with blank space it
            // is valid tag name.
            if (curInputCharacter == '=') {
                curInputToken.append(curInputCharacter);
                token = new Token(TokenType.TAG_NAME, curInputToken.toString());
                return token;
            }

            // If following characters are valid tag or variable name group them
            // and create token.
            if (Character.isLetter(curInputCharacter)) {
                curInputToken.append(curInputCharacter);
                buildName(curInputToken);
                token = new Token(TokenType.NAME, curInputToken.toString());
                return token;
            }

            // Check if current character is number(include negative number)
            if (Character.isDigit(curInputCharacter)
                    || (Character.isDigit(data[currentIndex])
                            && curInputCharacter == '-')) {
                curInputToken.append(curInputCharacter);
                buildNumber(curInputToken);

                return generateNumberToken(curInputToken.toString());

            }

            // If current character is valid symbol
            if (curInputCharacter == '-' || curInputCharacter == '+'
                    || curInputCharacter == '*' || curInputCharacter == '/'
                    || curInputCharacter == '^') {
                token = new Token(TokenType.OPERATOR,
                        Character.toString((curInputCharacter)));
                return token;
            }

            if (curInputCharacter == '\"') {
                buildString(curInputToken);

                try {
                    return generateNumberToken(curInputToken.toString());
                } catch (NumberFormatException ex) {
                }

                token = new Token(TokenType.STRING_CONSTANT,
                        curInputToken.toString());
                return token;
            }

            // If current character is @ character followed by allowed
            // characters build function name.
            if (curInputCharacter == '@') {
                if (!Character.isLetter(data[currentIndex])) {
                    throw new LexerException("Illegal function name!");
                }
                curInputToken.append(curInputCharacter);
                buildName(curInputToken);
                token = new Token(TokenType.FUNCTION, curInputToken.toString());
                return token;
            }
            throw new LexerException("Invalid token!");
        }

        return null;
    }

    /**
     * Builds name token value. Valid names are those starting with letter,
     * followed by zero or more letters, digits or underscores.
     *
     * @param curInputToken
     *            String builder that stores current token that we are
     *            processing.
     * @throws LexerException
     *             if input is not valid function name.
     */
    private void buildName(StringBuilder curInputToken) {
        while (currentIndex <= data.length
                && !Character.isWhitespace(data[currentIndex])
                && data[currentIndex] != '$') {
            if (Character.isLetterOrDigit(data[currentIndex])
                    || data[currentIndex] == '_') {
                curInputToken.append(data[currentIndex++]);
            } else {
                throw new LexerException("Invalid name!");
            }
        }
    }

    /**
     * Builds string token value. Valid escape sequences in strings are \" and
     * \\.
     *
     * @param curInputToken
     *            String builder that stores current token that we are
     *            processing.
     * @throws LexerException
     *             if input contains invalid escape sequences or if quote marks
     *             are not closed.
     */
    private void buildString(StringBuilder curInputToken) {
        while (currentIndex <= data.length) {
            if (data[currentIndex] == '\"') {
                currentIndex++;
                return;
            }
            // Check if current escaped character is allowed in string.
            if (data[currentIndex] == '\\') {
                if (isEscapedInString()) {
                    switch (data[++currentIndex]) {
                    case 'n':
                        curInputToken.append('\n');
                        currentIndex++;
                        continue;
                    case 'r':
                        curInputToken.append('\r');
                        currentIndex++;
                        continue;
                    case 't':
                        curInputToken.append('\t');
                        currentIndex++;
                        continue;
                    default:
                        curInputToken.append(data[++currentIndex]);
                        currentIndex++;
                        continue;
                    }
                }
                throw new LexerException("Illegal escape character in string.");
            }
            curInputToken.append(data[currentIndex++]);
        }
        throw new LexerException("Closing double qoute mark not found!");
    }

    /**
     * Builds Number token value. If input token contains letters and symbols
     * (except - sing) number will be interpreted as string constant!
     *
     * @param curInputToken
     *            String builder that stores current token that we are
     *            processing
     */
    private void buildNumber(StringBuilder curInputToken) {
        boolean hasDot = false;
        while (currentIndex <= data.length
                && (!Character.isWhitespace(data[currentIndex])
                        && data[currentIndex] != '$')) {
            if (Character.isDigit(data[currentIndex])) {
                curInputToken.append(data[currentIndex++]);
                continue;
            }
            if (data[currentIndex] == '.' && !hasDot) {
                hasDot = true;
                curInputToken.append(data[currentIndex++]);
                continue;
            }
            throw new LexerException("Illegal number format.");
        }
    }

    /**
     * Function tries to build number(Integer or Double) token form given
     * string. If string do not contain any valid numbers function will throw
     * NumberFormatExcepiton.
     *
     * @param curInputToken
     *            String representation of current token.
     * @return Double or Integer constant token.
     */
    private Token generateNumberToken(String curInputToken) {
        if (Pattern.matches(".*[a-zA-Z]+.*", curInputToken)) {
            throw new NumberFormatException();
        }

        // Check if string contains number
        if (!curInputToken.contains(".")) {
            try {
                Integer value = Integer.parseInt(curInputToken.toString());
                token = new Token(TokenType.INT_CONSTANT, value);
                return token;
            } catch (NumberFormatException ex) {
                token = new Token(TokenType.STRING_CONSTANT, curInputToken);
                return token;
                // TODO: i to sam dodao
//                throw new LexerException(curInputToken.toString()
//                        + " is not valid number format.");
            }
        } else {
            try {
                Double value = Double.parseDouble(curInputToken.toString());
                token = new Token(TokenType.DOUBLE_CONSTANT, value);
                return token;
            } catch (NumberFormatException ex) {
                // TODO: to sam mjenjao
//                throw new LexerException(curInputToken.toString()
//                        + " is not valid number format.");
                token = new Token(TokenType.STRING_CONSTANT, curInputToken);
                return token;
            }
        }
    }
}
