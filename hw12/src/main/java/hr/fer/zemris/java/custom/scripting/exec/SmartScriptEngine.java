package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.text.DecimalFormat;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * SmartSctiptEngine is used to process parsed script and execute it.
 * 
 * @author Matija Bartolac
 *
 */
public class SmartScriptEngine {
    /**
     * Reference to parsed tree root.
     */
    private DocumentNode documentNode;
    /**
     * Reference to given request context.
     */
    private RequestContext requestContext;
    /**
     * Multistack storage.
     */
    private ObjectMultistack multistack = new ObjectMultistack();

    /**
     * Default constructor.
     * 
     * @param documentNode
     *            reference to parsed tree root.
     * @param requestContext
     *            reference to given request context.
     */
    public SmartScriptEngine(DocumentNode documentNode,
            RequestContext requestContext) {
        this.documentNode = documentNode;
        this.requestContext = requestContext;
    }

    /**
     * Method executes given script.
     */
    public void execute() {
        try {
            documentNode.accept(visitor);
            requestContext.write(System.lineSeparator());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }

    /**
     * Node visitor that process elements in parsed tree.
     */
    private INodeVisitor visitor = new INodeVisitor() {
        @Override
        public void visitTextNode(TextNode node) {
            if (node.getText().equals("{$END$}")) {
                return;
            }
            try {
                requestContext.write(node.getText());
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.exit(-1);
            }
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            String varName = node.getVariable().asText();
            
            Number start = resolveNumber(node.getStartExpression());
            Double end = ((Number) resolveNumber(node.getEndExpression()))
                    .doubleValue();

            Number step;
            if (node.getStepExpression() == null) {
                step = new Integer(1);
            } else {
                step = ((Number) resolveNumber(node.getStepExpression()));
            }
            multistack.push(varName, new ValueWrapper(start));

            int numOfChildren = node.numberOfChilderen();
            while (start.doubleValue() <= end) {
                for (int i = 0; i < numOfChildren; i++) {
                    node.getChild(i).accept(this);
                }

                start = start.doubleValue() + step.doubleValue();
                multistack.peek(varName).add(step);
            }

            multistack.pop(varName);
        }

        /**
         * Resolves number stored in element.
         * 
         * @param expression
         *            element holding number value
         * @return Integer or Double value of number
         */
        private Number resolveNumber(Element expression) {
            if (expression instanceof ElementConstantInteger) {
                return ((ElementConstantInteger) expression).getValue();
            }
            if (expression instanceof ElementConstantDouble) {
                return ((ElementConstantDouble) expression).getValue();
            }

            throw new IllegalArgumentException(
                    "Error: Value is not a valid number!");
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            ObjectStack stack = new ObjectStack();
            Element[] elements = node.getElements();

            for (int i = 0, len = elements.length; i < len; i++) {
                Element e = elements[i];

                if (e instanceof ElementConstantDouble) {
                    stack.push(e.asText());
                    continue;
                }
                if (e instanceof ElementConstantInteger) {
                    stack.push(((ElementConstantInteger) e).getValue());
                    continue;
                }
                if (e instanceof ElementVariable) {
                    if (((ElementVariable) e).getName() == null) {
                        throw new IllegalArgumentException("Error: variable "
                                + ((ElementVariable) e).getName()
                                + " not defined.");
                    }

                    stack.push(getNumber(e.asText()));
                    continue;
                }
                if (e instanceof ElementString) {
                    stack.push(((ElementString) e).getValue());
                    continue;
                }
                if (e instanceof ElementOperator) {
                    Object num2 = stack.pop();
                    Object num1 = stack.pop();
                    String operation = e.asText();

                    try {
                        performOperation(stack, operation, num1, num2);
                    } catch (EmptyStackException ex) {
                        System.out.println(
                                "Error: not enough parameters to perform operation "
                                        + operation);
                    }
                }
                if (e instanceof ElementFunction) {
                    // TODO: Handle functions here.
                    try {
                        performOperation(stack, e.asText());
                        continue;
                    } catch (EmptyStackException ex1) {
                        System.out.println(
                                "Error: Invalid number of parameters for function: "
                                        + e.asText());
                    } catch (UnsupportedOperationException ex2) {
                        System.out.println(ex2.getMessage());
                    }
                }
            }
            if (!stack.isEmpty()) {
                StringBuilder sb = new StringBuilder();

                while (!stack.isEmpty()) {
                    sb.insert(0, stack.pop());
                }

                try {
                    requestContext
                            .write(sb.toString());
                } catch (IOException e) {
                    System.out.println("Error: cannot write to output stream.");
                    System.exit(-1);
                }
            }
        }

        private void performOperation(ObjectStack stack, String operation,
                Object first, Object second) {
            first = getNumber(first);
            second = getNumber(second);

            ValueWrapper num1 = new ValueWrapper(first);

            switch (operation) {
            case "+":
                num1.add(second);
                break;
            case "-":
                num1.subtract(second);
                break;
            case "*":
                num1.multiply(second);
                break;
            case "/":
                num1.divide(second);
                break;
            default:
                throw new IllegalStateException(
                        "Error: Unsupported operation: " + operation);
            }
            stack.push(num1.getValue());
        }

        private Number getNumber(Object num) {
            if (num == null) {
                throw new IllegalArgumentException(
                        "Error: in function getNumber, num canot be null");
            }
            if (num instanceof Integer || num instanceof Double) {
                return (Number) num;
            }

            ValueWrapper v = null;
            String val;
            if (num instanceof String) {
                Number out = createNumber((String) num);
                if (out != null) {
                    return out;
                }

                if (!multistack.isEmpty((String) num)) {
                    v = multistack.peek((String) num);
                }
                if (v == null) {
                    val = requestContext.getParameter((String) num);
                    if (val == null) {
                        throw new IllegalArgumentException(
                                "Error: no such variable: " + num);
                    }
                } else {
                    val = v.getValue().toString();
                }

                return getNumber(val);
            }

            throw new IllegalArgumentException(
                    "Error: " + num + " is not valid number or variable.");
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            int numOfChildren = node.numberOfChilderen();

            for (int i = 0; i < numOfChildren; i++) {
                node.getChild(i).accept(this);
            }
        }

        // ====================================================================
        // =========================== Functions ==================+===========
        // ====================================================================
        /**
         * Determines which operation has been called and performs it.
         * 
         * @param stack
         *            local parameters stack
         * @param operationName
         *            name of operation to perform
         * @throws UnsupportedOperationException
         *             if user tries to call function that is not supported
         * @throws EmptyStackException
         *             if there is not enough parameters on stack needed for
         *             selected operation
         */
        private void performOperation(ObjectStack stack, String operationName) {
            switch (operationName) {
            case "@sin":
                sin(stack);
                return;
            case "@decfmt":
                decfmt(stack);
                return;
            case "@dup":
                dup(stack);
                return;
            case "@swap":
                swap(stack);
                return;
            case "@setMimeType":
                setMimeType(stack);
                return;
            case "@paramGet":
                paramGet(stack, "parameters");
                return;
            case "@pparamGet":
                paramGet(stack, "persistent");
                return;
            case "@pparamSet":
                paramSet(stack, "persistent");
                return;
            case "@pparamDel":
                paramRemove(stack, "persistent");
                return;
            case "@tparamGet":
                paramGet(stack, "temporary");
                return;
            case "@tparamSet":
                paramSet(stack, "temporary");
                return;
            case "@tparamDel":
                paramRemove(stack, "temporary");
                return;
            default:
                throw new UnsupportedOperationException("Error: function "
                        + operationName + " is not supported.");
            }
        }

        /**
         * Calculates sin of value from top of stack.
         * 
         * @param stack
         *            local parameters stack
         * @throws IllegalArgumentException
         *             if passed argument is not of number type
         */
        private void sin(ObjectStack stack) {
            Object o = stack.pop();

            checkNumber(o, "sin");
            Double result = Math.sin(((Number) o).doubleValue());

            stack.push(result);
        }

        /**
         * Formats passed decimal number using passed style.
         * 
         * @param stack
         *            local parameters stack
         * @throws IllegalArgumentException
         *             if string from which we are building decimal format is
         *             invalid
         *             <p>
         *             if user tries to build decimal format from something that
         *             is not string
         *             <p>
         *             if passed number is not a valid number
         */
        private void decfmt(ObjectStack stack) {
            Object f = stack.pop();
            Object x = stack.pop();

            if (!(f instanceof String)) {
                throw new IllegalArgumentException(
                        "Error: cannot make decimal format object from "
                                + f.getClass().getName());
            }

            DecimalFormat fmt = new DecimalFormat((String) f);
            checkNumber(x, "decfmt");

            String number = fmt.format(((Number) x).doubleValue());

            stack.push(number);
        }

        /**
         * Duplicates element on top of stack.
         * 
         * @param stack
         *            local parameters stack
         */
        private void dup(ObjectStack stack) {
            Object o = stack.peek();
            stack.push(o);
        }

        /**
         * Swaps top two elements from stack.
         * 
         * @param stack
         *            local parameters stack
         */
        private void swap(ObjectStack stack) {
            Object o2 = stack.pop();
            Object o1 = stack.pop();

            stack.push(o2);
            stack.push(o1);
        }

        /**
         * Sets mime type of document that we are currently evaluating.
         * 
         * @param stack
         *            local parameters stack
         */
        private void setMimeType(ObjectStack stack) {
            Object mimeType = stack.pop();

            if (!(mimeType instanceof String)) {
                throw new IllegalArgumentException(
                        "Error: cannot set mime type from object type "
                                + mimeType.getClass().getName());
            }

            requestContext.setMimeType((String) mimeType);
        }

        /**
         * Gets parameter from requestContext parameters map that is mapped to
         * name and pushes that value to stack. If there is no such mapping it
         * pushes defValue onto stack.
         * 
         * @param stack
         *            local parameters stack
         */
        private void paramGet(ObjectStack stack, String mapName) {
            Object defValue = stack.pop();
            Object name = stack.pop();

            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Error: Key in " + mapName
                        + " " + name.getClass().getName());
            }

            String value = null;

            switch (mapName.toLowerCase()) {
            case "parameters":
                value = requestContext.getParameter((String) name);
                break;
            case "temporary":
                value = requestContext.getTemporaryParameter((String) name);
                break;
            case "persistent":
                value = requestContext.getPersistentParameter((String) name);
            }

            if (value != null) {
                resolveValue(stack, value);
            } else {
                stack.push(defValue);
            }
        }

        /**
         * Removes parameter from persistent parameters or temporary parameters.
         * 
         * @param stack
         *            local parameters stack
         * @param mapName
         *            name of map from which we are removing parameter
         * @throws IllegalArgumentException
         *             if user provides invalid key
         */
        private void paramSet(ObjectStack stack, String mapName) {
            Object name = stack.pop();
            Object value = stack.pop();

            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Error: Key in " + mapName
                        + " " + name.getClass().getName());
            }

            if (!(value instanceof String)) {
                value = String.valueOf(value);
            }

            switch (mapName.toLowerCase()) {
            case "parameters":
                throw new IllegalArgumentException(
                        "Error: cannot write to parameters map.");
            case "temporary":
                requestContext.setTemporaryParameter((String) name,
                        (String) value);
            case "persistent":
                requestContext.setPersistentParameter((String) name,
                        (String) value);
            }
        }

        /**
         * Removes parameter from persistent parameters or temporary parameters.
         * 
         * @param stack
         *            local parameters stack
         * @param mapName
         *            name of map from which we are removing parameter
         */
        private void paramRemove(ObjectStack stack, String mapName) {
            Object name = stack.pop();

            if (!(name instanceof String)) {
                throw new IllegalArgumentException("Error: Key in " + mapName
                        + " " + name.getClass().getName());
            }

            switch (mapName.toLowerCase()) {
            case "parameters":
                throw new IllegalArgumentException(
                        "Error: cannot write to parameters map.");
            case "temporary":
                requestContext.removeTemporaryParameter((String) name);
            case "persistent":
                requestContext.removePersistentParameter((String) name);
            }
        }

        // ====================================================================
        // ========================== Helper methods ==========================
        // ====================================================================

        /**
         * Tries to resolve value from parameters list to number. If value can't
         * be resolved to number store it as a string.
         * 
         * @param stack
         *            where we want to push object
         * @param value
         *            value that we want to push on stack
         */
        private void resolveValue(ObjectStack stack, String value) {
            Number newNumber = createNumber(value);

            stack.push(newNumber == null ? value : newNumber);
        }

        private Number createNumber(String value) {
            try {
                Integer newNum = Integer.parseInt(value);
                return newNum;
            } catch (NumberFormatException ignorable) {
            }

            try {
                Double newNum = Double.parseDouble(value);
                return newNum;
            } catch (NumberFormatException ignorable) {
            }

            return null;
        }

        /**
         * Checks if passed parameter is any of valid number types.
         * 
         * @param o
         *            object under test
         * @param operationName
         *            name of operation for which we are checking parameters.
         */
        private void checkNumber(Object o, String operationName) {
            if (!(o instanceof Number)) {
                throw new IllegalArgumentException("Error: Cannot perform "
                        + operationName + " operation on "
                        + o.getClass().getSimpleName() + " type.");
            }
        }
    };
}
