package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models function expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementFunction extends Element {
    /**
     * Name of stored function.
     */
    private String name;

    /**
     * Default constructor. Creates new function element with given name.
     *
     * @param name
     *            of function
     */
    public ElementFunction(String name) {
        this.name = name;
    }

    /**
     * Returns name of function element.
     *
     * @return <code>String</code> name of element.
     */
    public String getName() {
        return name;
    }

    @Override
    public String asText() {
        return name;
    }
}
