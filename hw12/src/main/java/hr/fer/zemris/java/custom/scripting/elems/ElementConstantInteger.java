package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models integer constant expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementConstantInteger extends Element {
    /**
     * Value of stored integer constant.
     */
    private Integer value;

    /**
     * Default constructor. Creates new constant integer element with given
     * name.
     *
     * @param value
     *            <code>int</code> that we are storing.
     */
    public ElementConstantInteger(int value) {
        this.value = value;
    }

    /**
     * Returns value of element.
     *
     * @return <code>int</code> value of element.
     */
    public Integer getValue() {
        return value;
    }

    @Override
    public String asText() {
        return String.valueOf(value);
    }
}
