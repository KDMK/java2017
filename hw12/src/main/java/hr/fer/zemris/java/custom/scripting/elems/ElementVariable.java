package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models element expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementVariable extends Element {
    /**
     * Name of variable element.
     */
    private String name;

    /**
     * Default constructor. Creates new variable element with given name.
     *
     * @param name
     *            of variable.
     */
    public ElementVariable(String name) {
        this.name = name;
    }

    @Override
    public String asText() {
        return name.trim();
    }

    /**
     * Returns name of this variable element.
     *
     * @return <code>String</code> name.
     */
    public String getName() {
        return name.trim();
    }
}
