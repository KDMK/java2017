package hr.fer.zemris.java.webserver;

/**
 * Interface that defines methods which is used to process requests.
 * 
 * @author Matija Bartolac
 *
 */
public interface IWebWorker {

    /**
     * Process request defined by this worker.
     * 
     * @param context
     *            execution context
     * @throws Exception
     *             if there was error during initializing, reading, or
     *             processing request.
     */
    public void processRequest(RequestContext context) throws Exception;
}
