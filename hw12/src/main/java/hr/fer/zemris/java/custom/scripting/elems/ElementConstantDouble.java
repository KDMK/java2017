package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class models double constant expression.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ElementConstantDouble extends Element {
    /**
     * Value of stored double constant.
     */
    private Double value;

    /**
     * Default constructor. Creates new constant double element with given name.
     *
     * @param value
     *            <code>double</code> that we are storing.
     */
    public ElementConstantDouble(double value) {
        this.value = value;
    }

    /**
     * Returns value of element.
     *
     * @return <code>double</code> value of element.
     */
    public Double getValue() {
        return value;
    }

    @Override
    public String asText() {
        return String.valueOf(value);
    }
}
