package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * Base class for all graph nodes.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public abstract class Node {
    /**
     * Array of children node's children.
     */
    private ArrayIndexedCollection children;
    /**
     * Signals if node have any children.
     */
    private boolean haveChildren;

    /**
     * Adds given child to internally managed collection of children. Collection
     * is created only if there is need to store child Nodes.
     *
     * @param child
     *            <code>Node</code> of child that we are adding to children
     *            array
     */
    public void addChildNode(Node child) {
        if (!haveChildren) {
            children = new ArrayIndexedCollection();
            haveChildren = true;
        }

        children.add(child);
    }

    /**
     * Returns a number of (direct) children.
     *
     * @return <int> number of children.
     */
    public int numberOfChilderen() {
        if (!haveChildren) {
            return 0;
        }
        return children.size();
    }

    /**
     * Returns selected child.
     *
     * @param index
     *            Index of child node that we want to get.
     * @return Node at given index.
     * @throws IndexOutOfBoundsException
     *             Exception is thrown if user tries to get child with index
     *             that is not in array
     */
    public Node getChild(int index) {
        // Check if array is created. If this step is omitted
        // NullPointerException is thrown in next step as we are trying to call
        // get(index) method on null.
        if (!haveChildren) {
            throw new IndexOutOfBoundsException(
                    "There is no elements children array!");
        }
        // Function get throws IndexOutOfBounds exception if called with invalid
        // index.
        return (Node) children.get(index);
    }

    /**
     * Defines operation to be performed for passed visitor.
     * 
     * @param visitor
     *            specific node visitor
     */
    public abstract void accept(INodeVisitor visitor);
}
