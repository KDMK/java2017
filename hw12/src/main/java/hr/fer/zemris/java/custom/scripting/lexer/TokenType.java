package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of token types present in language for which we are building
 * lexer.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public enum TokenType {
    /**
     * Symbols end of file. There are no more tokens further.
     */
    EOF,
    /**
     * Symbol combination that designates tag start - {$.
     */
    TAG_START,
    /**
     * Symbol combination that designates tag end - $}.
     */
    TAG_END,
    /**
     * Tag names are = or variable name.
     */
    TAG_NAME,
    /**
     * Every sequence that starts with letter and after follows zero or more
     * letters, digits or underscores.
     */
    NAME,
    /**
     * Every sequence that starts with @ followed by letter and then zeor or
     * more letters, digits or underscores.
     */
    FUNCTION,
    /**
     * Operator sign that designates supported mathematical operations.
     */
    OPERATOR,
    /**
     * Every character sequence outside of tags.
     */
    TEXT,
    /**
     * Every character sequence inside double quote marks.
     */
    STRING_CONSTANT,
    /**
     * Integer constant.
     */
    INT_CONSTANT,
    /**
     * Double constant.
     */
    DOUBLE_CONSTANT;
}
