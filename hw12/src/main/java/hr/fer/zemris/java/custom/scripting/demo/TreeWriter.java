package hr.fer.zemris.java.custom.scripting.demo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Demo program for showing visitor design pattern on our tree document model
 * created by parser. Program accepts single command line argument: relative
 * path to script.
 * 
 * @author Matija Bartolac
 *
 */
public class TreeWriter {

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid number of arguments! Aborting...");
            System.exit(-1);
        }

        String basePath = new File(args[0]).getAbsolutePath();
        String docBody = null;

        try {
            docBody = new String(Files.readAllBytes(Paths.get(basePath)));
        } catch (IOException e) {
            System.out.println(
                    "File: \"" + e.getMessage() + "\" doesn't exists.");
            return;
        }

        SmartScriptParser parser = null;
        try {
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException ex) {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }

        WriterVisitor visitor = new WriterVisitor();
        parser.getDocumentNode().accept(visitor);
    }

    /**
     * One implementation of INodeVisitor. It traverses throughout tree
     * structure of document and reconstructs original document structure in
     * textual format.
     * 
     * @author Matija Bartolac
     *
     */
    private static class WriterVisitor implements INodeVisitor {

        @Override
        public void visitTextNode(TextNode node) {
            System.out.print(node.toString());
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            int numOfChildren = node.numberOfChilderen();

            System.out.print(node.toString());

            for (int i = 0; i < numOfChildren; i++) {
                node.getChild(i).accept(this);
            }
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            System.out.print(node.toString());
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            int numOfChildren = node.numberOfChilderen();

            for (int i = 0; i < numOfChildren; i++) {
                node.getChild(i).accept(this);
            }
        }

    }
}
