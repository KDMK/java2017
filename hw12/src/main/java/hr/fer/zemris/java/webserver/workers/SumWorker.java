package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class models web worker which, when requested, takes two numbers from
 * parameters map and calculates their sum.
 * 
 * @author Matija Bartolac
 *
 */
public class SumWorker implements IWebWorker {

    @Override
    public void processRequest(RequestContext context) throws Exception {
        String a = context.getParameter("a");
        String b = context.getParameter("b");

        int num1 = (a == null) ? 1 : Integer.parseInt(a);
        int num2 = (b == null) ? 2 : Integer.parseInt(b);

        String sum = String.valueOf(num1 + num2);

        context.setTemporaryParameter("a", String.valueOf(num1));
        context.setTemporaryParameter("b", String.valueOf(num2));
        context.setTemporaryParameter("sum", sum);
    }

}
