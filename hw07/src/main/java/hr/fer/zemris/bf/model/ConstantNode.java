package hr.fer.zemris.bf.model;

/**
 * Class models expression constant.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ConstantNode implements Node{
    /**
     * Boolean value.
     */
    private Byte value;

    /**
     * Default constructor.
     * 
     * @param value value of this constant.
     */
    public ConstantNode(Byte value) {
        this.value = value;
    }

    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }
    
    /**
     * @return value of this constant node.
     */
    public Byte getValue() {
        return this.value;
    }
}
