package hr.fer.zemris.bf.model;

import java.util.function.UnaryOperator;

/**
 * Class models binary operator node. Unary operator in this grammars is
 * <code>not</code>.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class UnaryOperatorNode implements Node {
    /**
     * Name of unary operator.
     */
    private String name;
    /**
     * Child of unary operator.
     */
    private Node child;
    /**
     * Operation that unary operator performs.
     */
    private UnaryOperator<Boolean> operator;

    /**
     * Default constructor.
     * 
     * @param name
     *            of operator.
     * @param child
     *            of unary operator
     * @param operator
     *            operation that is performed
     */
    public UnaryOperatorNode(String name, Node child,
            UnaryOperator<Boolean> operator) {
        this.name = name;
        this.child = child;
        this.operator = operator;
    }

    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * @return name of operator.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return unary operator.
     */
    public UnaryOperator<Boolean> getOperator() {
        return this.operator;
    }

    /**
     * @return child of this node.
     */
    public Node getChild() {
        return this.child;
    }
}
