package hr.fer.zemris.bf.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

import hr.fer.zemris.bf.model.Node;

/**
 * Utility class that contains extra methods for working with Boolean functions.
 * 
 * @author Matija Bartolac
 * @version v1.0
 */
public class Util {

    /**
     * Method generates truth table for given variables, and for each
     * combination calls consumer that performs action upon it.
     * 
     * @param variables
     *            list of all variables present in given expression
     * @param consumer
     *            action that we want to perform for given variables
     */
    public static void forEach(List<String> variables,
            Consumer<boolean[]> consumer) {
        List<boolean[]> table = createTruthTable(variables.size());

        table.forEach(consumer);
    }

    /**
     * Evaluates given expression and returns only rows that match provided
     * expression value.
     * 
     * @param variables
     *            list of input variables.
     * @param expression
     *            expression that we want to evaluate.
     * @param expressionValue
     *            value that we are comparing evaluated values to.
     * @return set of rows that match desired value.
     */
    public static Set<boolean[]> filterAssignments(List<String> variables,
            Node expression, boolean expressionValue) {
        List<boolean[]> filteredRows = new ArrayList<>();
        ExpressionEvaluator eval = new ExpressionEvaluator(variables);

        forEach(variables, values -> {
            eval.setValues(values);
            expression.accept(eval);
            if (eval.getResult() == expressionValue) {
                filteredRows.add(values);
            }
        });

        return new LinkedHashSet<boolean[]>(filteredRows);
    }

    /**
     * Returns position of input combination in truth table.
     * 
     * @param values
     *            combination of input variables.
     * @return position in table.
     */
    public static int booleanArrayToInt(boolean[] values) {
        List<boolean[]> table = createTruthTable(values.length);
        String lookupRow = Arrays.toString(values);
        
        int pos = 0;
        for(int size = table.size(); pos < size; pos++) {
            String tableRow = Arrays.toString(table.get(pos));
            if(tableRow.equals(lookupRow)) break;
        }
        
        return pos;
    }

    /**
     * Returns set of minterm positions in truth table for given expression.
     * 
     * @param variables
     *            input variables
     * @param expression
     *            expression for which we are generating minterms
     * @return set of positions of minterms in truth table.
     */
    public static Set<Integer> toSumOfMinterms(List<String> variables,
            Node expression) {
        Set<boolean[]> minterms = filterAssignments(variables, expression,
                true);
        Set<Integer> positions = new TreeSet<>();

        for (boolean[] row : minterms) {
            positions.add(booleanArrayToInt(row));
        }

        return positions;
    }

    /**
     * Returns set of maxterm positions in truth table for given expression.
     * 
     * @param variables
     *            input variables
     * @param expression
     *            expression for which we are generating maxterms
     * @return set of positions of maxterms in truth table.
     */
    public static Set<Integer> toProductOfMaxterms(List<String> variables,
            Node expression) {
        Set<boolean[]> maxterms = filterAssignments(variables, expression,
                false);
        Set<Integer> positions = new TreeSet<>();

        for (boolean[] row : maxterms) {
            positions.add(booleanArrayToInt(row));
        }

        return positions;
    }

    /**
     * Creates truth table for given number of variables.
     * 
     * @param numOfVariables
     *            for which we create truth table.
     * @return truth table in form of list, every list item is one row in table
     */
    private static List<boolean[]> createTruthTable(int numOfVariables) {
        int rows = (int) Math.pow(2, numOfVariables);
        List<boolean[]> table = new ArrayList<>(rows);

        for (int i = 0; i < rows; i++) {
            boolean[] row = new boolean[numOfVariables];
            for (int k = 0, j = numOfVariables - 1; j >= 0; j--, k++) {
                int number = i / (int) Math.pow(2, j) % 2;
                row[k] = (number == 1);
            }
            table.add(row);
        }

        return table;
    }
}
