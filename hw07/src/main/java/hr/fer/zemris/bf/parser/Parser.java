package hr.fer.zemris.bf.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

import hr.fer.zemris.bf.lexer.Lexer;
import hr.fer.zemris.bf.lexer.LexerException;
import hr.fer.zemris.bf.lexer.Token;
import hr.fer.zemris.bf.lexer.TokenType;
import hr.fer.zemris.bf.model.BinaryOperatorNode;
import hr.fer.zemris.bf.model.ConstantNode;
import hr.fer.zemris.bf.model.Node;
import hr.fer.zemris.bf.model.UnaryOperatorNode;
import hr.fer.zemris.bf.model.VariableNode;

/**
 * Parser for described expression format(in homework assignment). Parser takes
 * input text, constructs syntax tree and checks whether input is conforming to
 * the rules of used grammar. Grammar is as follows:
 * <ul>
 * <li>S -> E1</li>
 * <li>E1 -> E2 (OR E2)*</li>
 * <li>E2 -> E3 (XOR E3)*</li>
 * <li>E3 -> E4 (AND E4)*</li>
 * <li>E4 -> NOT E4 | E5</li>
 * <li>E5 -> VAR | KONST | '(' E1 ')'</li>
 * </ul>
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class Parser {
    /**
     * Lexer for expression.
     */
    private Lexer lexer;
    /**
     * Syntax tree of parsed expression.
     */
    private Node expression;
    /**
     * Value stores last token that lexer returned.
     */
    private Token curToken;
    /**
     * Value stores last token that lexer returned.
     */
    private Token prevToken;

    /**
     * Default constructor.
     * 
     * @param expression
     *            expression that we are parsing
     * @throws ParserException
     *             in case of error in syntax analysis
     * @throws LexerException
     *             in case of error in lexical analysis.
     */
    public Parser(String expression) {
        this.lexer = new Lexer(expression);
        try {
            this.expression = parse();
        } catch (LexerException e) {
            throw new ParserException(String
                    .format("Lexer has thrown exception : %s", e.getMessage()));
        }
    }

    /**
     * @return parsed syntax tree.
     */
    public Node getExpression() {
        return expression;
    }

    // =======================================================================
    // Parsing methods.
    // =======================================================================
    
    /**
     * 
     * 
     * @return syntax tree of input if successfully parsed
     * @throws LexerException
     *             in case of error in lexical analysis.
     * @throws ParserException
     *             in case of error in syntax analysis.
     */
    private Node parse() {
        List<Node> children = new ArrayList<>();
        curToken = lexer.nextToken();
        while (true) {
            if (curToken.getTokenType() == TokenType.EOF) break;

            if (curToken.getTokenType() == TokenType.CLOSED_BRACKET) {
                throw new ParserException(
                        "Invalid token: " + curToken.getValue().toString());
            }

            if (!children.isEmpty()
                    && curToken.getTokenType() != TokenType.OPERATOR) {
                throw new ParserException(
                        "Invalid token: " + curToken.getValue());
            }

            // Resolve expressions E1,E2,E3
            if (curToken.getTokenType() == TokenType.CONSTANT
                    || curToken.getTokenType() == TokenType.VARIABLE) {
                prevToken = new Token(curToken.getTokenType(),
                        curToken.getValue());
                curToken = lexer.nextToken();
                children.add(newExpression(new ArrayList<>()));
                if (curToken.getTokenType() == TokenType.EOF) {
                    return children.get(0);
                }
            }

            // Resolve unary operator E4
            if (curToken.getTokenType() == TokenType.OPERATOR
                    && curToken.getValue().equals("not")) {
                return new UnaryOperatorNode("not", parse(), a -> !a);
            }

            // If there is casted expression in children list, and we call
            // binary operator treat that as valid expression
            if (curToken.getTokenType() == TokenType.OPERATOR
                    && !curToken.getValue().equals("not")
                    && children.size() == 1) {
                return newExpression(children);
            }

            // Resolve brackets
            if (curToken.getTokenType() == TokenType.OPEN_BRACKET) {
                Node castedExpression = parseBracket();
                children.add(castedExpression);
                continue;
            }

            throw new ParserException(String.format(
                    "Invalid input token: %s of type %s. Expected OPERATOR.",
                    curToken.getValue(), curToken.getTokenType().name()));

        }

        return children.get(0);
    }

    /**
     * Builds value node based on input token type.
     * 
     * @param inputToken
     *            input token from which we want to create value.
     * @return appropriate value node, {@link VariableNode} or
     *         {@link ConstantNode}
     */
    private Node buildValueNode(Token inputToken) {
        switch (inputToken.getTokenType()) {
        case VARIABLE:
            return new VariableNode((String) inputToken.getValue());
        case CONSTANT:
            return new ConstantNode(
                    (byte) ((boolean) inputToken.getValue() ? 1 : 0));
        default:
            throw new ParserException(String.format(
                    "%s is not a valid node value.", inputToken.getValue()));
        }
    }

    /**
     * Resolves binary operation based on input operator.
     * 
     * @param operator
     *            binary operator.
     * @return appropriate binary operation.
     */
    private BinaryOperator<Boolean> resolveBinaryOperation(String operator) {
        if (operator.equals("and")) return (a1, a2) -> a1 & a2;
        else if (operator.equals("or")) return (a1, a2) -> a1 | a2;
        else if (operator.equals("xor")) return (a1, a2) -> a1 ^ a2;
        else {
            throw new ParserException("Unknown operator: " + operator);
        }
    }

    /**
     * Builds new expression node.
     * 
     * @param children
     *            list of expression children
     * @return new binary or unary expression
     * @throws ParserException
     *             if there is error in input sequence.
     * 
     */
    private Node newExpression(List<Node> children) {
        BinaryOperator<Boolean> oper = null;
        String operName = "";

        if (children.isEmpty()) {
            children.add(buildValueNode(prevToken));
        }

        while (true) {
            if (curToken.getTokenType() == TokenType.OPERATOR) {
                oper = resolveBinaryOperation((String) curToken.getValue());
                operName = (String) curToken.getValue();
            } else {
                break;
            }

            curToken = lexer.nextToken();

            if (curToken.getTokenType() == TokenType.OPEN_BRACKET) {
                children.add(parseBracket());
                if (curToken.getTokenType() == TokenType.EOF) break;
                curToken = lexer.nextToken();
                continue;
            }

            if (curToken.getTokenType() == TokenType.EOF) break;

            if (curToken.getValue().equals("not")) {
                curToken = lexer.nextToken();
                children.add(new UnaryOperatorNode("not", parse(), a -> !a));
                continue;
            }

            prevToken = new Token(curToken.getTokenType(), curToken.getValue());
            curToken = lexer.nextToken();

            // Determine precedence of operators
            if (curToken.getTokenType() == TokenType.OPERATOR) {
                if (curToken.getValue().equals("or")
                        && !operName.equals("or")) {
                    children.add(buildValueNode(prevToken));
                    return new BinaryOperatorNode(operName, children, oper);
                }
                if (curToken.getValue().equals("xor")
                        && operName.equals("and")) {
                    return new BinaryOperatorNode(operName, children, oper);
                }
                if (curToken.getValue().equals(operName)) {
                    children.add(buildValueNode(prevToken));
                    continue;
                }
                if (curToken.getValue().equals("not")) {
                    children.add(
                            new UnaryOperatorNode("not", parse(), a -> !a));
                    continue;
                }
            }
            if (curToken.getTokenType() == TokenType.EOF) {
                children.add(buildValueNode(prevToken));
                break;
            }

            children.add(newExpression(new ArrayList<>()));
        }

        // Input expression consists only of one constant/variable, not not
        // followed by operator or curent constatn/variable is in the end of
        // input
        if (operName.equals("")) {
            return children.get(0);
        }

        if (!(curToken.getTokenType() == TokenType.EOF)) {
            prevToken = new Token(curToken.getTokenType(), curToken.getValue());
            curToken = lexer.nextToken();
        }

        return new BinaryOperatorNode(operName, children, oper);
    }

    /**
     * Parses expression in bracket.
     * 
     * @return casted expression
     * @throws ParserException
     *             if there is an error in input sequence.
     */
    private Node parseBracket() {
        Node castedExpression = parse();
        if (prevToken.getTokenType() != TokenType.CLOSED_BRACKET
                && prevToken.getTokenType() != TokenType.EOF) {
            throw new ParserException("Closing bracket not found!");
        }
        return castedExpression;
    }
}
