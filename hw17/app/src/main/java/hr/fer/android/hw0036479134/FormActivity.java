package hr.fer.android.hw0036479134;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.fer.android.hw0036479134.hr.fer.android.hw0036479134.networking.UserService;
import hr.fer.android.hw0036479134.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class form activity allows user to get superhero information on their phone. To get information
 * you have to type superhero name and click "Učitaj" button.
 *
 * @author matija
 */
public class FormActivity extends AppCompatActivity {
    /**
     * Root path of server that provides superhero data.
     */
    private static final String ROOT = "https://raw.githubusercontent.com";
    /**
     * Exact name of entry that contains Capitan America data.
     */
    private static final String CPT_AMERICA_LOCATION2 = "capitan_america.json";
    /**
     * Exact name of entry that contains Green Lantern data.
     */
    private static final String GREEN_LANTERN_LOCATION = "green_lantern.json";

    // Labels
    @BindView(R.id.label_firstName)
    TextView firstNameLabel;
    @BindView(R.id.label_lastName)
    TextView lastNameLabel;
    @BindView(R.id.label_phone)
    TextView phoneNoLabel;
    @BindView(R.id.label_email)
    TextView emailLabel;
    @BindView(R.id.label_spouse)
    TextView spouseLabel;
    @BindView(R.id.label_age)
    TextView ageLabel;

    // Inputs
    @BindView(R.id.input_path)
    EditText inputPath;
    @BindView(R.id.output_firstName)
    TextView firstName;
    @BindView(R.id.output_lastName)
    TextView lastName;
    @BindView(R.id.output_phone)
    TextView phoneNo;
    @BindView(R.id.output_email)
    TextView email;
    @BindView(R.id.output_spouse)
    TextView spouse;
    @BindView(R.id.output_age)
    TextView age;
    @BindView(R.id.output_image)
    ImageView image;

    // Layout
    @BindView(R.id.output_firstName_layout)
    LinearLayout firstNameLayout;
    @BindView(R.id.output_lastName_layout)
    LinearLayout lastNameLayout;
    @BindView(R.id.output_phoneNo_layout)
    LinearLayout phoneNoLayout;
    @BindView(R.id.output_email_layout)
    LinearLayout emailLayout;
    @BindView(R.id.output_spouse_layout)
    LinearLayout spouseLayout;
    @BindView(R.id.output_age_layout)
    LinearLayout ageLayout;

    private Retrofit retrofit;
    private UserService service;

    /**
     * Launch activity for sending email. Superhero email is put as extra.
     */
    @OnClick(R.id.output_email)
    void sendEmail() {
        if (email.getText() == null) return;

        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                new String[]{email.getText().toString()});

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    @OnClick(R.id.output_phone)
    void sendCall() {
        if (phoneNo.getText() == null) return;

        /* Create the Intent */
        final Intent callIntent = new Intent(Intent.ACTION_DIAL);

        /* Fill it with Data */
        callIntent.setData(Uri.parse("tel:" + phoneNo.getText().toString()));

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(callIntent, "Call..."));
    }

    @OnClick(R.id.btn_load)
    void load() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(ROOT)
                .build();

        UserService service = retrofit.create(UserService.class);

        String inputPathString = inputPath.getEditableText().toString();
        if (inputPathString.equalsIgnoreCase("capitan_america")) {
            inputPathString = CPT_AMERICA_LOCATION2;
        }
        if (inputPathString.equalsIgnoreCase("green_lantern")) {
            inputPathString = GREEN_LANTERN_LOCATION;
        }

        service.getUser(inputPathString).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User newUser = response.body();

                Toast.makeText(FormActivity.this, String.valueOf(newUser.getFirstName()), Toast.LENGTH_LONG).show();

                if (newUser.getFirstName() != null) {
                    firstName.setText(newUser.getFirstName());
                    firstNameLayout.setVisibility(View.VISIBLE);
                }
                if (newUser.getLastName() != null) {
                    lastName.setText(newUser.getLastName());
                    lastNameLayout.setVisibility(View.VISIBLE);
                }
                if (newUser.getEmailAddress() != null) {
                    if (emailLayout.getChildCount() == 0) {
                        emailLayout.addView(emailLabel);
                        emailLayout.addView(email);
                    }
                    email.setText(newUser.getEmailAddress());
                    emailLayout.setVisibility(View.VISIBLE);
                } else {
                    emailLayout.removeAllViews();
                }
                if (newUser.getPhoneNum() != null) {
                    if (phoneNoLayout.getChildCount() == 0) {
                        phoneNoLayout.addView(phoneNoLabel);
                        phoneNoLayout.addView(phoneNo);
                    }
                    phoneNo.setText(newUser.getPhoneNum());
                    phoneNoLayout.setVisibility(View.VISIBLE);
                } else {
                    phoneNoLayout.removeAllViews();
                }
                if (newUser.getSpouse() != null && newUser.getSpouse().length() != 0) {
                    if (spouseLayout.getChildCount() == 0) {
                        spouseLayout.addView(spouseLabel);
                        spouseLayout.addView(spouse);
                    }
                    spouse.setText(newUser.getSpouse());
                    spouseLayout.setVisibility(View.VISIBLE);
                } else {
                    spouseLayout.removeAllViews();
                }
                if (newUser.getAge() != null) {
                    age.setText(newUser.getAge());
                    ageLayout.setVisibility(View.VISIBLE);
                }
                if (newUser.getAvatarUrl() != null) {
                    image.setVisibility(View.VISIBLE);
                    Glide.with(FormActivity.this)
                            .load(newUser.getAvatarUrl())
                            .into(image);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(FormActivity.this, "Error fetching " + call.request().url().toString() + ". " + t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ButterKnife.bind(this);

    }
}
