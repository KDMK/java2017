package hr.fer.android.hw0036479134;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Starting point of application. It provides actions for launching math and statistics activities.
 */
public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.btn_math)
    void openMath() {
        Intent i = new Intent(this, CalculusActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_stat)
    void openStat() {
        Intent i = new Intent(this, FormActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }
}
