package hr.fer.android.hw0036479134.hr.fer.android.hw0036479134.networking;

import hr.fer.android.hw0036479134.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Service that is used to get superhero data from server.
 *
 * Created by matija on 6/27/17.
 */

public interface UserService {
    @GET("/KDMK/android_homework/master/{file}")
    Call<User> getUser(@Path("file") String file);
}
