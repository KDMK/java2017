package hr.fer.android.hw0036479134;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static hr.fer.android.hw0036479134.DisplayActivity.*;

/**
 * Calculus activity acts as simple calculator. It supports summing, subtraction, division and
 * multiplication.
 *
 */
public class CalculusActivity extends AppCompatActivity {
    private static final int REQ_CODE = 314;

    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.input_first)
    TextView firstNumber;
    @BindView(R.id.input_second)
    TextView secondNumber;
    @BindView(R.id.group_operations)
    RadioGroup operGroup;

    @OnClick(R.id.btn_calculate)
    void performOperation() {
        Double first = null;
        Double second = null;

        int selectedID = operGroup.getCheckedRadioButtonId();
        String selectedOperation = ((RadioButton) findViewById(selectedID)).getText().toString();

        Bundle b = new Bundle();
        Intent i = new Intent(this, DisplayActivity.class);

        try {
            first = Double.parseDouble(firstNumber.getText().toString());
            second = Double.parseDouble(secondNumber.getText().toString());

            switch (selectedID) {
                case R.id.operation_sum:
                    b.putSerializable(RESULT, Double.valueOf(first + second));
                    break;
                case R.id.operation_subtract:
                    b.putSerializable(RESULT, Double.valueOf(first - second));
                    break;
                case R.id.operation_multiplication:
                    b.putSerializable(RESULT, Double.valueOf(first * second));
                    break;
                case R.id.operation_divide:
                    b.putSerializable(RESULT, Double.valueOf(first / second));
                    break;
                default:
                    Toast.makeText(this, "Invalid operation!", Toast.LENGTH_SHORT);
                    return;
            }

            b.putSerializable(OPERATION, selectedOperation);
        } catch (Exception ex) {
            b.putSerializable(FIRST_PARAMETER, first);
            b.putSerializable(SECOND_PARAMETER, second);
            b.putSerializable(OPERATION, selectedOperation);
            b.putSerializable(ERROR, ex.getMessage());
        }

        i.putExtras(b);
        startActivityForResult(i, REQ_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculus);

        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE) {
            if (resultCode == DisplayActivity.RESULT_OK) {
                firstNumber.setText("");
                secondNumber.setText("");
            }
        }
    }
}
