package hr.fer.android.hw0036479134.models;

import com.google.gson.annotations.SerializedName;

/**
 * Model for superhero data.
 * <p>
 * Created by matija on 6/27/17.
 */

public class User {
    /**
     * Url to superhero avatar
     */
    @SerializedName("avatar_location")
    private String avatarUrl;
    /**
     * Superhero first name
     */
    @SerializedName("first_name")
    private String firstName;
    /**
     * Superhero last name
     */
    @SerializedName("last_name")
    private String lastName;
    /**
     * Superhero phone number
     */
    @SerializedName("phone_no")
    private String phoneNum;
    /**
     * Superhero email
     */
    @SerializedName("email_sknf")
    private String emailAddress;
    /**
     * Superhero spouse.
     */
    @SerializedName("spouse")
    private String spouse;
    /**
     * Superhero age.
     */
    @SerializedName("age")
    private String age;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name
     *
     * @param firstName first name of superhero
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name
     *
     * @param lastName the last name of superhero
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phoneNo
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * Sets the phone number of superhero
     *
     * @param phoneNum
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets superhero email address.
     *
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     * Sets superhero spouse
     *
     * @param spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * Sets age of superhero
     *
     * @param age
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * Sets the avatar url
     *
     * @param avatarUrl
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * Default constructor.
     *
     * @param firstName first name of superhero
     * @param lastName last name of superhero
     * @param phoneNum phone number of superhero
     * @param emailAddress email address of superhero
     * @param spouse spouse of superhero
     * @param age age of superhero
     */
    public User(String firstName, String lastName, String phoneNum, String emailAddress,
                String spouse, String age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.emailAddress = emailAddress;
        this.spouse = spouse;
        this.age = age;
    }
}
