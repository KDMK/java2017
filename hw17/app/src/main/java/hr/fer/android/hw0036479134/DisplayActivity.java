package hr.fer.android.hw0036479134;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Display activity is used to present result of calculation. Additionally, it allows user to send
 * textual report of this activity to email.
 */
public class DisplayActivity extends AppCompatActivity {
    static final String FIRST_PARAMETER = "first_number";
    static final String SECOND_PARAMETER = "second_number";
    static final String ERROR = "error";
    static final String RESULT = "result";
    static final String OPERATION = "operation";

    @BindView(R.id.output_result)
    TextView outputField;

    @OnClick(R.id.btn_ok)
    void returnToMath() {
        if (getIntent().getSerializableExtra(ERROR) == null) {
            setResult(RESULT_OK);
            finish();
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    @OnClick(R.id.btn_result)
    void sendReportToMail() {
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                new String[]{"ana@baotic.org"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "0036479134:dz report");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, resolveActivityResult(getIntent()
                .getExtras()));

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private String resolveActivityResult(Bundle b) {
        if (b.getSerializable(RESULT) == null) {
            String error = (String) b.getSerializable(ERROR);
            Double first = (Double) b.getSerializable(FIRST_PARAMETER);
            Double second = (Double) b.getSerializable(SECOND_PARAMETER);
            String operation = (String) b.getSerializable(OPERATION);

            return String.format("Prilikom obavljanja opracije %s nad operandima %.4f " +
                    "i %.4f došlo je do slijedeće pogreške:%n%s", operation, first, second, error);
        } else {
            String operation = (String) b.getSerializable(OPERATION);
            Double result = (Double) b.getSerializable(RESULT);

            return String.format("Rezultat operacije %s je %.2f.", operation, result);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();

        outputField.setText(resolveActivityResult(b));
    }
}
