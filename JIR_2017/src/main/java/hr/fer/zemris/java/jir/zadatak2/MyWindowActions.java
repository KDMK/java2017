package hr.fer.zemris.java.jir.zadatak2;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by matija on 9/4/17.
 */
public class MyWindowActions {
    private MainWindow windowReference;

    public MyWindowActions(MainWindow windowReference) {
        this.windowReference = windowReference;
    }

    public ActionListener loadFile = e -> {
        JFileChooser fileChooser = new JFileChooser();
        int status = fileChooser.showDialog(null,"Load file");

        if(status == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "No files selected.");
            return;
        }

        if(status == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "An error has occured while opening " +
                                                           "file", "Error!", JOptionPane
                                                  .ERROR_MESSAGE);
        }

        // Do some logic here
    };

    public ActionListener exitProgram = (e) -> {
        System.exit(0);
    };

}
