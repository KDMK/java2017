package hr.fer.zemris.java.jir.zadatak1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matija on 9/4/17.
 */
@SuppressWarnings("Duplicates")
public class FunctionFileVisitor extends SimpleFileVisitor<Path> {
    private List<EvaluatedFileModel> evaluatedFiles;

    public List<EvaluatedFileModel> getEvaluatedFiles() {
        return evaluatedFiles;
    }

    public EvaluatedFileModel getEvaluatedFile(int index) {
        return evaluatedFiles.get(index);
    }

    public FunctionFileVisitor() { this.evaluatedFiles = new ArrayList<>(); }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(file.toString().endsWith("-out.txt")) {
            return FileVisitResult.CONTINUE;
        }

        List<String> allLines = Files.readAllLines(file);

        EvaluatedFileModel evaluatedFileModel;
        try {
            System.out.println(file.getFileName());
            evaluatedFileModel = new EvaluatedFileModel(allLines);
            evaluatedFileModel.process();

            evaluatedFiles.add(evaluatedFileModel);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("");
            return FileVisitResult.CONTINUE;
        }

        File outputFile = new File(file.toString()
                                       .replace("-bool", "-out"));

        if (!outputFile.exists()) {
            outputFile.createNewFile();
        }

        OutputStream fos = new FileOutputStream(outputFile, false);
        evaluatedFileModel.writeToOutput(fos);

        return FileVisitResult.CONTINUE;
    }
}
