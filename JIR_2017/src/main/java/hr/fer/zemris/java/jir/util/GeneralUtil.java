package hr.fer.zemris.java.jir.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class contains general utility methods used throughout earlier homeworks.
 */
@SuppressWarnings("Duplicates")
public class GeneralUtil {
    public static final int ERROR_NUM_FORMAT = -1;
    public static final int ERROR_WRONG_NUM_OF_ARGS = -2;
    public static final int ERROR_NO_SUCH_FILE = -3;

    public static final String SEPARATOR = "\\s+";

    public static Number resolveNumber(String inputNumber) throws NumberFormatException {
        Number outputNumber = null;

        try {
            outputNumber = Integer.parseInt(inputNumber);
        } catch (NumberFormatException ignorable) {}

        try {
            outputNumber = Double.parseDouble(inputNumber);
        } catch (NumberFormatException ex) {
            System.out.println(String.format("[ERROR] %s is not valid number format.",
                                             inputNumber));
            System.exit(ERROR_NUM_FORMAT);
        }

        return outputNumber;
    }

    public static double calculate(double first, double second, String operator) {
        switch (operator) {
            case "+":
                return first + second;
            case "-":
                return first - second;
            case "*":
                return first * second;
            case "/":
                return first / second;
        }

        return Double.NaN;
    }

    private BufferedImage createGraphics(Map<String, Integer> data, int totalItems, String
            selectedItem) {
        BufferedImage outImage = new BufferedImage(500, 500, BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g2d = (Graphics2D) outImage.getGraphics();
//        MyBarChart.paintGraph(500, 500, g2d, data, totalItems, selectedItem);
        // Do some painting here.


        return outImage;
    }

    public static List<boolean[]> generateTruthTable(int n) {
        int numOfRows = (int) Math.pow(2,n);
        List<boolean[]> rows = new ArrayList<>(numOfRows);

        for (int i=0; i < numOfRows; i++) {
            boolean[] row = new boolean[n];

            for (int j=n-1, k=0; j>=0; j--,k++) {
                row[k] = (i/(int) Math.pow(2, j))%2 == 1;
            }

            rows.add(row);
        }

        return rows;
    }

    public static boolean validFunctionName(String line) {
        return line.matches("[a-zA-z].*");
    }
}
