package hr.fer.zemris.java.jir.zadatak1;

import hr.fer.zemris.java.jir.util.GeneralUtil;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by matija on 9/4/17.
 */
public class EvaluatedFileModel {
    private static final String TABULATE_KEYWORD = "#tabulate";

    private List<String> allLines;
    private List<String> functionNames;
    private SortedSet<String> variableNames;
    private String[] arrayVariables;
    private String[] arrayFunctions;
    private List<BooleanFunction> allFunctions;
    private List<boolean[]> inputCombinations;
    private List<String> outputFunctions;
    private List<String[]> outputLines;


    private EvaluatedFileModel(
            List<String> allLines, List<String> functionNames, TreeSet<String>
            variableNames, List<BooleanFunction> allFunctions,
            List<boolean[]> inputCombinations, List<String[]> outputLines,
            List<String> outputFunctions) {
        this.allLines = allLines;
        this.functionNames = functionNames;
        this.variableNames = variableNames;
        this.allFunctions = allFunctions;
        this.inputCombinations = inputCombinations;
        this.outputLines = outputLines;
        this.outputFunctions = outputFunctions;
    }

    public EvaluatedFileModel(List<String> inputFileAllLines) {
        this(inputFileAllLines, new ArrayList<>(), new TreeSet<>(), new ArrayList<>(), new ArrayList<>(),
             new ArrayList<>(), new ArrayList<>());
        buildInputFileModel(allLines);
    }

    private void buildInputFileModel(List<String> allLines) {
        for (String line : allLines) {
            if (line.trim().length() == 0) {
                continue;
            }

            if (line.startsWith(TABULATE_KEYWORD)) {
                if (!extractOutput(line)) {
                    throw new IllegalArgumentException(String.format("[ERROR] %s is not valid function name", line));
                }
                continue;
            }
            createBooleanFunction(line);
        }

        variableNames.removeAll(functionNames);

        arrayVariables = new String[variableNames.size()];
        arrayFunctions = new String[functionNames.size()];

        variableNames.toArray(this.arrayVariables);
        functionNames.toArray(this.arrayFunctions);
    }

    private void createBooleanFunction(String line) throws IllegalArgumentException {
        BooleanFunction newFunction = new BooleanFunction(line);

        allFunctions.add(newFunction);
        functionNames.add(newFunction.getFunctionName());
        variableNames.addAll(newFunction.getAllVariables());
    }

    private boolean extractOutput(String line) {
        line = line.replace(TABULATE_KEYWORD, "").trim();
        if (!(GeneralUtil.validFunctionName(line) && containsFunction(line))) {
            return false;
        }

        outputFunctions.add(line);
        return true;
    }

    public void process() {
        // uzmi jednu kombinaciju -> svaku funk evaluiraj za tu kombinaciju i dodaj u izlazni
        // -> prijedi na slijedcu funckiju
        inputCombinations = GeneralUtil.generateTruthTable(variableNames.size());

        for (boolean[] inputCombination : inputCombinations) {
            String[] outputLine = new String[outputFunctions.size()];
            int i = 0;
            for (String functionName: outputFunctions) {
                BooleanFunction function = getFunction(functionName);

                Stack<String> functionCopy = new Stack();
                functionCopy.addAll(function.getFunction());

                outputLine[i++] = String.valueOf(evaluateStack(functionCopy, inputCombination, null));
            }
                outputLines.add(outputLine);
        }
    }

    private BooleanFunction getFunction(String functionName) {
        for(BooleanFunction function: allFunctions) {
            if(function.getFunctionName().equals(functionName)) return function;
        }

        return null;
    }

    public void writeToOutput(OutputStream out) {
        PrintWriter outputWriter = new PrintWriter(out);

        String header = "";

        for(String var: arrayVariables) {
            header += String.format("+-- %3s --", var);
        }

        for(String fun: outputFunctions) {
            header += String.format("+-- %3s --", fun);
        }

        header += "+";

        outputWriter.write(header);
        outputWriter.write(System.lineSeparator());

        int i = 0;
        for(boolean[] inputCombination: inputCombinations) {
            String row = "";
            String divider = "";

            for(boolean inputValue: inputCombination) {
                row += inputValue ? "+     1   " : "+     0   ";
                divider += "+---------";
            }

            for(String outputValue: outputLines.get(i++)) {
                row += Boolean.valueOf(outputValue) ? "+     1   " : "+     0   ";
                divider += "+---------";
            }
            row += "+";
            divider += "+";

            outputWriter.write(row);
            outputWriter.write(System.lineSeparator());
            outputWriter.write(divider);
            outputWriter.write(System.lineSeparator());
        }

        outputWriter.close();
    }

    private boolean containsFunction(String functionName) {
        for (String name : functionNames) {
            if (name.equals(functionName)) {
                return true;
            }
        }

        return false;
    }

    private boolean evaluateStack(Stack<String> stack, boolean[] input, String operation) {
        String curItem = stack.pop();

        if(stack.isEmpty()) {
            if(operation.equals(BooleanFunction.OPERATION_NOT)) {
                return !resolveValue(curItem, input);
            }

            return resolveValue(curItem, input);
        }

        if(operation != null && operation.equals(BooleanFunction.OPERATION_NOT) && !isOperation(curItem)) {
            return !resolveValue(curItem, input);
        }

        if (isOperation(curItem)) {
            operation = curItem;

            if (curItem.equals(BooleanFunction.OPERATION_NOT)) {
                curItem = stack.pop();
                if (isOperation(curItem)) {
                    return evaluateStack(stack, input, curItem);
                } else {
                    return unaryOperation(curItem, input);
                }
            }

            if (curItem.equals(BooleanFunction.OPERATION_AND) ||
                curItem.equals(BooleanFunction.OPERATION_OR)) {
                boolean firstArg;
                boolean secondArg;

                String first = stack.pop();
                if (isOperation(first)) {
                    firstArg = evaluateStack(stack, input, first);
                } else {
                    firstArg = resolveValue(first, input);
                }

                String second = stack.pop();
                if (isOperation(second)) {
                    secondArg = evaluateStack(stack, input, second);
                } else {
                    secondArg = resolveValue(second, input);
                }

                return binaryOperation(firstArg, secondArg, operation, input);
            }
        }

        return false;
    }

    private boolean isOperation(String curItem) {
        return curItem.equals(BooleanFunction.OPERATION_AND) || curItem.equals(BooleanFunction
                                                                                       .OPERATION_OR) ||
               curItem.equals(BooleanFunction.OPERATION_NOT);
    }

    private boolean binaryOperation(
            boolean firstOperand, boolean secondOperand,
            String operation, boolean[] input) {
        switch (operation) {
            case BooleanFunction.OPERATION_AND:
                return firstOperand && secondOperand;
            case BooleanFunction.OPERATION_OR:
                return firstOperand || secondOperand;
        }

        return false;
    }

    private boolean unaryOperation(String operandString, boolean[] input) {
        boolean operand = resolveValue(operandString, input);

        return !operand;
    }

    private boolean resolveValue(String operandString, boolean[] input) {
        if(operandString.equalsIgnoreCase("true") || operandString.equalsIgnoreCase("false")) {
            return Boolean.parseBoolean(operandString);
        }

        int index;

        index = indexOf(arrayVariables, operandString);
        if (index != -1) {
            return input[index];
        }

        index = indexOf(arrayFunctions, operandString);
        BooleanFunction fun = allFunctions.get(index);

        Stack<String> stackCopy = new Stack<>();
        stackCopy.addAll(fun.getFunction());

        return evaluateStack(stackCopy, input, stackCopy.peek());
    }

    private int indexOf(Object[] array, Object item) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }

        return -1;
    }
}
