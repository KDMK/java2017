package hr.fer.zemris.java.jir.zadatak1;

import hr.fer.zemris.java.jir.util.GeneralUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by matija on 9/4/17.
 */
public class Parser {

    public static void main(String[] args) throws IOException {
        if(args.length != 1) {
            System.out.println("[ERROR] Wrong number of input arguments");
            System.exit(GeneralUtil.ERROR_WRONG_NUM_OF_ARGS);
        }

        Path inputPath = Paths.get(args[0]).toAbsolutePath();
        if(!inputPath.toFile().exists()) {
            System.out.println("[ERROR] " + inputPath.toString() + " doesn't exist");
            System.exit(GeneralUtil.ERROR_NO_SUCH_FILE);
        }

        Files.walkFileTree(inputPath, new FunctionFileVisitor());
    }
}
