package hr.fer.zemris.java.jir.zadatak1;

import hr.fer.zemris.java.jir.util.GeneralUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;

/**
 * Created by matija on 9/4/17.
 */
public class BooleanFunction {
    static final String OPERATION_OR = "+";
    static final String OPERATION_AND = "*";
    static final String OPERATION_NOT = "!";

    private String functionName;
    private Stack<String> functionMembers;
    private List<String> functionVariables;

    public BooleanFunction(String line) {
        this.functionMembers = new Stack<>();
        this.functionVariables = new ArrayList<>();

        parse(line);
    }

    public String getFunctionName() {
        return functionName;
    }

    private void parse(String line) {
        String[] splittedLine = line.split(GeneralUtil.SEPARATOR);
        String functionName = splittedLine[0].trim();

        if (GeneralUtil.validFunctionName(functionName)) {
            this.functionName = functionName;
        } else {
            throw new IllegalArgumentException(String.format("[ERROR] %s is not valid function name.", splittedLine[0]));
        }

        if (!splittedLine[1].trim().equals("<=")) {
            throw new IllegalArgumentException(String.format("[ERROR] %s is not valid " +
                                                             "function!", line));
        }

        for (int i = 2; i < splittedLine.length; i++) {
            String curElement = splittedLine[i].trim();

            if (!(curElement.equals(OPERATION_AND) || curElement.equals(OPERATION_NOT) || curElement
                    .equals(OPERATION_OR))) {
                functionVariables.add(curElement);
            }

            functionMembers.push(curElement);
        }
    }

    public Collection<? extends String> getAllVariables() {
        return functionVariables;
    }

    public Stack<String> getFunction() {
        return functionMembers;
    }
}
