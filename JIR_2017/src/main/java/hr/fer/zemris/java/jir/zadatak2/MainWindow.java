package hr.fer.zemris.java.jir.zadatak2;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Created by matija on 9/4/17.
 */
@SuppressWarnings("Duplicates")
public class MainWindow extends JFrame {
    private MyWindowButtons buttons;

    public MainWindow() {
        this.buttons = new MyWindowButtons(this);
        initGUI();
    }

    private void initGUI() {
        createMenuBar();

        setTitle("Expression evaluator");
        setSize(800,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void createMenuBar() {
        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        file.add(buttons.openMenuItem);
        file.addSeparator();
        file.add(buttons.exitMenuItem);

        menubar.add(file);

        setJMenuBar(menubar);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainWindow window = new MainWindow();
            window.setVisible(true);
        });
    }
}
