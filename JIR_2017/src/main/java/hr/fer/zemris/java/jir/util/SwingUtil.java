package hr.fer.zemris.java.jir.util;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by matija on 9/4/17.
 */
public class SwingUtil {

    public static TableModel createTableModel(List<String> labels, List<String[]> values) {
        DefaultTableModel model = new DefaultTableModel();

        for(String label: labels) {
            model.addColumn(label);
        }

        for(int i = 0; i < values.size(); i++) {
            model.addRow(values.get(i));
        }

        return model;
    }

    // Use this to write image to output stream.
    // ImageIO.write(image, "png", resp.getOutputStream());
}
