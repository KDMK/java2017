package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Test;

import hr.fer.java.zemris.custom.scripting.exec.ObjectMultistack;
import hr.fer.java.zemris.custom.scripting.exec.ValueWrapper;

@SuppressWarnings("javadoc")
public class ObjectMultistackTest {

    @Test
    public void testLegalPopAndPush() {
        ObjectMultistack multistack = new ObjectMultistack();
        
        ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
        multistack.push("year", year);
        
        ValueWrapper price = new ValueWrapper(200.51);
        multistack.push("price", price);
        
        assertEquals(2000, multistack.peek("year").getValue());
        assertEquals(200.51,  multistack.peek("price").getValue());
        
        multistack.push("year", new ValueWrapper(Integer.valueOf(1900)));
        assertEquals(1900, multistack.peek("year").getValue());

        multistack.peek("year").setValue(
                ((Integer) multistack.peek("year").getValue()).intValue() + 50);
        assertEquals(1950, multistack.peek("year").getValue());
        
        multistack.pop("year");
        assertEquals(2000, multistack.peek("year").getValue());
        
        multistack.peek("year").add("5");
        assertEquals(2005, multistack.peek("year").getValue());
        
        multistack.peek("year").add(5);
        assertEquals(2010, multistack.peek("year").getValue());
        
        multistack.peek("year").add(5.0);
        assertEquals(2015.0, multistack.peek("year").getValue());
    }
    
    @Test(expected = EmptyStackException.class)
    public void testEmptyStack() {
        ObjectMultistack multistack = new ObjectMultistack();
        
        multistack.push("year", new ValueWrapper(Integer.valueOf(2000)));
        
        multistack.pop("year");
        multistack.pop("year"); // should throw empty stack exception
    }
    
    @Test(expected = EmptyStackException.class)
    public void testKeyNotInMap() {
        ObjectMultistack multistack = new ObjectMultistack();
        
        assertEquals(null, multistack.pop("test"));
    }
    
    @Test
    public void testPushToSameStack() {
        ObjectMultistack multistack = new ObjectMultistack();
        
        multistack.push("test", new ValueWrapper("Janko"));
        multistack.push("test", new ValueWrapper("Darko"));
        multistack.push("test", new ValueWrapper("Mirko"));
        multistack.push("test", new ValueWrapper("Slavko"));
        
        assertEquals("Slavko", multistack.pop("test").getValue());
        assertEquals("Mirko", multistack.pop("test").getValue());
        assertEquals("Darko", multistack.pop("test").getValue());
        assertEquals("Janko", multistack.pop("test").getValue());
        
        assertTrue(multistack.isEmpty("test"));
    }

}
