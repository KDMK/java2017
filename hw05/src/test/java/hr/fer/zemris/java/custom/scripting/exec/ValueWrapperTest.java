package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import hr.fer.java.zemris.custom.scripting.exec.ValueWrapper;

@SuppressWarnings("javadoc")
public class ValueWrapperTest {
    private static final double DELTA = 1E-15;

    // =========================================================================
    // Test addition
    // =========================================================================
    @Test
    public void testAddingTwoNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(null);

        v1.add(v2.getValue());

        assertEquals(0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testAddingNullToInteger() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(null);

        v1.add(v2.getValue());

        assertEquals(1, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testAddingIntegerToNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(4));

        v1.add(v2.getValue());

        assertEquals(4, v1.getValue());
        assertEquals(4, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(Integer.class, v2.getValue().getClass());
    }

    @Test
    public void testAddingNullToDouble() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(4));
        ValueWrapper v2 = new ValueWrapper(null);

        v1.add(v2.getValue());

        assertEquals(4.0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testAddingDoubleToNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(4));

        v1.add(v2.getValue());

        assertEquals(4.0, v1.getValue());
        assertEquals(4.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
        assertEquals(Double.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testAddingNullToObject() {
        ValueWrapper v1 = new ValueWrapper("Ankica");
        ValueWrapper v2 = new ValueWrapper(null);

        v1.add(v2.getValue()); // throws RuntimeException
    }

    @Test
    public void testAddingIntToDouble() {
        ValueWrapper v1 = new ValueWrapper("1.2E1");
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(1));

        v1.add(v2.getValue());

        assertEquals(13.0, (Double) v1.getValue(), DELTA);
        assertEquals(1, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testAddingDoubleToInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.14));

        v1.add(v2.getValue());

        assertEquals(3.14, (Double) v1.getValue(), DELTA);
        assertEquals(2.14, (Double) v2.getValue(), DELTA);
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testAddInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(12));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(1));

        v1.add(v2.getValue());

        assertEquals(13, v1.getValue());
        assertEquals(1, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testAddIntAsString() {
        ValueWrapper v1 = new ValueWrapper("12");
        ValueWrapper v2 = new ValueWrapper("1");

        v1.add(v2.getValue());

        assertEquals(13, v1.getValue());
        assertEquals("1", v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(String.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testAddIntegerToString() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddStringToInteger() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddObjectToInteger() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddIntegerToObject() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(8));
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddObjectToDouble() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddDoubleToObject() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(8));
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddDoubleToString() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(1));
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddStringToDouble() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.add(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testAddObjectToObject() {
        ValueWrapper v7 = new ValueWrapper("Ljubica");
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.add(v8.getValue()); // throws RuntimeException
    }

    // =========================================================================
    // Test subtraction
    // =========================================================================
    @Test
    public void testSubtractionTwoNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(null);

        v1.subtract(v2.getValue());

        assertEquals(0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractionNullFromInteger() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(null);

        v1.subtract(v2.getValue());

        assertEquals(1, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractionIntegerFromNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(4));

        v1.subtract(v2.getValue());

        assertEquals(-4, v1.getValue());
        assertEquals(4, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(Integer.class, v2.getValue().getClass());
    }

    @Test
    public void testSubtractionNullFromDouble() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(4));
        ValueWrapper v2 = new ValueWrapper(null);

        v1.subtract(v2.getValue());

        assertEquals(4.0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractionDoubleToNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(4));

        v1.subtract(v2.getValue());

        assertEquals(-4.0, v1.getValue());
        assertEquals(4.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
        assertEquals(Double.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractionNullFromObject() {
        ValueWrapper v1 = new ValueWrapper("Ankica");
        ValueWrapper v2 = new ValueWrapper(null);

        v1.subtract(v2.getValue()); // throws RuntimeException
    }

    @Test
    public void testSubtractionIntFromDouble() {
        ValueWrapper v1 = new ValueWrapper("1.2E1");
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(1));

        v1.subtract(v2.getValue());

        assertEquals(11.0, v1.getValue());
        assertEquals(1, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractionDoubleFromInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.14));

        v1.subtract(v2.getValue());

        assertEquals(-1.14, (Double) v1.getValue(), DELTA);
        assertEquals(2.14, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(12));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(1));

        v1.subtract(v2.getValue());

        assertEquals(11, v1.getValue());
        assertEquals(1, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testSubtractIntAsString() {
        ValueWrapper v1 = new ValueWrapper("12");
        ValueWrapper v2 = new ValueWrapper("1");

        v1.subtract(v2.getValue());

        assertEquals(11, v1.getValue());
        assertEquals("1", v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(String.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractIntegerFromString() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractStringFromInteger() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractObjectFromInteger() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractIntegerFromObject() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(8));
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractObjectFromDouble() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractDoubleFromObject() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(8));
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractDoubleFromString() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(1));
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractStringFromDouble() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testSubtractObjectFromObject() {
        ValueWrapper v7 = new ValueWrapper("Ljubica");
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.subtract(v8.getValue()); // throws RuntimeException
    }

    // =========================================================================
    // Test division
    // =========================================================================
    @Test(expected = IllegalArgumentException.class)
    public void testDivisionTwoNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(null);
        v1.divide(v2.getValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionIntegerWithNull() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(null);
        v1.divide(v2.getValue());
    }

    @Test
    public void testDivisionNullWithInteger() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(4));

        v1.divide(v2.getValue());

        assertEquals(0, v1.getValue());
        assertEquals(4, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(Integer.class, v2.getValue().getClass());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionDoubleWithNull() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(4));
        ValueWrapper v2 = new ValueWrapper(null);
        v1.divide(v2.getValue());
    }

    @Test
    public void testDivisionNullWithDouble() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(4));

        v1.divide(v2.getValue());

        assertEquals(0.0, v1.getValue());
        assertEquals(4.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
        assertEquals(Double.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testDivisionObjectWithNull() {
        ValueWrapper v1 = new ValueWrapper("Ankica");
        ValueWrapper v2 = new ValueWrapper(null);

        v1.divide(v2.getValue()); // throws RuntimeException
    }

    @Test
    public void testDivisionDoubleWithInt() {
        ValueWrapper v1 = new ValueWrapper("1.2E1");
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));

        v1.divide(v2.getValue());

        assertEquals(6.0, v1.getValue());
        assertEquals(2, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testDivisionIntWithDouble() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(6));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.0));

        v1.divide(v2.getValue());

        assertEquals(3.0, (Double) v1.getValue(), DELTA);
        assertEquals(2.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testDivideInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(12));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));

        v1.divide(v2.getValue());

        assertEquals(6, v1.getValue());
        assertEquals(2, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testDivideIntAsString() {
        ValueWrapper v1 = new ValueWrapper("12");
        ValueWrapper v2 = new ValueWrapper("2");

        v1.divide(v2.getValue());

        assertEquals(6, v1.getValue());
        assertEquals("2", v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(String.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testDivideStringWithInteger() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideIntegerWithString() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideIntWithObject() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideObjectWithInt() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(8));
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideDoubleWithObject() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideObjectWithDouble() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(8));
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideStringWithDouble() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(1));
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideStringWithObject() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.divide(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testDivideObjectWithObject() {
        ValueWrapper v7 = new ValueWrapper("Ljubica");
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.divide(v8.getValue()); // throws RuntimeException
    }
    // =========================================================================
    // Test multiplication
    // =========================================================================
    @Test
    public void testMultiplicationTwoNull() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(null);
        
        v1.multiply(v2.getValue());
        
        assertEquals(0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationIntegerWithNull() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v2 = new ValueWrapper(null);
        
        v1.multiply(v2.getValue());
        
        assertEquals(0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationNullWithInteger() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(4));

        v1.multiply(v2.getValue());

        assertEquals(0, v1.getValue());
        assertEquals(4, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(Integer.class, v2.getValue().getClass());
    }

    @Test
    public void testMultiplicationDoubleWithNull() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(4));
        ValueWrapper v2 = new ValueWrapper(null);
        
        v1.multiply(v2.getValue());
        
        assertEquals(0.0, v1.getValue());
        assertEquals(null, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationNullWithDouble() {
        ValueWrapper v1 = new ValueWrapper(null);
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(4));

        v1.multiply(v2.getValue());

        assertEquals(0.0, v1.getValue());
        assertEquals(4.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
        assertEquals(Double.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationObjectWithNull() {
        ValueWrapper v1 = new ValueWrapper("Ankica");
        ValueWrapper v2 = new ValueWrapper(null);

        v1.multiply(v2.getValue()); // throws RuntimeException
    }

    @Test
    public void testMultiplicationDoubleWithInt() {
        ValueWrapper v1 = new ValueWrapper("1.2E1");
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));

        v1.multiply(v2.getValue());

        assertEquals(24.0, v1.getValue());
        assertEquals(2, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationIntWithDouble() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(6));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.0));

        v1.multiply(v2.getValue());

        assertEquals(12.0, (Double) v1.getValue(), DELTA);
        assertEquals(2.0, v2.getValue());
        assertEquals(Double.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationInt() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(12));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));

        v1.multiply(v2.getValue());

        assertEquals(24, v1.getValue());
        assertEquals(2, v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
    }

    @Test
    public void testMultiplicationIntAsString() {
        ValueWrapper v1 = new ValueWrapper("12");
        ValueWrapper v2 = new ValueWrapper("2");

        v1.multiply(v2.getValue());

        assertEquals(24, v1.getValue());
        assertEquals("2", v2.getValue());
        assertEquals(Integer.class, v1.getValue().getClass());
        assertEquals(String.class, v2.getValue().getClass());
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationStringWithInteger() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationIntegerWithString() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationIntWithObject() {
        ValueWrapper v7 = new ValueWrapper(Integer.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationObjectWithInt() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Integer.valueOf(8));
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationDoubleWithObject() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(8));
        ValueWrapper v8 = new ValueWrapper(new ArrayList<Integer>());
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationObjectWithDouble() {
        ValueWrapper v7 = new ValueWrapper(new ArrayList<Integer>());
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(8));
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationStringWithDouble() {
        ValueWrapper v7 = new ValueWrapper("Ankica");
        ValueWrapper v8 = new ValueWrapper(Double.valueOf(1));
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationStringWithObject() {
        ValueWrapper v7 = new ValueWrapper(Double.valueOf(1));
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.multiply(v8.getValue()); // throws RuntimeException
    }

    @Test(expected = RuntimeException.class)
    public void testMultiplicationObjectWithObject() {
        ValueWrapper v7 = new ValueWrapper("Ljubica");
        ValueWrapper v8 = new ValueWrapper("Ankica");
        v7.multiply(v8.getValue()); // throws RuntimeException
    }
    
    
    // =========================================================================
    // Test comparison
    // =========================================================================
    @Test(expected = RuntimeException.class)
    public void testComparisonDifferentClassTypes() {
        ValueWrapper v1 = new ValueWrapper("Ljubica");
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));

        v1.numCompare(v2.getValue());
    }
    
    @Test
    public void testComparisonEquals1() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(3));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));

        int result = v1.numCompare(v2.getValue());
        
        assertEquals(0, result);
    }
    
    @Test
    public void testComparisonEquals2() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(3));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(3.0));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(0, result);
    }
    
    @Test
    public void testComparisonEquals3() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(3.0));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(0, result);
    }

    @Test
    public void testComparisonEquals4() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf("3.0"));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(0, result);
    }
    
    @Test
    public void testComparisonEquals5() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(3.0));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf("3"));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(0, result);
    }
    
    @Test
    public void testGreaterThan1() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(3));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(1, result);
    }
    

    @Test
    public void testGreaterThan2() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(3.0));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(2));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(1, result);
    }
    

    @Test
    public void testGreaterThan3() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(3));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.0));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(1, result);
    }
    
    @Test
    public void testGreaterThan4() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(3.0));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(2.0));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(1, result);
    }
    
    @Test
    public void testLessThan1() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(2));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(-1, result);
    }
    

    @Test
    public void testLessThan2() {
        ValueWrapper v1 = new ValueWrapper(Integer.valueOf(2));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(3.0));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(-1, result);
    }
    

    @Test
    public void testLessThan3() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(2.0));
        ValueWrapper v2 = new ValueWrapper(Integer.valueOf(3));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(-1, result);
    }
    
    @Test
    public void testLessThan4() {
        ValueWrapper v1 = new ValueWrapper(Double.valueOf(2.0));
        ValueWrapper v2 = new ValueWrapper(Double.valueOf(3.0));
        
        int result = v1.numCompare(v2.getValue());
        
        assertEquals(-1, result);
    }

}
