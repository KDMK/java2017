package ht.fer.zemris.java.hw05.demo4;

/**
 * Class models single student entry in student grades table. JMBAG is unique
 * key in table.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class StudentRecord {
    /**
     * Students unique number.
     */
    private String jmbag;
    /**
     * Students first name.
     */
    private String lastName;
    /**
     * Students last name.
     */
    private String firstName;
    /**
     * Students mid-term exam score.
     */
    private double scoreMidterms;
    /**
     * Students final exam score.
     */
    private double scoreFinals;
    /**
     * Students lab score.
     */
    private double scoreLab;
    /**
     * Students final grade.
     */
    private int grade;

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the scoreMidterms
     */
    public double getScoreMidterms() {
        return scoreMidterms;
    }

    /**
     * @param scoreMidterms
     *            the scoreMidterms to set
     */
    public void setScoreMidterms(double scoreMidterms) {
        this.scoreMidterms = scoreMidterms;
    }

    /**
     * @return the scoreFinals
     */
    public double getScoreFinals() {
        return scoreFinals;
    }

    /**
     * @param scoreFinals
     *            the scoreFinals to set
     */
    public void setScoreFinals(double scoreFinals) {
        this.scoreFinals = scoreFinals;
    }

    /**
     * @return the scoreLab
     */
    public double getScoreLab() {
        return scoreLab;
    }

    /**
     * @param scoreLab
     *            the scoreLab to set
     */
    public void setScoreLab(double scoreLab) {
        this.scoreLab = scoreLab;
    }

    /**
     * @return the grade
     */
    public int getGrade() {
        return grade;
    }

    /**
     * @param grade
     *            the grade to set
     */
    public void setGrade(int grade) {
        this.grade = grade;
    }

    /**
     * @return the jmbag
     */
    public String getJmbag() {
        return jmbag;
    }

    /**
     * Default constructor.
     * 
     * @param jmbag
     *            students unique number
     * @param lastName
     *            students last name
     * @param firstName
     *            students first name
     * @param d
     *            students mid-term exam score
     * @param e
     *            students final exam score
     * @param f
     *            students lab score
     * @param grade
     *            students final grade
     */
    public StudentRecord(String jmbag, String lastName, String firstName,
            double d, double e, double f, int grade) {
        super();
        this.jmbag = jmbag;
        this.lastName = lastName;
        this.firstName = firstName;
        this.scoreMidterms = d;
        this.scoreFinals = e;
        this.scoreLab = f;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return String.format("(%s) %s %s  %05.2f %05.2f %05.2f %d", jmbag, lastName,
                firstName, scoreMidterms, scoreFinals, scoreLab, grade);
    }

}
