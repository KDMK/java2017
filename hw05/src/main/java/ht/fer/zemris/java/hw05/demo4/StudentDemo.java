package ht.fer.zemris.java.hw05.demo4;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Demonstration program that shows example usage of Java stream API.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class StudentDemo {

    /**
     * Program starting point.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        String basePath = new File("src/main/resources/studenti.txt")
                .getAbsolutePath();
        List<String> lines = null;

        try {
            lines = Files.readAllLines(Paths.get(basePath),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println("Can't open file: " + e.getMessage());
            System.exit(1);
        }

        List<StudentRecord> records = null;
        try {
            records = convert(lines);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.exit(2);
        }

        System.out
                .println("Number of students with final score bigger than 25: "
                        + vratiBodovaViseOd25(records));

        System.out.println("Number of students with final grade 5: "
                + vratiBrojOdlikasa(records));

        vratiListuOdlikasa(records).forEach(System.out::println);
        System.out.println();

        vratiSortiranuListuOdlikasa(records).forEach(System.out::println);
        System.out.println();

        vratiPopisNepolozenih(records).forEach(System.out::println);

        @SuppressWarnings("unused")
        Map<Integer, List<StudentRecord>> list = razvrstajStudentePoOcjenama(
                records);

        Map<Integer, Integer> list2 = vratiBrojStudenataPoOcjenama(records);

        for (Entry<Integer, Integer> e : list2.entrySet()) {
            System.out.print(
                    String.format("%d  =>  %d%n", e.getKey(), e.getValue()));
        }
        System.out.println();

        Map<Boolean, List<StudentRecord>> map1 = razvrstajProlazPad(records);
        for (Entry<Boolean, List<StudentRecord>> e : map1.entrySet()) {
            System.out.println(e.getKey());
            for (StudentRecord t : e.getValue()) {
                System.out.println(t);
            }
        }

    }

    // Note: Function naming left as in task for testing purpose!

    /**
     * Finds number of students whose mid-term exam added to final exam and lab
     * score is more than 25.
     * 
     * @param records
     *            list of all student records
     * @return number
     */
    public static long vratiBodovaViseOd25(List<StudentRecord> records) {
        return records
                .stream().filter((t -> (t.getScoreFinals()
                        + t.getScoreMidterms() + t.getScoreLab()) > 25))
                .count();
    }

    /**
     * Finds number of students whose final grade is 5.
     * 
     * @param records
     *            list of all student records
     * @return number of excellent students
     */
    public static long vratiBrojOdlikasa(List<StudentRecord> records) {
        return records.stream().filter(t -> t.getGrade() == 5).count();
    }

    /**
     * Returns list of students whose final grade is 5.
     * 
     * @param records
     *            list of all student records
     * @return list of excellent students
     */
    public static List<StudentRecord> vratiListuOdlikasa(
            List<StudentRecord> records) {
        return records.stream().filter(t -> t.getGrade() == 5)
                .collect(Collectors.toList());
    }

    /**
     * Returns list of excellent students sorted by biggest total score.
     * 
     * @param records
     *            list of all student records
     * @return sorted list of excellent students
     */
    public static List<StudentRecord> vratiSortiranuListuOdlikasa(
            List<StudentRecord> records) {
        return vratiListuOdlikasa(records).stream().sorted((s1, s2) -> {
            Double s1Score = s1.getScoreMidterms() + s1.getScoreFinals()
                    + s1.getScoreLab();
            Double s2Score = s2.getScoreMidterms() + s2.getScoreFinals()
                    + s2.getScoreLab();

            return -s1Score.compareTo(s2Score);
        }).collect(Collectors.toList());
    }

    /**
     * Returns list of all students that failed the class.
     * 
     * @param records
     *            list of all student records
     * @return list of all students that failed sorted by jmbag(ascending)
     */
    public static List<String> vratiPopisNepolozenih(
            List<StudentRecord> records) {
        return records.stream().filter(t -> t.getGrade() == 1)
                .map(t -> t.getJmbag()).sorted().collect(Collectors.toList());
    }

    /**
     * Returns map of students grouped by their final grade.
     * 
     * @param records
     *            list of student records
     * @return map of students mapped by grade
     */
    public static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(
            List<StudentRecord> records) {
        return records.stream()
                .collect(Collectors.groupingBy(StudentRecord::getGrade));
    }

    /**
     * Returns map that contains number of each grade.
     * 
     * @param records
     *            list of student records
     * @return map grades - total students
     */
    public static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(
            List<StudentRecord> records) {
        return records.stream().collect(Collectors
                .toMap(StudentRecord::getGrade, (t) -> 1, (p1, p2) -> p1 + 1));
    }

    /**
     * Sorts students in map whether they passed the class or they didn't.
     * 
     * @param records
     *            list of student records
     * @return map
     *         <p>
     *         true - students that have passed class
     *         <p>
     *         false - students that have failed class
     */
    public static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(
            List<StudentRecord> records) {
        return records.stream()
                .collect(Collectors.partitioningBy(p -> p.getGrade() != 1));
    }

    /**
     * Builds list of student records from input file.
     * 
     * @param lines
     *            from file
     * @return list of student records
     * @throw IllegalArgument exception if JMBAG is already in list
     */
    private static List<StudentRecord> convert(List<String> lines) {
        List<StudentRecord> records = new ArrayList<>(500);
        Map<String, Integer> jmbagIndex = new HashMap<>(650);

        for (int i = 0, len = lines.size(); i < len; i++) {
            if (lines.get(i).length() == 0) {
                continue;
            }
            String[] parameters = lines.get(i).split("\\t");
            if (parameters.length != 7) {
                throw new IllegalArgumentException(String.format(
                        "Invalid number of arguments at line %d.%n", i + 1));
            }
            if (jmbagIndex.containsKey(parameters[0])) {
                throw new IllegalArgumentException(String.format(
                        "Jmbag %s is already in list.%n", parameters[0]));
            }
            records.add(new StudentRecord(parameters[0], parameters[1],
                    parameters[2], Double.parseDouble(parameters[3]),
                    Double.parseDouble(parameters[4]),
                    Double.parseDouble(parameters[5]),
                    Integer.parseInt(parameters[6])));
            jmbagIndex.put(parameters[0], i);
        }

        return records;
    }
}
