package hr.fer.java.zemris.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;

/**
 * Object multi-stack models special kind of Map data structure. For each Map
 * key there can be multiple entries for that given key. Multiple entries are
 * organized like stack - last added value is first one you will get when you
 * call <code>pop(key)</code>.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ObjectMultistack {
    /**
     * Internal map of stacks.
     */
    private Map<String, MultistackEntry> multistack;

    /**
     * Multi-stack entry for implementing stack. Stack is implemented as single
     * linked list.
     * 
     * @author Matija Bartolac
     * @version v1.0
     */
    private static class MultistackEntry {
        /**
         * Wrapped value.
         */
        private ValueWrapper value;
        /**
         * Reference to next node.
         */
        private MultistackEntry next;

        /**
         * Default constructor.
         * 
         * @param value
         *            WrappedValue
         * @param next
         *            reference to next node
         */
        MultistackEntry(ValueWrapper value, MultistackEntry next) {
            this.value = value;
            this.next = next;
        }
    }

    /**
     * Default constructor.
     */
    public ObjectMultistack() {
        multistack = new HashMap<>();
    }

    /**
     * Puts given wrapped value to stack associated to given name.
     * 
     * @param name
     *            key in map
     * @param valueWrapper
     *            wrapped value
     * @throws IllegalArgumentException
     *             If key is null.
     */
    public void push(String name, ValueWrapper valueWrapper) {
        if (name == null) {
            throw new IllegalArgumentException("Key cannot be null.");
        }
        if (valueWrapper == null) {
            throw new IllegalArgumentException(
                    "Expected ValueWrapper, got null!");
        }

        MultistackEntry newNode = new MultistackEntry(valueWrapper,
                multistack.get(name));
        multistack.put(name, newNode);
    }

    /**
     * Pops value from top of stack associated to given name.
     * 
     * @param name
     *            key in map
     * @return wrapped value that is stored under given key
     * @throws IllegalArgumentException
     *             if given name is null
     * @throws EmptyStackException
     *             if requested stack is empty
     */
    public ValueWrapper pop(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Key cannot be null");
        }
        if (isEmpty(name)) {
            throw new EmptyStackException();
        }

        MultistackEntry poppedNode = multistack.get(name);
        multistack.put(name, poppedNode.next);
        poppedNode.next = null;

        return poppedNode.value;
    }

    /**
     * Peeks top element from requested stack. Method does not remove any
     * element from stack.
     * 
     * @param name
     *            key in map
     * @return wrapped value that is stored under given key.
     * @throws IllegalArgumentException
     *             if given name is null
     * @throws EmptyStackException
     *             if requested stack is empty
     */
    public ValueWrapper peek(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Key cannot be null.");
        }
        if (isEmpty(name)) {
            throw new EmptyStackException();
        }

        return ((MultistackEntry) multistack.get(name)).value;
    }

    /**
     * Checks if stack with given key is empty.
     * 
     * @param name
     *            key in table
     * @return True if it is empty, false otherwise.
     */
    public boolean isEmpty(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Key cannot be null.");
        }
        if (!multistack.containsKey(name)) {
            return true;
        }

        return multistack.get(name) == null;
    }
}
