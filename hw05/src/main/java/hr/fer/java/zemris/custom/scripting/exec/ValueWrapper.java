package hr.fer.java.zemris.custom.scripting.exec;

/**
 * Value wrapper is used to provide uniform way of handling values stored in
 * stack, regardless of its type. It provides storage for single element and
 * basic operations. It can store any object type and null. Supported operations
 * are adding, subtraction, multiplication, dividing and comparison. Operations
 * can only be performed on numerical types.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ValueWrapper {
    /**
     * Wrapped value.
     */
    private Object value;

    /**
     * @return wrapped value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value
     *            the value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Default constructor.
     * 
     * @param value
     *            that we are wrapping.
     * @throws RuntimeException
     *             if passed arguments are of wrong type
     */
    public ValueWrapper(Object value) {
        if (value == null) {
            return; // By default this value is set to null so we don't have to
                    // do anything.
        }
        // If passed value is of any other reference type store it as it is.
        this.value = value;
    }

    /**
     * Adds passed value to stored value.
     * 
     * @param incValue
     *            term
     * @throws RuntimeException
     *             if passed arguments are of wrong type
     */
    public void add(Object incValue) {
        Number term1 = resolveNumber(value);
        Number term2 = resolveNumber(incValue);

        Number res = term1.doubleValue() + term2.doubleValue();
        updateValue(term1, term2, res);
    }

    /**
     * Subtract passed value from stored value.
     * 
     * @param decValue
     *            subtrahend
     * @throws RuntimeException
     *             if passed arguments are of wrong type
     */
    public void subtract(Object decValue) {
        Number term1 = resolveNumber(value);
        Number term2 = resolveNumber(decValue);

        Number res = term1.doubleValue() - term2.doubleValue();
        updateValue(term1, term2, res);
    }

    /**
     * Multiplies stored value with passed value.
     * 
     * @param mulValue
     *            multiplier
     * @throws RuntimeException
     *             if passed arguments are of wrong type
     */
    public void multiply(Object mulValue) {
        Number term1 = resolveNumber(value);
        Number term2 = resolveNumber(mulValue);

        Number res = term1.doubleValue() * term2.doubleValue();
        updateValue(term1, term2, res);
    }

    /**
     * Divides stored value with passed value.
     * 
     * @param divValue
     *            divider
     * @throws RuntimeException
     *             if passed arguments are of wrong type
     * @throws IllegalArgumentException
     *             if user tries to divide with zero
     */
    public void divide(Object divValue) {
        Number term1 = resolveNumber(value);
        Number term2 = resolveNumber(divValue);

        if (term2.equals(0)) {
            throw new IllegalArgumentException("Can't divide by zero!");
        }

        Number res = term1.doubleValue() / term2.doubleValue();
        updateValue(term1, term2, res);
    }

    /**
     * Compares passed value to stored value. Comparison is allowed only on
     * numerical types.
     *
     * @param withValue
     *            value that we are comparing stored value to.
     * @return <code>1</code> - if passed value is greater than stored value.
     *         <p>
     *         <code>0</code> - if passed value is equal to stored value.
     *         <p>
     *         <code>-1</code> - if passed value is less than stored
     * @throws RuntimeException
     *             If values cannot be compared.
     */
    public int numCompare(Object withValue) {
        Number o1 = resolveNumber(value);
        Number o2 = resolveNumber(withValue);

        Double c1 = o1.doubleValue();
        Double c2 = o2.doubleValue();

        return c1.compareTo(c2);
    }

    // ========================================================================
    // Helper methods
    // ========================================================================
    /**
     * Determines operand types and updates stored value accordingly.
     * 
     * @param num1
     *            first operand
     * @param num2
     *            second operand
     * @param res
     *            result of operation
     */
    private void updateValue(Number num1, Number num2, Number res) {
        if (num1 instanceof Integer && num2 instanceof Integer) {
            value = Integer.valueOf(res.intValue());
        } else {
            value = Double.valueOf(res.doubleValue());
        }
    }

    /**
     * Determines if passed object is any valid number representation and if it
     * is resolves it to right type.
     * 
     * @param o1
     *            input number(in form of String or Number)
     * @return Integer or Double of passed value.
     * @throws RuntimeException
     *             if passed string cannot be parsed to any valid number format.
     */
    private Number resolveNumber(Object o1) {
        if (o1 == null) {
            o1 = new Integer(0);
        }

        // Stored object is already valid number type
        if (o1 instanceof Integer || o1 instanceof Double) {
            return (Number) o1;
        }

        // Passed object is string, try to parse it to valid number format
        if (o1 instanceof String) {
            String input = (String) o1;
            try {
                return Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                // Do nothing here; check if string contains double value.
            }
            try {
                return Double.parseDouble(input);
            } catch (NumberFormatException e) {
                // String don't contain ant valid number format.
            }
        }

        throw new RuntimeException(String.format(
                "Object of type %s containing %s can't be resolved to a number!",
                o1.getClass().getName(), o1.toString()));
    }
}
