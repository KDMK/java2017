package hr.fer.zemris.java.hw05.observer2;

/**
 * Observer example shows simple usage of observer design pattern. It creates
 * storage and registers few observers, all with different functionality. When
 * we perform some operations on our storage observers will be notified and
 * desired action will be performed. Program takes no command line arguments and
 * output is set to {@link System#out}
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ObserverExample {

    /**
     * Starting point of Observer application.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        IntegerStorage istorage = new IntegerStorage(20);
        IntegerStorageObserver observer = new SquareValue();

        istorage.addObserver(observer);
        istorage.addObserver(new ChangeCounter());
        istorage.addObserver(new DoubleValue(1));
        istorage.addObserver(new DoubleValue(2));
        istorage.addObserver(new DoubleValue(3));

        istorage.setValue(5);
        istorage.setValue(2);
        istorage.setValue(25);
        istorage.setValue(13);
        istorage.setValue(22);
        istorage.setValue(15);

    }

}
