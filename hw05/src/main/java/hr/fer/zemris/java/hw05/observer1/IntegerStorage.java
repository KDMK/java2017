package hr.fer.zemris.java.hw05.observer1;

import java.util.ArrayList;
import java.util.List;

/**
 * Class IntegerStorage models the subject in our implementation of observer
 * pattern.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class IntegerStorage {
    /**
     * Value that storage holds.
     */
    private int value;
    /**
     * List of all observers registered to this subject.
     */
    private List<IntegerStorageObserver> observers;

    /**
     * Sets the initial value of current storage object.
     * 
     * @param initialValue
     *            that is stored in storage container
     */
    public IntegerStorage(int initialValue) {
        this.value = initialValue;
    }

    /**
     * Adds an observer to watch changes.
     * 
     * @param observer
     *            instance of specific observer
     */
    public void addObserver(IntegerStorageObserver observer) {
        if (observers == null) {
            observers = new ArrayList<>();
        }
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    /**
     * Removes observer passed as an argument of function. Current subject
     * doesn't have observer that we are trying to remove exception will be
     * thrown.
     * 
     * @param observer
     *            instance of specific observer
     * @throws IllegalArgumentException
     *             if observer is not in list
     */
    public void removeObserver(IntegerStorageObserver observer) {
        if (!observers.contains(observer)) {
            throw new IllegalArgumentException(
                    "Observer you try to remove is not registered!");
        }
        observers.remove(observer);
    }

    /**
     * Function removes all observers from current subject.
     */
    public void clearObservers() {
        if (observers != null) {
            observers.clear();
        }
    }

    /**
     * @return the value of storage
     */
    public int getValue() {
        return value;
    }

    /**
     * Updates value of internal storage to passed value and notifies all
     * registered observers that changes has been made.
     * 
     * @param value
     *            integer value that we want to store.
     */
    public void setValue(int value) {
        // Only if new value is different than the current value:
        if (this.value != value) {
            // Update current value
            this.value = value;
            List<IntegerStorageObserver> observersCopy = new ArrayList<>(
                    observers);
            // Notify all registered observers
            if (observers != null) {
                for (IntegerStorageObserver observer : observersCopy) {
                    observer.valueChanged(this);
                }
            }
        }
    }
}
