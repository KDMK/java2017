package hr.fer.zemris.java.hw05.observer1;

/**
 * Observer interface which is used to create observers for IntegerStorage
 * class.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public interface IntegerStorageObserver {
    /**
     * Provides some operation when change in subject it is detected.
     * 
     * @param istorage reference to storage which this observer watch
     */
    public void valueChanged(IntegerStorage istorage);
}
