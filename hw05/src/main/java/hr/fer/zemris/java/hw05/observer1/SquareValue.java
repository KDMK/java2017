package hr.fer.zemris.java.hw05.observer1;

/**
 * Class models specific observer for IntegerStorage class. Observer will, after
 * change has been made to subject, print newly added value and its square to
 * {@link System#out}
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class SquareValue implements IntegerStorageObserver {

    @Override
    public void valueChanged(IntegerStorage istorage) {
        System.out.printf("Provided new value: %d, square is %d%n",
                istorage.getValue(), istorage.getValue() * istorage.getValue());
    }
}
