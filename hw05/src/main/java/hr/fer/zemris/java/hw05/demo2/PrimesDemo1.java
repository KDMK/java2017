package hr.fer.zemris.java.hw05.demo2;

/**
 * First demonstration program for Primes collection.
 * 
 * @author Matija Bartolac
 *
 */
public class PrimesDemo1 {

    /**
     * Starting point of program.
     * 
     * @param args
     *            Command line arguments
     */
    public static void main(String[] args) {
        PrimesCollection primesCollection = new PrimesCollection(5);

        for (Integer prime : primesCollection) {
            System.out.println("Got prime: " + prime);
        }
    }

}
