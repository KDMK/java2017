package hr.fer.zemris.java.hw05.observer2;

/**
 * Class models specific observer for IntegerStorage class. Observer will, after
 * change in subject, print on standard output double of stored value in passed
 * storage object. Observer will auto de-register itself after set number of
 * repetitions.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class DoubleValue implements IntegerStorageObserver {
    /**
     * Number of times this observer will execute until it de-registers itself
     * from subject.
     */
    private int repetition;

    /**
     * Default constructor. Sets number of repetitions after which observer will
     * automatically de-register itself from subject.
     * 
     * @param i
     *            number of repetitions
     */
    public DoubleValue(int i) {
        this.repetition = i;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * hr.fer.zemris.java.hw05.observer1.IntegerStorageObserver#valueChanged(hr.
     * fer.zemris.java.hw05.observer1.IntegerStorage)
     */
    @Override
    public void valueChanged(IntegerStorageChange istorage) {
        if (istorage == null) {
            throw new IllegalArgumentException("Cannot access null!");
        }
        if (repetition <= 0) {
            istorage.getSubject().removeObserver(this);
            return;
        }
        System.out.println(
                "Double value: " + 2 * istorage.getSubject().getValue());
        repetition--;
    }
}
