package hr.fer.zemris.java.hw05.observer1;

/**
 * Class models specific observer for class IntegerStorage class. This observer,
 * after it's registered, will track further changes made to subject that is
 * registered to. After every change user will be notified on {@link System#out}
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ChangeCounter implements IntegerStorageObserver {
    /**
     * Number of modifications since observer is created.
     */
    private int modificationCount;

    @Override
    public void valueChanged(IntegerStorage istorage) {
        modificationCount++;
        System.out.println(
                "Number of value changes since tracking: " + modificationCount);
    }
}
