package hr.fer.zemris.java.hw05.observer2;

/**
 * Class models object that is used to wrap more data for observers to work
 * with. Using this class observers can see last modified elements and we can
 * get reference to object that observer is registered to.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class IntegerStorageChange {
    /**
     * Reference to subject.
     */
    private IntegerStorage subject;
    /**
     * Value before change.
     */
    private int oldValue;
    /**
     * Value after change.
     */
    private int newValue;

    /**
     * Default constructor. Creates internal copy of subject upon which
     * listeners will be called.
     * 
     * @param subject
     *            reference to object on which we will call listeners
     * @param newValue
     *            that we are adding to storage
     */
    public IntegerStorageChange(IntegerStorage subject, int newValue) {
        super();
        this.subject = subject;
        this.oldValue = subject.getValue();
        this.newValue = newValue;
    }

    /**
     * @return the subject
     */
    public IntegerStorage getSubject() {
        return subject;
    }

    /**
     * @return the oldValue
     */
    public int getOldValue() {
        return oldValue;
    }

    /**
     * @return the newValue
     */
    public int getNewValue() {
        return newValue;
    }

}
