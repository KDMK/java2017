package hr.fer.zemris.java.hw05.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class PrimesCollection models function that finds first n prime numbers.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class PrimesCollection implements Iterable<Integer> {
    /**
     * Size of primes collection.
     */
    private int numOfPrimes;

    /**
     * Default constructor. Creates
     * 
     * @param numOfPrimes
     *            size of collection
     * @throws IllegalArgumentException
     *             if user tries to create collection of less than one element
     */
    public PrimesCollection(int numOfPrimes) {
        if (numOfPrimes < 1) {
            throw new IllegalArgumentException(String.format(
                    "Cannot get collection of %d elements!%d", numOfPrimes));
        }
        this.numOfPrimes = numOfPrimes;
    }

    /**
     * Creates new iterator that produces prime numbers.
     * 
     * @return new prime numbers iterator
     */
    public Iterator<Integer> iterator() {
        return new PrimeIterator(numOfPrimes);
    }

    /**
     * Class models iterator that works as prime numbers generator.
     * 
     * @author Matija Bartolac
     * @version v1.0
     *
     */
    private class PrimeIterator implements Iterator<Integer> {
        /**
         * Size of prime number collection.
         */
        private int numOfIterations;
        /**
         * Last returned prime number.
         */
        private int curPrime;

        /**
         * Default constructor. Creates "collection" of prime numbers.
         * Collection size is provided as only argument.
         * 
         * @param numOfIterations
         *            collection size.
         */
        private PrimeIterator(int numOfIterations) {
            this.numOfIterations = numOfIterations;
            this.curPrime = 1;
        }

        @Override
        public boolean hasNext() {
            return numOfIterations > 0;
        }

        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            while (true) {
                curPrime++;
                if (isPrime(curPrime)) {
                    break;
                }
            }
            numOfIterations--;

            return Integer.valueOf(curPrime);
        }

        /**
         * Checks if passed number is prime number.
         * 
         * @param i
         *            number that is under test
         * @return <code>True</code> if number is prime, false otherwise.
         */
        private boolean isPrime(int i) {
            for (int div = 2; div < i; div++) {
                if ((i % div) == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
