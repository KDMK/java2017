package hr.fer.zemris.java.hw05.demo2;

/**
 * Second demonstration program for Primes collection.
 * 
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class PrimesDemo2 {

    /**
     * Starting point of our program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        PrimesCollection primesCollection = new PrimesCollection(2);
        for (Integer prime : primesCollection) {
            for (Integer prime2 : primesCollection) {
                System.out.println("Got prime pair: " + prime + ", " + prime2);
            }
        }
    }

}
