package hr.fer.zemris.java.zavrsni.zadatak1;

/**
 * Created by matija on 8/29/17.
 */
public class Expression {
    private String expressionName;
    private String firstParameter;
    private String secondParameter;
    private String operator;

    public String getExpressionName() {
        return expressionName;
    }

    public String getFirstParameter() {
        return firstParameter;
    }

    public String getSecondParameter() {
        return secondParameter;
    }

    public Expression(
            String expressionName, String firstParameter,
            String operator, String secondParameter) {
        this.expressionName = expressionName;
        this.firstParameter = firstParameter;
        this.secondParameter = secondParameter;
        this.operator = operator;
    }

    public double calculate(double first, double second) {
        switch (operator) {
            case "+":
                return first + second;
            case "-":
                return first - second;
            case "*":
                return first * second;
            case "/":
                return first / second;
        }

        return Double.NaN;
    }
}
