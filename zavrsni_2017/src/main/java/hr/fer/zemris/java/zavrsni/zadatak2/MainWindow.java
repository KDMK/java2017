package hr.fer.zemris.java.zavrsni.zadatak2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by matija on 8/29/17.
 */
public class MainWindow extends JFrame {
    private Buttons buttons;

    JTextArea fileContentContainer;
    JTable tableContainer;

    public MainWindow() {
        this.buttons = new Buttons(this);

        initUI();
    }

    private void initUI() {
        createMenuBar();
        createViewPanes();

        setTitle("Expression evaluator");
        setSize(800,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    private void createMenuBar() {
        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        file.add(buttons.openMenuItem);
        file.addSeparator();
        file.add(buttons.exitMenuItem);

        menubar.add(file);

        setJMenuBar(menubar);
    }

    private void createViewPanes() {
        fileContentContainer = new JTextArea();
        tableContainer = new JTable();

        JScrollPane pane1 = new JScrollPane(fileContentContainer);
        JScrollPane pane2 = new JScrollPane(tableContainer);

        JSplitPane contentContainer = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                                                     pane1, pane2);
        contentContainer.setOneTouchExpandable(true);
        contentContainer.setDividerLocation(150);

        fileContentContainer.setEditable(false);

        Dimension minimumSize = new Dimension(300,200);
        fileContentContainer.setMinimumSize(minimumSize);
        tableContainer.setMinimumSize(minimumSize);

        setContentPane(contentContainer);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            MainWindow myWindow = new MainWindow();
            myWindow.setVisible(true);
        });
    }
}
