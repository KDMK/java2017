package hr.fer.zemris.java.zavrsni.zadatak1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by matija on 8/29/17.
 */
public class ExpressionEvaluator {

    public static void main(String... args) throws IOException {
        Files.walkFileTree(Paths.get("jmbag0036479134/src/main/resources")
                                .toAbsolutePath(), new FileEvaluatorVisitor());
    }
}
