package hr.fer.zemris.java.zavrsni.zadatak1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matija on 8/29/17.
 */
@SuppressWarnings("ALL")
public class FileEvaluatorVisitor extends SimpleFileVisitor<Path> {
    private List<EvaluatedFileModel> evaluatedFiles;

    public FileEvaluatorVisitor() {
        evaluatedFiles = new ArrayList<>();
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
            throws IOException {
        if (file.toString().endsWith("-out.txt")) {
            return FileVisitResult.CONTINUE;
        }

        List<String> allLines = Files.readAllLines(file);

        EvaluatedFileModel evaluatedFileModel =
                new EvaluatedFileModel(allLines);
        evaluatedFileModel.process();

        evaluatedFiles.add(evaluatedFileModel);

        File outputFile = new File(file.toString()
                                       .replace("-in", "-out"));
        if (!outputFile.exists()) {
            outputFile.createNewFile();
        }

        OutputStream fos = new FileOutputStream(outputFile, false);
        evaluatedFileModel.writeToOutput(fos);

        return FileVisitResult.CONTINUE;
    }

    public List<EvaluatedFileModel> getEvaluatedFiles() {
        return evaluatedFiles;
    }

    public EvaluatedFileModel getEvaluatedFile(int index) {
        return evaluatedFiles.get(index);
    }
}
