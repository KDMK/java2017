package hr.fer.zemris.java.zavrsni.zadatak2;

import hr.fer.zemris.java.zavrsni.zadatak1.EvaluatedFileModel;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by matija on 8/29/17.
 */
public class Util {

    public static String formatOutput(List<String> inputLines) {
        StringBuilder outputBuilder = new StringBuilder();

        for(String line: inputLines) {
            if(line.trim().length() == 0) continue;
            if(!line.startsWith("#")) break;

            outputBuilder.append(line + System.lineSeparator());
        }

        return outputBuilder.toString();
    }

    public static String[] createResultsArray(String[] inputVariables, String outputValues) {
        return (String.join(";", inputVariables)+ ";" + outputValues).split(";");
    }

    public static TableModel createTableModel(EvaluatedFileModel evaluatedFile) {
        DefaultTableModel model = new DefaultTableModel();

        for(String label: evaluatedFile.getLabels()) {
            model.addColumn(label);
        }

        for(String outputVariable: evaluatedFile.getOutputVariables()) {
            model.addColumn(outputVariable);
        }

        for(int i = 0; i < evaluatedFile.getInputNumbers().size(); i++) {
            model.addRow(Util.createResultsArray(evaluatedFile.getInputNumbers().get(i),
                                                 evaluatedFile.getOutputLines().get(i)));
        }

        return model;
    }
}
