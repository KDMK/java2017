package hr.fer.zemris.java.zavrsni.zadatak2;

import hr.fer.zemris.java.zavrsni.zadatak1.EvaluatedFileModel;
import hr.fer.zemris.java.zavrsni.zadatak1.FileEvaluatorVisitor;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;

/**
 * Created by matija on 8/29/17.
 */
public class Actions {
    private MainWindow windowReference;

    public Actions(MainWindow windowReference) {
        this.windowReference = windowReference;
    }

    public ActionListener loadFile = e -> {
        JFileChooser fileChooser = new JFileChooser();
        int status = fileChooser.showDialog(null,"Load file");

        if(status == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "No files selected.");
            return;
        }

        if(status == JFileChooser.ERROR_OPTION) {
            JOptionPane.showMessageDialog(windowReference, "An error has occured while opening " +
                                                           "file", "Error!", JOptionPane
                    .ERROR_MESSAGE);
        }

        FileEvaluatorVisitor fileEvaluator = new FileEvaluatorVisitor();
        try {
            fileEvaluator.visitFile(fileChooser.getSelectedFile().toPath(), null);
        } catch (Exception e1) {
            JOptionPane.showMessageDialog(windowReference, "An error occured while trying to " +
                                                           "parse input file.", "Error!",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        EvaluatedFileModel evaluatedFile = fileEvaluator.getEvaluatedFile(0);
        String evaluatedFileText = Util.formatOutput(evaluatedFile.getAllLines());
        TableModel model = Util.createTableModel(evaluatedFile);

        windowReference.fileContentContainer.setText(evaluatedFileText);
        windowReference.tableContainer.setModel(model);
    };

    public ActionListener exitProgram = (e) -> {
        System.exit(0);
    };

}
