package hr.fer.zemris.java.zavrsni.zadatak2;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by matija on 8/29/17.
 */
public class Buttons {
    private MainWindow windowReference;
    private Actions actions;

    JMenuItem exitMenuItem;
    JMenuItem openMenuItem;

    public Buttons(MainWindow windowReference) {
        this.windowReference = windowReference;
        this.actions = new Actions(windowReference);

        this.exitMenuItem = createExitMenuItem();
        this.openMenuItem = createOpenMenuItem();
    }

    private JMenuItem createExitMenuItem() {
        return createMenuItem("Exit", KeyEvent.VK_E, "Ends exection of program.", actions.exitProgram);
    }

    private JMenuItem createOpenMenuItem() {
        return createMenuItem("Load file", KeyEvent.VK_O, "Loads file from memory.", actions
                .loadFile);
    }

    private static JMenuItem createMenuItem(String label, int mnemonic, String tooltip,
                                     ActionListener listener) {
        JMenuItem item = new JMenuItem(label);
        item.setMnemonic(mnemonic);
        item.setToolTipText(tooltip);
        item.addActionListener(listener);

        return item;
    }

}
