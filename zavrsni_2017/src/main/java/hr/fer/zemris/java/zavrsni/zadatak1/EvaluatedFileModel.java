package hr.fer.zemris.java.zavrsni.zadatak1;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class EvaluatedFileModel {
    private static final String PARAMETER_SEPARATOR = "\\s*;\\s*";
    private static final String EXPAND_KEYWORD = "#EXPAND";
    private static final String EXPR_KEYWORD = "#EXPR";
    private static final String LABELS_KEYWORD = "#LABELS";

    private List<String> allLines;
    private List<String> labels;
    private List<Expression> expressions;
    private List<String[]> inputNumbers;
    private List<String> outputVariables;
    private List<String> outputLines;

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public void setOutputVariables(List<String> outputVariables) {
        this.outputVariables = outputVariables;
    }

    public void addExpression(Expression expression) {
        this.expressions.add(expression);
    }

    public void addInputNumberCombination(String[] inputNumberCombination) {
        this.inputNumbers.add(inputNumberCombination);
    }

    private EvaluatedFileModel(
            List<String> labels, List<Expression> expressions, List<String[]> inputNumbers,
            List<String> outputVariables, List<String> outputLines) {
        this.labels = labels;
        this.expressions = expressions;
        this.inputNumbers = inputNumbers;
        this.outputVariables = outputVariables;
        this.outputLines = outputLines;
    }

    public EvaluatedFileModel(List<String> inputFileAllLines) {
        this(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(),
             new ArrayList<>(), new ArrayList<>());
        this.allLines = inputFileAllLines;
        buildInputFileModel(inputFileAllLines);
    }

    public void buildInputFileModel(List<String> allLines) {
        for (String line : allLines) {
            if (line.trim().length() == 0) {
                continue;
            }

            if (line.startsWith(EXPAND_KEYWORD)) {
                extractKeywords(line);
            } else if (line.startsWith(EXPR_KEYWORD)) {
                extractExpressions(line);
            } else if (line.startsWith(LABELS_KEYWORD)) {
                extractLabels(line);
            } else {
                extractInputNumberCombination(line);
            }
        }
    }

    public void process() {
        for (String[] currentNumberCombination : inputNumbers) {
            StringBuilder newOutputLine = new StringBuilder();

            for (String expressionName : outputVariables) {
                Expression currentExpression = findExpression(expressionName);

                newOutputLine.append(
                        evaluateExpression(currentExpression,
                                           currentNumberCombination));
                newOutputLine.append(';');
            }

            newOutputLine.deleteCharAt(newOutputLine.length() - 1);
            outputLines.add(newOutputLine.toString());
        }
    }

    private double evaluateExpression(Expression currentExpression, String[] currentNumberCombination) {
        String firstParameter = currentExpression.getFirstParameter();
        String secondParameter = currentExpression.getSecondParameter();

        double firstOperationArgument =
                resolveArgument(currentNumberCombination, firstParameter);
        double secondOperationArgument =
                resolveArgument(currentNumberCombination, secondParameter);

        return currentExpression.calculate(firstOperationArgument,
                                           secondOperationArgument);
    }

    private double resolveArgument(String[] currentNumberCombination, String parameter) {
        double operationArgument;

        int parameterIndex = labels.indexOf(parameter);
        if (parameterIndex != -1) {
            operationArgument =
                    Double.valueOf(currentNumberCombination[parameterIndex]);
        } else {
            operationArgument =
                    evaluateExpression(findExpression(parameter),
                                       currentNumberCombination);
        }
        return operationArgument;
    }

    private Expression findExpression(String name) {
        for (Expression currentExpression : expressions) {
            if (currentExpression.getExpressionName().equals(name)) {
                return currentExpression;
            }
        }
        throw new NoSuchElementException(
                String.format("Cannot find %s expression", name));
    }

    private void extractInputNumberCombination(String line) {
        String[] inputNumberCombination = line.split(PARAMETER_SEPARATOR);
        this.addInputNumberCombination(inputNumberCombination);
    }

    private void extractLabels(String line) {
        line = line.replace(LABELS_KEYWORD, "").trim();
        String[] labels = line.split(PARAMETER_SEPARATOR);
        this.setLabels(Arrays.asList(labels));
    }

    private void extractExpressions(String line) {
        line = line.replace(EXPR_KEYWORD, "").trim();
        Expression newExpression = buildExpression(line);
        this.addExpression(newExpression);
    }

    private void extractKeywords(String line) {
        line = line.replace(EXPAND_KEYWORD, "").trim();
        String[] outputArguments = line.split(PARAMETER_SEPARATOR);
        this.setOutputVariables(Arrays.asList(outputArguments));
    }

    private Expression buildExpression(String line) {
        line = line.replace('$', ' ').trim();
        String[] splittedExpression = line.split("\\s+");

        Expression expression =
                new Expression(splittedExpression[0].trim(),
                               splittedExpression[2].trim(),
                               splittedExpression[3].trim(),
                               splittedExpression[4].trim());

        return expression;
    }

    public void writeToOutput(OutputStream out) {
        PrintWriter outputWriter = new PrintWriter(out);

        for (int i = 0; i < inputNumbers.size(); i++) {
            outputWriter.println(
                    String.join(";", inputNumbers.get(i)) + ";" +
                    outputLines.get(i));
        }

        outputWriter.close();
    }

    public List<String> getAllLines() {
        return allLines;
    }

    public List<String> getLabels() {
        return labels;
    }

    public List<String[]> getInputNumbers() {
        return inputNumbers;
    }

    public List<String> getOutputVariables() {
        return outputVariables;
    }

    public List<String> getOutputLines() {
        return outputLines;
    }
}
