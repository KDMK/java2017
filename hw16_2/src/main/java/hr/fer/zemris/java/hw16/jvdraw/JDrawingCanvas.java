package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;

/**
 * Class models canvas for drawing shapes.
 * 
 * @author Matija Bartolac
 *
 */
/**
 * @author matija
 *
 */
public class JDrawingCanvas extends JComponent implements DrawingModel {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * List of drawable objects.
     */
    private List<GeometricalObject> drawables;
    /**
     * List of registerd listeners.
     */
    private List<DrawingModelListener> listeners;

    /**
     * Current foregroundColor.
     */
    private IColorProvider foregroundColor;
    /**
     * Current backgroundColor.
     */
    private IColorProvider backgroundColor;

    /**
     * Models listener that is triggered when there was change on objects list.
     */
    private DrawingModelListener refreshListener = new DrawingModelListener() {
        @Override
        public void objectsRemoved(DrawingModel source, int index0,
                int index1) {
            JDrawingCanvas.this.repaint();
        }

        @Override
        public void objectsChanged(DrawingModel source, int index0,
                int index1) {
            JDrawingCanvas.this.repaint();
        }

        @Override
        public void objectsAdded(DrawingModel source, int indexo, int index1) {
            JDrawingCanvas.this.repaint();
        }
    };

    /**
     * Default constructor.
     * 
     * @param foregroundColor
     *            reference to foreground color.
     * @param backgroundColor
     *            reference to background color
     */
    public JDrawingCanvas(IColorProvider foregroundColor,
            IColorProvider backgroundColor) {
        setBackground(Color.WHITE);
        this.foregroundColor = foregroundColor;
        this.backgroundColor = backgroundColor;

        drawables = new ArrayList<>();
        listeners = new ArrayList<>();

        listeners.add(refreshListener);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getParent().getWidth(), getParent().getHeight());

        g2d.setColor(foregroundColor.getCurrentColor());
        g2d.setBackground(backgroundColor.getCurrentColor());

        for (GeometricalObject o : drawables) {
            o.draw(g2d);
        }

    }

    /**
     * Returns list of all objects in this canvas
     * 
     * @return the list of objects
     */
    public List<GeometricalObject> getAllObjects() {
        return Collections.unmodifiableList(drawables);
    }

    @Override
    public int getNumOfDrawables() {
        return drawables.size();
    }

    @Override
    public GeometricalObject getObject(int index) {
        if (index > 0 && index < getNumOfDrawables()) {
            return drawables.get(index);
        }

        return drawables.get(index);
    }

    @Override
    public void add(GeometricalObject object) {
        drawables.add(object);
        repaint();
    }

    @Override
    public void addDrawingModelListener(DrawingModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeDrawingModelListener(DrawingModelListener l) {
        listeners.remove(l);
    }

    /**
     * Notifies all listeners that change occurred.
     */
    public void notifyListeners() {
        for (DrawingModelListener l : listeners) {
            l.objectsAdded(this, drawables.size(), drawables.size());
        }
    }

    /**
     * @return the foreground color
     */
    public Color getForegroundColor() {
        return this.foregroundColor.getCurrentColor();
    }

    /**
     * @return the background color
     */
    public Color getBackgroundColor() {
        return this.backgroundColor.getCurrentColor();
    }

    /**
     * Returns index of searched object.
     * 
     * @param obj
     *            object that we are looking for.
     * @return the index of object
     */
    public int getIndex(GeometricalObject obj) {
        return drawables.indexOf(obj);
    }

    /**
     * Updates object in list.
     * 
     * @param curObject
     *            reference to current object
     * @param index
     *            index of an object
     */
    public void updateObject(GeometricalObject curObject, int index) {
        drawables.remove(index);
        drawables.add(index, curObject);
    }

    /**
     * Removes object from canvas.
     * 
     * @param obj
     *            object to remove.
     */
    public void removeDrawable(GeometricalObject obj) {
        drawables.remove(obj);
    }

    /**
     * Removes all objects from canvas.
     */
    public void clearCanvas() {
        drawables.clear();
    }
}
