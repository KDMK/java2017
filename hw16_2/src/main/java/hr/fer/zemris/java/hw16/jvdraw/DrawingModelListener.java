package hr.fer.zemris.java.hw16.jvdraw;

/**
 * Drawing model listener interface defines method that are called when there
 * was change on drawing model.
 * 
 * @author Matija Bartolac
 *
 */
public interface DrawingModelListener {
    /**
     * Objects were added to source
     * 
     * @param source
     *            of change
     * @param index0
     *            starting index
     * @param index1
     *            end index.
     */
    public void objectsAdded(DrawingModel source, int index0, int index1);

    /**
     * Objects were removed from source
     * 
     * @param source
     *            of change
     * @param index0
     *            starting index
     * @param index1
     *            end index.
     */
    public void objectsRemoved(DrawingModel source, int index0, int index1);

    /**
     * Objects were changed,
     * 
     * @param source
     *            of change
     * @param index0
     *            starting index
     * @param index1
     *            end index.
     */
    public void objectsChanged(DrawingModel source, int index0, int index1);
}
