package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

/**
 * Class models color area component that is used as indicator of selected
 * color. If user clicks on it it will open color chooser dialog.
 * 
 * @author Matija Bartolac
 *
 */
public class JColorArea extends JComponent implements IColorProvider {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Default icon width.
     */
    private static final int WIDTH = 20;
    /**
     * Default icon height.
     */
    private static final int HEIGHT = 20;

    /**
     * Preferred dimension of color area.
     */
    private Dimension colorAreaDimension = new Dimension(WIDTH, HEIGHT);

    /**
     * Color change listeners
     */
    private List<ColorChangeListener> colorChangeListeners;

    /**
     * currently selected color.
     */
    private Color selectedColor;

    @Override
    public Dimension getPreferredSize() {
        return colorAreaDimension;
    }

    /**
     * Default constructor.
     * 
     * @param selectedColor currently selected color.
     */
    public JColorArea(Color selectedColor) {
        if (selectedColor == null) {
            this.selectedColor = Color.BLACK;
        }

        colorChangeListeners = new ArrayList<>();
        colorChangeListeners.add(new ColorChangeListener() {
            @Override
            public void newColorSelected(IColorProvider source, Color oldColor,
                    Color newColor) {
                JColorArea.this.selectedColor = newColor;
                JColorArea.this.repaint();
            }
        });

        this.selectedColor = selectedColor;

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Color oldColor = JColorArea.this.selectedColor;
                Color newColor = JColorChooser.showDialog(null,
                        "Choose color...", selectedColor);
                for (ColorChangeListener lst : colorChangeListeners) {
                    lst.newColorSelected(JColorArea.this, oldColor, newColor);
                }
            }
        });
    }

    @Override
    public Color getCurrentColor() {
        return this.selectedColor;
    }

    @Override
    public void addColorChangeListener(ColorChangeListener l) {
        if (!colorChangeListeners.contains(l)) {
            this.colorChangeListeners.add(l);
        }
    }

    @Override
    public void removeColorChangeListener(ColorChangeListener l) {
        if (colorChangeListeners.contains(l)) {
            colorChangeListeners.remove(l);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(selectedColor);

        // g2d.fillRect(getParent().getX(), getParent().getY(),
        // getPreferredSize().width, getPreferredSize().height);

        g2d.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
    }

    @Override
    public Dimension getMinimumSize() {
        return colorAreaDimension;
    }

    @Override
    public Dimension getMaximumSize() {
        return colorAreaDimension;
    }
}
