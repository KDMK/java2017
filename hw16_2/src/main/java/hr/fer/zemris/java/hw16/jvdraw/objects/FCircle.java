package hr.fer.zemris.java.hw16.jvdraw.objects;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Class models filled circle
 * 
 * @author Matija Bartolac
 *
 */
public class FCircle implements GeometricalObject {
    /**
     * Center x coordinate
     */
    private int centerX;
    /**
     * Center y coordinate
     */
    private int centerY;
    /**
     * Circle radius
     */
    private int radius;
    /**
     * Number of circle instance.
     */
    private int instance;

    /**
     * Fill color.
     */
    private Color fillColor;
    /**
     * Border color.
     */
    private Color borderColor;

    /**
     * Static counter of created FCircle instances
     */
    private static int counter = 1;

    /**
     * Default constructor.
     * 
     * @param centerX
     *            center x coordinate
     * @param centerY
     *            center y coordinate
     * @param radius
     *            circle radius
     * @param fillColor
     *            fill color
     * @param borderColor
     *            border color
     */
    public FCircle(int centerX, int centerY, int radius, Color fillColor,
            Color borderColor) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.fillColor = fillColor;
        this.borderColor = borderColor;

        this.instance = counter++;
    }

    @Override
    public void draw(Graphics2D canvas) {
        canvas.setColor(fillColor);
        canvas.fillOval(centerX, centerY, 2 * radius, 2 * radius);
        canvas.setColor(borderColor);
        canvas.drawOval(centerX, centerY, 2 * radius, 2 * radius);
    }

    @Override
    public String toString() {
        return "Filled circle " + instance;
    }

    /**
     * @return the centerX
     */
    public int getCenterX() {
        return centerX;
    }

    /**
     * @param centerX
     *            the centerX to set
     */
    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    /**
     * @return the centerY
     */
    public int getCenterY() {
        return centerY;
    }

    /**
     * @param centerY
     *            the centerY to set
     */
    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    /**
     * @return the radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * @param radius
     *            the radius to set
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    /**
     * @return the fillColor
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * @param fillColor
     *            the fillColor to set
     */
    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * @return the borderColor
     */
    public Color getBorderColor() {
        return borderColor;
    }

    /**
     * @param borderColor
     *            the borderColor to set
     */
    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    @Override
    public String generateStringRepresentation() {
        return String.format("FCIRCLE %d %d %d %d %d %d %d %d %d%n", centerX,
                centerY, radius, borderColor.getRed(), borderColor.getGreen(),
                borderColor.getBlue(), fillColor.getRed(), fillColor.getGreen(),
                fillColor.getBlue());
    }
}
