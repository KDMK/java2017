package hr.fer.zemris.java.hw16.jvdraw;

import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;

/**
 * Interface that defines methods for drawing model
 * 
 * @author matija bartolac
 *
 */
public interface DrawingModel {
    /**
     * Returns current number of drawable objects.
     * 
     * @return number of objects
     */
    public int getNumOfDrawables();

    /**
     * Returns drawable object at given index.
     * 
     * @param index
     *            index of an object
     * @return geometrical object at given index.
     */
    public GeometricalObject getObject(int index);

    /**
     * Adds new drawable object to drawing model.
     * 
     * @param object
     *            object that we want to add.
     */
    public void add(GeometricalObject object);

    /**
     * Adds drawing model listener
     * 
     * @param l
     *            the drawing model listener
     */
    public void addDrawingModelListener(DrawingModelListener l);

    /**
     * Removes drawing model listener
     * 
     * @param l
     *            the drawing model listener
     */
    public void removeDrawingModelListener(DrawingModelListener l);
}
