package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;

/**
 * Interface used for JColorPane to store color information.
 * 
 * @author Matija Bartolac
 *
 */
public interface IColorProvider {
    /**
     * Returns current color.
     * 
     * @return the current color.
     */
    public Color getCurrentColor();

    /**
     * Adds color change listener
     * 
     * @param l
     *            the color change listener
     */
    public void addColorChangeListener(ColorChangeListener l);

    /**
     * Removes color change listener
     * 
     * @param l
     *            the color change listener
     */
    public void removeColorChangeListener(ColorChangeListener l);
}
