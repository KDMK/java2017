package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * Models list that is used to display drawables contained in viewport.
 * 
 * @author Matija Bartolac
 *
 */
public class DrawingObjectListModel
        extends AbstractListModel<GeometricalObject> {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Reference to list of objects.
     */
    private List<GeometricalObject> objects;
    /**
     * List of listeners.
     */
    private List<ListDataListener> listeners;
    /**
     * Currently selected shape for drawing
     */
    private String shape;

    /**
     * Last created object.
     */
    private GeometricalObject curObject;
    /**
     * Indicates if mouse was pressed for object creation.
     */
    private boolean mousePressCounter;

    /**
     * Default constructor.
     * 
     * @param canvas
     *            reference to canvas.
     */
    public DrawingObjectListModel(JDrawingCanvas canvas) {
        this.objects = canvas.getAllObjects();
        this.listeners = new ArrayList<>();

        // ovome bas nije mjesto ovdje...
        canvas.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseMoved(MouseEvent e) {
                if (mousePressCounter && curObject != null) {
                    switch (shape) {
                    case "LINE":
                        int index = canvas.getIndex(curObject);
                        ((Line) curObject).setEndX(e.getX());
                        ((Line) curObject).setEndY(e.getY());
                        canvas.updateObject(curObject, index);
                        break;
                    case "CIRCLE":
                        int index2 = canvas.getIndex(curObject);
                        Circle curCircle = (Circle) curObject;
                        curCircle.setRadius(delta(curCircle.getCenterX(),
                                curCircle.getCenterY(), e.getX(), e.getY()));
                        canvas.updateObject(curObject, index2);
                        break;
                    case "FILLED CIRCLE":
                        int index3 = canvas.getIndex(curObject);
                        FCircle curFCircle = (FCircle) curObject;
                        curFCircle.setRadius(delta(curFCircle.getCenterX(),
                                curFCircle.getCenterY(), e.getX(), e.getY()));
                        canvas.updateObject(curObject, index3);
                        break;
                    }

                    ListDataEvent event1 = new ListDataEvent(this,
                            ListDataEvent.CONTENTS_CHANGED, 0, objects.size());
                    for (ListDataListener l : listeners) {
                        l.intervalAdded(event1);
                    }

                    canvas.notifyListeners();

                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
            }
        });

        canvas.addMouseListener(new MouseAdapter() {
            int startX;
            int startY;

            @Override
            public void mousePressed(MouseEvent e) {
                if (!mousePressCounter && shape != null) {
                    mousePressCounter = true;
                    startX = e.getX();
                    startY = e.getY();

                    switch (shape) {
                    case "LINE":
                        curObject = new Line(startX, e.getX(), startY, e.getY(),
                                canvas.getForegroundColor());
                        canvas.add(curObject);
                        break;
                    case "CIRCLE":
                        int radius = delta(startX, startY, e.getX(), e.getY());
                        curObject = new Circle(startX - radius, startY - radius,
                                radius, canvas.getForegroundColor());
                        canvas.add(curObject);
                        break;
                    case "FILLED CIRCLE":
                        int fradius = delta(startX, startY, e.getX(), e.getY());
                        curObject = new FCircle(startX - fradius,
                                startY - fradius, fradius,
                                canvas.getBackgroundColor(),
                                canvas.getForegroundColor());
                        canvas.add(curObject);
                    }

                    ListDataEvent event1 = new ListDataEvent(this,
                            ListDataEvent.INTERVAL_ADDED, objects.size(),
                            objects.size());
                    for (ListDataListener l : listeners) {
                        l.intervalAdded(event1);
                    }

                    canvas.notifyListeners();
                } else if (mousePressCounter) {
                    switch (shape) {
                    case "LINE":
                        int index1 = canvas.getIndex(curObject);
                        ((Line) curObject).setEndX(e.getX());
                        ((Line) curObject).setEndY(e.getY());
                        canvas.updateObject(curObject, index1);
                        break;
                    case "CIRCLE":
                        int index2 = canvas.getIndex(curObject);
                        ((Circle) curObject).setRadius(
                                delta(startX, startY, e.getX(), e.getY()));
                        canvas.updateObject(curObject, index2);
                        break;
                    case "FILLED CIRCLE":
                        int index3 = canvas.getIndex(curObject);
                        FCircle curFCircle = (FCircle) curObject;
                        curFCircle.setRadius(delta(curFCircle.getCenterX(),
                                curFCircle.getCenterY(), e.getX(), e.getY()));
                        canvas.updateObject(curObject, index3);
                        break;
                    }

                    ListDataEvent event1 = new ListDataEvent(this,
                            ListDataEvent.INTERVAL_ADDED, objects.size(),
                            objects.size());
                    for (ListDataListener l : listeners) {
                        l.intervalAdded(event1);
                    }

                    canvas.notifyListeners();

                    mousePressCounter = false;
                }
            }
        });
    }

    /**
     * Calculates distance between two points.
     * 
     * @param x1
     *            coordinate of first point.
     * @param y1
     *            coordinate of first point.
     * @param x2
     *            coordinate of second point.
     * @param y2
     *            coordinate of second point.
     * @return distance between points.
     */
    private int delta(int x1, int y1, int x2, int y2) {
        return (int) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y2));
    }

    /**
     * Sets shape name that we are drawing.
     * 
     * @param shape
     *            name
     */
    public void setShape(String shape) {
        this.shape = shape;
    }

    @Override
    public int getSize() {
        return objects.size();
    }

    @Override
    public GeometricalObject getElementAt(int index) {
        return objects.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }

    /**
     * @return list of all listeners
     */
    public List<ListDataListener> getListListeneres() {
        return Collections.unmodifiableList(listeners);
    }
}
