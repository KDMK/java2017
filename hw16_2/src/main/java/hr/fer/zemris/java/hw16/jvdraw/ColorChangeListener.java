package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;

/**
 * Interface that defines methods for color change listeners.
 * 
 * @author matija bartolac
 *
 */
public interface ColorChangeListener {

    /**
     * Invoked when new color is selected.
     * 
     * @param source
     *            color source.
     * @param oldColor
     *            old color.
     * @param newColor
     *            new color.
     */
    public void newColorSelected(IColorProvider source, Color oldColor,
            Color newColor);
}
