package hr.fer.zemris.java.hw16.jvdraw.objects;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Class models circle.
 * 
 * @author Matija Bartolac
 *
 */
public class Circle implements GeometricalObject {
    /**
     * Center x coordinate of circle.
     */
    private int centerX;
    /**
     * Center y coordinate of circle.
     */
    private int centerY;
    /**
     * Radius of circle
     */
    private int radius;
    /**
     * Number of circle instance.
     */
    private int instance;

    /**
     * Color of border line
     */
    private Color borderColor;

    /**
     * Static counter of created circle instances.
     */
    private static int counter = 1;

    /**
     * Default constructor.
     * 
     * @param centerX
     *            center x coordinate
     * @param centerY
     *            center y coordinate
     * @param radius
     *            of circle
     * @param borderColor
     *            used line border color
     */
    public Circle(int centerX, int centerY, int radius, Color borderColor) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.borderColor = borderColor;

        this.instance = counter++;
    }

    @Override
    public void draw(Graphics2D canvas) {
        canvas.setColor(borderColor);
        canvas.drawOval(centerX, centerY, 2 * radius, 2 * radius);
    }

    @Override
    public String toString() {
        return "Circle " + instance;
    }

    /**
     * @return the centerX
     */
    public int getCenterX() {
        return centerX;
    }

    /**
     * @param centerX
     *            the centerX to set
     */
    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    /**
     * @return the centerY
     */
    public int getCenterY() {
        return centerY;
    }

    /**
     * @param centerY
     *            the centerY to set
     */
    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    /**
     * @return the borderColor
     */
    public Color getBorderColor() {
        return borderColor;
    }

    /**
     * @param borderColor
     *            the borderColor to set
     */
    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * @return the radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * @param radius
     *            the radius to set
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String generateStringRepresentation() {
        return String.format("CIRCLE %d %d %d %d %d %d%n", centerX, centerY,
                radius, borderColor.getRed(), borderColor.getGreen(),
                borderColor.getBlue());
    }
}
