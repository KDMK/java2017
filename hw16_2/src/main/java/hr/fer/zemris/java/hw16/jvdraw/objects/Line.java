package hr.fer.zemris.java.hw16.jvdraw.objects;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Class models line object.
 * 
 * @author Matija Bartolac
 *
 */
public class Line implements GeometricalObject {
    /**
     * Start x coordinate.
     */
    private int startX;
    /**
     * End x coordinate.
     */
    private int endX;
    /**
     * Start y coordinate.
     */
    private int startY;
    /**
     * End y coordinate;
     */
    private int endY;
    /**
     * Number of line instance.
     */
    private int instance;

    /**
     * Line color
     */
    private Color lineColor;

    /**
     * Static counter of created line objects.
     */
    private static int counter = 1;

    /**
     * Default constructor.
     * 
     * @param startX
     *            starting point x coordinate
     * @param endX
     *            end point x coordinate
     * @param startY
     *            starting point y coordinate
     * @param endY
     *            end point y coordinate
     * @param lineColor
     *            line color
     */
    public Line(int startX, int endX, int startY, int endY, Color lineColor) {
        super();
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
        this.instance = counter++;
        this.lineColor = lineColor;
    }

    @Override
    public void draw(Graphics2D canvas) {
        canvas.setColor(lineColor);
        canvas.drawLine(startX, startY, endX, endY);
    }

    @Override
    public String toString() {
        return "Line " + instance;
    }

    /**
     * @return the startX
     */
    public int getStartX() {
        return startX;
    }

    /**
     * @param startX
     *            the startX to set
     */
    public void setStartX(int startX) {
        this.startX = startX;
    }

    /**
     * @return the endX
     */
    public int getEndX() {
        return endX;
    }

    /**
     * @param endX
     *            the endX to set
     */
    public void setEndX(int endX) {
        this.endX = endX;
    }

    /**
     * @return the startY
     */
    public int getStartY() {
        return startY;
    }

    /**
     * @param startY
     *            the startY to set
     */
    public void setStartY(int startY) {
        this.startY = startY;
    }

    /**
     * @return the endY
     */
    public int getEndY() {
        return endY;
    }

    /**
     * @param endY
     *            the endY to set
     */
    public void setEndY(int endY) {
        this.endY = endY;
    }

    /**
     * @return the lineColor
     */
    public Color getLineColor() {
        return lineColor;
    }

    /**
     * @param lineColor
     *            the lineColor to set
     */
    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    @Override
    public String generateStringRepresentation() {
        return String.format("LINE %d %d %d %d %d %d %d%n", startX, startY,
                endX, endY, lineColor.getRed(), lineColor.getGreen(),
                lineColor.getBlue());
    }
}
