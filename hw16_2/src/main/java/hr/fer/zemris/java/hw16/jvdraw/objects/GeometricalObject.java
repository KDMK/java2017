package hr.fer.zemris.java.hw16.jvdraw.objects;

import java.awt.Graphics2D;

/**
 * Interface models general geometrical objects.
 * 
 * @author matija
 *
 */
public interface GeometricalObject {

    /**
     * Draws object on canvas.
     * 
     * @param canvas
     *            graphics object
     */
    public void draw(Graphics2D canvas);

    /**
     * Generates string representation of graphical object. Used to store image
     * data as vectors.
     * 
     * @return string representation of object
     */
    public String generateStringRepresentation();
}
