package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * JVDraw application serves as simple paint application. It allows us to create
 * vector drawings. Supported shapes are lines, circles and filled circles.
 * 
 * @author Matija Bartolac
 *
 */
// Nije mi neka isprika ako kazem da znam da sam ovo lose napisao, ali nisam
// stigao bolje napraviti. Nadam se da sam barem osnovne funkcionalnosti pokrio.
// Malo sam precjenio vrijeme kada sam ostavi ovaj zadatak za cetvrtak :)
public class JVDraw extends JFrame {
    /**
     * Default serialization ID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Drawing canvas.
     */
    private JDrawingCanvas canvas;
    /**
     * Collection of all painted objects.
     */
    private DrawingObjectListModel collection;

    /**
     * Default constructor.
     */
    public JVDraw() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(50, 50);
        setSize(1000, 600);
        setTitle("JVDraw");

        initGUI();
    }

    /**
     * Initializes GUI elements.
     */
    public void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        createMenu();
        createToolbar();
        initActions();
    }

    /**
     * Creates status bar that show information about selected colors.
     * 
     * @param foregroundColor
     *            foreground color area
     * @param backgroundColor
     *            background color area
     */
    private void createStatusBar(JColorArea foregroundColor,
            JColorArea backgroundColor) {
        JLabel foregroundLabel = new JLabel(
                String.format("Foreground color: R-%d, G-%d, B-%d",
                        foregroundColor.getCurrentColor().getRed(),
                        foregroundColor.getCurrentColor().getGreen(),
                        foregroundColor.getCurrentColor().getBlue()));
        JLabel backgroundLabel = new JLabel(
                String.format("Background color: R-%d, G-%d, B-%d",
                        backgroundColor.getCurrentColor().getRed(),
                        backgroundColor.getCurrentColor().getGreen(),
                        backgroundColor.getCurrentColor().getBlue()));

        JPanel labelPanel = new JPanel(new GridLayout(1, 2));
        labelPanel.add(foregroundLabel);
        labelPanel.add(backgroundLabel);

        foregroundColor.addColorChangeListener(new ColorChangeListener() {
            @Override
            public void newColorSelected(IColorProvider source, Color oldColor,
                    Color newColor) {
                foregroundLabel.setText(String.format(
                        "Foreground color: R-%d, G-%d, B-%d", newColor.getRed(),
                        newColor.getGreen(), newColor.getBlue()));
            }
        });

        backgroundColor.addColorChangeListener(new ColorChangeListener() {
            @Override
            public void newColorSelected(IColorProvider source, Color oldColor,
                    Color newColor) {
                backgroundLabel.setText(String.format(
                        "Background color: R-%d, G-%d, B-%d", newColor.getRed(),
                        newColor.getGreen(), newColor.getBlue()));
            }
        });

        getContentPane().add(labelPanel, BorderLayout.PAGE_END);
    }

    /**
     * Creates toolbar.
     */
    private void createToolbar() {
        JToolBar toolBar = new JToolBar("Tools");
        JColorArea foregroundColor = new JColorArea(Color.RED);
        JColorArea backgroundColor = new JColorArea(Color.YELLOW);

        toolBar.add(foregroundColor);
        toolBar.addSeparator();
        toolBar.add(backgroundColor);
        toolBar.addSeparator();

        ButtonGroup bg = new ButtonGroup();
        JToggleButton tg1 = new JToggleButton("Line");
        JToggleButton tg2 = new JToggleButton("Circle");
        JToggleButton tg3 = new JToggleButton("Filled circle");

        bg.add(tg1);
        bg.add(tg2);
        bg.add(tg3);

        toolBar.add(tg1);
        toolBar.add(tg2);
        toolBar.add(tg3);

        createStatusBar(foregroundColor, backgroundColor);

        canvas = new JDrawingCanvas(foregroundColor, backgroundColor);

        collection = new DrawingObjectListModel(canvas);
        JList<GeometricalObject> list = new JList<>(collection);

        ChangeListener cl = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                collection.setShape(((JToggleButton) e.getSource()).getText()
                        .toUpperCase());
            }
        };

        tg1.addChangeListener(cl);
        tg2.addChangeListener(cl);
        tg3.addChangeListener(cl);

        list.addKeyListener(new KeyAdapter() {
            @SuppressWarnings("unchecked")
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    GeometricalObject g = ((JList<GeometricalObject>) e
                            .getComponent()).getSelectedValue();

                    canvas.removeDrawable(g);

                    ListDataEvent event1 = new ListDataEvent(this,
                            ListDataEvent.INTERVAL_ADDED,
                            canvas.getAllObjects().size(),
                            canvas.getAllObjects().size());
                    for (ListDataListener l : collection.getListListeneres()) {
                        l.intervalAdded(event1);
                    }

                    canvas.notifyListeners();
                }
            }
        });

        list.addMouseListener(new MouseAdapter() {
            @SuppressWarnings("unchecked")
            @Override
            public void mousePressed(MouseEvent e) {
                GeometricalObject g = ((JList<GeometricalObject>) e
                        .getComponent()).getSelectedValue();

                JFrame pop = new JFrame();
                pop.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

                JPanel popup;

                JLabel label1;
                JTextField area1;
                JLabel label2;
                JTextField area2;
                JLabel label3;
                JTextField area3;
                JLabel label4;
                JTextField area4;

                if (g instanceof FCircle || g instanceof Circle) {
                    popup = new JPanel(new GridLayout(5, 2));

                    label1 = new JLabel("centerX");
                    area1 = new JTextField("");
                    label2 = new JLabel("centerY");
                    area2 = new JTextField("");
                    label3 = new JLabel("Radius");
                    area3 = new JTextField("");
                    label4 = null;
                    area4 = null;

                    popup.add(label1);
                    popup.add(area1);

                    popup.add(label2);
                    popup.add(area2);

                    popup.add(label3);
                    popup.add(area3);
                } else {
                    popup = new JPanel(new GridLayout(6, 2));

                    label1 = new JLabel("StartX");
                    area1 = new JTextField("");
                    label2 = new JLabel("StartY");
                    area2 = new JTextField("");
                    label3 = new JLabel("EndX");
                    area3 = new JTextField("");
                    label4 = new JLabel("EndY");
                    area4 = new JTextField("");

                    popup.add(label1);
                    popup.add(area1);

                    popup.add(label2);
                    popup.add(area2);

                    popup.add(label3);
                    popup.add(area3);

                    popup.add(label4);
                    popup.add(area4);
                }

                pop.add(popup);

                JPanel foregroundColor = new JPanel(new GridLayout(1, 2));
                JPanel backgroundColor = new JPanel(new GridLayout(1, 2));

                JLabel foreground = new JLabel("Foreground color:");
                JColorArea foregroundColorValue;

                JLabel background = new JLabel("Background color:");
                JColorArea backgroundColorValue;

                JButton btnOk = new JButton("OK");
                JButton btnCancel = new JButton("Cancel");

                popup.add(foregroundColor);
                popup.add(backgroundColor);

                foregroundColor.add(foreground);

                popup.add(btnOk);
                popup.add(btnCancel);

                if (g instanceof Circle) {
                    area1.setText(String.valueOf(((Circle) g).getCenterX()));
                    area2.setText(String.valueOf(((Circle) g).getCenterY()));
                    area3.setText(String.valueOf(((Circle) g).getRadius()));
                    foregroundColorValue = new JColorArea(
                            ((Circle) g).getBorderColor());
                    foregroundColor.add(foregroundColorValue);
                    backgroundColorValue = null;
                } else if (g instanceof FCircle) {
                    area1.setText(String.valueOf(((FCircle) g).getCenterX()));
                    area2.setText(String.valueOf(((FCircle) g).getCenterY()));
                    area3.setText(String.valueOf(((FCircle) g).getRadius()));
                    foregroundColorValue = new JColorArea(
                            ((FCircle) g).getBorderColor());
                    backgroundColorValue = new JColorArea(
                            ((FCircle) g).getFillColor());

                    foregroundColor.add(foregroundColorValue);
                    backgroundColor.add(background);
                    backgroundColor.add(backgroundColorValue);
                } else if (g instanceof Line) {
                    area1.setText(String.valueOf(((Line) g).getStartX()));
                    area2.setText(String.valueOf(((Line) g).getStartY()));
                    area3.setText(String.valueOf(((Line) g).getEndX()));
                    area4.setText(String.valueOf(((Line) g).getEndY()));
                    foregroundColorValue = new JColorArea(
                            ((Line) g).getLineColor());
                    foregroundColor.add(foregroundColorValue);
                    backgroundColorValue = null;
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid input.");
                    pop.dispose();
                    return;
                }

                btnCancel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        pop.dispose();
                    }
                });

                btnOk.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (g instanceof Line) {
                            Line curLine = (Line) g;
                            int index = canvas.getIndex(g);

                            curLine.setStartX(
                                    Integer.parseInt(area1.getText()));
                            curLine.setStartY(
                                    Integer.parseInt(area2.getText()));
                            curLine.setEndX(Integer.parseInt(area3.getText()));
                            curLine.setEndY(Integer.parseInt(area4.getText()));
                            curLine.setLineColor(
                                    foregroundColorValue.getCurrentColor());
                            canvas.updateObject(curLine, index);
                        } else if (g instanceof Circle) {
                            Circle curCircle = (Circle) g;
                            int index = canvas.getIndex(g);

                            curCircle.setCenterX(
                                    Integer.parseInt(area1.getText()));
                            curCircle.setCenterY(
                                    Integer.parseInt(area2.getText()));
                            curCircle.setRadius(
                                    Integer.parseInt(area3.getText()));
                            curCircle.setBorderColor(
                                    foregroundColorValue.getCurrentColor());
                            canvas.updateObject(curCircle, index);
                        } else if (g instanceof FCircle) {
                            FCircle curCircle = (FCircle) g;
                            int index = canvas.getIndex(g);

                            curCircle.setCenterX(
                                    Integer.parseInt(area1.getText()));
                            curCircle.setCenterY(
                                    Integer.parseInt(area2.getText()));
                            curCircle.setRadius(
                                    Integer.parseInt(area3.getText()));
                            curCircle.setBorderColor(
                                    foregroundColorValue.getCurrentColor());
                            curCircle.setFillColor(
                                    backgroundColorValue.getCurrentColor());
                            canvas.updateObject(curCircle, index);
                        }

                        ListDataEvent event1 = new ListDataEvent(this,
                                ListDataEvent.INTERVAL_ADDED,
                                canvas.getAllObjects().size(),
                                canvas.getAllObjects().size());
                        for (ListDataListener l : collection
                                .getListListeneres()) {
                            l.intervalAdded(event1);
                            pop.dispose();
                        }

                        canvas.notifyListeners();
                    }
                });

                pop.setSize(getPreferredSize());
                pop.setVisible(true);
            }
        });

        getContentPane().add(new JScrollPane(list), BorderLayout.EAST);
        getContentPane().add(canvas, BorderLayout.CENTER);
        getContentPane().add(toolBar, BorderLayout.PAGE_START);
    }

    /**
     * Creates menu.
     */
    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        fileMenu.add(new JMenuItem(openAction));
        fileMenu.add(new JMenuItem(saveAction));
        fileMenu.add(new JMenuItem(saveAsAction));
        fileMenu.add(new JMenuItem(exportAction));
        fileMenu.addSeparator();
        fileMenu.add(new JMenuItem(exitAction));
        setJMenuBar(menuBar);
    }

    /**
     * Initializes actions.
     */
    private void initActions() {
        openAction.putValue(Action.NAME, "Open");
        openAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control o"));
        openAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
        openAction.putValue(Action.SHORT_DESCRIPTION, "Opens drawing.");

        exportAction.putValue(Action.NAME, "Export as...");
        exportAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control E"));
        exportAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
        exportAction.putValue(Action.SHORT_DESCRIPTION,
                "Exports drawing as image.");

        exitAction.putValue(Action.NAME, "Exit");
        exitAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control w"));
        exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
        exitAction.putValue(Action.SHORT_DESCRIPTION, "Exits application.");

        saveAction.putValue(Action.NAME, "Save");
        saveAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control s"));
        saveAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
        saveAction.putValue(Action.SHORT_DESCRIPTION, "Saves current file.");

        saveAsAction.putValue(Action.NAME, "Save as");
        saveAsAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift s"));
        saveAsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
        saveAsAction.putValue(Action.SHORT_DESCRIPTION,
                "Saves current file as...");
    }

    // Actions
    // -----------------------------------------------------------------------/
    /**
     * Action models exit operation.
     */
    private final Action exitAction = new AbstractAction() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };

    /**
     * Action models open operation.
     */
    private final Action openAction = new AbstractAction() {
        /**
         * Default serial version ID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(filter);
            int status = fc.showSaveDialog(JVDraw.this);

            if (status == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (status == JFileChooser.ERROR_OPTION) {
                return;
            }

            openFile(fc.getSelectedFile().getAbsolutePath());
        }
    };

    /**
     * Helper function that is used to load file.
     * 
     * @param fileLocation
     *            path to file on disk.
     */
    private final void openFile(String fileLocation) {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get(fileLocation));
        } catch (IOException e) {
            JOptionPane.showMessageDialog(JVDraw.this,
                    "Error opening " + e.getMessage());
        }

        canvas.clearCanvas();

        for (String line : lines) {
            if (line.startsWith("LINE")) {
                String[] args = line.substring(4).trim().split("\\s");
                Line newLine = new Line(Integer.valueOf(args[0]),
                        Integer.valueOf(args[2]), Integer.valueOf(args[1]),
                        Integer.valueOf(args[3]),
                        new Color(Integer.valueOf(args[4]),
                                Integer.valueOf(args[5]), Integer
                                        .valueOf(args[6])));
                canvas.add(newLine);
            } else if (line.startsWith("CIRCLE")) {
                String[] args = line.substring(6).trim().split("\\s");
                Circle newCircle = new Circle(Integer.valueOf(args[0]),
                        Integer.valueOf(args[1]), Integer.valueOf(args[2]),
                        new Color(Integer.valueOf(args[3]),
                                Integer.valueOf(args[4]), Integer
                                        .valueOf(args[5])));
                canvas.add(newCircle);
            } else if (line.startsWith("FCIRCLE")) {
                String[] args = line.substring(7).trim().split("\\s");
                FCircle newFCircle = new FCircle(Integer.valueOf(args[0]),
                        Integer.valueOf(args[1]), Integer.valueOf(args[2]),
                        new Color(Integer.valueOf(args[3]),
                                Integer.valueOf(args[4]),
                                Integer.valueOf(args[5])),
                        new Color(Integer.valueOf(args[6]),
                                Integer.valueOf(args[7]),
                                Integer.valueOf(args[8])));
                canvas.add(newFCircle);
            }
        }

        ListDataEvent event1 = new ListDataEvent(this,
                ListDataEvent.INTERVAL_ADDED, canvas.getAllObjects().size(),
                canvas.getAllObjects().size());
        for (ListDataListener l : collection.getListListeneres()) {
            l.intervalAdded(event1);
        }

        canvas.notifyListeners();
    }

    /**
     * Action models save as action
     */
    private final Action saveAsAction = new AbstractAction() {
        /**
         * Default serial version ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(filter);
            int status = fc.showSaveDialog(JVDraw.this);

            if (status == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (status == JFileChooser.ERROR_OPTION) {
                return;
            }

            String fileName = fc.getSelectedFile().getAbsolutePath();
            if (!fileName.endsWith(".jvd")) {
                fileName += ".jvd";
            }

            saveFile(fileName);
        }
    };

    /**
     * File filter for .jvd files.
     */
    private FileNameExtensionFilter filter = new FileNameExtensionFilter(
            ".jvd Java vector drawing", "jvd");

    /**
     * Action models save operation.
     */
    private final Action saveAction = new AbstractAction() {
        /**
         * Default serial version ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            saveFile(new File("untitled.jvd").getAbsolutePath());
        }
    };

    /**
     * Helper method that save file to disk.
     * 
     * @param fc
     *            file location
     */
    private void saveFile(String fc) {
        File f = new File(fc);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(JVDraw.this,
                        "Error writing to " + e1.getMessage());
                return;
            }
        }

        try (OutputStream os = new FileOutputStream(f);) {
            for (GeometricalObject g : canvas.getAllObjects()) {
                os.write(g.generateStringRepresentation()
                        .getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(JVDraw.this,
                    "Error writing to " + ex.getMessage());
            return;
        }

        JOptionPane.showMessageDialog(JVDraw.this, "Succesfully saved.");
    }

    /**
     * Action models export as image operation
     */
    private final Action exportAction = new AbstractAction() {
        /**
         * Default serial version ID.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            FileNameExtensionFilter imageFilter = new FileNameExtensionFilter(
                    "Image Files", "jpg", "png", "gif", "jpeg");
            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(imageFilter);

            int status = fc.showSaveDialog(JVDraw.this);

            if (status == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (status == JFileChooser.ERROR_OPTION) {
                return;
            }

            String fileName = fc.getSelectedFile().getAbsolutePath();

            Rectangle bounds = canvas.getBounds();

            BufferedImage image = new BufferedImage((int) bounds.getWidth(),
                    (int) bounds.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
            Graphics2D g = image.createGraphics();
            for (GeometricalObject obj : canvas.getAllObjects()) {
                obj.draw(g);
            }
            g.dispose();

            try {
                File file = new File(fileName);
                if (!file.exists()) {
                    file.createNewFile();
                }

                ImageIO.write(image,
                        fc.getSelectedFile().getName().split("\\.")[1], file);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    };

    /**
     * Starting point of our program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new JVDraw().setVisible(true);
        });
    }
}
