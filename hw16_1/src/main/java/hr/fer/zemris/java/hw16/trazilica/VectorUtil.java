package hr.fer.zemris.java.hw16.trazilica;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Class contains utilty functions that allows us to work with n-dimensional
 * word vectors.
 * 
 * @author Matija Bartolac
 *
 */
public class VectorUtil {
    /**
     * Creates IDF vector for given vocabulary.
     * 
     * @param db
     *            database
     * @return IDF vector for given vocabulary.
     */
    public static double[] createIdfVector(TextDatabase db) {
        double[] vector = new double[db.getVocabularySize()];

        int numOfArticles = db.getNumOfArticles();
        List<Article> articles = db.getArticles();
        Map<String, Integer> vocabulary = db.getVocabulary();

        for (Entry<String, Integer> word : vocabulary.entrySet()) {
            double curIdf = idf(numOfArticles, articles, word.getKey());
            vector[word.getValue()] = curIdf;
        }

        return vector;
    }

    /**
     * Calculate TF-IDF vector for input vocabulary and given article.
     * 
     * @param vocabulary
     *            working vocabulary
     * @param idfVector
     *            IDF vector for working database
     * @param article
     *            under test
     * @return TF-IDF vector
     */
    public static double[] createTfIdfVector(Map<String, Integer> vocabulary,
            double[] idfVector, List<String> article) {
        double[] tfIdfVector = new double[idfVector.length];

        for (Entry<String, Integer> word : vocabulary.entrySet()) {
            double tf = tf(article, word.getKey());

            double tfIdf = tf * idfVector[word.getValue()];

            tfIdfVector[word.getValue()] = tfIdf;
        }

        return tfIdfVector;
    }

    /**
     * Calculates TF value for given article.
     * 
     * @param article
     *            under test
     * @param testWord
     *            word that we are testing
     * @return TF value
     */
    private static double tf(List<String> article, String testWord) {
        double count = 0;

        for (String word : article) {
            if (word.equalsIgnoreCase(testWord)) {
                count++;
            }
        }

        return count / article.size();
    }

    /**
     * Calculates IDF value for given word.
     * 
     * @param numOfDocuments
     *            number of documents in database
     * @param articles
     *            list of all articles
     * @param testWord
     *            word that we are testing
     * @return IDF value for word under test.
     */
    private static double idf(int numOfDocuments, List<Article> articles,
            String testWord) {
        double count = 0;

        for (Article a : articles) {
            if (a.containsWord(testWord)) {
                count++;
            }
        }

        return Math.log(numOfDocuments / count);
    }

    /**
     * Calculates vector product of two n-dimensional vectors.
     * 
     * @param vec1
     *            first vector
     * @param vec2
     *            second vector
     * @return vector product
     */
    private static double vectorProduct(double[] vec1, double[] vec2) {
        double result = 0;

        for (int i = 0; i < vec1.length; i++) {
            result += (vec1[i] * vec2[i]);
        }

        return result;
    }

    /**
     * Calculates norm of given vector.
     * 
     * @param vec
     *            input vector
     * @return norm of input vector
     */
    private static double vectorNorm(double[] vec) {
        double result = 0;

        for (double component : vec) {
            result += (component * component);
        }

        return Math.sqrt(result);
    }

    /**
     * Calculates cosine value between two vectors.
     * 
     * @param vec1
     *            first vector
     * @param vec2
     *            second vector
     * @return cosine of angle betwen vec1 and vec2
     */
    public static double calculateCosine(double[] vec1, double[] vec2) {
        double vec2Norm = vectorNorm(vec2);

        if (vec2Norm < 10e-10) return 0.0;

        return (vectorProduct(vec1, vec2)) / (vectorNorm(vec1) * vec2Norm);
    }
}
