package hr.fer.zemris.java.hw16.trazilica;

import java.util.Collections;
import java.util.List;

/**
 * Class models single article in database.
 * 
 * @author Matija Bartolac
 *
 */
public class Article {
    /**
     * Path to article on physical storage.
     */
    private String filePath;
    /**
     * Article content(without stopwords)
     */
    private List<String> words;
    /**
     * TF-IDF vector
     */
    private double[] tfIdfVector;

    /**
     * Default constructor
     * 
     * @param filePath
     *            path to article on physical storage.
     * @param words
     *            article content
     */
    public Article(String filePath, List<String> words) {
        this.filePath = filePath;
        this.words = words;
    }

    /**
     * Sets TF-IDF vector.
     * 
     * @param tfIdfVector
     *            value.
     */
    public void setTfIdfVector(double[] tfIdfVector) {
        this.tfIdfVector = tfIdfVector;
    }

    /**
     * Gets TF-IDF vector.
     * 
     * @return the tfIdfVector
     */
    public double[] getTfIdfVector() {
        return tfIdfVector;
    }

    /**
     * Returns list of words from article.
     * 
     * @return the words
     */
    public List<String> getWords() {
        return Collections.unmodifiableList(words);
    }

    /**
     * Returns location of article on disk.
     * 
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Checks if article contains word.
     * 
     * @param word
     *            that we are searching
     * @return true if article contains word, false otherwise
     */
    public boolean containsWord(String word) {
        return words.contains(word);
    }
}
