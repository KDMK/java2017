package hr.fer.zemris.java.hw16.trazilica;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import static hr.fer.zemris.java.hw16.trazilica.ConsoleCommands.*;

/**
 * Konzola represents simple search engine for custom database. Database
 * consists of old articles published in "Vjesnik" newspaper. Program
 * communicates with user through command line. It doesn't take any comman line
 * argument. Supported operations are:
 * <ul>
 * <li>query : searches database for given argument. Arguments are any letter
 * sequences(pismo23glava is parsed as [pismo, glava], numbers are omitted)</li>
 * <li>results : displays results of last query</li>
 * <li>type : prints content of entry from result set. Single input parameter is
 * required and it represents position in result set.</li>
 * <li>exit : ends execution of program.
 * </ul>
 * <br/>
 * 
 * @author Matija Bartolac
 *
 */
public class Konzola {
    /**
     * Location of database articles.
     */
    private final static String ARTICLES_LOCATION = "src/main/resources/clanci";
    /**
     * Defines how many results will be written to screen.
     */
    private final static int RESULTS_FILTER = 10;

    /**
     * Articles database.
     */
    private static TextDatabase database;

    /**
     * Starting point of our program.
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Initializing database...");

        database = DatabaseUtil.createTextDatabase(ARTICLES_LOCATION);
        if (database == null) {
            System.out.println("Error creating database. Aborting...");
        }

        System.out.println("Database sucessfully initialized!");
        System.out.println("Vocabulary size: " + database.getVocabularySize());
        System.out.println();

        Scanner sc = new Scanner(System.in);
        TreeSet<Result> resultSet = new TreeSet<>();

        while (true) {
            System.out.print(String.format("Enter command > "));

            String input = sc.nextLine();

            if (input.trim().equals("exit")) break;

            if (input.startsWith("results")) {
                input = input.replace("results", "").trim();
                if (input.length() != 0) {
                    System.out.println(String.format(
                            "Invalid argument for results command - %s",
                            input));
                    continue;
                }

                printResults(resultSet, RESULTS_FILTER);
                continue;
            }

            if (input.startsWith("type")) {
                input = input.replace("type", "").trim();

                if (input.length() == 0) {
                    System.out
                            .println("Argument is expected for type command.");
                    continue;
                }

                int index = -1;
                try {
                    index = Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    System.out.println(input + " is not valid index!");
                    continue;
                }

                printDocument(resultSet, index);
                continue;
            }

            if (input.startsWith("query")) {
                input = input.replace("query", "").trim();
                String[] inputArgs = input
                        .split(DatabaseUtil.wordDelimiter.toString());
                List<String> queryWords = new LinkedList<String>(
                        Arrays.asList(inputArgs));

                filterQueryWords(queryWords, database.getVocabulary());

                // makni query argument
                System.out.println("Query is: " + queryWords.toString());

                double[] queryVector = VectorUtil.createTfIdfVector(
                        database.getVocabulary(), database.getIdfVector(),
                        queryWords);

                resultSet.clear();
                for (Article a : database.getArticles()) {
                    double sim = VectorUtil.calculateCosine(a.getTfIdfVector(),
                            queryVector);

                    if (sim < 10e-10 || Double.isNaN(sim)) continue;

                    resultSet.add(new Result(sim, a.getFilePath()));
                }

                printResults(resultSet, RESULTS_FILTER);
                continue;
            }

            System.out.println("Nepoznata naredba.");
        }

        sc.close();
    }

    /**
     * Class models single result that we obtain while searching.
     * 
     * @author Matija Bartolac
     *
     */
    protected static class Result implements Comparable<Result> {
        /**
         * Similarity of result with input document.
         */
        private double similarity;
        /**
         * File location on physical storage.
         */
        private String filePath;

        /**
         * Default constructor.
         * 
         * @param similarity
         *            similarity of result with input document
         * @param filePath
         *            file location on physical storage
         */
        public Result(double similarity, String filePath) {
            super();
            this.similarity = similarity;
            this.filePath = filePath;
        }

        /**
         * Gets file path on physical storage.
         * 
         * @return file path
         */
        public String getFilePath() {
            return filePath;
        }

        /**
         * Gets result similarity to searched term.
         * 
         * @return the similarity
         */
        public double getSimilarity() {
            return similarity;
        }

        @Override
        public int compareTo(Result o) {
            if (this.similarity > o.similarity) return -1;
            if (this.similarity < o.similarity) return 1;
            return 0;
        }

        @Override
        public String toString() {
            return String.format("(%.4f) %s", similarity, filePath);
        }
    }
}
