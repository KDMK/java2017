package hr.fer.zemris.java.hw16.trazilica;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * DatabaseUtil class contains static methods that are required to initialize
 * database.
 * 
 * @author Matija Bartolac
 *
 */
public class DatabaseUtil {
    /**
     * Location of stop-words.
     */
    private final static String STOP_WORDS_LOCATION = "src/main/resources/stoprijeci.txt";
    /**
     * Word delimiter used to extract relevant words from articles.
     */
    static Pattern wordDelimiter = Pattern.compile("[^a-zA-ZŠĐČĆŽšžčćđ\\-]+");

    /**
     * Stop-words set.
     */
    private static Set<String> stopWords = loadStopWords(STOP_WORDS_LOCATION);

    /**
     * Loads stop words from physical storage to internal set of elements.
     * 
     * @param filePath
     *            location of stop-words.
     * @return Set of stop words.
     */
    private static Set<String> loadStopWords(String filePath) {
        Path p = Paths.get(filePath).toAbsolutePath();

        List<String> lines = null;
        try {
            lines = Files.readAllLines(p);
        } catch (IOException e) {
            return null;
        }

        Set<String> stopwords = new HashSet<>(lines);

        for (String line : lines) {
            stopwords.add(line.replaceAll("\\.", ""));
        }

        return stopwords;
    }

    /**
     * Builds TextDatabase that is further used to perform queries.
     * 
     * @param articlesLocation
     *            location of articles
     * @return TextDatabase that is ready for performing queries.
     */
    public static TextDatabase createTextDatabase(String articlesLocation) {
        // There was an error while reading stop-words
        if (stopWords == null) {
            return null;
        }

        Path p = Paths.get(articlesLocation).toAbsolutePath();

        // user provided file, not directory - return error
        if (!p.toFile().isDirectory()) {
            return null;
        }

        TextDatabase database = new TextDatabase();
        Map<String, Integer> vocabulary = new HashMap<>();
        int index = 0;

        File[] fileList = p.toFile().listFiles();
        for (File f : fileList) {
            Scanner sc = null;
            try {
                sc = new Scanner(f, "UTF-8");
                sc.useDelimiter(wordDelimiter);

                List<String> words = new ArrayList<>();
                while (sc.hasNext()) {
                    String word = sc.next().toLowerCase().trim();

                    if (stopWords.contains(word)) continue;

                    if (!vocabulary.containsKey(word)) {
                        vocabulary.put(word, index++);
                    }

                    words.add(word);
                }

                sc.close();

                Article newArticle = new Article(f.getAbsolutePath(), words);
                database.addArticle(newArticle);
            } catch (FileNotFoundException e) {
                // There was an error while reading some of articles.
                return null;
            }
        }

        database.setVocabulary(vocabulary);
        database.setIdfVector(VectorUtil.createIdfVector(database));

        for (Article a : database.getArticles()) {
            a.setTfIdfVector(
                    VectorUtil.createTfIdfVector(database.getVocabulary(),
                            database.getIdfVector(), a.getWords()));
        }

        return database;
    }
}
