package hr.fer.zemris.java.hw16.trazilica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * TextDatabase models database that consists of newspaper Articles. Article is
 * defined {@link hr.fer.zemris.java.hw16.trazilica.Article here}.
 * 
 * @author Matija Bartolac
 *
 */
public class TextDatabase {
    /**
     * List of all articles.
     */
    private List<Article> articles;
    /**
     * Number of articles.
     */
    private int numOfArticles;
    /**
     * Vocabulary represents map of different words from all articles.
     */
    private Map<String, Integer> vocabulary;
    /**
     * Flag that indicates whether vocabulary is already set. When set, further
     * changes are not permitted.
     */
    private boolean vocabularySet;
    /**
     * Database IDF vector.
     */
    private double[] idfVector;

    /**
     * Default constructor.
     */
    public TextDatabase() {
        this.articles = new ArrayList<>();
    }

    /**
     * Constructor that is used if we already have list of Articles
     * 
     * @param articles
     *            list of articles
     */
    public TextDatabase(List<Article> articles) {
        this.articles = articles;
        this.numOfArticles = articles.size();
    }

    /**
     * Returns the articles.
     * 
     * @return the articles
     */
    public List<Article> getArticles() {
        return Collections.unmodifiableList(articles);
    }

    /**
     * Adds new article.
     * 
     * @param art
     *            new article.
     */
    public void addArticle(Article art) {
        this.articles.add(art);
        this.numOfArticles++;
    }

    /**
     * Return number of articles in database.
     * 
     * @return the numOfArticles
     */
    public int getNumOfArticles() {
        return numOfArticles;
    }

    /**
     * Sets database vocabulary. Operation can be performed only once. Further
     * changes to vocabulary are not permitted.
     * 
     * @param vocabulary
     *            built vocabulary.
     */
    public void setVocabulary(Map<String, Integer> vocabulary) {
        if (!vocabularySet) {
            this.vocabulary = vocabulary;
            vocabularySet = true;
        }
    }

    /**
     * Returns vocabulary built on this database.
     * 
     * @return the vocabulary
     */
    public Map<String, Integer> getVocabulary() {
        return Collections.unmodifiableMap(vocabulary);
    }

    /**
     * Returns number of words in vocabulary.
     * 
     * @return the vocabulary size
     */
    public int getVocabularySize() {
        return vocabulary.size();
    }

    /**
     * Sets IDF vector.
     * 
     * @param idfVector
     *            the IDF vector
     */
    public void setIdfVector(double[] idfVector) {
        this.idfVector = idfVector;
    }

    /**
     * Returns the IDF vector.
     * 
     * @return the IDF vector.
     */
    public double[] getIdfVector() {
        return idfVector;
    }
}
