package hr.fer.zemris.java.hw16.trazilica;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.zemris.java.hw16.trazilica.Konzola.Result;

/**
 * Console commands used in Konzola class.
 * 
 * @author Matija Bartolac
 *
 */
public class ConsoleCommands {

    /**
     * Prints input document to standard output.
     * 
     * @param resultSet
     *            set of queried results
     * @param position
     *            position in results set.
     */
    static void printDocument(TreeSet<Result> resultSet, int position) {
        if (resultSet.isEmpty()) {
            System.out.println("Result set is empty!");
            return;
        }

        if (position < 0 || position >= resultSet.size()) {
            System.out.println("Index is out of bounds");
            return;
        }

        Iterator<Result> i = resultSet.iterator();
        while (position > 0) {
            i.next();
            position--;
        }

        Result r = i.next();

        Path p = Paths.get(r.getFilePath());
        List<String> lines = null;
        try {
            lines = Files.readAllLines(p);
        } catch (IOException e) {
            System.out.println("Cannot open: " + e.getMessage());
            return;
        }

        System.out.println(String.format("Dokument: %s", r.getFilePath()));
        System.out.println("-------------------------------------------------");

        for (String line : lines) {
            System.out.println(line);
        }
    }

    /**
     * Prints result set to standard output.
     * 
     * @param resultSet
     *            set of queried results
     * @param numOfResults
     *            number of results that we want to show to user.
     */
    static void printResults(Set<Result> resultSet, int numOfResults) {
        if (resultSet.isEmpty()) {
            System.out.println("Result set is empty!");
            return;
        }

        int i = 0;
        System.out.println(
                String.format("Najboljih %d rezultata:", numOfResults));
        for (Result r : resultSet) {
            if (i >= numOfResults) break;
            System.out.println(String.format("[%d] ", i++) + r.toString());
        }

        System.out.println();
    }

    /**
     * Removes words from query that are not in built vocabulary.
     * 
     * @param query
     *            list of words
     * @param vocabulary
     *            working vocabulary
     */
    static void filterQueryWords(List<String> query,
            Map<String, Integer> vocabulary) {
        Iterator<String> i = query.iterator();

        while (i.hasNext()) {
            String curString = i.next();

            if (!vocabulary.containsKey(curString)) {
                i.remove();
            }
        }
    }
}
