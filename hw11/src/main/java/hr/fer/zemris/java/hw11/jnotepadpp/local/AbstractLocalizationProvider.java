package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that models Localization observer.
 * 
 * @author Matija Bartolac
 *
 */
public abstract class AbstractLocalizationProvider {
    /**
     * List of listeners.
     */
    List<ILocalizationListener> listeners;

    /**
     * Default constructor.
     */
    public AbstractLocalizationProvider() {
        this.listeners = new ArrayList<>();
    }

    /**
     * Adds new listener to list.
     * 
     * @param listener new listener
     */
    public void addLocaliztionListener(ILocalizationListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes given listener from array.
     * 
     * @param listener to remove
     */
    public void removeLocalizationListener(ILocalizationListener listener) {
        listeners.remove(listener);
    }

    /**
     * Notifies all listeners.
     */
    public void fire() {
        for(ILocalizationListener l : listeners) {
            l.localizationChanged();
        }
    }
}
