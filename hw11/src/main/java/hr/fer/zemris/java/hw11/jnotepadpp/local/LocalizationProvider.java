package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class models singleton used for multi-language support.
 * 
 * @author Matija Bartolac
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
    /**
     * Current language
     */
    private String language;
    /**
     * Resource bundle.
     */
    private ResourceBundle bundle;

    /**
     * Single instance of our object.
     */
    private static final LocalizationProvider instance = new LocalizationProvider();

    /**
     * Default constructor.
     */
    private LocalizationProvider() {
        this.language = "en";
        Locale locale = Locale.forLanguageTag(this.language);
        bundle = ResourceBundle
                .getBundle("hr.fer.zemris.java.hw11.jnotepadpp.lang", locale);
    }

    /**
     * @return this object
     */
    public static LocalizationProvider getInstance() {
        return instance;
    }

    /**
     * Gets value with given key from bundle.
     * 
     * @param key wanted parameter key
     * @return stored value
     */
    public String getString(String key) {
        return bundle.getString(key);
    }
    
    /**
     * @param language new language
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
