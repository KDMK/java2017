package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Localization provider in observer-provider model
 * 
 * @author Matija Bartolac
 *
 */
public interface ILocalizationProvider {

    /**
     * Adds new localization listener.
     * 
     * @param listener new listener
     */
    void addLocalizationListener(ILocalizationListener listener);
    /**
     * Removes localization listener.
     * 
     * @param listener old listener
     */
    void removeLocalizationListener(ILocalizationListener listener);
    /**
     * Gets value with key from bundle.
     * 
     * @param key of object
     * @return value from bundle
     */
    String getString(String key);
}
