package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * JNotepadPP is simple text editor with basic functionality covered. It allows
 * user to edit all kind of formatted files. Program also provides basic
 * statistics about input files.
 * 
 * @author Matija Bartolac
 *
 */
/**
 * @author Matija
 *
 */
public class JNotepadPP extends JFrame {
    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Tabs that holds text views.
     */
    private JTabbedPane tabs;
    /**
     * Status bar.
     */
    private JPanel statusBar;
    /**
     * List with all editors.
     */
    List<JNotepadPPTab> editors;
    /**
     * Currently active path.
     */
    private Path activePath;
    /**
     * Currently active editor.
     */
    private JTextArea activeEditor;
    /**
     * Close icon.
     */
    private ImageIcon closeIcon;

    /**
     * Indicates if file is saved or not. Indicates whether saving was succesful
     * or not.
     */
    private boolean saved;
    /**
     * Indicates if saving was aborted.
     */
    private boolean abort;
    /**
     * Stores data after loading file in memory.
     */
    private String initialData;
    /**
     * Buffer for copy/paste/cut.
     */
    private String buffer;

    /**
     * Statistic metric : number of lines
     */
    private JLabel length;
    /**
     * Statistic metric : line position
     */
    private JLabel ln;
    /**
     * Statistic metric : column position
     */
    private JLabel col;
    /**
     * Statistic metric : number of selected characters
     */
    private JLabel sel;
    /**
     * Clock
     */
    private JLabel clock;
    /**
     * Current app language.
     */
    private String language;
    /**
     * Current locale.
     */
    Locale locale;
    /**
     * String resource bundle.
     */
    ResourceBundle bundle;

    /**
     * Default constructor.
     */
    public JNotepadPP() {
        this.tabs = new JTabbedPane();
        this.editors = new ArrayList<>();
        this.closeIcon = createImageIcon("resources/close.gif", "Close icon.");
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(0, 0);
        setSize(800, 600);
        setTitle("JNotepad++");
        this.language = "en";

        // Localization
        locale = Locale.forLanguageTag(this.language);
        bundle = ResourceBundle
                .getBundle("hr.fer.zemris.java.hw11.jnotepadpp.lang", locale);        
        
        Timer t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DateTimeFormatter dtf = DateTimeFormatter
                        .ofPattern("dd/MM/yyyy HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                clock.setText(dtf.format(now));
            }
        });
        t.start();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                JNotepadPP.this.exitAction.actionPerformed(
                        new ActionEvent(this, 0, bundle.getString("exit")));
            }
        });

        initGUI();
    }

    /**
     * Initialize graphical user interface.
     */
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        cp.add(tabs, BorderLayout.CENTER);

        createActions();
        createMenus();
        createToolbars();
        createStatusBar();
    }

    /**
     * Creates status bar with text metrics and clock.
     */
    private void createStatusBar() {
        Container cp = getContentPane();
        statusBar = new JPanel();
        statusBar.setBorder(
                BorderFactory.createMatteBorder(2, 0, 0, 0, Color.DARK_GRAY));

        cp.add(statusBar, BorderLayout.SOUTH);

        statusBar.setLayout(new GridLayout(1, 3));
        length = new JLabel();
        statusBar.add(length, "1,1");

        JPanel rightSide = new JPanel(new GridLayout(1, 4));
        ln = new JLabel();
        col = new JLabel();
        sel = new JLabel();
        clock = new JLabel();
        rightSide.add(ln, "1,1");
        rightSide.add(col, "1,2");
        rightSide.add(sel, "1,3");

        statusBar.add(rightSide, "1,2");
        statusBar.add(clock, "1,3");
    }

    /**
     * Method creates all needed action listeners.
     */
    private void createActions() {
        // Listener that listens for tab changes.
        this.tabs.addChangeListener((e) -> {
            if (editors.size() > 0) {
                int index = this.tabs.getSelectedIndex();
                this.activePath = editors.get(index).getOpenedFilePath();
                this.activeEditor = editors.get(index).getEditor();
                this.initialData = editors.get(index).getInitialValue();
                addTag(tabs, activeEditor.getName(), closeIcon, index);

                String pathStr = "untitled.txt";
                if (activePath != null) {
                    pathStr = activePath.toString();
                }

                setTitle(pathStr + " - JNotepad++");
                this.tabs.setToolTipTextAt(index, pathStr);
            }
            if (editors.size() == 0) {
                setTitle("JNotepad++");
                activePath = null;
                disableKeys();
            } else {
                enableKeys();
            }
        });

        disableKeys();

        openDocumentAction.putValue(Action.NAME, bundle.getString("open"));
        openDocumentAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control O"));
        openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
        openDocumentAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to open a document from disk.");

        newDocumentAction.putValue(Action.NAME, bundle.getString("new"));
        newDocumentAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control N"));
        newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
        newDocumentAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to create new document.");

        saveDocumentAction.putValue(Action.NAME, bundle.getString("save"));
        saveDocumentAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control S"));
        saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
        saveDocumentAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to save a document to disk.");

        saveAsDocumentAction.putValue(Action.NAME, bundle.getString("saveAs"));
        saveAsDocumentAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift S"));
        saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
        saveAsDocumentAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to save a document to disk under specific file name.");

        cutSelectedPartAction.putValue(Action.NAME, bundle.getString("cut"));
        cutSelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control F2"));
        cutSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
        cutSelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to cut selected text.");

        copySelectedPartAction.putValue(Action.NAME, bundle.getString("copy"));
        copySelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control F3"));
        copySelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
        copySelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to copy selected text.");

        pasteSelectedPartAction.putValue(Action.NAME,
                bundle.getString("paste"));
        pasteSelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control F4"));
        pasteSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
        pasteSelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Used to paste selected text.");

        getStatisticsAction.putValue(Action.NAME, bundle.getString("stat"));
        getStatisticsAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control F1"));
        getStatisticsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
        getStatisticsAction.putValue(Action.SHORT_DESCRIPTION,
                "Gets basic text statistics.");

        invertSelectedPartAction.putValue(Action.NAME, bundle.getString("inv"));
        invertSelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control I"));
        invertSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);
        invertSelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Toggles invert case of selected text.");

        uppercaseSelectedPartAction.putValue(Action.NAME,
                bundle.getString("upr"));
        uppercaseSelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control U"));
        uppercaseSelectedPartAction.putValue(Action.MNEMONIC_KEY,
                KeyEvent.VK_U);
        uppercaseSelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Toggles case of selected text to uppercase.");

        lowercaseSelectedPartAction.putValue(Action.NAME,
                bundle.getString("lwr"));
        lowercaseSelectedPartAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control L"));
        lowercaseSelectedPartAction.putValue(Action.MNEMONIC_KEY,
                KeyEvent.VK_L);
        lowercaseSelectedPartAction.putValue(Action.SHORT_DESCRIPTION,
                "Toggles case of selected text to lowercase.");

        exitAction.putValue(Action.NAME, bundle.getString("exit"));
        exitAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control Q"));
        exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);
        exitAction.putValue(Action.SHORT_DESCRIPTION, "Exit program.");
    }

    /**
     * Method creates all needed menu entries.
     */
    private void createMenus() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu(bundle.getString("file"));
        menuBar.add(fileMenu);

        fileMenu.add(new JMenuItem(newDocumentAction));
        fileMenu.add(new JMenuItem(openDocumentAction));
        fileMenu.add(new JMenuItem(saveDocumentAction));
        fileMenu.add(new JMenuItem(saveAsDocumentAction));
        fileMenu.addSeparator();
        fileMenu.add(new JMenuItem(exitAction));

        JMenu editMenu = new JMenu(bundle.getString("edit"));
        menuBar.add(editMenu);
        editMenu.add(new JMenuItem(cutSelectedPartAction));
        editMenu.add(new JMenuItem(copySelectedPartAction));
        editMenu.add(new JMenuItem(pasteSelectedPartAction));

        JMenu viewMenu = new JMenu(bundle.getString("view"));
        menuBar.add(viewMenu);
        viewMenu.add(new JMenuItem(getStatisticsAction));

        JMenu toolsMenu = new JMenu(bundle.getString("tools"));
        JMenu caseMenu = new JMenu(bundle.getString("ccse"));
        toolsMenu.add(caseMenu);
        menuBar.add(toolsMenu);

        caseMenu.add(new JMenuItem(uppercaseSelectedPartAction));
        caseMenu.add(new JMenuItem(lowercaseSelectedPartAction));
        caseMenu.add(new JMenuItem(invertSelectedPartAction));

        JMenu languageMenu = new JMenu(bundle.getString("language"));
        menuBar.add(languageMenu);

        JMenuItem cro = new JMenuItem(bundle.getString("cro"));
        JMenuItem eng = new JMenuItem(bundle.getString("eng"));
        JMenuItem de = new JMenuItem(bundle.getString("de"));
        
        // Add names
        cro.setName("hr");
        eng.setName("en");
        de.setName("de");

        // Event listeners
        cro.addMouseListener(changeLanguageAction);
        eng.addMouseListener(changeLanguageAction);
        de.addMouseListener(changeLanguageAction);
        
        languageMenu.add(cro);
        languageMenu.add(eng);
        languageMenu.add(de);

        setJMenuBar(menuBar);
    }

    /**
     * Method creates all needed toolbars.
     */
    private void createToolbars() {
        JToolBar toolBar = new JToolBar(bundle.getString("tools"));

        toolBar.add(new JButton(newDocumentAction));
        toolBar.add(new JButton(openDocumentAction));
        toolBar.add(new JButton(saveDocumentAction));
        toolBar.add(new JButton(saveAsDocumentAction));
        toolBar.addSeparator();
        toolBar.add(new JButton(cutSelectedPartAction));
        toolBar.add(new JButton(copySelectedPartAction));
        toolBar.add(new JButton(pasteSelectedPartAction));

        getContentPane().add(toolBar, BorderLayout.PAGE_START);
    }

    // =========================================================================
    // ============================= ACTIONS ===================================
    // =========================================================================
    /**
     * Defines action that will create new document when called. Method does not
     * write anything to storage until save action is called.
     */
    private final Action newDocumentAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            JTextArea newTextPane = new JTextArea();
            Path p = null;
            newTextPane.setName("untitled");
            setTitle("untitled.txt" + " - JNotepad++");
            JNotepadPP.this.editors.add(new JNotepadPPTab(newTextPane, p));
            JNotepadPP.this.tabs.addTab("untitled",
                    new JScrollPane(newTextPane));
            JNotepadPP.this.initialData = newTextPane.getText();
            tabs.setSelectedIndex(editors.size() - 1);

            JNotepadPP.this.activeEditor.addCaretListener(carListener);
            ln.setText("ln: 1");
            col.setText("col: 0");
            sel.setText("sel: 0");
            length.setText("length: 0");
        }
    };

    /**
     * Defines action that will open existing document when called. If file
     * cannot be opened(error of any kind occurred) user is notified in form of
     * alert dialog.
     */
    private final Action openDocumentAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Open file...");
            if (fc.showOpenDialog(
                    JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
                return;
            }

            File fileName = fc.getSelectedFile();
            Path filePath = fileName.toPath();

            if (!Files.isReadable(filePath)) {
                JOptionPane.showConfirmDialog(JNotepadPP.this,
                        "Cannot open file " + filePath + "!", "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            byte[] data = null;
            try {
                data = Files.readAllBytes(filePath);
            } catch (IOException ex) {
                JOptionPane.showConfirmDialog(JNotepadPP.this,
                        "Error reading file " + filePath + "!", "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            // Update model variables
            String text = new String(data, StandardCharsets.UTF_8);

            JTextArea tp = new JTextArea();
            tp.setText(text);
            tp.setName(filePath.getFileName().toString());

            // Update visuals
            editors.add(new JNotepadPPTab(tp, filePath));
            tabs.addTab(tp.getName(), tp);
            activeEditor = tp;
            activePath = filePath;
            initialData = tp.getText();
            setTitle(activePath.toString() + " - JNotepad++");
            tabs.setSelectedIndex(editors.size() - 1);

            // Add caret listener
            JNotepadPP.this.activeEditor.addCaretListener(carListener);
        }
    };

    /**
     * Defines action that will close document. User input is required. User can
     * cancel action and in that case closing will be aborted.
     */
    private final Action closeDocumentAction = new AbstractAction() {
        /**
         * Default serial version UID
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            abort = false;
            if (initialData.equals(activeEditor.getText())) {
                closeTab();
                return;
            }
            int choice = JOptionPane.showConfirmDialog(JNotepadPP.this,
                    "Do you want to save changes made to document?",
                    "Are you sure...", JOptionPane.YES_NO_CANCEL_OPTION);
            switch (choice) {
            case JOptionPane.CANCEL_OPTION:
                abort = true;
                return;
            case JOptionPane.YES_OPTION:
                saveDocumentAction.actionPerformed(e);
                if (!saved) {
                    return;
                }
            case JOptionPane.NO_OPTION:
                closeTab();
                return;
            }
        }
    };

    /**
     * Defines save action. If user already loaded some file function overwrites
     * old data. If user created new file he is prompted for save location.
     */
    private final Action saveDocumentAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (editors.size() == 0) {
                JOptionPane.showMessageDialog(JNotepadPP.this,
                        "Nothing to save!", "Info",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            if (initialData.equals(activeEditor.getText())) {
                return;
            }

            if (activePath == null) {
                activePath = chooseFile();
                if (activePath == null) {
                    abort = true;
                    return;
                }
            }

            saveFile();
        }
    };

    /**
     * Defines save as action. User is always asked where to save(no matter if
     * anything is loaded). If destination file already exists user is prompted
     * to overwrite it.
     */
    private final Action saveAsDocumentAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (editors.size() == 0) {
                JOptionPane.showMessageDialog(JNotepadPP.this,
                        "Nothing to save!", "Info",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            Path oldPath = activePath;
            activePath = chooseFile();
            if (activePath == null) {
                activePath = oldPath;
                return;
            }

            if (activePath.toFile().exists()) {
                int option = JOptionPane.showConfirmDialog(JNotepadPP.this,
                        "Do you want to overwrite file?", "Are you sure...",
                        JOptionPane.YES_NO_OPTION);
                if (option == 0) return;
            }

            saveFile();
            int index = JNotepadPP.this.tabs.getSelectedIndex();
            activeEditor.setName(activePath.getFileName().toString());
            addTag(tabs, activeEditor.getName(), closeIcon, index);
        }
    };

    /**
     * Defines cut action. Cut value is stored in internal buffer.
     */
    private final Action cutSelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (activeEditor == null) return;
            Document doc = activeEditor.getDocument();

            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            if (len == 0) return;

            int offset = Math.min(activeEditor.getCaret().getDot(),
                    activeEditor.getCaret().getMark());

            try {
                buffer = doc.getText(offset, len);
                doc.remove(offset, len);
            } catch (BadLocationException igonrable) {
            }
        }
    };

    /**
     * Defines copy action. Copy value is stored in internal buffer.
     */
    private final Action copySelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (activeEditor == null) return;
            Document doc = activeEditor.getDocument();

            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            if (len == 0) return;

            int offset = Math.min(activeEditor.getCaret().getDot(),
                    activeEditor.getCaret().getMark());

            try {
                buffer = doc.getText(offset, len);
            } catch (BadLocationException igonrable) {
            }
        }
    };

    /**
     * Defines paste action. Paste value is fetched from buffer.
     */
    private final Action pasteSelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (activeEditor == null) return;
            Document doc = activeEditor.getDocument();

            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());

            int offset = Math.min(activeEditor.getCaret().getDot(),
                    activeEditor.getCaret().getMark());

            try {
                if (len != 0) doc.remove(offset, len);
                doc.insertString(offset, buffer, null);
            } catch (BadLocationException igonrable) {
            }
        }
    };

    /**
     * Defines action that calculates basic file statistics. That includes:
     * <ul>
     * <li>Total number of characters</li>
     * <li>Total number of non-whitespace characters</li>
     * <li>Total number of lines</li>
     * <li>Word count</li>
     * <li>------------------------------------------------</li>
     * <li>Full filepath</li>
     * <li>Size of file</li>
     * <li>Creation date</li>
     * <li>Modification date</li>
     * <li>Last access date</li>
     * </ul>
     */
    private final Action getStatisticsAction = new AbstractAction() {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (editors.size() == 0) return;

            String fileData = "";
            if (activePath != null) {
                fileData = getFileData();
            }

            String textData = getTextData();

            JOptionPane.showMessageDialog(JNotepadPP.this,
                    textData + System.lineSeparator() + System.lineSeparator()
                            + fileData,
                    "Statistics", JOptionPane.INFORMATION_MESSAGE);
        }

        private String getTextData() {
            StringBuilder sb = new StringBuilder();

            int totalChars = activeEditor.getText().toCharArray().length;
            int totalNonBlank = activeEditor.getText().replaceAll("\\s", "")
                    .toCharArray().length;
            int totalLines = countLines(activeEditor.getText());
            int totalWords = countWords(activeEditor.getText());

            sb.append(String.format("Total characters: %d%n", totalChars));
            sb.append(String.format("Total non-blank characters: %d%n",
                    totalNonBlank));
            sb.append(String.format("Total lines: %d%n", totalLines));
            sb.append(String.format("Total words: %d%n", totalWords));

            return sb.toString();
        }

        private int countWords(String str) {
            String trim = str.trim();
            if (trim.isEmpty()) return 0;
            return trim.split("\\w+").length;
        }

        private int countLines(String str) {
            if (str == null || str.isEmpty()) {
                return 0;
            }
            int lines = 1;
            int pos = 0;
            while ((pos = str.indexOf(System.lineSeparator(), pos) + 1) != 0) {
                lines++;
            }
            return lines;
        }

        private String getFileData() {
            StringBuilder sb = new StringBuilder();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            BasicFileAttributeView faView = Files.getFileAttributeView(
                    activePath, BasicFileAttributeView.class,
                    LinkOption.NOFOLLOW_LINKS);
            BasicFileAttributes attributes;
            try {
                attributes = faView.readAttributes();
            } catch (IOException e) {
                return "";
            }

            FileTime fileTimeCreated = attributes.creationTime();
            FileTime filetimeLastModified = attributes.lastModifiedTime();
            FileTime filetimeLastAccessed = attributes.lastAccessTime();

            String fullPath = activePath.toString();
            String created = sdf.format(new Date(fileTimeCreated.toMillis()));
            String lastModified = sdf
                    .format(new Date(filetimeLastModified.toMillis()));
            String lastAccessed = sdf
                    .format(new Date(filetimeLastAccessed.toMillis()));
            long size = attributes.size();

            sb.append(String.format("%s%n", fullPath));
            sb.append(String.format("Created on: %s%n", created));
            sb.append(String.format("Last modified on: %s%n", lastModified));
            sb.append(String.format("Last accessed on : %s%n", lastAccessed));
            sb.append(String.format("File size: %d bytes", size));

            return sb.toString();
        }

    };

    /**
     * Defines exit action. Exit can be aborted. Prompts user to save everything
     * before exiting.
     */
    private final Action exitAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            int i = editors.size() - 1;
            while (i >= 0) {
                closeDocumentAction
                        .actionPerformed(new ActionEvent(this, i--, "Close"));
                if (abort) {
                    abort = false;
                    return;
                }
            }

            System.exit(0);
        }
    };

    /**
     * Action for toggling current text invert case.
     */
    private final Action invertSelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            Document doc = activeEditor.getDocument();

            int offset = 0;
            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            if (len == 0) {
                return;

            } else {
                offset = Math.min(activeEditor.getCaret().getDot(),
                        activeEditor.getCaret().getMark());
            }

            try {
                String text = doc.getText(offset, len);
                text = invertText(text);
                doc.remove(offset, len);
                doc.insertString(offset, text, null);
            } catch (BadLocationException e1) {
            }
        }

        private String invertText(String text) {
            StringBuilder sb = new StringBuilder(text.length());
            for (char c : text.toCharArray()) {
                if (Character.isUpperCase(c)) {
                    sb.append(Character.toLowerCase(c));
                } else if (Character.isLowerCase(c)) {
                    sb.append(Character.toUpperCase(c));
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }
    };

    /**
     * Action for toggling current text invert case.
     */
    private final Action uppercaseSelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            Document doc = activeEditor.getDocument();

            int offset = 0;
            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            if (len == 0) {
                return;

            } else {
                offset = Math.min(activeEditor.getCaret().getDot(),
                        activeEditor.getCaret().getMark());
            }

            try {
                String text = doc.getText(offset, len);
                text = invertText(text);
                doc.remove(offset, len);
                doc.insertString(offset, text, null);
            } catch (BadLocationException e1) {
            }
        }

        private String invertText(String text) {
            StringBuilder sb = new StringBuilder(text.length());
            for (char c : text.toCharArray()) {
                sb.append(Character.toUpperCase(c));
            }
            return sb.toString();
        }
    };

    /**
     * Action for toggling current text invert case.
     */
    private final Action lowercaseSelectedPartAction = new AbstractAction() {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {
            Document doc = activeEditor.getDocument();

            int offset = 0;
            int len = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            if (len == 0) {
                return;

            } else {
                offset = Math.min(activeEditor.getCaret().getDot(),
                        activeEditor.getCaret().getMark());
            }

            try {
                String text = doc.getText(offset, len);
                text = lowercaseText(text);
                doc.remove(offset, len);
                doc.insertString(offset, text, null);
            } catch (BadLocationException e1) {
            }
        }

        private String lowercaseText(String text) {
            StringBuilder sb = new StringBuilder(text.length());
            for (char c : text.toCharArray()) {
                sb.append(Character.toLowerCase(c));
            }
            return sb.toString();
        }
    };

    /**
     * Carret listener used for status bar
     */
    private final CaretListener carListener = new CaretListener() {
        @Override
        public void caretUpdate(CaretEvent e) {
            int chars = activeEditor.getText().toCharArray().length;

            length.setText("length: " + chars);

            int sel = Math.abs(activeEditor.getCaret().getDot()
                    - activeEditor.getCaret().getMark());
            JNotepadPP.this.sel.setText("sel: " + sel);

            JTextArea editArea = (JTextArea) activeEditor;
            int line = 1;
            int column = 1;
            try {
                int caretpos = editArea.getCaretPosition();
                line = editArea.getLineOfOffset(caretpos);

                column = caretpos - editArea.getLineStartOffset(line);
                line += 1;
            } catch (Exception ex) {
                System.out.println("Error!");
            }

            ln.setText("ln: " + line);
            col.setText("col: " + column);
            if (sel == 0) {
                JNotepadPP.this.invertSelectedPartAction.setEnabled(false);
                JNotepadPP.this.uppercaseSelectedPartAction.setEnabled(false);
                JNotepadPP.this.lowercaseSelectedPartAction.setEnabled(false);
            } else {
                JNotepadPP.this.invertSelectedPartAction.setEnabled(true);
                JNotepadPP.this.uppercaseSelectedPartAction.setEnabled(true);
                JNotepadPP.this.lowercaseSelectedPartAction.setEnabled(true);
            }
        }
    };

    /**
     * Changle language adapter 
     */
    private final MouseListener changeLanguageAction = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
            language = e.getComponent().getName();
            // TODO: Do Later
        }
    };

    // =========================================================================
    // +++++++++++++++++++++++++++ ACTIONS END +++++++++++++++++++++++++++++++++
    // =========================================================================
    // +++++++++
    // HELPERS++
    // +++++++++
    /**
     * Adds tab tag.
     * 
     * @param tab
     *            container
     * @param title
     *            tab title
     * @param icon
     *            tab icon
     * @param index
     *            tab index
     */
    public void addTag(JTabbedPane tab, String title, Icon icon, int index) {
        MouseListener close = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                JNotepadPP.this.closeDocumentAction
                        .actionPerformed(new ActionEvent(this, 0, "Close Me"));
            }

        };
        final ButtonClose buttonClose = new ButtonClose(title, icon, close);

        tab.setTabComponentAt(index, buttonClose);
        tab.validate();
        tab.setSelectedIndex(index);
    }

    /**
     * Creates new icon from path.
     * 
     * @param path
     *            of icon
     * @param description
     *            icon description
     * @return ImageIcon object or null if there was an error.
     */
    protected ImageIcon createImageIcon(String path, String description) {
        Path imgURL = Paths.get(path).toAbsolutePath();
        if (imgURL != null) {
            return new ImageIcon(imgURL.toString(), description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * Disables text manipulation buttons if there is nothing loaded in window.
     */
    private void disableKeys() {
        saveAsDocumentAction.setEnabled(false);
        saveDocumentAction.setEnabled(false);
        copySelectedPartAction.setEnabled(false);
        cutSelectedPartAction.setEnabled(false);
        pasteSelectedPartAction.setEnabled(false);
        getStatisticsAction.setEnabled(false);
        invertSelectedPartAction.setEnabled(false);
        uppercaseSelectedPartAction.setEnabled(false);
        lowercaseSelectedPartAction.setEnabled(false);
    }

    /**
     * Enables text manipulation buttons when there is loaded file.
     */
    private void enableKeys() {
        saveAsDocumentAction.setEnabled(true);
        saveDocumentAction.setEnabled(true);
        copySelectedPartAction.setEnabled(true);
        cutSelectedPartAction.setEnabled(true);
        pasteSelectedPartAction.setEnabled(true);
        getStatisticsAction.setEnabled(true);
    }

    /**
     * Returns index of currently active editor in all opened editor list.
     * 
     * @param editors
     *            list of opened editors.
     * @param activeEditor2
     *            currently active editor.
     * @return index, or -1 if not found
     */
    private int findIndex(List<JNotepadPPTab> editors,
            JTextArea activeEditor2) {
        for (int i = 0, len = editors.size(); i < len; i++) {
            if (editors.get(i).getEditor().equals(activeEditor2)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Closes current editor tab.
     */
    private void closeTab() {
        saved = false;
        int index = findIndex(editors, activeEditor);
        editors.remove(index);
        ln.setText("");
        col.setText("");
        sel.setText("");
        length.setText("");
        tabs.remove(index);
    }

    /**
     * Method is used for getting file from user. User is prompted for file with
     * pop-up dialog. If nothing was selected(or aborted) function returns null.
     * 
     * @return path to selected file or null if nothing was selected
     */
    private Path chooseFile() {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Save file...");
        if (fc.showSaveDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
            JOptionPane.showMessageDialog(JNotepadPP.this, "Nothing saved.",
                    "Info", JOptionPane.INFORMATION_MESSAGE);
            JNotepadPP.this.saved = false;
            return null;
        }
        return fc.getSelectedFile().toPath();
    }

    /**
     * Saves file that is currently open in editor.
     */
    private void saveFile() {
        try {
            Files.write(activePath,
                    activeEditor.getText().getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(JNotepadPP.this,
                    "Saving failed. File on disk may be corrupted!", "Error",
                    JOptionPane.ERROR_MESSAGE);
            JNotepadPP.this.saved = false;
            return;
        }

        JNotepadPP.this.saved = true;
        JOptionPane.showMessageDialog(JNotepadPP.this, "File saved.", "Info",
                JOptionPane.INFORMATION_MESSAGE);
        return;
    }

    /**
     * Starting point of program.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new JNotepadPP().setVisible(true);
        });
    }
}
