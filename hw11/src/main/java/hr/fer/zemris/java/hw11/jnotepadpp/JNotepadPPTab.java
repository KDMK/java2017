package hr.fer.zemris.java.hw11.jnotepadpp;

import java.nio.file.Path;

import javax.swing.JComponent;
import javax.swing.JTextArea;

/**
 * Components models single editor tab. Every editor is defined with its text
 * pane and path.
 * 
 * @author Matija Bartolac
 *
 */
public class JNotepadPPTab extends JComponent {
    /**
     * Default serialization UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Text editor buffer.
     */
    private JTextArea editor;
    /**
     * Path to file currently loaded in editor.
     */
    private Path openedFilePath;
    /**
     * Initial value after loading.
     */
    private String initialValue;

    /**
     * Default constructor.
     * 
     * @param editor reference to this editor
     * @param openedFilePath path to file
     */
    public JNotepadPPTab(JTextArea editor, Path openedFilePath) {
        this.editor = editor;
        this.openedFilePath = openedFilePath;
        this.initialValue = editor.getText();
    }

    /**
     * @return editor reference
     */
    public JTextArea getEditor() {
        return editor;
    }

    /**
     * @return path to loaded file
     */
    public Path getOpenedFilePath() {
        return openedFilePath;
    }

    /**
     * Sets this file path to new opened file path
     * 
     * @param openedFilePath new file path
     */
    public void setOpenedFilePath(Path openedFilePath) {
        this.openedFilePath = openedFilePath;
    }

    /**
     * Returns initial value that file had after loading.
     * 
     * @return initial content
     */
    public String getInitialValue() {
        return initialValue;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((editor == null) ? 0 : editor.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof JNotepadPPTab)) {
            return false;
        }
        JNotepadPPTab other = (JNotepadPPTab) obj;
        if (editor == null) {
            if (other.editor != null) {
                return false;
            }
        } else if (!editor.equals(other.editor)) {
            return false;
        }
        return true;
    }

}
