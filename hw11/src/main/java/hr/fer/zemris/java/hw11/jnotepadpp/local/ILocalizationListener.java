package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Functional interface that models on change method.
 * 
 * @author Matija Bartolac
 *
 */
public interface ILocalizationListener {
    /**
     * Notifies that localization has changed.
     */
    void localizationChanged();
}
