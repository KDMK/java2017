package hr.fer.zemris.java.custom.collections;

/**
 * Class collection represents some general collection of objects.
 *
 * @author Matija Bartolac
 *
 */
public class Collection {

    /**
     * Checks if collection is empty.
     *
     * @return Returns true if collection contains no objects and false
     *         otherwise
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Returns the number of currently stored objects in collection.
     *
     * @return Size of collection.
     */
    public int size() {
        return 0;
    }

    /**
     * Adds the given object into collection.
     *
     * @param value
     *            Value of element that we want to store in collection.
     */
    public void add(Object value) {
        // Implement when extending
    }

    /**
     * Checks if given value is in collection.
     *
     * @param value
     *            Value of element that we are looking for.
     * @return True if collection contains given value and false otherwise
     */
    public boolean contains(Object value) {
        // Determine if value is in collection using equals method
        return false;
    }

    /**
     * Checks if given value is in collection and removes it from collection.
     *
     * @param value
     *            Value of element that we want to remove.
     * @return True if collection contains given value, and method removes one
     *         occurrence of it.
     */
    public boolean remove(Object value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets all collection elements in form of an array. This method never
     * returns null.
     *
     * @return Array of collection elements.
     */
    public Object[] toArray() {
        // This method never return null means that you should return empty
        // array if sizeOf collection is 0
        throw new UnsupportedOperationException();
    }

    /**
     * Process each element of collection using given processor. The order in
     * which elements will be sent is undefined in this class.
     *
     * @param processor
     *            Reference to processor used to process elements.
     */
    public void forEach(Processor processor) {
        // Implement this method when extending.
    }

    /**
     * Adds all elements from given collection to this collection. This other
     * collection remains unchanged.
     *
     * @param other
     *            Reference to collection whose elements we want to append to
     *            this collection.
     */
    void addAll(Collection other) {

        /**
         * LocalProcessor class is used to define new sub-type of processor that
         * we use to add element in our collection. We use LocalProcessor class
         * instance as follows: collectionInstance.forEach(locProcessor);. This
         * line of code ensures that process method of our localProcessor will
         * be called for each element in collection upon which we called forEach
         * method.
         *
         * @author Matija Bartolac
         *
         */
        class LocalProcessor extends Processor {
            /**
             * Inserts given value to collection upon which is called.
             */
            @Override
            public void process(Object value) {
                add(value);
            }
        }

        if (other == null) {
            throw new NullPointerException();
        }
        // Create new instance of local processor and call it for each element
        other.forEach(new LocalProcessor());
    }

    /**
     * Removes all elements from this collection.
     */
    public void clear() {
        // Implement when extending.
    }
}
