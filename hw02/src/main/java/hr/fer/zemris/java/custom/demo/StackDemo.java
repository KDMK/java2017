package hr.fer.zemris.java.custom.demo;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Demonstration program for our implementation of stack data structure.
 * Programs runs as command-line application which accepts a single command-line
 * argument: expression which should be evaluated. Expression must be in postfix
 * representation. If program starts from console, expression has to be enclosed
 * in quotation marks.
 *
 *
 * @author Matija Bartolac
 *
 */
public class StackDemo {

    /**
     * Starting point of our program.
     *
     * @param args Command line arguments; not used.
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of parametes. Aborting...");
            System.exit(1);
        }

        ObjectStack myStack = new ObjectStack();
        for (String word: args[0].replaceAll("\"", "").split(" ")) {
            try {
                myStack.push(Integer.parseInt(word));
            } catch (NumberFormatException ex) {
                Integer second = (Integer) myStack.pop();
                Integer first = (Integer) myStack.pop();
                myStack.push(performOperation(first, second, word.charAt(0)));
            }
        }

        if (myStack.size() != 1) {
            System.out.println("Error! There is more than one element on stack");
        } else {
            System.out.println("Expression evaluates to: " + myStack.pop());
        }
    }

    /**
     * Method returns the result of given operation with two whole numbers. If
     * operation is not supported method throws UnsupportedOperationException.
     *
     * @param first First operand.
     * @param second Second operand.
     * @param operation Operator.
     * @return Result of operation.
     */
    private static Integer performOperation(Integer first, Integer second,
            char operation) {

        switch (operation) {
        case '+':
            return first + second;
        case '-':
            return first - second;
        case '/':
            if (second == 0) {
                throw new IllegalArgumentException("Can't divide by zero");
            }
            return first / second;
        case '*':
            return first * second;
        case '%':
            return first % second;
        default:
            throw new UnsupportedOperationException();
        }
    }
}
