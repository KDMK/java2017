/**
 * Package contains classes for working with complex numbers.
 *
 * @author Matija Bartolac
 *
 */
package hr.fer.zemris.java.custom.hw02;
