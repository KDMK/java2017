package hr.fer.zemris.java.custom.hw02.demo;

import hr.fer.zemris.java.custom.hw02.ComplexNumber;

/**
 * Simple program that test behavior of our ComplexNumber class.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ComplexDemo {

    /**
     * Starting point of our program.
     *
     * @param args Command line arguments; not used.
     */
    public static void main(String[] args) {
        ComplexNumber c1 = new ComplexNumber(2, 3);
        ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
        ComplexNumber c3 = c1.add(ComplexNumber.fromMagnitudeAndAngle(2, 1.57))
                .div(c2).power(3).root(2)[1];

        System.out.println(c3);
    }

}
