package hr.fer.zemris.java.custom.collections;

/**
 * Class that models empty stack exception.
 *
 * @author Matija Bartolac
 *
 */
public class EmptyStackException extends RuntimeException {

    /**
     * Default serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Does not take any arguments.
     */
    public EmptyStackException() {
        super();
    }

    /**
     * Constructor that accepts a message.
     *
     * @param message Message string that will be displayed
     */
    public EmptyStackException(String message) {
        super(message);
    }

    /**
     * Constructor that creates exception using cause object.
     *
     * @param cause Throwable that caused exception.
     */
    public EmptyStackException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor that creates exception using cause and string.
     *
     * @param message Message string that will be displayed.
     * @param cause Throwable that caused exception.
     */
    public EmptyStackException(String message, Throwable cause) {
        super(message, cause);
    }
}
