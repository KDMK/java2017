/**
 * Package contains demonstraion of structures implemented in collections
 * package.
 *
 * @author Matija Bartolac
 *
 */
package hr.fer.zemris.java.custom.demo;
