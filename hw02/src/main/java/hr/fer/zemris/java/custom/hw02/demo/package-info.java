/**
 * Package contains classes for demonstration usage of complex number
 * implementation provided in @see hr.fer.zemis.java.custom.hw02
 *
 * @author Matija Bartolac
 *
 */
package hr.fer.zemris.java.custom.hw02.demo;
