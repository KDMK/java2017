package hr.fer.zemris.java.custom.hw02;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class provides support for working with complex numbers. Instance of
 * ComplexNumber is unmodifiable. User is provided with various methods for
 * creating new number complex numbers: you can create number using only real
 * part of number, using only imaginary part, from magnitude and angle and also
 * from parsing a string. Class also provides methods for basic calculation with
 * complex numbers and also raising it to power n and n-th root.
 *
 * @author Matija Bartolac
 * @version v1.0
 */
public class ComplexNumber {
    /**
     * Real part of imaginary number.
     */
    private final double real;
    /**
     * Imaginary part of real number.
     */
    private final double imaginary;
    /**
     * Magnitude of complex number.
     */
    private final double magnitude;
    /**
     * Angle of complex number.
     */
    private final double angle;

    /**
     * Get real part of complex number.
     *
     * @return Real part of complex number.
     */
    public double getReal() {
        return real;
    }

    /**
     * Get imaginary part of complex number.
     *
     * @return Imaginary part of complex number.
     */
    public double getImaginary() {
        return imaginary;
    }

    /**
     * Get magnitude of complex number.
     *
     * @return Magnitude of complex number
     */
    public double getMagnitude() {
        return magnitude;
    }

    /**
     * Get angle of complex number.
     *
     * @return Angle of complex number.
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Default constructor. Creates new imaginary number with given parameters.
     *
     * @param real      Value for the real part of number.
     * @param imaginary Value for the imaginary part of number.
     */
    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
        this.magnitude = Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
        this.angle = Math.atan2(imaginary, real);
    }

    // Factory methods

    /**
     * Creates complex number with only real part.
     *
     * @param real Real part of complex number.
     * @return Reference to new complex number.
     */
    public static ComplexNumber fromReal(double real) {
        return new ComplexNumber(real, 0);
    }

    /**
     * Creates complex number with only imaginary part.
     *
     * @param imaginary Imaginary part of complex number.
     * @return Reference to new complex number.
     */
    public static ComplexNumber fromImaginary(double imaginary) {
        return new ComplexNumber(0, imaginary);
    }

    /**
     * Creates complex number using the magnitude and angle information.
     *
     * @param magnitude Magnitude of complex number.
     * @param angle     Angle of complex number.
     * @return Reference to new complex number.
     */
    public static ComplexNumber fromMagnitudeAndAngle(double magnitude,
                                                      double angle) {
        double real = magnitude * Math.cos(angle);
        double imaginary = magnitude * Math.sin(angle);

        return new ComplexNumber(real, imaginary);
    }

    /**
     * Parse string and creates complex number from it. Expects from user to
     * input single number. Method does not resolve multi-number inputs. Throws
     * exception if called upon empty string or string that contains characters
     * that are not elements of complex numbers.
     *
     * @param s String containing complex number.
     * @return Reference to new complex number.
     */
    public static ComplexNumber parse(String s) {
        if (s.trim().isEmpty()) {
            throw new IllegalArgumentException("Cannot parse empty string!");
        }

        Pattern realPattern = Pattern
                .compile("([+-]{0,1}[\\d]*[.,]{0,1}[\\d]*)(?!i|\\d|\\.|,)\\b");
        Pattern imaginaryPattern = Pattern
                .compile("[-+]{0,1}[\\d]*[.,]{0,1}[\\d]*i");
        Pattern forbiddenPattern = Pattern.compile("[^i\\d\\.+\\-,]");

        Matcher realMatcher = realPattern.matcher(s);
        Matcher imaginaryMatcher = imaginaryPattern.matcher(s);
        Matcher forbiddenMatcher = forbiddenPattern.matcher(s);

        if (forbiddenMatcher.find()) {
            throw new IllegalArgumentException(
                    "Cannot parse: Illegal characters.");
        } else {
            double real = 0;
            double imaginary = 0;

            if (realMatcher.find()
                    && !(realMatcher.group(0).trim().length() == 0)) {
                real = Double.parseDouble(realMatcher.group(0));
            }
            if (imaginaryMatcher.find()
                    && !(imaginaryMatcher.group(0).trim().length() == 0)) {
                String imag = imaginaryMatcher.group(0).replace("i", "").trim();
                if (imag.length() == 0) {
                    imaginary = 1;
                } else if (imag.equals("-")) {
                    imaginary = -1;
                } else {
                    imaginary = Double.parseDouble(imag);
                }
            }

            return new ComplexNumber(real, imaginary);
        }
    }

    // Operation methods

    /**
     * Adds two complex numbers. Method returns reference to new complex number.
     *
     * @param c Reference to other complex number.
     * @return Reference to new complex number.
     */
    public ComplexNumber add(ComplexNumber c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newRealValue = this.real + c.getReal();
        double newImaginaryValue = this.imaginary + c.getImaginary();

        return new ComplexNumber(newRealValue, newImaginaryValue);
    }

    /**
     * Subtracts two complex numbers. Method returns reference to new complex
     * number.
     *
     * @param c Reference to other complex number
     * @return Reference to new complex number
     */
    public ComplexNumber sub(ComplexNumber c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newRealValue = this.real - c.getReal();
        double newImaginaryValue = this.imaginary - c.getImaginary();

        return new ComplexNumber(newRealValue, newImaginaryValue);
    }

    /**
     * Multiplies two imaginary numbers.
     *
     * @param c Reference to other complex number.
     * @return Reference to new complex number.
     */
    public ComplexNumber mul(ComplexNumber c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        double newMagnitude = magnitude * c.getMagnitude();
        double newAngle = angle + c.getAngle();

        return fromMagnitudeAndAngle(newMagnitude, newAngle);
    }

    /**
     * Divides two imaginary numbers.
     *
     * @param c Reference to other complex number.
     * @return Reference to new complex number.
     */
    public ComplexNumber div(ComplexNumber c) {
        if (c == null) {
            throw new IllegalArgumentException("Null is not valid argument!");
        }

        if (c.getMagnitude() == 0) {
            throw new IllegalArgumentException("Can't divide by zero");
        }

        double newMagnitude = magnitude / c.getMagnitude();
        double newAngle = angle - c.getAngle();

        return fromMagnitudeAndAngle(newMagnitude, newAngle);
    }

    /**
     * Raises complex number to the power of n.
     *
     * @param n Power that we are calculating.
     * @return Reference to new complex number.
     */
    public ComplexNumber power(int n) {
        double newMagnitude = Math.pow(magnitude, n);
        double newAngle = angle * n;

        return fromMagnitudeAndAngle(newMagnitude, newAngle);
    }

    /**
     * Calculates n-th root of complex number.
     *
     * @param n Power that we are calculating.
     * @return Array of roots.
     */
    public ComplexNumber[] root(int n) {
        if (n == 0) {
            throw new IllegalArgumentException("Cannot calculate 0 root!");
        }

        ComplexNumber[] roots = new ComplexNumber[n];
        double newMagnitude = Math.pow(magnitude, 1.0 / n);
        double nAngle = Math.atan2(imaginary, real);
        double newAngle;

        for (int k = 0; k < n; k++) {
            newAngle = (nAngle + (2 * k * Math.PI)) / n;
            roots[k] = fromMagnitudeAndAngle(newMagnitude, newAngle);
        }

        return roots;
    }

    @Override
    public String toString() {
        DecimalFormat numFormat = new DecimalFormat("+0.00000;-0.00000");

        return numFormat.format(real) + numFormat.format(imaginary)
                + "i";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ComplexNumber)) {
            return false;
        }

        ComplexNumber other = (ComplexNumber) obj;
        return Double.doubleToLongBits(imaginary) == Double.doubleToLongBits(other.imaginary) &&
                Double.doubleToLongBits(real) == Double.doubleToLongBits(other.real);
    }
}
