package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class that contains test methods for ArrayIndexedCollection class.
 *
 * @author Matija Bartolac
 *
 */
public class ArrayIndexedCollectionTest {

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#add(java.lang.Object)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddNull() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        n.add(null);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#add(java.lang.Object)}.
     */
    @Test
    public void testAddNormal() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();

        n.add("Ivica");
        assertEquals("Ivica", n.get(0));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#get(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOutOfBounds() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);

        n.get(5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#get(int)}.
     */
    @Test
    public void testGetNormal() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);

        assertEquals("Marica", n.get(1));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#clear()}.
     */
    @Test
    public void testClear() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);

        n.clear();
        assertEquals(0, n.size());
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#insert(java.lang.Object, int)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInsertNull() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        n.insert(null, 0);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#insert(java.lang.Object, int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testInsertOutOfBounds() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        n.insert("Ovo ne stane tu ->", 5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#insert(java.lang.Object, int)}.
     */
    @Test
    public void testInsertNormal() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();

        fillArray(n);
        n.insert("Snjeguljica", 2);

        assertEquals("Snjeguljica", n.get(2));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#indexOf(java.lang.Object)}.
     */
    @Test
    public void testIndexOfHaveElement() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        assertEquals(3, n.indexOf("Crvenkapica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#indexOf(java.lang.Object)}.
     */
    @Test
    public void testIndexOfWithoutElement() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        assertEquals(-1, n.indexOf("Trnoruzica"));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#remove(int)}.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexOutOfBounds() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        n.remove(5);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#remove(int)}.
     */
    @Test
    public void testRemoveNormal() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        n.remove(3);

        assertNotEquals("Crvenkapica", n.get(3));
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#remove(java.lang.Object)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRemoveObjectNull() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);
        n.remove(null);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#remove(java.lang.Object)}.
     */
    @Test
    public void testRemoveObjectNotFound() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);

        assertFalse(n.remove("Trnoruzica"));        
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.collections.ArrayIndexedCollection#remove(java.lang.Object)}.
     */
    @Test
    public void testRemoveObjectFound() {
        ArrayIndexedCollection n = new ArrayIndexedCollection();
        fillArray(n);

        assertTrue(n.remove("Crvenkapica"));
    }

    /**
     * Fill the collection with some sample elements.
     *
     * @param myCollection Reference to collection
     */
    private void fillArray(ArrayIndexedCollection myCollection) {
        myCollection.add("Ivica");
        myCollection.add("Marica");
        myCollection.add("Vuk");
        myCollection.add("Crvenkapica");
        myCollection.add("i sedam patuljaka");
    }

}
