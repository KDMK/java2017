package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import java.text.DecimalFormat;

import org.junit.Test;

import hr.fer.zemris.java.custom.hw02.ComplexNumber;

/**
 * Class containing test for class ComplexNumbes.
 *
 * @author Matija Bartolac
 * @version v1.0
 *
 */
public class ComplexNumberTest {
    /**
     * Defining relative mistake for comparing two double numbers.
     */
    private static final double DELTA = 1e-15;

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#ComplexNumber(double, double)}.
     */
    @Test
    public void testComplexNumber() {
        ComplexNumber n = new ComplexNumber(1,1);

        assertEquals(1, n.getReal(), DELTA);
        assertEquals(1, n.getImaginary(), DELTA);
        assertEquals(Math.PI/4, n.getAngle(), DELTA);
        assertEquals(Math.sqrt(2), n.getMagnitude(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#fromReal(double)}.
     */
    @Test
    public void testFromReal() {
        ComplexNumber n = ComplexNumber.fromReal(1);

        assertEquals(1, n.getReal(), DELTA);
        assertEquals(0, n.getImaginary(), DELTA);
        assertEquals(0, n.getAngle(), DELTA);
        assertEquals(1, n.getMagnitude(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#fromImaginary(double)}.
     */
    @Test
    public void testFromImaginary() {
        ComplexNumber n = ComplexNumber.fromImaginary(1);

        assertEquals(0, n.getReal(), DELTA);
        assertEquals(1, n.getImaginary(), DELTA);
        assertEquals(Math.PI/2, n.getAngle(), DELTA);
        assertEquals(1, n.getMagnitude(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#fromMagnitudeAndAngle(double, double)}.
     */
    @Test
    public void testFromMagnitudeAndAngle() {
        ComplexNumber n = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), Math.PI/4);

        assertEquals(1, n.getReal(), DELTA);
        assertEquals(1, n.getImaginary(), DELTA);
        assertEquals(Math.PI/4, n.getAngle(), DELTA);
        assertEquals(Math.sqrt(2), n.getMagnitude(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#parse(String)}.
     */
    @Test
    public void testParse() {
        ComplexNumber n1 = ComplexNumber.parse("2i+1");
        ComplexNumber n2 = ComplexNumber.parse("-2i-1");
        ComplexNumber n3 = ComplexNumber.parse("2-i");
        ComplexNumber n4 = ComplexNumber.parse("i");
        ComplexNumber n5 = ComplexNumber.parse("2");
        ComplexNumber n6 = ComplexNumber.parse("2.3+2.12i");

        assertEquals(1, n1.getReal(), DELTA);
        assertEquals(2, n1.getImaginary(), DELTA);
        assertEquals(-1, n2.getReal(), DELTA);
        assertEquals(-2, n2.getImaginary(), DELTA);
        assertEquals(2, n3.getReal(), DELTA);
        assertEquals(-1, n3.getImaginary(), DELTA);
        assertEquals(0, n4.getReal(), DELTA);
        assertEquals(1, n4.getImaginary(), DELTA);
        assertEquals(2, n5.getReal(), DELTA);
        assertEquals(0, n5.getImaginary(), DELTA);
        assertEquals(2.3, n6.getReal(), DELTA);
        assertEquals(2.12, n6.getImaginary(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#add(ComplexNumber)}.
     */
    @Test
    public void testAdd() {
        ComplexNumber n1 = ComplexNumber.parse("2i+1");
        ComplexNumber n2 = ComplexNumber.parse("-3i-2");
        ComplexNumber n3 = n1.add(n2);

        assertEquals(-1, n3.getReal(), DELTA);
        assertEquals(-1, n3.getImaginary(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#sub(ComplexNumber)}.
     */
    @Test
    public void testSub() {
        ComplexNumber n1 = ComplexNumber.parse("2i+1");
        ComplexNumber n2 = ComplexNumber.parse("-3i-2");
        ComplexNumber n3 = n1.sub(n2);

        assertEquals(3, n3.getReal(), DELTA);
        assertEquals(5, n3.getImaginary(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#mul(ComplexNumber)}.
     */
    @Test
    public void testMul() {
        ComplexNumber n1 = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI / 4);
        ComplexNumber n2 = ComplexNumber.fromMagnitudeAndAngle(2, -Math.PI / 4);
        ComplexNumber n3 = n1.mul(n2);

        assertEquals(2, n3.getMagnitude(), DELTA);
        assertEquals(0, n3.getAngle(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#div(ComplexNumber)}.
     */
    @Test
    public void testDiv() {
        ComplexNumber n1 = ComplexNumber.fromMagnitudeAndAngle(1.0, Math.PI / 4);
        ComplexNumber n2 = ComplexNumber.fromMagnitudeAndAngle(2.0, -Math.PI / 4);
        ComplexNumber n3 = n1.div(n2);

        assertEquals(0.5, n3.getMagnitude(), DELTA);
        assertEquals(Math.PI / 2, n3.getAngle(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#power(int)}.
     */
    @Test
    public void testPower() {
        ComplexNumber n1 = ComplexNumber.fromMagnitudeAndAngle(2.0, -Math.PI / 4);
        ComplexNumber n2 = n1.power(4);

        assertEquals(16, n2.getMagnitude(), DELTA);
        assertEquals(-Math.PI, n2.getAngle(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#root(int)}.
     */
    @Test
    public void testRoot() {
        ComplexNumber n1 = ComplexNumber.fromMagnitudeAndAngle(2.0, -Math.PI / 4);
        ComplexNumber[] n1Roots = n1.root(2);

        double angle = Math.atan2(n1.getImaginary(), n1.getReal());

        assertEquals(Math.sqrt(2), n1Roots[0].getMagnitude(), DELTA);
        assertEquals(angle / 2, n1Roots[0].getAngle(), DELTA);
        assertEquals(Math.sqrt(2), n1Roots[1].getMagnitude(), DELTA);
        assertEquals((angle + (2 * Math.PI)) / 2, n1Roots[1].getAngle(), DELTA);
    }

    /**
     * Test method for
     * {@link hr.fer.zemris.java.custom.hw02.ComplexNumber#toString()}.
     */
    @Test
    public void testToString() {
        ComplexNumber n1 = new ComplexNumber(2,3);

        DecimalFormat format = new DecimalFormat("+0.00000;-0.00000");
        String expected = format.format(2) + format.format(3) + "i";

        assertEquals(expected, n1.toString());
    }

}
